package crawling;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class CrawlingMan_one_by_one {

	public static void main(String[] args) throws IOException {
		
		/**BF는 여기서 안다룸 Support 참고**/
		//리뷰 많은 호텔들 대상으로 크롤링 해다가 다 만들면 어떨까? 일일히 실행시키면 슬픔 ㅠ ㅎㅎ
		int totalNum_t=0;
		int totalNum_f=0;

		String outputString_t = "";
		String outputString_f = "";
		/** Choose **/
		
		String partOfUrl="intercontinental-chicago-chicago-2";
		
		String[] strs = {"http://www.yelp.com/biz/palmer-house-a-hilton-hotel-chicago","http://www.yelp.com/biz/hyatt-regency-chicago-chicago-3","http://www.yelp.com/biz/w-chicago-lakeshore-chicago-2","http://www.yelp.com/biz/hilton-chicago-chicago-4","http://www.yelp.com/biz/the-james-hotel-chicago","http://www.yelp.com/biz/the-drake-hotel-chicago-2","http://www.yelp.com/biz/w-chicago-city-center-chicago-2","http://www.yelp.com/biz/intercontinental-chicago-chicago-2","http://www.yelp.com/biz/dana-hotel-and-spa-chicago","http://www.yelp.com/biz/allegro-chicago-a-kimpton-hotel-chicago","","",""};
		for (String string : strs) {
			
		
		
		
		// KY
		String where = "E:\\FakeReviewDetectionData";
		// or Dear IRbig2?
		// String where="D:\\KY\\FakeReviewDetectionData";

		String category ="Hotel";
//		String category ="restaurant";
		String tf ="T";
//		String TF ="F";

		String typeOfFile="csv";
//		String typeOfFile="txt";
//		String typeOfFile="html";
		String bizId="";
		
		
		String seperator = "";
		String nameOfService = "";		
		if(typeOfFile.equals("csv")){
			seperator=",";
		}else{
			seperator="\t";
		}
		
		int crawlIndex=0;
		int crawlIndex_f=0;
		int crawlItr =0;
		int crawlItr_f =0;
		
		
		// 이거를 크롤링한다음에 넣기
		
		

		long time = System.currentTimeMillis();
		SimpleDateFormat dayTime = new SimpleDateFormat("dd-hh-mm");
		String str = dayTime.format(new Date(time));
		
//		String resultFile = where + "\\Yelp\\" + category+ "\\itr1500\\" + yelp + test + "-" + str + ".csv";
		String resultFile_t = where + "\\Yelp\\" + category+ "\\T\\";
		String resultFile_f = where + "\\Yelp\\" + category+ "\\F\\";
		// String resultFile =
		// where+"\\result\\topic"+topicNum+"\\itr1500\\"+yelp+test+"-"+str+".csv";

//		String urloftarget = "http://www.yelp.com/biz/"+partOfUrl+"?sort_by=rating_desc;start=";
		String urloftarget = string+"?sort_by=rating_desc;start=";
		boolean whileCheck =true;
		
		while(whileCheck){
			
//		String urloftarget = "http://www.yelp.com/biz/intercontinental-chicago-chicago-2?sort_by=rating_desc;start=240";
			crawlIndex=crawlIndex+crawlItr*40;
			
			crawlItr++;
		//Using jsoup
		Document doc = Jsoup.connect(urloftarget+crawlIndex).get();
//		Elements linktest = doc.select("a[href]");
		Elements forNameOfService = doc.select("title");
		
		/**name of the service**/
		for(Element l: forNameOfService){
//			System.out.println("link:	"+l.attr("abs:href"));
			//abs : 절대경로로 바꿔줌 이거 안하면 상대경로가 불규칙해져서  필수 인듯
//			Iterator<Element> iterElem = l.getElementsByTag("title").iterator();
			
//			System.out.println(l.getElementsByTag("title").text());
			nameOfService=l.getElementsByTag("title").text();
//			System.out.println("chek "+nameOfService);
//			Iterator<Element> iterElem = l.getElementsByTag("title").iterator();
//            StringBuilder builder = new StringBuilder();
//            for (String item : items) {
//              builder.append(item + ": " + iterElem.next().text() + "   \t");
//            }
//            System.out.println(builder.toString());
            // Hotel
//            System.out.println(iterElem.next().text());
		}
		nameOfService=nameOfService.replace(",", "");	
		nameOfService=nameOfService.replace("|", "");	
		/**authors of reviews**/
//		Elements forAuthor = doc.select("meta[itemprop=author]");
//		Elements forAuthor = doc.select("a[class=user-display-name]");
		// <li class="user-name">
		Elements forAuthor = doc.select("li.user-name *");
//		System.out.println("0-author "+forAuthor.get(2).getElement(id).attr("content"));
		
		//review description
		//<p class="review_comment ieSucks"
		Elements forReviewDescription = doc.select("p[class=review_comment ieSucks]");
		Elements forReviewRating = doc.select("img[alt*=rating]");
//		Elements forCheckEnd = doc.select("h3");
		Elements forCheckEnd = doc.select("h3:containsOwn(Whoops)");
		
		if(forCheckEnd.size()!=0){
			System.out.println("Whoops!!");
			break;
			// quit the crawling whe it meets whoops page
			
		}
//		System.out.println(forCheckEnd.get(0).text());
//		System.out.println(forCheckEnd.size());
		
//		img alt="5.0 star rating"
//		System.out.println(forReviewDescription.size());
//		System.out.println("# of Author"+forAuthor.size());
//		System.out.println(forReviewRating);
		int i=0;
		int elementNum;
//		for (Element element : forAuthor) {
		for (elementNum=0;elementNum<forAuthor.size();elementNum++) {
			if(i==40){
				break;
			}
			
//			System.out.println(element.getElementsByTag("title").text());
			
			String userPage;
			
			String author;
			if(forAuthor.get(elementNum).attr("abs:href").equals("")){
				userPage="";
				author=forAuthor.get(elementNum).text();
				elementNum++;
			}else{
			userPage=forAuthor.get(elementNum).attr("abs:href");
			
			
			author=forAuthor.get(elementNum).text();
//			System.out.println(author);
//			System.out.println(userPage);
			}
			
//			System.out.println(i+" author: "+author);
			String reviewDescription ="\""+forReviewDescription.get(i).text()+"\"";
//			String reviewDescription =forReviewDescription.get(i).text();
			// 0: rating of business
			String reviewRating = forReviewRating.get(i+1).attr("alt");
			reviewRating=reviewRating.replace(" star rating", "");
//			System.out.println(reviewRating);
			i++;
		
			
//			System.out.println(reviewDescription.getBytes().length);
			if(reviewDescription.getBytes().length<150){
				continue;
			}
			if(!reviewRating.equals("5.0")){
				System.out.println("rating: "+reviewRating);
				whileCheck=false;
				break;
			}
//			img alt="5.0 star rating"
			reviewDescription=reviewDescription.replace(",","|");
			
			outputString_t+=nameOfService+seperator;
			outputString_t+=tf+seperator;
			outputString_t+=author+seperator;
			outputString_t+=userPage+seperator;
			outputString_t+=reviewDescription+seperator;
			outputString_t+="\n";
			totalNum_t++;
			
//			System.out.println("author "+element.text());
//			System.out.println("userID:	"+element.attr("abs:href"));
			
		}
		
			
		}
//		System.out.println(outputString);	
//		meta itemprop="author"
//		nameOfService=l.getElementsByTag("title").text();
		
		
		
		
		
		/**fake crawling**/
		
		
		
		
		
		
		
		
		Document docAgain = Jsoup.connect(urloftarget+crawlIndex).get();
		Elements forBizId = docAgain.select("meta[name=yelp-biz-id]");
		
		bizId=forBizId.get(0).attr("content");
		System.out.println("forBizId"+bizId);
//		String urloftarget_f="http://www.yelp.com/not_recommended_reviews/"+bizId+"?not_recommended_start=";
//		String urloftarget_f="http://www.yelp.com/not_recommended_reviews/"+bizId+"?not_recommended_start=";
		String urloftarget_f="http://www.yelp.com/not_recommended_reviews/"+bizId+"?not_recommended_start=";
//    	String url="http://www.yelp.com/not_recommended_reviews/SrNUBeLzCCBdwdUF9qepqg?not_recommended_start=0";
 
		crawlIndex=0;
		crawlItr=0;
		whileCheck=true;
		while(whileCheck){
			System.out.println("while fake");
			System.out.println(crawlIndex_f);
//			String urloftarget = "http://www.yelp.com/biz/intercontinental-chicago-chicago-2?sort_by=rating_desc;start=240";
				crawlIndex_f=crawlIndex+crawlItr_f*10;
				
				
			//Using jsoup
			Document doc = Jsoup.connect(urloftarget_f+String.valueOf(crawlIndex_f)).get();
			crawlItr_f++;
			System.out.println(urloftarget_f+String.valueOf(crawlIndex_f));
//		   	Document doc =Jsoup.connect(urloftarget_f).get();
//			Elements linktest = doc.select("a[href]");
//			Elements forNameOfService = doc.select("title");
			
			/**name of the service**/
//			for(Element l: forNameOfService){
//
//				nameOfService=l.getElementsByTag("title").text();
//				System.out.println(nameOfService);
//
//			}
//			nameOfService=nameOfService.replace(",", "");	
//			nameOfService=nameOfService.replace("|", "");	
			/**authors of reviews**/
	    	Elements forAuthor =doc.select("li.user-name");
//	    	System.out.println(forAuthor);
			
//			Elements forAuthor = doc.select("meta[itemprop=author]");
//			Elements forAuthor = doc.select("a[class=user-display-name]");
			// <li class="user-name">
//			Elements forAuthor = doc.select("span");
			
//			Elements forAuthor = doc.select(":contains(E M)");
			//<div class="ysection not-recommended-reviews">
			
//			Elements forAuthor = doc.select("li.user-name *");
//			System.out.println("0-author "+forAuthor.get(2).getElement(id).attr("content"));
//			System.out.println(forAuthor);
			//review description
			//<p class="review_comment ieSucks"
			Elements forReviewDescription = doc.select("p[class=review_comment ieSucks]");
			Elements forReviewRating = doc.select("img[alt*=rating]");
//			Elements forCheckEnd = doc.select("h3");
			Elements forCheckEnd = doc.select("h3:containsOwn(Whoops)");
//			System.out.println(forAuthor);
			if(forCheckEnd.size()!=0){
				System.out.println("Whoops!!");
				break;
				// quit the crawling whe it meets whoops page
				
			}
//			System.out.println(forCheckEnd.get(0).text());
//			System.out.println(forCheckEnd.size());
			
//			img alt="5.0 star rating"
//			System.out.println(forReviewDescription.size());
//			System.out.println("# of Author"+forAuthor.size());
//			System.out.println(forReviewRating);
			int i=0;
			int elementNum;
//			for (Element element : forAuthor) {
			for (elementNum=0;elementNum<forAuthor.size();elementNum++) {
				if(i==10){
					break;
				}
				
//				System.out.println(element.getElementsByTag("title").text());
				
				String userPage;
				
				String author;
				if(forAuthor.get(elementNum).attr("abs:href").equals("")){
					userPage="";
					author=forAuthor.get(elementNum).text();
//					elementNum++;
				}else{
				userPage=forAuthor.get(elementNum).attr("abs:href");
				
				
				author=forAuthor.get(elementNum).text();
//				System.out.println(author);
//				System.out.println(userPage);
				}
				
//				System.out.println(i+" author: "+author);
				String reviewDescription =forReviewDescription.get(i).text();
//				String reviewDescription =forReviewDescription.get(i).text();
				// 0: rating of business
				String reviewRating = forReviewRating.get(i).attr("alt");
				reviewRating=reviewRating.replace(" star rating", "");
//				System.out.println(reviewRating);
				
//				System.out.println("i:"+i+"\t"+reviewDescription);
//				System.out.println(forAuthor.size());
				
				i++;
			
				if(reviewDescription.equals("This review has been removed for violating our Content Guidelines or Terms of Services")){
					whileCheck=false;
					System.out.println("break!");
					break;
				}
//				System.out.println(reviewDescription.getBytes().length);
				if(reviewDescription.getBytes().length<150){
					continue;
				}
				if(!reviewRating.equals("5.0")){
					System.out.println("rating: "+reviewRating);
					
					continue;
				}
				
				
//				System.out.println(reviewDescription.equals("This review has been removed for violating our Content Guidelines or Terms of Services"));
//				System.out.println("??");
				reviewDescription="\""+reviewDescription+"\"";
				
//				if(!reviewRating.equals("5.0")){
//					System.out.println("rating: "+reviewRating);
//					
//					break;
//				}
//				img alt="5.0 star rating"
				reviewDescription=reviewDescription.replace(",","|");
				
				outputString_f+=nameOfService+seperator;
				outputString_f+=tf+seperator;
				outputString_f+=author+seperator;
				outputString_f+=userPage+seperator;
				outputString_f+=reviewDescription+seperator;
				outputString_f+="\n";
				totalNum_f++;
				
//				System.out.println("author "+element.text());
//				System.out.println("userID:	"+element.attr("abs:href"));
				
			}
			
				
			}
		
		

		/** wrighting part **/
		try {
//			for truthful
			resultFile_t += "t_"+nameOfService +"_"+totalNum_t+ "---" + str + "."+typeOfFile;
			FileWriter writer_t = new FileWriter(resultFile_t, true);

			writer_t.append(outputString_t);

			writer_t.flush();
			writer_t.close();

			//for fake
			resultFile_f += "f_"+nameOfService +"_"+totalNum_f+ "---" + str + "."+typeOfFile;
			FileWriter writer_f = new FileWriter(resultFile_f, true);
			
			writer_f.append(outputString_f);
			
			writer_f.flush();
			writer_f.close();
//			System.out.println(outputString);
			// System.out.println(cntResult);
		} catch (IOException e) {
			e.printStackTrace();
		}
		}
	}

}
