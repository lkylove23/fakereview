package crawling;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

public class CrawlingMan_get_fake_reviewer_id {

	public static void main(String[] args) throws IOException {
		
		// restaurant의  fake review 전체 수 counting
		/**BF는 여기서 안다룸 Support 참고**/
		/*
		 * E:\FakeReviewDetectionData\Yelp\insurance\BalancedForLDA\
		 * 파일 읽어서 T면 continue
		 * 사람별로 하기
		 *  
		 * 
		 * biz 의 not recommend 접속후(biz 아니까) 
		 * 이름, 지역, review ID 캐치
		 * 그다음 검색
		 * 
		 * 나온애들중에서 리뷰아이디 포함하는지 확인해서 있으면
		 * 아예 foldTotal_withFakeID 만들자 네
		 * */	
		
		
		// 이건 다음에........  마지막띄쓰기 를 +로 바꾸고 마지막 점 을 지운다 (쿼리화)		 * e.g.  Sam N. => Sam+N
		
		int numOfFilteredBusiness=0;
		int totalBusiness=0;

		int numOfFilteredReview_t=0;
		int numOfFilteredReview_f=0;
		int totalReview_t =0;
		int totalReview_f =0;
		int NumofAccess=0;
		int crawlIndex=0;
		int crawlIndex_f=0;
		int crawlItr =0;
		int crawlItr_f =0;
		String urloftarget="";
		boolean whileCheck =true;
		
				
		String outputString_t = "";
		String outputString_f = "";
		/** Choose **/
		String location="Chicago";	
//		String location="Chicago";
//		String categories ="hotels";
//		String categories ="restaurants";
//		String categories ="electronics";
//		String categories ="fashion";
//		String categories ="hospitals";
//		String categories ="musicvenues";
		String categories ="insurance";
		
		String typeOfFile="csv";
//		String typeOfFile="txt";
//		String typeOfFile="html";
		String bizId="";
		
		
		String where = "E:\\FakeReviewDetectionData";
//		String where = "D:\\KY\\FakeReviewDetectionData";
		
		long time = System.currentTimeMillis();
		SimpleDateFormat dayTime = new SimpleDateFormat("dd-hh-mm");
		String str = dayTime.format(new Date(time));
		
		// if there is no such directory than make
		File f_ForCheck_category = new File(where + "\\Yelp\\" + categories);
		if(!f_ForCheck_category.isDirectory()){
			f_ForCheck_category.mkdir();
		}
		File f_ForCheck_T = new File(where + "\\Yelp\\" + categories+"\\T");
		if(!f_ForCheck_T.isDirectory()){
			f_ForCheck_T.mkdir();
		}
		File f_ForCheck_F = new File(where + "\\Yelp\\" + categories+"\\F");
		if(!f_ForCheck_F.isDirectory()){
			f_ForCheck_F.mkdir();
		}
		
		String inputFile = where+"\\Yelp\\"+categories+"\\BalancedForLDA\\"+categories+"_foldTotal.csv";
		String resultFile_t = where + "\\Yelp\\" + categories+ "\\T\\";
		String resultFile_f = where + "\\Yelp\\" + categories+ "\\F\\";
		String File = where + "\\Yelp\\" + categories+ "\\";
		String listFile = "";
		
		File f = new File(resultFile_t+str);
		f.mkdir();
		f= new File(resultFile_f+str);
		f.mkdir();
		
		String seperator = "";
		int cntItr=1;
		
		if(typeOfFile.equals("csv")){
			seperator=",";
		}else{
			seperator="\t";
		}
		
		BufferedReader br = null;
		String line="";
		String username;
		String reviewContent;
		try {
			br = new BufferedReader(new FileReader(inputFile));

				for(int i=0;(line = br.readLine()) != null;i++) {
					
				// use comma as separator
				String[] tempArray = line.split(seperator);
				if(tempArray[1].equals("T")){
					continue;
				}
				username = tempArray[2];
				reviewContent = tempArray[4];
				
				/*getFakeUserID (username, biz) 불러오기
				 * return reviewID
				 */
			
				
				//새로운 foldTotal 저장
				// biz	TorF	Username	UserID(www부분 제외)	reviewContent
				
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		
		ArrayList<String> listOfServices=new ArrayList<String>();
			
		
		int numOfResult=10;
	
		
		listFile+= File+"list---" + str + "."+typeOfFile;
		FileWriter writer_l = new FileWriter(listFile, true);
		
		
		for(;numOfResult==10;crawlItr++){
		ArrayList<String> tempAL = new ArrayList<String>();
		// Assistant 이용 리뷰갯수 15개 이하일경우 제외 -> iteration Stop
		System.out.println(crawlItr*10);
		NumofAccess=NumofAccess+11;
		System.out.println("NumofAccess: "+NumofAccess);
		tempAL=Assistant.getListOfService(location, categories, crawlItr*10);
		numOfResult=tempAL.size();
		totalBusiness+=tempAL.size();
		for (String string : tempAL) {
			if(listOfServices.contains(string)){
				System.out.println("****************중복**************");
			}
			
		}
	
		
		
		
		listOfServices.addAll(tempAL);
		
		}
		
		/**Print the list of businesses**/
		try {

			for (int t = 0; t < listOfServices.size(); t++) {
				writer_l.append(listOfServices.get(t));
				writer_l.append("\n");
			}

			writer_l.flush();
			writer_l.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	
		
		/**Iteration for each service**/
		
	
		for(String service: listOfServices){
			
			numOfFilteredBusiness=0;	
//			numOfFilteredBusiness_f=0;
			outputString_t="";
			outputString_f="";
			String resultFile="";
	
		System.out.println(cntItr+" of "+listOfServices.size()+"\t"+service);
		cntItr++;
		
		
		
		 crawlIndex=0;
		 crawlIndex_f=0;
		 crawlItr =0;
		 crawlItr_f =0;
		
		
		// 이거를 크롤링한다음에 넣기
		

		
//		String resultFile = where + "\\Yelp\\" + type+ "\\itr1500\\" + yelp + test + "-" + str + ".csv";

		// String resultFile =
		// where+"\\result\\topic"+topicNum+"\\itr1500\\"+yelp+test+"-"+str+".csv";

//		String urloftarget = "http://www.yelp.com/biz/"+partOfUrl+"?sort_by=rating_desc;start=";
		urloftarget = "http://www.yelp.com/biz/"+service+"?sort_by=rating_desc;start=";
		whileCheck =true;
		
		while(whileCheck){
			
//		String urloftarget = "http://www.yelp.com/biz/intercontinental-chicago-chicago-2?sort_by=rating_desc;start=240";
//			crawlIndex=crawlIndex+crawlItr*40;
			crawlIndex=crawlItr*40;
			
			crawlItr++;
		//Using jsoup
			
		MatrixTime(100);
		NumofAccess++;	
		System.out.println("NumofAccess: "+NumofAccess);
		Document doc = Jsoup.connect(urloftarget+crawlIndex).timeout(100000).get();
		/**authors of reviews**/
		// <li class="user-name">
		Elements forAuthor = doc.select("li.user-name *");
//		System.out.println("0-author "+forAuthor.get(2).getElement(id).attr("content"));
		
		/**review description**/
		//<p class="review_comment ieSucks"
		Elements forReviewDescription = doc.select("p[class=review_comment ieSucks]");
//		Elements forReviewRating = doc.select("img[alt*=rating]");
		Elements forReviewRating = doc.select("meta[itemprop=ratingValue]");
//		Elements forCheckEnd = doc.select("h3");
		Elements forCheckEnd = doc.select("h3:containsOwn(Whoops)");
		
		if(forCheckEnd.size()!=0){
			System.out.println("Whoops!!");
			break;
			// quit the crawling whe it meets whoops page
			
		}

		int i=0;
		int elementNum;
//		for (Element element : forAuthor) {
		totalReview_t+= forAuthor.size();
		
		for (elementNum=0;elementNum<forAuthor.size();elementNum++) {
			if(i==40){
				break;
			}
			
//			System.out.println(element.getElementsByTag("title").text());
			
			String userPage;
			
			String author;
			if(forAuthor.get(elementNum).attr("abs:href").equals("")){
				userPage="";
				author=forAuthor.get(elementNum).text();
				elementNum++;
			}else{
			userPage=forAuthor.get(elementNum).attr("abs:href");
			
			
			author=forAuthor.get(elementNum).text();
//			System.out.println(author);
//			System.out.println(userPage);
			}
			
//			System.out.println(i+" author: "+author);
			String reviewDescription ="\""+forReviewDescription.get(i).text()+"\"";
//			String reviewDescription =forReviewDescription.get(i).text();
			// 0: rating of business
			String reviewRating = forReviewRating.get(i+1).attr("content");
//			reviewRating=reviewRating.replace(" star rating", "");
//			System.out.println(reviewRating);
			i++;
		
			
//			System.out.println(reviewDescription.getBytes().length);
			if(reviewDescription.getBytes().length<150){
				continue;
			}
			if(!reviewRating.equals("5.0")){
//				System.out.println("rating: "+reviewRating);
				whileCheck=false;
				break;
			}
			//
			
//			img alt="5.0 star rating"
			reviewDescription=reviewDescription.replace(",","|");
			
			outputString_t+=service+seperator;
			outputString_t+="T"+seperator;
			outputString_t+=author+seperator;
			outputString_t+=userPage+seperator;
			outputString_t+=reviewDescription+seperator;
			outputString_t+="\n";
			numOfFilteredBusiness++;
			numOfFilteredReview_t++;
		}
		
			
		}
		/**fake crawling**/
		
		MatrixTime(100);
		NumofAccess++;
		System.out.println("NumofAccess: "+NumofAccess);
		Document docAgain = Jsoup.connect(urloftarget+crawlIndex).timeout(100000).get();
		Elements forBizId = docAgain.select("meta[name=yelp-biz-id]");
		
		bizId=forBizId.get(0).attr("content");
//		System.out.println("forBizId"+bizId);
//		String urloftarget_f="http://www.yelp.com/not_recommended_reviews/"+bizId+"?not_recommended_start=";
//		String urloftarget_f="http://www.yelp.com/not_recommended_reviews/"+bizId+"?not_recommended_start=";
		String urloftarget_f="http://www.yelp.com/not_recommended_reviews/"+bizId+"?not_recommended_start=";
//    	String url="http://www.yelp.com/not_recommended_reviews/SrNUBeLzCCBdwdUF9qepqg?not_recommended_start=0";
 
		crawlIndex=0;
		crawlItr=0;
		whileCheck=true;
		System.out.println("fake of"+service);
		while(whileCheck){
				crawlIndex_f=crawlItr_f*10;
				MatrixTime(100);
				NumofAccess++;
				System.out.println("NumofAccess: "+NumofAccess);
			Document doc = Jsoup.connect(urloftarget_f+String.valueOf(crawlIndex_f)).timeout(100000).get();
			crawlItr_f++;
			System.out.println(urloftarget_f+String.valueOf(crawlIndex_f));
			
			/**authors of reviews**/
	    	Elements forAuthor =doc.select("li.user-name");
			Elements forReviewDescription = doc.select("p[class=review_comment ieSucks]");
			Elements forReviewRating = doc.select("img[alt*=rating]");
			Elements forCheckEnd = doc.select("h3:containsOwn(Whoops)");
			if(forCheckEnd.size()!=0){
				System.out.println("Whoops!!");
				break;
				// quit the crawling whe it meets whoops page
			}
			if(forAuthor.size()==0){
				System.out.println("no one even violator!");
				break;
				// quit the crawling whe it meets whoops page
			}
			int i=0;
			int elementNum;
			
			totalReview_f+= forAuthor.size();
			for (elementNum=0;elementNum<forAuthor.size();elementNum++) {
				if(i==10){
					break;
				}
				
				String userPage;
				
				String author;
				if(forAuthor.get(elementNum).attr("abs:href").equals("")){
					userPage="";
					author=forAuthor.get(elementNum).text();
				}else{
				userPage=forAuthor.get(elementNum).attr("abs:href");
				
				
				author=forAuthor.get(elementNum).text();
				}
				
				String reviewDescription =forReviewDescription.get(i).text();
				// 0: rating of business
				String reviewRating = forReviewRating.get(i).attr("alt");
				reviewRating=reviewRating.replace(" star rating", "");
				i++;
			
				if(reviewDescription.equals("This review has been removed for violating our Content Guidelines or Terms of Services")){
					whileCheck=false;
					System.out.println("break!");
					break;
				}
				
				
				
//				System.out.println(reviewDescription.getBytes().length);
				if(reviewDescription.getBytes().length<150){
					continue;
				}
				if(!reviewRating.equals("5.0")){
					System.out.println("rating: "+reviewRating);
					
					continue;
				}
				
				
//				System.out.println(reviewDescription.equals("This review has been removed for violating our Content Guidelines or Terms of Services"));
//				System.out.println("??");
				reviewDescription="\""+reviewDescription+"\"";
				
//				if(!reviewRating.equals("5.0")){
//					System.out.println("rating: "+reviewRating);
//					
//					break;
//				}
//				img alt="5.0 star rating"
				reviewDescription=reviewDescription.replace(",","|");
				
				outputString_f+=service+seperator;
				outputString_f+="F"+seperator;
				outputString_f+=author+seperator;
				outputString_f+=userPage+seperator;
				outputString_f+=reviewDescription+seperator;
				outputString_f+="\n";
				numOfFilteredReview_f++;
//				numOfFilteredBusiness_f++;
//				System.out.println("author "+element.text());
//				System.out.println("userID:	"+element.attr("abs:href"));
				
			}
			
				
			}
		
		

		/** wrighting part **/
//		try {
////			for truthful
////			resultFile = resultFile_t+str+"\\"+ numOfFilteredBusiness_t+"_t_"+service +"_"+ "---" + str + "."+typeOfFile;
////			FileWriter writer_t = new FileWriter(resultFile, true);
////
////			writer_t.append(outputString_t);
////
////			writer_t.flush();
////			writer_t.close();
////
////			//for fake
////			resultFile= resultFile_f+str+"\\"+ numOfFilteredBusiness_f+"_f_"+service +"_"+ "---" + str + "."+typeOfFile;
////			FileWriter writer_f = new FileWriter(resultFile, true);
////			
////			writer_f.append(outputString_f);
////			
////			writer_f.flush();
////			writer_f.close();
////			System.out.println(outputString);
//			// System.out.println(cntResult);
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
		
		}
		try {
			File += "Statistics_" + str + "."+typeOfFile;
			FileWriter writer_total = new FileWriter(File, true);

//			total review_t after filtering review_t
//			total review_f after filtering review_f
			writer_total.append("totalreview_t,"+Integer.toString(totalReview_t));
			writer_total.append("\n");
			
			writer_total.append("afterfiltering review_t,"+Integer.toString(numOfFilteredReview_t));
			writer_total.append("\n");

			writer_total.append("totalreview_f,"+Integer.toString(totalReview_f));
			writer_total.append("\n");
			
			writer_total.append("afterfiltering review_f,"+Integer.toString(numOfFilteredReview_f));
			writer_total.append("\n");
			
			writer_total.append("total businese,"+Integer.toString(totalBusiness));
			writer_total.append("\n");

			writer_total.flush();
			writer_total.close();
			
			
		} catch(IOException e){
			e.printStackTrace();
		}
		
			
		}

	  public static void MatrixTime(int delayTime){
	       long saveTime = System.currentTimeMillis();
	       long currTime = 0;
	       while( currTime - saveTime < delayTime){
	           currTime = System.currentTimeMillis();
	       }
	   }

		
		
	}


