package crawling;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class CrawlingSupport_BF {

	public static void main(String[] args) throws IOException {
		
		
		/**Crawl 한거에서 사용자 페이지 불러와서 BF 처리한후 CSV에 각 사용자 별로 뒤에서 add 해주기 식**/
		
		File path = new File("");
		String outputString = "";
		/** Choose **/
		// KY
		String where = "E:\\FakeReviewDetectionData";
		// or Dear IRbig2?
		// String where="D:\\KY\\FakeReviewDetectionData";
		String category ="Hotel";
		String tf ="T";
//		String TF ="F";
		String nameOfService = "";
//		String nameOfService = "";
//		String typeOfFile="csv";
//		String typeOfFile="txt";
		String typeOfFile="html";
		
		// 이거를 크롤링한다음에 넣기
		
		

		long time = System.currentTimeMillis();
		SimpleDateFormat dayTime = new SimpleDateFormat("dd-hh-mm");
		String str = dayTime.format(new Date(time));
		
//		String resultFile = where + "\\Yelp\\" + category+ "\\itr1500\\" + yelp + test + "-" + str + ".csv";
		String resultFile = where + "\\Yelp\\" + category+ "\\";
		// String resultFile =
		// where+"\\result\\topic"+topicNum+"\\itr1500\\"+yelp+test+"-"+str+".csv";

		String urloftarget = "http://www.yelp.com/biz/intercontinental-chicago-chicago-2?sort_by=rating_desc";
		
		//Using jsoup
		Document doc = Jsoup.connect(urloftarget).get();
//		Elements linktest = doc.select("a[href]");
		Elements forNameOfService = doc.select("title");
		
		/**name of the service**/
		for(Element l: forNameOfService){
//			System.out.println("link:	"+l.attr("abs:href"));
			//abs : 절대경로로 바꿔줌 이거 안하면 상대경로가 불규칙해져서  필수 인듯
//			Iterator<Element> iterElem = l.getElementsByTag("title").iterator();
			
//			System.out.println(l.getElementsByTag("title").text());
			nameOfService=l.getElementsByTag("title").text();
//			System.out.println("chek "+nameOfService);
//			Iterator<Element> iterElem = l.getElementsByTag("title").iterator();
//            StringBuilder builder = new StringBuilder();
//            for (String item : items) {
//              builder.append(item + ": " + iterElem.next().text() + "   \t");
//            }
//            System.out.println(builder.toString());
            // Hotel
//            System.out.println(iterElem.next().text());
		}
		nameOfService=nameOfService.replace(",", "");	
		nameOfService=nameOfService.replace("|", "");	
		/**authors of reviews**/
//		Elements forAuthor = doc.select("meta[itemprop=author]");
		Elements forAuthor = doc.select("a[class=user-display-name]");
//		System.out.println("0-author "+forAuthor.get(2).getElement(id).attr("content"));

		
		for (Element element : forAuthor) {
//			System.out.println(element.getElementsByTag("title").text());

			
			String author=element.text();
			String userPage=element.attr("abs:href");
			
			outputString+=nameOfService+"\t";
			outputString+=tf+"\t";
			outputString+=author+"\t";
			outputString+=userPage+"\t";
			outputString+="\n";
			//여기에다가 사람 페이지 들어가서 행동적 특성 집어넣는거 무커지 논문 참고 뭐 가져 올지
			
			
//			System.out.println("author "+element.text());
//			System.out.println("userID:	"+element.attr("abs:href"));
			
		}
		System.out.println(outputString);
//		meta itemprop="author"
//		nameOfService=l.getElementsByTag("title").text();
		
		
		

		/** wrighting part **/
//		try {

//			System.out.println(nameOfService);
//			resultFile += nameOfService + "-" + str + "."+typeOfFile;
//			FileWriter writer = new FileWriter(resultFile, true);
//
//			writer.append(outputString);
//
//			writer.flush();
//			writer.close();
////			System.out.println(outputString);
//			// System.out.println(cntResult);
//		} catch (IOException e) {
//			e.printStackTrace();
//		}

	}

}
