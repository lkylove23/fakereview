package crawling;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class CrawlingMan_special {

	public static void main(String[] args) throws IOException {
		
		/**Fake review 수 받아오기**/
		//리뷰 많은 호텔들 대상으로 크롤링 해다가 다 만들면 어떨까? 일일히 실행시키면 슬픔 ㅠ ㅎㅎ
		int numOfFilteredBusiness=0;
		int totalBusiness=0;

		int numOfFilteredReview_t=0;
		int numOfFilteredReview_f=0;
		int totalReview_t =0;
		int totalReview_f =0;
		int NumofAccess=0;
		int crawlIndex=0;
		int crawlIndex_f=0;
		int crawlItr =0;
		int crawlItr_f =0;
		String urloftarget="";
		boolean whileCheck =true;
		
				
		String outputString_t = "";
		String outputString_f = "";
		/** Choose **/
		String location="Chicago";	
//		String location="Chicago";
//		String categories ="hotels";
		String categories ="restaurants";
//		String categories ="electronics";
//		String categories ="fashion";
//		String categories ="hospitals";
//		String categories ="musicvenues";
//		String categories ="insurance";
		
		String typeOfFile="csv";
//		String typeOfFile="txt";
//		String typeOfFile="html";
		String bizId="";
		
		
		String where = "E:\\FakeReviewDetectionData";
//		String where = "D:\\KY\\FakeReviewDetectionData";
		
		long time = System.currentTimeMillis();
		SimpleDateFormat dayTime = new SimpleDateFormat("dd-hh-mm");
		String str = dayTime.format(new Date(time));
		
		// if there is no such directory than make
		File f_ForCheck_category = new File(where + "\\Yelp\\" + categories);
		if(!f_ForCheck_category.isDirectory()){
			f_ForCheck_category.mkdir();
		}
		File f_ForCheck_T = new File(where + "\\Yelp\\" + categories+"\\T");
		if(!f_ForCheck_T.isDirectory()){
			f_ForCheck_T.mkdir();
		}
		File f_ForCheck_F = new File(where + "\\Yelp\\" + categories+"\\F");
		if(!f_ForCheck_F.isDirectory()){
			f_ForCheck_F.mkdir();
		}
		
		
		String resultFile_t = where + "\\Yelp\\" + categories+ "\\T\\";
		String resultFile_f = where + "\\Yelp\\" + categories+ "\\F\\";
		String File = where + "\\Yelp\\" + categories+ "\\";
		String listFile = "";
		
		
		File f = new File(resultFile_t+str);
		f.mkdir();
		f= new File(resultFile_f+str);
		f.mkdir();
		
		String seperator = "";
		int cntItr=1;
		
		
		if(typeOfFile.equals("csv")){
			seperator=",";
		}else{
			seperator="\t";
		}
		
		
		ArrayList<String> listOfServices=new ArrayList<String>();
		ArrayList<Integer> numOfFakeReview=new ArrayList<Integer>();
			
		
		int numOfResult=10;
	
		
		listFile+= File+"list---" + str + "."+typeOfFile;
		FileWriter writer_l = new FileWriter(listFile, true);

		String inputFile = where+"\\Yelp\\" + categories+"\\List1.txt";
		BufferedReader br = new BufferedReader(new FileReader(inputFile));
		String line ="";
		while ((line = br.readLine()) != null) {
			System.out.println("line:"+line);
//			String[] token = line.split(",");
//			listOfServices.add(token[0]);
			listOfServices.add(line);
		}
		System.out.println(listOfServices);
		/**Iteration for each service**/
		
	
		for(String service: listOfServices){
			
			numOfFilteredBusiness=0;	
//			numOfFilteredBusiness_f=0;
			outputString_t="";
			outputString_f="";
			String resultFile="";
	
		System.out.println(cntItr+" of "+listOfServices.size()+"\t"+service);
		cntItr++;
		
		
		
		 crawlIndex=0;
		 crawlIndex_f=0;
		 crawlItr =0;
		 crawlItr_f =0;
		
		
		// 이거를 크롤링한다음에 넣기
		
		urloftarget = "http://www.yelp.com/biz/"+service+"?sort_by=rating_desc";
		whileCheck =true;

		/**fake crawling**/
		
		MatrixTime(1);
		NumofAccess++;
		System.out.println("NumofAccess: "+NumofAccess);
		Document docAgain = Jsoup.connect(urloftarget).timeout(100000).get();
//		Elements forBizId = docAgain.select("meta[name=yelp-biz-id]");
		Elements tdest =docAgain.select("a[class=subtle-text inline-block]");
	  	for (Element element : tdest) {

	  		String[] token = element.text().split(" ");
			numOfFakeReview.add(Integer.parseInt(token[0]));
			
	  		//    		System.out.println(element.attr("content"));	
		}
		
		
		}
		System.out.println(numOfFakeReview);
		
			
		}
	  public static void MatrixTime(int delayTime){
	       long saveTime = System.currentTimeMillis();
	       long currTime = 0;
	       while( currTime - saveTime < delayTime*1000){
	           currTime = System.currentTimeMillis();
	       }
	   }

		
		
	}


