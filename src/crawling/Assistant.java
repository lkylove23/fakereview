package crawling;

import java.io.IOException;
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Assistant {
	int crawlIndex=0;
	int crawlIndex_f=0;
	int crawlItr =0;
	int crawlItr_f =0;
	/**
	 * 서비스의 리스트를 반환하는 함수
	 * @param location
	 * @param category
	 * @param startNum
	 * @return partOfUrl (이름으로도 사용)
	 * @throws IOException 
	 */
		public static ArrayList<String> getListOfService(String location,String category,int startNum) throws IOException{
			String urloftarget="http://www.yelp.com/search?find_desc=&find_loc=" +
					location +
					"%2C+IL%2C+USA&sortby=review_count&cflt=" +
					category +
					"&start="+startNum;
			ArrayList<String> output = new ArrayList<String>();
			Document doc = Jsoup.connect(urloftarget).timeout(100000).get();
			Elements forListOfService =doc.select("span a[data-hovercard-id]");
//			System.out.println(forListOfService.get(0).attr("href"));
			for(Element e: forListOfService){
				String tempOut =e.attr("href").replace("/biz/", "");
				
				
				//15개 체크 하는 부분
//				<span itemprop="reviewCount">226
				String tempOutForCheck =e.attr("href");
				MatrixTime(500);
				Document docForCheck = Jsoup.connect("http://www.yelp.com"+tempOutForCheck+"?start=0&sort_by=rating_desc").timeout(100000).get();
				
				Elements forReviewcnt =docForCheck.select("span[itemprop=reviewCount]");
				System.out.print("review count:\t"+forReviewcnt.get(0).text());
				System.out.println("\tThe name of service:        \t"+tempOut);
//				System.out.println("The name of service 2nd try:\t"+e.attr("href"));
//				System.out.println("The name of service 3nd try:\t"+e.attr("href").replace("/biz/", ""));
				
//				System.out.println(("http://www.yelp.com"+tempOutForCheck+"?start=0&sort_by=rating_desc")+"\n"+("http://www.yelp.com/biz/intercontinental-chicago-chicago-2?start=80&sort_by=rating_desc"));
				if(Integer.parseInt(forReviewcnt.get(0).text())>15){
					output.add(tempOut);
				}
			}
//			System.out.println("size of output:\t"+output.size());
//			System.out.println("fromAssistant"+output);
			return output;
		}
		public static void MatrixTime(int delayTime){
		       long saveTime = System.currentTimeMillis();
		       long currTime = 0;
		       while( currTime - saveTime < delayTime){
		           currTime = System.currentTimeMillis();
		       }
		   }
}
