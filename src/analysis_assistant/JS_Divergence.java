package analysis_assistant;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;

public class JS_Divergence {

    public static void main(String[] args) throws Exception {
        /**w1: capital theta
         * w2: df
         * w3: w1*w2
         *                   **/
        //  1/topic
        /**Choose**/
        //KY
        String where = "D:\\";
        //or Dear IRbig2?
//		String where="D:\\KY\\";
        double tau = 0.0;

        int classChange_Train = 0; //classChange ��° doc ���� �ٸ� class
        int classChange_Test = 0;
        int topicNum = 0;

        double highAccuracy = 0.0;
        ArrayList<Integer> highTopic_F = new ArrayList<Integer>();
        ArrayList<Integer> highTopic_T = new ArrayList<Integer>();
        double ave;
        double aveOfAve = 0.0;
        String categories = "";
        String s_itr = "";
//	    String s_itr=" (2)";
//        String s_itr=" (3)";
//	    String s_itr=" (4)";
//	    String s_itr=" (5)";

//			changeID=115;
        categories = "restaurants";

        String trainingLDAFile = "";
        String testingLDAFile = "";
//		if(args.length!=0){
//			if(args.length!=6){
//				System.out.println("You have to type 5 argument\n # of topics\ttraining file\t# of fake reviews in training file\ttest file\tresult file\tvalue of tau");
//			}
//			numTopics = Integer.parseInt(args[0]);
//			trainingFile=args[1];
//			classChange_Train=Integer.parseInt(args[2]);
//			testingFile=args[3];
//			outDirPath=args[4];
//			tau=Double.parseDouble(args[5]);
//
//		}else{

        StringBuffer Total_ResultOut_folds = new StringBuffer();
//        ArrayList<Integer> listOfDocNum = new ArrayList<Integer>();
//        listOfDocNum.add(200);
////       listOfDocNum.add(300);
//        listOfDocNum.add(500);
//        listOfDocNum.add(1000);
//        listOfDocNum.add(5000);
//        listOfDocNum.add(10000);
//        listOfDocNum.add(20000);
//        listOfDocNum.add(35000);
        ArrayList<Integer> listOfTopicNum = new ArrayList<Integer>();
//        listOfTopicNum.add(2);
//        listOfTopicNum.add(10);
//        listOfTopicNum.add(50);
        listOfTopicNum.add(100);
//        listOfTopicNum.add(200);
//        listOfTopicNum.add(300);
        int maxdocnum;
        int maxtopicnum;
        int doc_i;
        int topic_i;
        long startTime = 0;
        long endTime;
        //////
        int changeID = 0;

        int totalNumOfReview = 0;
        for (int c_it = 0; c_it < 7; c_it++) {
            if (c_it == 0) {
                //Electronics
//                changeID = 44;
                totalNumOfReview = 440;
                categories = "electronics";
            } else if (c_it == 1) {
                //Fashion
                totalNumOfReview = 2690;
                categories = "fashion";
            } else if (c_it == 2) {
                //hospitals
                totalNumOfReview = 200;
                categories = "hospitals";
            } else if (c_it == 3) {
                //hotels
                totalNumOfReview = 1100;
                categories = "hotels";

            } else if (c_it == 4) {
                //Insurance
                totalNumOfReview = 130;
                categories = "insurance";
            } else if (c_it == 5) {
                //Musicvenues
                totalNumOfReview = 1190;
                categories = "musicvenues";
            } else if (c_it == 6) {
                //Restaurant
//                totalNumOfReview = 37990;
                totalNumOfReview = 37980;
//			changeID=115;
                categories = "restaurants";
            }
//            System
            String resultFile_helper = where + "FakeReviewDetectionData\\Yelp\\" + categories + "\\";
            String resultFile_helper_setting = where + "FakeReviewDetectionData\\result\\Yelp\\" + categories + "\\";
            String TotalResultFile = resultFile_helper_setting + "result_total.txt";
            File forCheckCategory = new File(resultFile_helper_setting);
            if (!forCheckCategory.isDirectory()) {
                forCheckCategory.mkdir();
            }
//            for (doc_i = 0, maxdocnum = listOfDocNum.size(); doc_i < maxdocnum; doc_i++) {
            classChange_Test = totalNumOfReview / 10;        // the # of fake reviews
            classChange_Train = classChange_Test * 4;

            String classificationResultFile_check = resultFile_helper_setting + categories + "_" + totalNumOfReview + s_itr;
            forCheckCategory = new File(classificationResultFile_check);
            if (!forCheckCategory.isDirectory()) {
                forCheckCategory.mkdir();
            }
            for (topic_i = 0, maxtopicnum = listOfTopicNum.size(); topic_i < maxtopicnum; topic_i++) {
                topicNum = listOfTopicNum.get(topic_i);
                int numOfFinD = topicNum; // num of features in the document
                tau = 1 / (double) topicNum;
                String test = "";
                String train = "";

                int max_iter = 0;
                double[] highest_accuracy_folds = new double[5];
                double highest_avg = 0.0;
                double tmp_avg = 0.0;
                double[] accuracy_folds = new double[5];
                double[] precision_F_folds = new double[5];
                double[] precision_T_folds = new double[5];
                double[] recall_F_folds = new double[5];
                double[] recall_T_folds = new double[5];
                double[] F1_F_folds = new double[5];
                double[] F1_T_folds = new double[5];
//                double[] highest_accuracy_folds = new double[5];
                double[] highest_precision_F_folds = new double[5];
                double[] highest_precision_T_folds = new double[5];
                double[] highest_recall_F_folds = new double[5];
                double[] highest_recall_T_folds = new double[5];
                double[] highest_F1_F_folds = new double[5];
                double[] highest_F1_T_folds = new double[5];
                StringBuffer ResultOut_folds = new StringBuffer();
                StringBuffer[] High_DocResultOut = new StringBuffer[5];
                int cal_itr = 0;

                double[] highfeatureSelection = new double[topicNum];
                String featureSelectionResultFile = resultFile_helper_setting + categories + "_" + totalNumOfReview + "_lda" + topicNum + "_FSresult.txt";
                StringBuffer FSResultOut = new StringBuffer();
                int high_cal_itr = 0;
                String classificationResultFile = resultFile_helper_setting + categories + "_" + totalNumOfReview + "_lda" + topicNum + "_result.txt";
                startTime = System.currentTimeMillis();
                String line = "";
//                while (cal_itr < 25000) {
                /**inner CV**/

                StringBuffer[] DocResultOut = new StringBuffer[5];
                int tmp_it = -1;

                for (int it = 0; it < 5; it++) {
                    if (it == 0) {
                        test = "fold1";
                        train = "fold2,3,4,5";
                    } else if (it == 1) {
                        test = "fold2";
                        train = "fold1,3,4,5";
                    } else if (it == 2) {
                        test = "fold3";
                        train = "fold1,2,4,5";
                    } else if (it == 3) {
                        test = "fold4";
                        train = "fold1,2,3,5";
                    } else if (it == 4) {
                        test = "fold5";
                        train = "fold1,2,3,4";
                    }
                    if (it != tmp_it) {
                        trainingLDAFile = resultFile_helper + "BalancedForLDA\\" + categories + "_" + train + "_lad" + topicNum + ".txt";
                        testingLDAFile = resultFile_helper + "BalancedForLDA\\" + categories + "_" + test + "_lad" + topicNum + ".txt";
                        tmp_it = it;
                    }
                    double[] SelectedFeature = new double[topicNum];
                    double highAvgAcc = 0.0;
//                            while (cal_itr < max_iter) {
                    cal_itr = 0;
                    max_iter = 200;
                    String[] temp;
                    BufferedReader br = new BufferedReader(new FileReader(trainingLDAFile));
                    double[] w1_F = new double[topicNum];
                    double[] w1_T = new double[topicNum];
                    double[] M = new double[topicNum];
                    double[] cnt_F = new double[topicNum];
                    double[] cnt_T = new double[topicNum];
                    for (int i = 0; i < topicNum; i++) {
                        cnt_F[i] = 0.0;
                        cnt_T[i] = 0.0;
                    }
                    while ((line = br.readLine()) != null) {
                        temp = line.split("\t");
                        if (temp[1].equals("f")) {
                            for (int ii = 0; ii < numOfFinD; ii++) {
                                w1_F[ii] += Double.parseDouble(temp[ii + 2]); //temp[ ] should be "value"
                            }
                        } else {
                            for (int ii = 0; ii < numOfFinD; ii++) {
                                w1_T[ii] += Double.parseDouble(temp[ii + 2]); //temp[ ] should be "value"

                            }
                        }
                    }
                    //
                    br = new BufferedReader(new FileReader(testingLDAFile));
                    while ((line = br.readLine()) != null) {
                        temp = line.split("\t");
                        if (temp[1].equals("f")) {
                            for (int ii = 0; ii < numOfFinD; ii++) {
                                w1_F[ii] += Double.parseDouble(temp[ii + 2]); //temp[ ] should be "value"
                            }
                        } else {
                            for (int ii = 0; ii < numOfFinD; ii++) {
                                w1_T[ii] += Double.parseDouble(temp[ii + 2]); //temp[ ] should be "value"
                            }
                        }
                    }
                    // make relative
                    for (int i = 0; i < topicNum; i++) {
                        double tmp = w1_F[i] + w1_T[i];
                        w1_F[i] /= tmp;
                        w1_T[i] /= tmp;
                    }

                    // make sum=1 w_1
                    double w1_F_sum = 0.0;
                    double w1_T_sum = 0.0;

                    for (int i = 0; i < topicNum; i++) {
                        w1_F_sum += w1_F[i];
                        w1_T_sum += w1_T[i];
//                                double tmp = w1_F[i] + w1_T[i];
//                                w1_F[i] /= tmp;
//                                w1_T[i] /= tmp;
                    }
                    for (int i = 0; i < topicNum; i++) {
                        w1_F[i] /= w1_F_sum;
                        w1_T[i] /= w1_T_sum;
                    }
                    double temp_F=0.0;
                    double temp_T=0.0;
                    for (int i = 0; i < topicNum; i++) {
                      System.out.print(w1_F[i]+"\t");
                    }
                    System.out.println();
                    for (int i = 0; i < topicNum; i++) {
                        System.out.print(w1_T[i]+"\t");
                    }
                    System.out.println();

                    // M = 1/2(w1_F+w1_T)
                    for (int i = 0; i < topicNum; i++) {
                        M[i] = (w1_F[i] + w1_T[i]) / 2;
                    }
                    // Calculating KL divergence  D(F||T)
                    double KL_FllT = 0.0;
                    double KL_TllF = 0.0;

                    //  Calculating JS divergence  JSD(F||T) = (D(F||M))/2+(D(T||M))/2
                    double JS_FT = 0.0;
                    double KL_FllM = 0.0;
                    double KL_TllM = 0.0;

                    for (int j = 0; j < topicNum; j++) {
                        KL_FllT += w1_F[j] * Math.log10(w1_F[j] / w1_T[j])/Math.log10(2);
                        KL_TllF += w1_T[j] * Math.log10(w1_T[j] / w1_F[j])/Math.log10(2);
                        KL_FllM += w1_F[j] * Math.log10(w1_F[j] / M[j])/Math.log10(2);
                        KL_TllM += w1_T[j] * Math.log10(w1_T[j] / M[j])/Math.log10(2);
                    }
                    JS_FT=KL_FllM/2+KL_TllM/2;
                    System.out.println(categories);
                    System.out.println("KL F||T:\t"+KL_FllT);
                    System.out.println("KL T||F:\t"+KL_TllF);
                    System.out.println("JS F|F:\t"+JS_FT);
                    System.out.println();
                }
            }
        }
    }
}
