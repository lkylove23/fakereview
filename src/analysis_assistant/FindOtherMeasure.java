package analysis_assistant;

import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FindOtherMeasure {
	public static void main(String[] args) {
		
		/**based on ppt from SVMLight.ppt**/
		//truthful-> positive   + 1
		//fake -> negative 		- 1
		
		/*			Predicted
		 * 			n		p
		 * Actual n a	|	b
		 * 		  p c	|	d
		 * 
		 * */
		String where="E:\\FakeReviewDetectionData";
		
		 long time = System.currentTimeMillis(); 
			SimpleDateFormat dayTime = new SimpleDateFormat("dd-hh-mm");
			String str = dayTime.format(new Date(time));
		
		
		/**Choose**/
				
		String mode="n_gram";
		String specificMode="unigram";
//		String specificMode="bigram";
//		String nameOfTest= "Ott_Fold1";
//		String nameOfTest= "Ott_Fold2";
//		String nameOfTest= "Ott_Fold3";
//		String nameOfTest= "Ott_Fold4";
		String nameOfTest= "Ott_Fold5";
		
		//off
	    String yelp="";
//		on
//	    String yelp="yelp\\";
		

		double actual_p= 80;// for ott test

		double actual_n= 80;// for ott test
		
		String outputFile = where+"\\result\\"+mode+"\\"+yelp+nameOfTest+"_"+specificMode+"_"+str+".csv";
		
		double p_p=0;
		double r_p=0;
		double f1_p	=0;	
		double p_n=0;
		double r_n=0;
		double f1_n =0;		
		double accuracy=0;
		
		double a=0;
		double b=0;
		double c=0;
		double d=0;
		
		double total =actual_p+actual_n;

		/**input**/
		
		accuracy=90;
		p_p=89.02;
		r_p=91.25;
		
		/*****/
		
		
		d=r_p*actual_p;
		c=actual_p-d;
		b=d/p_p-d;
		a=actual_n-b;
		
		
		//Eavaluation
		f1_p=2*(p_p*r_p)/(p_p+r_p);
		
		p_n=a/(a+c);
		r_n=a/(a+b);
		f1_n=2*(p_n*r_n)/(p_n+r_n);
		
		
		//write
		
		String outputString="";
//		Accuracy	T-P	T-R	T-F	F-P	F-R	F-F
		outputString+=accuracy+","+p_p+","+r_p+","+f1_p+","+p_n+","+r_n+","+f1_n;
		
		
		System.out.println("Accuracy\tT-P\tT-R\tT-F\tF-P\tF-R\tF-F");
		System.out.println(accuracy+"\t"+p_p+"\t"+r_p+"\t"+f1_p+"\t"+p_n+"\t"+r_n+"\t"+f1_n);
		
		try{
		 /*record for reresult*/
	    FileWriter writer = new FileWriter(outputFile);
	    writer.append(outputString);


	    writer.flush();
	    writer.close();
	    
//	    System.out.println(cntResult);
	}
	catch(IOException e)
	{
	     e.printStackTrace();
	}
		
	}

}
