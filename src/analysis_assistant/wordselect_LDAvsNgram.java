package analysis_assistant;

import dataSetting.Stemmer;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.NavigableMap;
import java.util.TreeMap;

/**
 * Created by lkylove23 on 2015-03-19.
 */
public class wordselect_LDAvsNgram {
    public static void main(String[] args) throws Exception {


//		String categories ="electronics";
//        String categories ="fashion";
//		String categories ="hospitals";
//		String categories ="hotels";
//		String categories ="insurance";
//        		String categories ="musicvenues";
        String categories ="restaurants";

        String tFile="D:\\FakeReviewDetectionData\\Yelp\\"+categories+"\\T\\BalancedForLDA\\T_"+categories+"_total.csv";
        String fFile="D:\\FakeReviewDetectionData\\Yelp\\"+categories+"\\F\\BalancedForLDA\\F_"+categories+"_total.csv";
//        String resultFile="D:\\FakeReviewDetectionData\\Yelp\\fashion\\BalancedForLDA\\"+categories+"_similarDWords.txt";
        String pwWordfile="D:\\FakeReviewDetectionData\\Yelp\\"+categories+"\\BalancedForLDA\\"+categories+"_fold2,3,4,5_pwn_UB_porter.txt";
        String nwWordfile="D:\\FakeReviewDetectionData\\Yelp\\"+categories+"\\BalancedForLDA\\"+categories+"_fold2,3,4,5_nwn_UB_porter.txt";
        String pwTopicfile="D:\\FakeReviewDetectionData\\Yelp\\"+categories+"\\BalancedForLDA\\"+categories+"_fold2,3,4,5_pwn_lda100_porter.txt";
        String nwTopicfile="D:\\FakeReviewDetectionData\\Yelp\\"+categories+"\\BalancedForLDA\\"+categories+"_fold2,3,4,5_nwn_lda100_porter.txt";

        HashMap<String,Double> wordInT = new HashMap<String, Double>();
        HashMap<String,Double> wordInF = new HashMap<String, Double>();
        TreeMap<Double,String> wordRanking = new TreeMap<Double,String>();
        //word count in truthful
        BufferedReader br = new BufferedReader(new FileReader(tFile));
        String line="";

        ArrayList<String> pwlist=new ArrayList<String>();
        while ((line = br.readLine()) != null) {
            String[] Temp = line.split(",");
            String[] Temp2 = Temp[4].replace("|"," ").replace("\"", " ").replace(".", " ").replace("!", " ").replace("?", " ").split(" ");
            //porter
            Stemmer porter = new Stemmer();
            for (int i = 0; i < Temp2.length; i++) {
                char[] temptemp = Temp2[i].toCharArray();
                porter.add(temptemp, temptemp.length);
                porter.stem();
                String stemed = porter.toString();
                // save
                if (!wordInT.containsKey(stemed)) {
                    wordInT.put(stemed, 1.0);
                } else {
                    wordInT.put(stemed, wordInT.get(stemed) + 1);
                }
            }
        }
        //word count in fake
         br = new BufferedReader(new FileReader(fFile));


        ArrayList<String> nwlist=new ArrayList<String>();
        while ((line = br.readLine()) != null) {
            String[] Temp = line.split(",");
            String[] Temp2= Temp[4].replace("|"," ").replace("\"", " ").replace("."," ").replace("!"," ").replace("?"," ").split(" ");
            //porter
            Stemmer porter = new Stemmer();
            for(int i=0; i<Temp2.length;i++) {
                char[] temptemp = Temp2[i].toCharArray();
                porter.add(temptemp, temptemp.length);
                porter.stem();
                String stemed=  porter.toString();
                // save
                if(!wordInF.containsKey(stemed)){
                    wordInF.put(stemed,1.0);
                }else{
                    wordInF.put(stemed,wordInF.get(stemed)+1);
                }
            }
        }
        System.out.println("size of word in T: "+wordInT.size());
        System.out.println("size of word in F: "+wordInF.size());
        //차이작은거 고르기
        for( String key : wordInT.keySet() ){
//            System.out.println( String.format("키 : %s, 값 : %s", key, map.get(key)) );
            if(!wordInF.containsKey(key)){
//                System.out.println(key+" is not in F");
                continue;
            }

            if(wordInT.get(key)>wordInF.get(key)){
                wordRanking.put(wordInF.get(key)/wordInT.get(key),key);
            }else{
                wordRanking.put(wordInT.get(key)/wordInF.get(key),key);
            }
        }
        //word weight
        br = new BufferedReader(new FileReader(pwWordfile));
        HashMap<String,Double> wordWeight=new HashMap<String, Double>();
        while ((line = br.readLine()) != null) {
            String[] temp= line.split("\t");
            wordWeight.put(temp[2],Double.parseDouble(temp[1]));
        }
        br = new BufferedReader(new FileReader(nwWordfile));
        while ((line = br.readLine()) != null) {
            String[] temp= line.split("\t");
            wordWeight.put(temp[2],Double.parseDouble(temp[1]));
        }



        //topic
        br = new BufferedReader(new FileReader(pwTopicfile));
        HashMap<String,String> topicwordPW=new HashMap<String, String>(); // word, topicid weight
        HashMap<String,String> topicwordPWorder=new HashMap<String, String>(); // word, topicid weight
        while ((line = br.readLine()) != null) {
            String[] temp =line.split("\t");
            String[] temp2 =temp[2].split(" ");
            for(int i =0;i<temp2.length/2;i++){
                if(topicwordPW.containsKey(temp2[2*i])){
                    topicwordPW.put(temp2[2*i],topicwordPW.get(temp2[2*i])+" "+temp[0]+" "+temp[1]);
                }else{
                    topicwordPW.put(temp2[2 * i], temp[0] + " " + temp[1]);
                }

            }
        }
        br = new BufferedReader(new FileReader(nwTopicfile));
        HashMap<String,String> topicwordNW=new HashMap<String, String>(); // word, topicid weight
        HashMap<String,String> topicwordNWorder=new HashMap<String, String>(); // word, topicid weight
        while ((line = br.readLine()) != null) {
            String[] temp =line.split("\t");
            String[] temp2 =temp[2].split(" ");
            for(int i =0;i<temp2.length/2;i++){
                if(topicwordNW.containsKey(temp2[2*i])){
                    topicwordNW.put(temp2[2*i],topicwordNW.get(temp2[2*i])+" "+temp[0]+" "+temp[1]);
                }else{
                    topicwordNW.put(temp2[2 * i], temp[0] + " " + temp[1]);
                }

            }
        }
        //sorting topicwordPW,NW

        for( String key : topicwordPW.keySet() ){
            String word=key;
            String[] temp= topicwordPW.get(key).split(" ");
            TreeMap<Double,Double> sortInside= new TreeMap<Double, Double>();
            for(int i=0; i<temp.length/2;i++) {
                sortInside.put(Double.parseDouble(temp[2*i+1]),Double.parseDouble(temp[2*i]));
            }
            String syn="";
            NavigableMap<Double,Double> sortMap=sortInside.descendingMap();
            for( Double key1 : sortMap.keySet() ){
                syn+=sortMap.get(key1)+" "+key1;
            }
            topicwordPWorder.put(word,syn);
        }
        for( String key : topicwordNW.keySet() ){
            String word=key;
            String[] temp= topicwordNW.get(key).split(" ");
            TreeMap<Double,Double> sortInside= new TreeMap<Double, Double>();
            for(int i=0; i<temp.length/2;i++) {
                sortInside.put(Double.parseDouble(temp[2*i+1]),Double.parseDouble(temp[2*i]));
            }
            String syn="";

            for( Double key1 : sortInside.keySet() ){
                syn+=sortInside.get(key1)+" "+key1;
            }
            topicwordNWorder.put(word,syn);
        }

        NavigableMap<Double,String> nmap=wordRanking.descendingMap();
        System.out.println(categories);
        System.out.println("word\t등장빈도비율(비슷할수록 1에가까움)\ttf in T\ttf in F\tword Weight\ttopic Weights in T\ttopic Weights in F");

        for( Double key : nmap.keySet() ){
            String word=nmap.get(key);
            System.out.println(word + "\t" + key + "\t" + wordInT.get(word) + "\t" + wordInF.get(word) + "\t"+wordWeight.get(word)+"\t" + topicwordPWorder.get(word)+"\t"+topicwordNWorder.get(word));
        }

    }
}
