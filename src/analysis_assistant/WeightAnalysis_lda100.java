package analysis_assistant;

import java.io.*;
import java.util.HashMap;
import java.util.Iterator;
import java.util.NavigableMap;
import java.util.TreeMap;

/**
 * Created by lkylove23 on 2015-03-07.
 */
public class WeightAnalysis_lda100 {
    public static void main(String args[]) throws IOException {

        String where = "D:\\";
        String categories ="";
        String porter="_porter";
        int totalnum=0;
//        for (int c_it = 0; c_it < 7; c_it++) {

        for(int c_it=0;c_it<7;c_it++) {
//        for(int c_it=5;c_it<7;c_it++) {
            if (c_it == 0) {

                //Electronics
//                changeID = 44;
                categories = "electronics";
                totalnum=440;
            } else if (c_it == 1) {

                //Fashion
                categories = "fashion";
                totalnum=2690;
            } else if (c_it == 2) {

                //hospitals
                categories = "hospitals";
                totalnum=200;
            } else if (c_it == 3) {

                //hotels
                categories = "hotels";
                totalnum=1100;
            } else if (c_it == 4) {

                //Insurance
                categories = "insurance";
                totalnum=130;
            } else if (c_it == 5) {

                //Musicvenues
                categories = "musicvenues";
                totalnum=1190;
            } else if (c_it == 6) {

                //Restaurant
//                totalNumOfReview = 37990;
                categories = "restaurants";
                totalnum=37990;
            }
            String File_helper = where + "FakeReviewDetectionData\\Yelp\\" + categories + "\\BalancedForLDA" + "\\";
            String inputFile=File_helper + categories + "_fold2,3,4,5_w_lda100"+porter+".txt";
            String topicWords=File_helper +"100_word\\"+ categories +"_"+totalnum+"_fold2,3,4,5_lda100_word.txt"; // donot need String porter here ^^;
            String positiveWeightFile=File_helper + categories + "_fold2,3,4,5_pwn_lda100"+porter+".txt";//normalised
            String negativeWeightFile=File_helper + categories + "_fold2,3,4,5_nwn_lda100"+porter+".txt";//normalised

            String line ="";
            TreeMap<Double,Integer> positiveWTM=new TreeMap<Double,Integer>();   // value, id  for easy sort
            TreeMap<Double,Integer> negativeWTM=new TreeMap<Double,Integer>();   // value, id  for easy sort

            //topicword setting
            BufferedReader br = new BufferedReader(new FileReader(topicWords));
            HashMap<Integer,String> topicNwords= new HashMap<Integer,String>(); // id words
            while ((line = br.readLine()) != null) {
                String[] temp = line.split("\t");
                Integer tempId= Integer.parseInt(temp[0])+1; //topicid  start from 1
                topicNwords.put(tempId, temp[2]);
            }

            //make unit vector

                br = new BufferedReader(new FileReader(inputFile));
                double length=0.0;
                while ((line = br.readLine()) != null) {
                    String[] temp = line.split(":");
                    Double tempValue= Double.parseDouble(temp[1]); //weight
                    length+=tempValue*tempValue;
                }
                length=Math.sqrt(length);
                br = new BufferedReader(new FileReader(inputFile));
                while ((line = br.readLine()) != null) {
                    String[] temp = line.split(":");
                    Double tempValue= Double.parseDouble(temp[1]); //weight
                    Integer tempId= Integer.parseInt(temp[0]);
                    tempValue=tempValue/length;
                    if(tempValue>=0){
                        positiveWTM.put(tempValue, tempId);
                    }else{
                        negativeWTM.put(tempValue, tempId);
                    }

                }

            // decending order for positive
            NavigableMap des_positiveWTM =positiveWTM.descendingMap();

            /**saving**/
            // output form:
            // topicID  weight  words

            // for positive weights
            StringBuffer pwstb= new StringBuffer();
            Iterator it = des_positiveWTM.keySet().iterator();
            Object obj;
            while (it.hasNext()) {  // Key를 뽑아낸 Iterator 를 돌려가며
                obj = it.next(); // Kef 를 하나씩 뽑아
                pwstb.append(des_positiveWTM.get(obj));
                pwstb.append("\t");
                pwstb.append(obj.toString());
                pwstb.append("\t");
                pwstb.append(topicNwords.get(des_positiveWTM.get(obj)));
                pwstb.append("\n");
//                System.out.println(obj + ": " + des_positiveWTM.get(obj)); // Value 를 출력
            }
            FileWriter pwfw = new FileWriter(positiveWeightFile);
            pwfw.append(pwstb.toString().trim());
            pwfw.close();
            // for negative weights
            StringBuffer nwstb= new StringBuffer();
            it = negativeWTM.keySet().iterator();
            while (it.hasNext()) {  // Key를 뽑아낸 Iterator 를 돌려가며
                obj = it.next(); // Kef 를 하나씩 뽑아
                nwstb.append(negativeWTM.get(obj));
                nwstb.append("\t");
                nwstb.append(obj.toString());
                nwstb.append("\t");
                nwstb.append(topicNwords.get(negativeWTM.get(obj)));
                nwstb.append("\n");
//                System.out.println(obj + ": " + des_positiveWTM.get(obj)); // Value 를 출력
            }
            FileWriter nwfw = new FileWriter(negativeWeightFile);
            nwfw.append(nwstb.toString().trim());
            nwfw.close();
        }

    }

}
