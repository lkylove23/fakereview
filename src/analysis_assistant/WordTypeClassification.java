package analysis_assistant;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

public class WordTypeClassification {
	public static void main(String[] args) {
		// TDF 불러와서 Type 구분하는 파트

		File path = new File("");
		/** Choose **/
		// KY
		String where = "E:\\FakeReviewDetectionData";
		// or Dear IRbig2?
		// String where="D:\\KY\\FakeReviewDetectionData";

		// int topicNum=8;
		// int topicNum=50;
		int topicNum = 100;
		// int topicNum=150;
		// int topicNum=200;
		// fold1 으로 palmer test 중
		String test = "fold1";
		// String test ="fold2";
		// String test ="fold3";
		// String test ="fold4";
		// String test ="fold5";
//		String test ="Palmer";

		// off
//		 String yelp="";
		// on
		String yelp = "yelp\\";

		/**
		 * Fold selection e.g. 23450101 -> 2,3,4,5 Fake normalised e.g. 23450000
		 * -> 2,3,4,5 truthful not normalised
		 **/
		String tdf_f_n = "";
		String tdf_t_n = "";

		if (test.equals("fold1")) {
			// fodl1
			// System.out.println("fold1 correct!");
			tdf_f_n = "23450101";
			tdf_t_n = "23450001";
		} else if (test.equals("fold2")) {
			// System.out.println("fold2 correct!");
			// fold2
			tdf_f_n = "13450101";
			tdf_t_n = "13450001";
		} else if (test.equals("fold3")) {
			// System.out.println("fold3 correct!");
			// fold3
			tdf_f_n = "12450101";
			tdf_t_n = "12450001";
		} else if (test.equals("fold4")) {

			// fold4
			tdf_f_n = "12350101";
			tdf_t_n = "12350001";
		} else if (test.equals("fold5")) {
			// fold5
			tdf_f_n = "12340101";
			tdf_t_n = "12340001";
		} else {
			// for test
			tdf_f_n = "23450101";
			tdf_t_n = "23450001";
		}

		String TDF = where + "\\input\\topic" + topicNum + "\\itr1500\\" + yelp
				+ "TDF.csv";
		String TT = where + "\\input\\topic" + topicNum + "\\itr1500\\" + yelp
				+ "TT.csv";
		
		 String resultFile = where+"\\result\\topic"+topicNum+"\\itr1500\\"+yelp+test+"_word_type.csv";

		ArrayList<Integer> topicType = new ArrayList<Integer>();

		/*
		 * Array List of TopicDF 0: foldID + F or T + Normalise or not e.g.
		 * 23450101 -> 2,3,4,5 Fake normalised e.g. 23450000 -> 2,3,4,5 truthful
		 * not normalised 1: Dist of Topic 0 2: Dist of Topic 1 3: Dist of Topic
		 * 2 4: Dist of Topic 3 5: Dist of Topic 4 6: Dist of Topic 5 7: Dist of
		 * Topic 6 8: Dist of Topic 7
		 */
		HashMap<String, ArrayList<Double>> topicDF = new HashMap<String, ArrayList<Double>>();

		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ",";
		/* Topic Distribution for fold 2,3,4,5 */
		try {
			br = new BufferedReader(new FileReader(TDF));
			for (int i = 0; (line = br.readLine()) != null; i++) {
				// use comma as separator
				String[] tempArray2 = line.split(cvsSplitBy);
				ArrayList<Double> tempAL = new ArrayList<Double>();
				for (int j = 0; j < tempArray2.length; j++) {
					tempAL.add(Double.parseDouble(tempArray2[j]));
				}
				topicDF.put(tempArray2[0], tempAL);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		// ///////////////////////////////////////////////////////

		for (int i = 0; i < topicNum; i++) {
			// 0[1T] 0.35 [0] 0.65 [2F] 1.0
			double point = topicDF.get(tdf_f_n).get(i + 1);
			// topicDF.get(tdf_t_n).get(i+1);
			if (point < 0.35) {
				topicType.add(i, 1);

			} else if ((0.35 <= point) && (point <= 0.65)) {
				topicType.add(i, 0);

			} else if (0.65 < point) {
				topicType.add(i, 2);
			}
		}

//		HashMap<Integer, HashSet<String>> topicTypeWord = new HashMap<Integer, HashSet<String>>();
		HashSet<String> topicTypeWordT = new HashSet<String>();
		HashSet<String> topicTypeWordF = new HashSet<String>();
		HashSet<String> topicTypeWordN = new HashSet<String>();
		HashSet<String> wordTypeT = new HashSet<String>();
		HashSet<String> wordTypeN = new HashSet<String>();
		HashSet<String> wordTypeF = new HashSet<String>();
		// type classification of words
		
		
		try {
			br = new BufferedReader(new FileReader(TT));
			
			HashSet<String> tempHS = new HashSet<String>();
			for (int i2 = 0; (line = br.readLine()) != null; i2++) {
				// use comma as separator
				if (i2 == 0) {
					continue;
				}
				tempHS.clear();
				String[] tempArray2 = line.split(cvsSplitBy);
				
				// ArrayList<String> = new ArrayList<Double>();
				for (int j = 1; j < tempArray2.length; j++) {
				
					tempHS.add(tempArray2[j]);
				}

				
				//test
				
			
				
				// topic type 별로 word 저장
				switch (topicType.get(i2 - 1)) {
				case 0:
						topicTypeWordN.addAll(tempHS);
					
					break;
					
				case 1:
					topicTypeWordT.addAll(tempHS);
					
					break;
				case 2:
					topicTypeWordF.addAll(tempHS);
					break;
				default:
					System.out.println("해당 숫자가 없습니다");
					break;
				}
				
//				System.out.println(topicTypeWord.get(0));
//				System.out.println(topicTypeWord.get(1));
//				System.out.println(topicTypeWord.get(2));
				// topicDF.put(tempArray2[0], tempAL);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		// word type 분류
		HashSet<String> wordTempT = new HashSet<String>();
		HashSet<String> wordTempN = new HashSet<String>();
		HashSet<String> wordTempF = new HashSet<String>();
		for (Iterator iterT = topicTypeWordT.iterator(); iterT.hasNext();) {
			String word = (String) iterT.next();
//			System.out.println(word);
			
			if (!topicTypeWordF.contains(word)) {
				// wordTempT.add(word);
				wordTypeT.add(word);
				
			} else {
				wordTypeN.add(word);
			}
		}

		for (Iterator iterF = topicTypeWordF.iterator(); iterF.hasNext();) {
			String word = (String) iterF.next();
			if (!topicTypeWordT.contains(word)) {
				// wordTempT.add(word);
				wordTypeF.add(word);
			} else {
				wordTypeN.add(word);
			}
		}

		for (Iterator iterN = topicTypeWordN.iterator(); iterN.hasNext();) {
			String word = (String) iterN.next();
			if (!topicTypeWordT.contains(word)) {
				if (!topicTypeWordN.contains(word)) {
					wordTypeN.add(word);
				}

			}
		}

		// print out

		System.out.println("Word type 0");
		System.out.println(wordTypeN);

		System.out.println("Word type 1");
		System.out.println(wordTypeT);

		System.out.println("Word type 2");
		System.out.println(wordTypeF);

		// write up

		try {
			FileWriter writer = new FileWriter(resultFile, false);
			
			writer.append("Neutral words");
			writer.append('\n');
			for (Iterator iterN = wordTypeN.iterator(); iterN.hasNext();) {
				writer.append((String) iterN.next());
				writer.append(',');
			}
			writer.append("");
			writer.append('\n');
			writer.append("Truthful biased words");
			writer.append('\n');
			for (Iterator iterT = wordTypeT.iterator(); iterT.hasNext();) {
				writer.append((String) iterT.next());
				writer.append(',');
			}
			writer.append("");
			writer.append('\n');
			writer.append("Fake biased words");
			writer.append('\n');
			for (Iterator iterF = wordTypeF.iterator(); iterF.hasNext();) {
				writer.append((String) iterF.next());
				writer.append(',');
			}
			writer.append("");
			
			
			writer.flush();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
		
		
		// 출력 및 파일저장
		
	}

