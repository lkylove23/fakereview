package analysis_assistant;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.NavigableMap;
import java.util.TreeMap;

/**
 * Created by lkylove23 on 2015-03-07.
 */
public class WeightAnalysis_lda100_UB_diff {
    public static void main(String args[]) throws IOException {

        String where = "D:\\";
        String categories ="";
        String porter="_porter";
        int totalnum=0;
//        for (int c_it = 0; c_it < 7; c_it++) {

        for(int c_it=0;c_it<6;c_it++) {
//        for(int c_it=5;c_it<7;c_it++) {
            if (c_it == 0) {

                //Electronics
//                changeID = 44;
                categories = "electronics";
                totalnum=440;
            } else if (c_it == 1) {

                //Fashion
                categories = "fashion";
                totalnum=2690;
            } else if (c_it == 2) {

                //hospitals
                categories = "hospitals";
                totalnum=200;
            } else if (c_it == 3) {

                //hotels
                categories = "hotels";
                totalnum=1100;
            } else if (c_it == 4) {

                //Insurance
                categories = "insurance";
                totalnum=130;
            } else if (c_it == 5) {

                //Musicvenues
                categories = "musicvenues";
                totalnum=1190;
            } else if (c_it == 6) {

                //Restaurant
//                totalNumOfReview = 37990;
                categories = "restaurants";
                totalnum=37990;
            }
            String File_helper = where + "FakeReviewDetectionData\\Yelp\\" + categories + "\\BalancedForLDA" + "\\";
            String inputFile=File_helper + categories + "_fold2,3,4,5_w_lda100"+porter+".txt";
            String topicWords=File_helper +"100_word\\"+ categories +"_"+totalnum+"_fold2,3,4,5_lda100_word.txt"; // donot need String porter here ^^;
            String positiveWeightFile=File_helper + categories + "_fold2,3,4,5_pw_lda100"+porter+".txt";
            String negativeWeightFile=File_helper + categories + "_fold2,3,4,5_nw_lda100"+porter+".txt";
            String positiveWeightOnlyinLDA100File=File_helper + categories + "_fold2,3,4,5_pwo_lda100"+porter+".txt";
            String negativeWeightOnlyinLDA100File=File_helper + categories + "_fold2,3,4,5_nwo_lda100"+porter+".txt";

            String line ="";
            TreeMap<Double,Integer> positiveWTM=new TreeMap<Double,Integer>();   // value, id  for easy sort
            TreeMap<Double,Integer> negativeWTM=new TreeMap<Double,Integer>();   // value, id  for easy sort

            //topicword setting
            BufferedReader br = new BufferedReader(new FileReader(topicWords));
            HashMap<Integer,String> topicNwords= new HashMap<Integer,String>(); // id words
            while ((line = br.readLine()) != null) {
                String[] temp = line.split("\t");
                Integer tempId= Integer.parseInt(temp[0])+1; //topicid  start from 1
                topicNwords.put(tempId, temp[2]);
            }

            // Divide into two group : positive ,negative groups
             br = new BufferedReader(new FileReader(inputFile));
            while ((line = br.readLine()) != null) {
                String[] temp = line.split(":");
                Double tempValue= Double.parseDouble(temp[1]); //weight
                Integer tempId= Integer.parseInt(temp[0]); //weight
                if(tempValue>=0){
                    positiveWTM.put(tempValue, tempId);
                }else{
                    negativeWTM.put(tempValue, tempId);
                }
            }

            // decending order for positive
            NavigableMap des_positiveWTM =positiveWTM.descendingMap();

            /**saving**/
            // output form:
            // topicID  weight  words

            //lda100 for positive weights
            HashMap<String, Double> positiveWordInLDA100 = new HashMap<String, Double>();
            HashMap<String, Double> negativeWordInLDA100 = new HashMap<String, Double>();
            HashMap<String, Double> positiveWordInUB = new HashMap<String, Double>();
            HashMap<String, Double> negativeWordInUB = new HashMap<String, Double>();
            StringBuffer pwstb= new StringBuffer();
            Iterator it = des_positiveWTM.keySet().iterator();
            Object obj;
            while (it.hasNext()) {  // Key를 뽑아낸 Iterator 를 돌려가며
                obj = it.next(); // Kef 를 하나씩 뽑아

                String [] temp2 = topicNwords.get(des_positiveWTM.get(obj)).split(" ");
                for(int i=0;i< temp2.length/2;i++){
                    positiveWordInLDA100.put(temp2[2 * i], Double.parseDouble(obj.toString()));
                }

            }
//            FileWriter pwfw = new FileWriter(positiveWeightFile);
//            pwfw.append(pwstb.toString().trim());
//            pwfw.close();
            // for negative weights
            StringBuffer nwstb= new StringBuffer();
            it = negativeWTM.keySet().iterator();
            while (it.hasNext()) {  // Key를 뽑아낸 Iterator 를 돌려가며
                obj = it.next(); // Kef 를 하나씩 뽑아

                String [] temp2 = topicNwords.get(negativeWTM.get(obj)).split(" ");
                for(int i=0;i< temp2.length/2;i++){
                    negativeWordInLDA100.put(temp2[2*i],Double.parseDouble(obj.toString()));
                }
            }
//            FileWriter nwfw = new FileWriter(negativeWeightFile);
//            nwfw.append(nwstb.toString().trim());
//            nwfw.close();

            /**UB part**/
            String File_helper_UB = where + "FakeReviewDetectionData\\Yelp\\" + categories + "\\BalancedForLDA" + "\\";
            String inputFile_UB=File_helper_UB + categories + "_fold2,3,4,5_w_UB"+porter+".txt";
            String wordIndex=File_helper_UB + categories + "_UB"+porter+"_Index.txt";
            String positiveWeightFile_UB=File_helper_UB + categories + "_fold2,3,4,5_pw_UB"+porter+".txt";
            String negativeWeightFile_UB=File_helper_UB + categories + "_fold2,3,4,5_nw_UB"+porter+".txt";

             line ="";
            TreeMap<Double,Integer> positiveWTM_UB=new TreeMap<Double,Integer>();   // value, id  for easy sort
            TreeMap<Double,Integer> negativeWTM_UB=new TreeMap<Double,Integer>();   // value, id  for easy sort

            //topicword setting
            br = new BufferedReader(new FileReader(wordIndex));
            HashMap<Integer,String> words= new HashMap<Integer,String>(); // id words
            while ((line = br.readLine()) != null) {
                String[] temp = line.split(",");
                Integer tempId= Integer.parseInt(temp[0]); //wordid  start from 1
                words.put(tempId, temp[1]);
            }

            // Divide into two group : positive ,negative groups
            br = new BufferedReader(new FileReader(inputFile_UB));
            while ((line = br.readLine()) != null) {
                String[] temp = line.split(":");
                Double tempValue= Double.parseDouble(temp[1]); //weight
                Integer tempId= Integer.parseInt(temp[0]); //weight
                if(tempValue>=0){
                    positiveWTM_UB.put(tempValue, tempId);
                }else{
                    negativeWTM_UB.put(tempValue, tempId);
                }
            }

            // decending order for positive
            NavigableMap des_positiveWTM_UB =positiveWTM_UB.descendingMap();

            /**saving**/
            // output form:
            // topicID  weight  words

            // for positive weights
//            StringBuffer pwstb= new StringBuffer();
             it = des_positiveWTM_UB.keySet().iterator();

            while (it.hasNext()) {  // Key를 뽑아낸 Iterator 를 돌려가며
                obj = it.next(); // Kef 를 하나씩 뽑아
//                pwstb.append(des_positiveWTM_UB.get(obj));
//                pwstb.append("\t");
//                pwstb.append(obj.toString());
//                pwstb.append("\t");
//                pwstb.append(words.get(des_positiveWTM_UB.get(obj)));
//                pwstb.append("\n");

                    positiveWordInUB.put(words.get(des_positiveWTM_UB.get(obj)),Double.parseDouble(obj.toString()));
            }
//            FileWriter pwfw = new FileWriter(positiveWeightFile_UB);
//            pwfw.append(pwstb.toString().trim());
//            pwfw.close();
            // for negative weights
//            StringBuffer nwstb= new StringBuffer();
            it = negativeWTM_UB.keySet().iterator();
            while (it.hasNext()) {  // Key를 뽑아낸 Iterator 를 돌려가며
                obj = it.next(); // Kef 를 하나씩 뽑아
//                nwstb.append(negativeWTM_UB.get(obj));
//                nwstb.append("\t");
//                nwstb.append(obj.toString());
//                nwstb.append("\t");
//                nwstb.append(words.get(negativeWTM_UB.get(obj)));
//                nwstb.append("\n");
                negativeWordInUB.put(words.get(negativeWTM_UB.get(obj)),Double.parseDouble(obj.toString()));
            }
        //  positive weight words only in lda100
            StringBuffer pwlda100only= new StringBuffer();
            it=positiveWordInLDA100.keySet().iterator();
            while (it.hasNext()) {
                obj =it.next();
                if(!positiveWeightFile_UB.contains(obj.toString())){
                    pwlda100only.append(obj.toString());
                    pwlda100only.append("\t");
                    pwlda100only.append(positiveWordInLDA100.get(obj));
                    pwlda100only.append("\n");
                }
            }
            FileWriter pwlda100fw= new FileWriter(positiveWeightOnlyinLDA100File);
            pwlda100fw.append(pwlda100only.toString().trim());
            pwlda100fw.close();

        //  negative weight words only in lda100
            StringBuffer nwlda100only= new StringBuffer();
            it=negativeWordInLDA100.keySet().iterator();
            while (it.hasNext()) {
                obj =it.next();
                if(!negativeWeightFile_UB.contains(obj.toString())){
                    nwlda100only.append(obj.toString());
                    nwlda100only.append("\t");
                    nwlda100only.append(negativeWordInLDA100.get(obj));
                    nwlda100only.append("\n");
                }
            }
            FileWriter nwlda100fw= new FileWriter(negativeWeightOnlyinLDA100File);
            nwlda100fw.append(nwlda100only.toString().trim());
            nwlda100fw.close();

            //  positive weight words only in UB
            //  negative weight words only in UB

            }

    }

}
