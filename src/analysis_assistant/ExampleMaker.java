package analysis_assistant;

import dataSetting.Stemmer;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.*;

/**
 * Created by lkylove23 on 2015-03-22.
 */
public class ExampleMaker {
    public static void main(String[] args) throws Exception {

//		String categories ="electronics";
        String categories ="fashion";
//		String categories ="hospitals";
//		String categories ="hotels";
//		String categories ="insurance";
//        String categories ="musicvenues";
//        String categories ="restaurants";
        String pwWordfile="D:\\FakeReviewDetectionData\\Yelp\\"+categories+"\\BalancedForLDA\\"+categories+"_fold2,3,4,5_pwn_UB_porter.txt";
        String nwWordfile="D:\\FakeReviewDetectionData\\Yelp\\"+categories+"\\BalancedForLDA\\"+categories+"_fold2,3,4,5_nwn_UB_porter.txt";
        String pwTopicfile="D:\\FakeReviewDetectionData\\Yelp\\"+categories+"\\BalancedForLDA\\"+categories+"_fold2,3,4,5_pwn_lda100_porter.txt";
        String nwTopicfile="D:\\FakeReviewDetectionData\\Yelp\\"+categories+"\\BalancedForLDA\\"+categories+"_fold2,3,4,5_nwn_lda100_porter.txt";

        //  input review

        //word weight
        String line="";
        BufferedReader br = new BufferedReader(new FileReader(pwWordfile));
        HashMap<String,Double> wordWeight=new HashMap<String, Double>();
        while ((line = br.readLine()) != null) {
            String[] temp= line.split("\t");
            wordWeight.put(temp[2],Double.parseDouble(temp[1]));
        }
        br = new BufferedReader(new FileReader(nwWordfile));
        while ((line = br.readLine()) != null) {
            String[] temp= line.split("\t");
            wordWeight.put(temp[2],Double.parseDouble(temp[1]));
        }

        //topic
        br = new BufferedReader(new FileReader(pwTopicfile));
        HashMap<String,String> topicwordPW=new HashMap<String, String>(); // word, topicid weight
        HashMap<String,String> topicwordPWorder=new HashMap<String, String>(); // word, topicid weight
        while ((line = br.readLine()) != null) {
            String[] temp =line.split("\t");
            String[] temp2 =temp[2].split(" ");
            for(int i =0;i<temp2.length/2;i++){
                if(topicwordPW.containsKey(temp2[2*i])){
                    topicwordPW.put(temp2[2*i],topicwordPW.get(temp2[2*i])+" "+temp[0]+" "+temp[1]);
                }else{
                    topicwordPW.put(temp2[2 * i], temp[0] + " " + temp[1]);
                }
            }
        }

        br = new BufferedReader(new FileReader(nwTopicfile));
        HashMap<String,String> topicwordNW=new HashMap<String, String>(); // word, topicid weight
        HashMap<String,String> topicwordNWorder=new HashMap<String, String>(); // word, topicid weight
        while ((line = br.readLine()) != null) {
            String[] temp =line.split("\t");
            String[] temp2 =temp[2].split(" ");
            for(int i =0;i<temp2.length/2;i++){
                if(topicwordNW.containsKey(temp2[2*i])){
                    topicwordNW.put(temp2[2*i],topicwordNW.get(temp2[2*i])+" "+temp[0]+" "+temp[1]);
                }else{
                    topicwordNW.put(temp2[2 * i], temp[0] + " " + temp[1]);
                }

            }
        }

        //input topic distribution of document
//        String input="1 1:0.0044693004266242705 2:3.369096000470749E-4 3:0.0026774300043197943 4:0.022872768281880233 5:0.004420189795465775 6:6.802160658505495E-5 7:5.5926463303590296E-5 8:0.0026442750959083396 9:0.04105631070496901 10:0.0024012728197279325 11:3.126498272182846E-4 12:6.464125482897242E-4 13:0.06115994147622042 14:6.957969507028809E-4 15:0.0020639347704213477 16:0.002893406025317316 17:0.15280657896853525 18:0.0010759366968385296 19:0.006783971553398553 20:6.047131995914653E-4 21:8.528048491049576E-5 22:9.923283276461227E-4 23:8.10334134687772E-5 24:0.006739358823255803 25:2.6284470975743797E-4 26:8.470657573417186E-5 27:8.163567854551771E-4 28:0.03290034288451703 29:0.005288132577642477 30:5.863996989265865E-4 31:8.225531187731139E-5 32:0.022695978791571727 33:8.494261550732823E-4 34:0.01909108558056376 35:3.6164567724628527E-4 36:0.01513083703918011 37:0.004722074041232826 38:1.1485113347030215E-4 39:0.002638139359395011 40:0.002632080684735392 41:1.1045408587355036E-4 42:1.1374555384261847E-4 43:0.0048481254801252885 44:0.037215209707432695 45:5.677071304274275E-4 46:0.018609420673114076 47:0.005004637710847388 48:0.005064268160273544 49:0.002289371475066068 50:0.00272588582600165 51:4.2034641908876997E-4 52:0.06535916713907433 53:6.684374705548527E-5 54:9.922975536200989E-5 55:0.004548742086046267 56:0.032644329177531725 57:0.0027421037366464128 58:1.2744942201156273E-4 59:4.010445579278045E-5 60:2.7852673187611204E-4 61:4.0218572393003083E-4 62:0.0026952115798722642 63:0.02317754817608684 64:1.035918729136213E-4 65:0.0011189279493437644 66:3.526731984180998E-4 67:0.037431647329300086 68:0.0026206266970307786 69:0.0023677788936456268 70:1.0389142065595015E-4 71:0.0024745862642472963 72:2.367375370892019E-4 73:6.11890943288121E-5 74:1.5637715558093645E-4 75:0.0022900677235573267 76:0.016989161257626315 77:6.117010122137391E-4 78:1.0141932702922893E-4 79:9.033933597854342E-5 80:0.04145724729281315 81:7.198945907293587E-4 82:0.008696649881606613 83:0.01515600020597687 84:0.06039217090372981 85:0.0011343602008817267 86:3.0573382108859217E-4 87:0.01486970535592254 88:5.959750776711574E-4 89:6.898576049389232E-4 90:0.12033262704859743 91:4.823507275118747E-4 92:1.2434256474330905E-4 93:0.00819292904700959 94:0.0022133279828356776 95:0.008255501008750013 96:2.3204947030749693E-4 97:0.00262329082799084 98:1.6507293582609752E-4 99:2.198535557362391E-4 100:0.010678825001969795";
        String input="-1 1:0.003295882642486397 2:0.0023884250509744243 3:0.018980880609084556 4:0.006016250261507294 5:0.05972363693744077 6:4.822198867368922E-4 7:3.96474799020369E-4 8:0.004551860743422113 9:0.04975945154609082 10:0.002829165029818246 11:0.0022164422723681297 12:0.00458255841740238 13:0.050338664390071404 14:0.004932655131282021 15:4.3770260934072045E-4 16:0.006318003805803829 17:0.018730584692000906 18:0.07859743577293911 19:0.005511109533176543 20:0.004286942712102432 21:6.045718094438813E-4 22:0.0070348302221438395 23:5.74463401065903E-4 24:0.005194840236455941 25:0.0018633630184864097 26:6.005032431177079E-4 27:0.005787329885054211 28:0.020327986074268667 29:0.023294738475898875 30:0.004157114343445315 31:5.831257032629347E-4 32:0.01895692885952863 33:0.00602176580140916 34:0.007595038451759769 35:0.002563784454319346 36:0.06468384495113118 37:0.033475804580993376 38:8.142045352075104E-4 39:0.018702340725999654 40:0.004465411924144536 41:7.830329134165567E-4 42:8.063668420155155E-4 43:0.005981455412131823 44:0.008335077939379966 45:0.018218576227186718 46:0.004180411706284532 47:0.007091004479858958 48:0.02170771518421753 49:0.0020358721857117686 50:0.005130416866951805 51:0.0029799267141652607 52:0.023331973504238865 53:4.7386978568141584E-4 54:7.034612058414861E-4 55:0.0038590617547110506 56:0.018513049512439955 57:0.03363334423712631 58:9.035165285349384E-4 59:2.8430916441024345E-4 60:0.0019745362663634147 61:0.002851181616803292 62:0.004912960499234937 63:0.008176900875959014 64:7.34385200984718E-4 65:0.007932322332384341 66:0.0025001766105547827 67:0.024063428617128377 68:0.004384212127037453 69:0.002591719340828418 70:7.365087597424412E-4 71:0.0033488999006524587 72:0.0016782836227012578 73:4.3378272901974774E-4 74:0.0011085914908255709 75:0.0020408080419117465 76:0.006888019684933255 77:0.004336480827716299 78:7.18983553142243E-4 79:6.404350992355213E-4 80:0.05260177776690888 81:0.005103488515372475 82:0.033264477589297466 83:0.008086321899866827 84:0.0023138400423127385 85:0.03642967999515975 86:0.002167413208516752 87:0.006056715762970801 88:0.0042249962751649975 89:0.004890549824080761 90:0.0014253764227215564 91:0.0034194886722851703 92:8.81491345137783E-4 93:0.0013055365115047484 94:0.0014967834159608719 95:0.0017491226868127817 96:0.0016450489029411182 97:0.018597076274137946 98:0.0011702377584665324 99:0.0015585894257476333 100:0.01892859080296257";//
//        String input="-1 1:0.009445611951631998 2:0.0012898980302617797 3:0.002585225284431254 4:0.018580410007365798 5:0.0015919557241065227 6:2.60428721345561E-4 7:2.1412104269136025E-4 8:0.017789548617705094 9:0.01154193687923098 10:0.03219044662174234 11:0.001197016636611725 12:0.002474866470587092 13:0.01185474782812304 14:0.0026639404636982565 15:2.363866236450232E-4 16:0.0034121148833906006 17:0.04077820192612053 18:0.004119350192883987 19:0.04897012420588547 20:0.0023152156095205987 21:3.2650636696148654E-4 22:0.0037992457176193573 23:3.1024595441673164E-4 24:0.0028055367221154264 25:0.0010063318864570066 26:3.243090881781396E-4 27:0.04145366997031519 28:0.0033127459932711443 29:0.0049149767509731514 30:0.0022451002182360394 31:3.1492413619050127E-4 32:0.13288800664811937 33:0.0032521279421208844 34:0.027098684192712708 35:0.0013846030112157405 36:0.0042707764574335515 37:0.002747755065910884 38:0.008105351472863015 39:0.010100426828930436 40:0.002411600080526529 41:4.2288645910579795E-4 42:4.3548823135977193E-4 43:0.0032303578255720275 44:0.0045014603291396145 45:0.002173533521162909 46:0.00225768224270624 47:0.019160844109202806 48:0.0730485590974598 49:0.001099497562690099 50:0.0027707440969952416 51:0.016940606505323277 52:0.02026634678112835 53:2.55919147599652E-4 54:3.7991278956411596E-4 55:0.0020841332885993312 56:0.002332567404373062 57:0.002832836275409624 58:4.8795510245996747E-4 59:1.535446259904735E-4 60:0.0010663723526193626 61:0.001539815348165407 62:0.05631271693690085 63:0.004416034880073905 64:3.966136696720372E-4 65:0.019615208059352223 66:0.00135025082070106 67:0.012995747621184708 68:0.002367747141409452 69:0.0013996895868103724 70:3.9776052343561455E-4 71:0.0018086141675800623 72:9.063775052349916E-4 73:2.342696445491869E-4 74:5.987083328394491E-4 75:0.0011021632319299225 76:0.003719958899432474 77:0.010007599671897459 78:3.88295822224118E-4 79:3.458747732851729E-4 80:0.005411339363108098 81:0.08707813551133897 82:0.0026336253509263202 83:0.027364007800734228 84:0.0012496174881863583 85:0.012008661849605832 86:0.008836168293563592 87:0.0032710030944132678 88:0.0099473910818553 89:0.025638092179458914 90:7.697918924858094E-4 91:0.0018467364931897714 92:4.7606013398751975E-4 93:0.03903322288857687 94:8.083561086368379E-4 95:9.446350043454466E-4 96:0.008554059090025396 97:0.017709207879864776 98:6.320011502844115E-4 99:8.417351967726612E-4 100:0.0025569855379539664"; // area fake
        TreeMap<Double,Double> topicDis= new TreeMap<Double, Double>(); // id dis
        TreeMap<Double,Double> topicDis2= new TreeMap<Double, Double>(); //  dis,id
        String[] inputchange=input.split(" ");
        for(int j=1;j<inputchange.length;j++) {
        String[] tememp =inputchange[j].split(":");
        topicDis.put(Double.parseDouble(tememp[0]),Double.parseDouble(tememp[1]));
        topicDis2.put(Double.parseDouble(tememp[1]),Double.parseDouble(tememp[0]));
        }
        System.out.println(topicDis.size());

        NavigableMap<Double,Double> topicDis2Order=topicDis2.descendingMap(); // dis id

        //new


        //sorting topicwordPW,NW


        for( String key : topicwordPW.keySet() ){
            String word=key;
            String[] temp= topicwordPW.get(key).split(" ");
            TreeMap<Double,Double> sortInside= new TreeMap<Double, Double>(); //weight, topicid
            TreeMap<Double,Double> sortInsideIdPW= new TreeMap<Double, Double>(); //topicid ,weight
            for(int i=0; i<temp.length/2;i++) {
                sortInside.put(Double.parseDouble(temp[2*i+1]),Double.parseDouble(temp[2*i]));
                sortInsideIdPW.put(Double.parseDouble(temp[2 * i]), Double.parseDouble(temp[2 * i + 1]));
            }
            String syn="";
            for( Double key1 : topicDis2Order.keySet() ){
                if(sortInsideIdPW.get(topicDis2Order.get(key1))==null){
                    continue;
                }
                syn+=topicDis2Order.get(key1)+" "+sortInsideIdPW.get(topicDis2Order.get(key1))+" "+key1+" ";

//                System.out.println(sortMap.get(key1)+" "+key1);
//                System.out.println(synB.toString());
            }
   ///

//            for( Double key1 : sortMap.keySet() ){
//                syn+=sortMap.get(key1)+" "+key1+" "+topicDis.get(sortMap.get(key1))+" ";
//
////                System.out.println(sortMap.get(key1)+" "+key1);
////                System.out.println(synB.toString());
//            }
            topicwordPWorder.put(word,syn);
        }
        for( String key : topicwordNW.keySet() ){
            String word=key;
            String[] temp= topicwordNW.get(key).split(" ");
            TreeMap<Double,Double> sortInside= new TreeMap<Double, Double>(); //weight, topicid
            TreeMap<Double,Double> sortInsideIdNW= new TreeMap<Double, Double>(); //topicid ,weight
            for(int i=0; i<temp.length/2;i++) {
                sortInside.put(Double.parseDouble(temp[2*i+1]),Double.parseDouble(temp[2*i]));
                sortInsideIdNW.put(Double.parseDouble(temp[2 * i]), Double.parseDouble(temp[2 * i + 1]));
            }
            String syn="";
            for( Double key1 : topicDis2Order.keySet() ){
                if(sortInsideIdNW.get(topicDis2Order.get(key1))==null){
                    continue;
                }
                syn+=topicDis2Order.get(key1)+" "+sortInsideIdNW.get(topicDis2Order.get(key1))+" "+key1+" ";

//                System.out.println(sortMap.get(key1)+" "+key1);
//                System.out.println(synB.toString());
            }
   ///

//            for( Double key1 : sortMap.keySet() ){
//                syn+=sortMap.get(key1)+" "+key1+" "+topicDis.get(sortMap.get(key1))+" ";
//
////                System.out.println(sortMap.get(key1)+" "+key1);
////                System.out.println(synB.toString());
//            }
            topicwordNWorder.put(word,syn);
        }
        for( String key : topicwordNW.keySet() ){
            String word=key;
            String[] temp= topicwordNW.get(key).split(" ");
            TreeMap<Double,Double> sortInside= new TreeMap<Double, Double>();
            for(int i=0; i<temp.length/2;i++) {
                sortInside.put(Double.parseDouble(temp[2*i+1]),Double.parseDouble(temp[2*i]));
            }
            String syn="";

            for( Double key1 : sortInside.keySet() ){
                syn+=sortInside.get(key1)+" "+key1+" "+topicDis.get(sortInside.get(key1))+" ";
            }
            topicwordNWorder.put(word,syn);
        }

        /**result
         *
         * -  for word
         * word (T or F)...
         *
         * - for topic
         * word (T{topic num, topic weight, topic proportion in this review} F {topic num, topic weight, , topic proportion in this review})
         * **/

//        Scanner scan = new Scanner(System.in);
//        while (true) {
//            System.out.println("put review :");

            String wordOut="";
            String topicOut="";
//            String temp = scan.nextLine();
//            String temp = "the young woman brought us over to a different changing area and on the way asked if we wanted to also look at shoes, and my daughter declined.  So she changed and we were then brought over to the check out area to pay.  The dress was $335, which I thought was reasonable compared to what some of my daughter's friends paid for their dresses: $400 - $500. ";
            String temp = "I really like this boutique. The owner was very friendly and very helpful. She helped me find the perfect outfit. This has to be one of the best boutiques in the Chicagoland area. It has only been open for about a year and a half but what a wonderful place to shop. The store was very clean and classy.";
//            String temp = "I really like this boutique. The owner was very friendly and very helpful. She helped me find the perfect outfit. This has to be one of the best boutiques in the Chicagoland area. It has only been open for about a year and a half but what a wonderful place to shop. The store was very clean and classy.";

            Stemmer porter = new Stemmer();
            String[] temp1 = temp.replace("."," ").replace("!"," ").replace("?"," ").toLowerCase().split(" ");
            for(int j=0; j<temp1.length;j++){
            char[] temptemp = temp1[j].toCharArray();
            porter.add(temptemp, temptemp.length);
            porter.stem();
            String tempS =porter.toString();
                wordOut+=tempS+" ";
                topicOut+=tempS+" ";
                if(wordWeight.containsKey(tempS)){
                    wordOut+="("+wordWeight.get(tempS)+") ";
                }
//                if(topicOrderFinalT.containsKey(tempS)){
                if(topicwordPWorder.containsKey(tempS)){
                    topicOut+= "("+topicwordPWorder.get(tempS)+") ";
//                    topicOut+= "("+topicOrderFinalT.get(tempS)+") ";
//                    System.out.println(topicOrderFinalT.containsKey(tempS));
//                    System.out.println(tempS);
//                    System.out.println(topicOrderFinalT);
                }
                if(topicwordNWorder.containsKey(tempS)){
                    topicOut+= "("+topicwordNWorder.get(tempS)+") ";
//                    topicOut+= "("+topicOrderFinalT.get(tempS)+") ";
//                    System.out.println(topicOrderFinalT.containsKey(tempS));
//                    System.out.println(tempS);
//                    System.out.println(topicOrderFinalT);
                }

            }
            System.out.println("word weight");
            System.out.println(wordOut);
            System.out.println("\ntopic weight");
            System.out.println(topicOut);




//            ArrayList<String> topicwordslist=new ArrayList<String>();
//            String temp2 = scan.nextLine();
//            String[] tempS2 =temp2.split(" ");
//            for(int j=0;j<tempS2.length;j++){
//                topicwordslist.add(tempS2[j]);
//            }
//
//            Stemmer porter = new Stemmer();
//            for(int i=0;i<tempS.length;i++) {
//                char[] temptemp = tempS[i].toCharArray();
//                porter.add(temptemp, temptemp.length);
//                porter.stem();
//                if(topicwordslist.contains(porter.toString())) {
//                    System.out.println(porter.toString());
//                }
//            }
//        }


//        System.out.println(word + "\t" + key + "\t" + wordInT.get(word) + "\t" + wordInF.get(word) + "\t"+wordWeight.get(word)+"\t" + topicwordPWorder.get(word)+"\t"+topicwordNWorder.get(word));
    }
}
