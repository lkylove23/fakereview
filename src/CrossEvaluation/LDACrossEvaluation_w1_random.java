package CrossEvaluation;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;

public class LDACrossEvaluation_w1_random {

    public static void main(String[] args) throws Exception {
        /**w1: capital theta
         * w2: df
         * w3: w1*w2
         * w4: (w1+w2)
         *                   **/
        //  1/topic
        /**Choose**/
        //KY
        String where = "D:\\";
        //or Dear IRbig2?
//		String where="D:\\KY\\";
        double tau = 0.0;

        int classChange_Train = 0; //classChange ��° doc ���� �ٸ� class
        int classChange_Test = 0;
        int topicNum = 0;

        double highAccuracy = 0.0;
        ArrayList<Integer> highTopic_F = new ArrayList<Integer>();
        ArrayList<Integer> highTopic_T = new ArrayList<Integer>();
        double ave;
        double aveOfAve = 0.0;
        String categories = "";
        String s_itr = "";
//	    String s_itr=" (2)";
//        String s_itr=" (3)";
//	    String s_itr=" (4)";
//	    String s_itr=" (5)";

//			changeID=115;
        categories = "restaurants";

        String trainingLDAFile = "";
        String testingLDAFile = "";
//		if(args.length!=0){
//			if(args.length!=6){
//				System.out.println("You have to type 5 argument\n # of topics\ttraining file\t# of fake reviews in training file\ttest file\tresult file\tvalue of tau");
//			}
//			numTopics = Integer.parseInt(args[0]);
//			trainingFile=args[1];
//			classChange_Train=Integer.parseInt(args[2]);
//			testingFile=args[3];
//			outDirPath=args[4];
//			tau=Double.parseDouble(args[7]);
//
//		}else{

        StringBuffer Total_ResultOut_folds = new StringBuffer();
//        ArrayList<Integer> listOfDocNum = new ArrayList<Integer>();
//        listOfDocNum.add(200);
////       listOfDocNum.add(300);
//        listOfDocNum.add(500);
//        listOfDocNum.add(1000);
//        listOfDocNum.add(5000);
//        listOfDocNum.add(10000);
//        listOfDocNum.add(20000);
//        listOfDocNum.add(35000);
        ArrayList<Integer> listOfTopicNum = new ArrayList<Integer>();
//        listOfTopicNum.add(2);
//        listOfTopicNum.add(10);
//        listOfTopicNum.add(50);
        listOfTopicNum.add(100);
//        listOfTopicNum.add(200);
//        listOfTopicNum.add(300);
        int maxdocnum;
        int maxtopicnum;
        int doc_i;
        int topic_i;
        long startTime=0;
        long endTime;
//        double constance=1.0; // w4
        double constance=0.2;
        //////
        int changeID=0;


//            System
            String resultFile_helper = where + "FakeReviewDetectionData\\Yelp\\"+categories+"\\";
            String resultFile_helper_setting = where + "FakeReviewDetectionData\\result\\Yelp\\"+categories+"\\";
            String TotalResultFile= resultFile_helper_setting+"result_total.txt";
            File  forCheckCategory = new File(resultFile_helper_setting);
            if (!forCheckCategory.isDirectory()) {
                forCheckCategory.mkdir();
            }
//            for (doc_i = 0, maxdocnum = listOfDocNum.size(); doc_i < maxdocnum; doc_i++) {
//                classChange_Train = classChange_Test * 4;

//                String classificationResultFile_check = resultFile_helper_setting + categories + "_" + totalNumOfReview + s_itr;
//                forCheckCategory = new File(classificationResultFile_check);
//                if (!forCheckCategory.isDirectory()) {
//                    forCheckCategory.mkdir();
//                }
                for (topic_i = 0, maxtopicnum = listOfTopicNum.size(); topic_i < maxtopicnum; topic_i++) {
                    topicNum = listOfTopicNum.get(topic_i);
                    int numOfFinD = topicNum; // num of features in the document
                    tau = 1 / (double) topicNum;
                    String test = "";
                    String train = "";
                    int max_iter = 0;
                    double[] highest_accuracy_folds = new double[7];
                    double highest_avg = 0.0;
                    double tmp_avg = 0.0;
                    double[] accuracy_folds = new double[7];
                    double[] precision_F_folds = new double[7];
                    double[] precision_T_folds = new double[7];
                    double[] recall_F_folds = new double[7];
                    double[] recall_T_folds = new double[7];
                    double[] F1_F_folds = new double[7];
                    double[] F1_T_folds = new double[7];
//                double[] highest_accuracy_folds = new double[7];
                    double[] highest_precision_F_folds = new double[7];
                    double[] highest_precision_T_folds = new double[7];
                    double[] highest_recall_F_folds = new double[7];
                    double[] highest_recall_T_folds = new double[7];
                    double[] highest_F1_F_folds = new double[7];
                    double[] highest_F1_T_folds = new double[7];
                    StringBuffer ResultOut_folds = new StringBuffer();
                    StringBuffer[] High_DocResultOut = new StringBuffer[7];
                    int cal_itr = 0;

                    double[] highfeatureSelection = new double[topicNum];
                    StringBuffer FSResultOut = new StringBuffer();
                    int high_cal_itr = 0;
                    startTime = System.currentTimeMillis();
                    String line = "";
//                while (cal_itr < 25000) {
                    /**inner CV**/

                        StringBuffer[] DocResultOut = new StringBuffer[7];
                        int tmp_it = -1;

                    for(int c_it=0;c_it<7;c_it++) {
                        if (c_it == 0) {
                            //Electronics
//                changeID = 44;
                            categories = "electronics";
                        } else if (c_it == 1) {
                            //Fashion
                            categories = "fashion";
                        } else if (c_it == 2) {
                            //hospitals
                            categories = "hospitals";
                        } else if (c_it == 3) {
                            //hotels
                            categories = "hotels";

                        } else if (c_it == 4) {
                            //Insurance
                            categories = "insurance";
                        } else if (c_it == 5) {
                            //Musicvenues
                            categories = "musicvenues";
                        } else if (c_it == 6) {
                            //Restaurant
//                totalNumOfReview = 37990;
                            categories = "restaurants";
                        }
                            if (c_it != tmp_it) {
                                trainingLDAFile =  where + "FakeReviewDetectionData\\Yelp\\forCrossEvaluation\\for_"+categories+"_lda"+topicNum+".txt";
                                testingLDAFile = where + "FakeReviewDetectionData\\Yelp\\forCrossEvaluation\\"+categories+"_lda"+topicNum+".txt";
                                tmp_it = c_it;
                            }
                            double[] SelectedFeature = new double[topicNum];
                            double highAvgAcc = 0.0;
//                            while (cal_itr < max_iter) {
                            cal_itr=0;
                            max_iter=400;
                            while (cal_itr < max_iter) {
                                if (cal_itr%100==0) {
                                    System.out.println("\n"+cal_itr);
                                }
                                //random here
                                double[] featureSelection = new double[topicNum];
                                for (int u = 0; u < topicNum; u++) {
//                            if(topicNum==2){
//                                featureSelection[0]=1.0;
//                                featureSelection[1]=1.0;
//                                break;
//                            }
                                    double ttt = Math.random() * 2;
                                    int dd = (int) ttt;
                                    featureSelection[u] = (double) dd;
//			System.out.print((double)dd);
                                }

                                Double[] doc_topicDis = new Double[topicNum];
                                int inner_test_start_f = 0;
                                int inner_test_end_f = 0;
                                int inner_test_start_t = 0;
                                int inner_test_end_t = 0;
//                                int numOfTrainDoc = totalNumOfReview / 5 * 4;
                                double[] inner_accs = new double[4];

                                double tmpAvgAcc = 0.0;

                                for (int inner_it = 0; inner_it < 4; inner_it++) {
                                    if (inner_it == 0) {
                                        inner_test_start_f = 0;
                                        inner_test_end_f = 149; // include test_start & end th doc
                                        inner_test_start_t = inner_test_start_f+600; // include test_start & end th doc
                                        inner_test_end_t = inner_test_end_f+600; // include test_start & end th doc
                                    } else if (inner_it == 1) {
                                        inner_test_start_f = 150;
                                        inner_test_end_f = 299; // include test_start & end th doc
                                        inner_test_start_t = inner_test_start_f+600; // include test_start & end th doc
                                        inner_test_end_t = inner_test_end_f+600; // include test_start & end th doc
                                    } else if (inner_it == 2) {
                                        inner_test_start_f = 300;
                                        inner_test_end_f = 449; // include test_start & end th doc
                                        inner_test_start_t = inner_test_start_f+600; // include test_start & end th doc
                                        inner_test_end_t = inner_test_end_f+600; // include test_start & end th doc
                                    } else if (inner_it == 3) {
                                        inner_test_start_f = 450;
                                        inner_test_end_f = 599; // include test_start & end th doc
                                        inner_test_start_t = inner_test_start_f+600; // include test_start & end th doc
                                        inner_test_end_t = inner_test_end_f+600; // include test_start & end th doc
                                    }
                                    Double acc = 0.0;
                                    BufferedReader br = new BufferedReader(new FileReader(trainingLDAFile));
                                    int id_inner = 0;
                                    int id_inner_train = 0;
                                    int id_inner_test = 0;
                                    Double[] w1_F = new Double[topicNum];
                                    Double[] w1_T = new Double[topicNum];
                                    for(int i=0;i<topicNum;i++){
                                        w1_F[i]=0.0;
                                        w1_T[i]=0.0;
                                    }
                                    double[] cnt_F = new double[topicNum];
                                    double[] cnt_T = new double[topicNum];
                                    for(int i=0;i<topicNum;i++){
                                        cnt_F[i]=0.0;
                                        cnt_T[i]=0.0;
                                    }
                                    while ((line = br.readLine()) != null) {
//                                        if (id_inner%30==0) {
//                                            System.out.println(cal_itr+"\t"+inner_it+"\t"+id_inner);
//                                        }
                                        //divide train and test in inner part
                                        if ((id_inner < inner_test_start_f || id_inner > inner_test_end_f)&&(id_inner < inner_test_start_t || id_inner > inner_test_end_t)) {
                                            String[] temp = line.split("\t");
                                            if (temp[1].equals("f")) {
                                                for (int ii = 0; ii < numOfFinD; ii++) {
                                                    w1_F[ii] += Double.parseDouble(temp[ii + 2]); //temp[ ] should be "value"
//                                                    double tmpProb=Double.parseDouble(temp[ii + 2]);
//                                                    if (tmpProb > tau) {
//                                                        cnt_F[ii]++;
//                                                    }
                                                }
                                            } else {

                                                for (int ii = 0; ii < numOfFinD; ii++) {
                                                    w1_T[ii] += Double.parseDouble(temp[ii + 2]); //temp[ ] should be "value"
//                                                    double tmpProb=Double.parseDouble(temp[ii + 2]);
//                                                    if (tmpProb > tau) {
//                                                        cnt_T[ii]++;
//                                                    }
                                                }
                                            }
                                            id_inner_train++;
                                        }
                                        id_inner++;
                                    }
                                    // Calculating w_1 in inner
                                    for (int i = 0; i < topicNum; i++) {
                                        double tmp = w1_F[i] + w1_T[i];
                                        w1_F[i] /= tmp;
                                        w1_T[i] /= tmp;
                                    }
                                    // Calculating w_2 in inner
//                                    double[] w2_F = new double[topicNum];
//                                    double[] w2_T = new double[topicNum];
//                                    for (int j = 0; j < topicNum; j++) {
//                                        w2_F[j] = Math.log10(((double) cnt_F[j] + 1.0) / ((double) cnt_T[j] + 1.0));
//                                        w2_T[j] = Math.log10(((double) cnt_T[j] + 1.0) / ((double) cnt_F[j] + 1.0));
//                                    }

                                    br = new BufferedReader(new FileReader(trainingLDAFile));
                                    double inner_acc = 0.0;
                                    id_inner=0;
                                    id_inner_test=0;
                                    while ((line = br.readLine()) != null) {
                                        Double F_Score = 0.0;
                                        Double T_Score = 0.0;
                                        if ((id_inner >= inner_test_start_f && id_inner <= inner_test_end_f)||(id_inner >= inner_test_start_t && id_inner <= inner_test_end_t)) {
//                            if(inner_it==3){System.out.println("test: "+id_inner_test);}
                                            String[] temp = line.split("\t");
                                            // Fill the vectors
                                            for (int ii = 0; ii < numOfFinD; ii++) {
                                                F_Score += w1_F[ii] * Double.parseDouble(temp[ii + 2])*featureSelection[ii];
                                                T_Score += w1_T[ii] * Double.parseDouble(temp[ii + 2])*featureSelection[ii];
//                                                F_Score +=w2_F[ii] * Double.parseDouble(temp[ii + 2])*featureSelection[ii];
//                                                T_Score += w2_T[ii] * Double.parseDouble(temp[ii + 2])*featureSelection[ii];
//                                                F_Score += (w1_F[ii]+w2_F[ii]) * Double.parseDouble(temp[ii + 2])*featureSelection[ii];
//                                                T_Score += (w1_T[ii]+w2_T[ii]) * Double.parseDouble(temp[ii + 2])*featureSelection[ii];
                                            }
                                            // Store dimension/value pairs in new LabeledFeatureVector object
                                            // Use cosine similarities (LinearKernel with L2-normalized input vectors)
                                            if (F_Score > T_Score && (temp[1].equals("f"))) {
                                                inner_acc++;
                                            } else if (F_Score < T_Score && (temp[1].equals("t"))) {

                                                inner_acc++;
                                            }
                                            id_inner_test++;
//                                            System.out.println(F_Score+"\t"+T_Score);
//                                            if(F_Score>T_Score){
//                                                System.out.println(">");
//                                            }else{
//                                                System.out.println("<");
//                                            }
                                        }

                                        id_inner++;

                                    }
                                    inner_accs[inner_it] = inner_acc / 300.0;
                                    ///
                                }
                                tmpAvgAcc = (inner_accs[0] + inner_accs[1] + inner_accs[2] + inner_accs[3]) / 4;
                                if (highAvgAcc < tmpAvgAcc) {
                                    highAvgAcc = tmpAvgAcc;
                                    System.out.print("inner avgAcc: " + highAvgAcc + " ");
                                    SelectedFeature=featureSelection;
                                }
                                cal_itr++;
                            }
                            System.out.println("//////////selection complete for "+categories+" fold"+(c_it+1)+"////////////////");
                            String[] temp;
                            BufferedReader br = new BufferedReader(new FileReader(trainingLDAFile));
                            double[] w1_F=new double[topicNum];
                            double[] w1_T=new double[topicNum];
                            double[] cnt_F = new double[topicNum];
                            double[] cnt_T = new double[topicNum];
                            for(int i=0;i<topicNum;i++){
                                cnt_F[i]=0.0;
                                cnt_T[i]=0.0;
                            }
                            while ((line = br.readLine()) != null) {
                                temp = line.split("\t");
                                if (temp[1].equals("f")) {
                                    for (int ii = 0; ii < numOfFinD; ii++) {
                                        w1_F[ii] += Double.parseDouble(temp[ii + 2]); //temp[ ] should be "value"
//                                        double tmpProb=Double.parseDouble(temp[ii + 2]);
//                                        if (tmpProb > tau) {
//                                            cnt_F[ii]++;
//                                        }
                                    }
                                } else {
                                    for (int ii = 0; ii < numOfFinD; ii++) {
                                        w1_T[ii] += Double.parseDouble(temp[ii + 2]); //temp[ ] should be "value"
//                                        double tmpProb=Double.parseDouble(temp[ii + 2]);
//                                        if (tmpProb > tau) {
//                                            cnt_T[ii]++;
//                                        }
                                    }
                                }
                            }


                            // Calculating w_1 in train
                            for (int i = 0; i < topicNum; i++) {
                                double tmp = w1_F[i] + w1_T[i];
                                w1_F[i] /= tmp;
                                w1_T[i] /= tmp;
                            }
                            // Calculating w_2 in inner
//                            double[] w2_F = new double[topicNum];
//                            double[] w2_T = new double[topicNum];
//                            for (int j = 0; j < topicNum; j++) {
//                                w2_F[j] = Math.log10(((double) cnt_F[j] + 1.0) / ((double) cnt_T[j] + 1.0));
//                                w2_T[j] = Math.log10(((double) cnt_T[j] + 1.0) / ((double) cnt_F[j] + 1.0));
//                            }
//                            DocResultOut[it] = new StringBuffer();
                            highAccuracy = 0;

                            double tp = 0.0;
                            double fp = 0.0;
                            double tn = 0.0;
                            double fn = 0.0;
                            double precision = 0.0;
                            double recall = 0.0;
                            double F1 = 0.0;
                            double accuracy = 0.0;

                            /**Score calculation**/
                            br = new BufferedReader(new FileReader(testingLDAFile));
//                            int[] cnt_F = new int[topicNum];
//                            int[] cnt_T = new int[topicNum];

//                ArrayList<Double> LDA_Train = new ArrayList();
                            while ((line = br.readLine()) != null) {
                                double[] LDA_Train = new double[topicNum];
                                temp = line.split("\t");
                                int testtt = 0;
                                double F_Score=0.0;
                                double T_Score=0.0;
                                for (int ii = 0; ii < topicNum; ii++) {
                                    F_Score +=w1_F[ii] * Double.parseDouble(temp[ii + 2])*SelectedFeature[ii];
                                    T_Score += w1_T[ii] * Double.parseDouble(temp[ii + 2])*SelectedFeature[ii];
//                                    F_Score +=w2_F[ii] * Double.parseDouble(temp[ii + 2])*SelectedFeature[ii];
//                                    T_Score += w2_T[ii] * Double.parseDouble(temp[ii + 2])*SelectedFeature[ii];
//                                    System.out.println("w1 f: "+w1_F[ii]+"w2 t: "+w1_T[ii]);
//                                    System.out.println("w2 f: "+w2_F[ii]+"w2 t: "+w2_T[ii]);
//                                    System.out.println();
//                                    F_Score +=(w1_F[ii] + constance* w2_F[ii]) * Double.parseDouble(temp[ii + 2])*SelectedFeature[ii];
//                                    T_Score +=(w1_T[ii] + constance* w2_T[ii]) * Double.parseDouble(temp[ii + 2])*SelectedFeature[ii];
                                }

                                if(F_Score<T_Score){
                                    if(temp[1].equals("t")){
                                        tp++;
                                    }else{
                                        fp++;
                                    }
                                }else{
                                    if(temp[1].equals("t")){
                                        fn++;
                                    }else{
                                        tn++;
                                    }
                                }

                            }
                            accuracy_folds[c_it] = (tn + tp) / (tn + tp + fn + fp);
                            precision_F_folds[c_it] = tn / (fn + tn);
                            precision_T_folds[c_it] = tp / (fp + tp);
                            recall_F_folds[c_it] = tn / (tn + fp);
                            recall_T_folds[c_it] = tp / (tp + fn);
                            F1_F_folds[c_it] = 2 * precision_F_folds[c_it] * recall_F_folds[c_it] / (precision_F_folds[c_it] + recall_F_folds[c_it]);
                            F1_T_folds[c_it] = 2 * precision_T_folds[c_it] * recall_T_folds[c_it] / (precision_T_folds[c_it] + recall_T_folds[c_it]);
                            System.out.println("c_it: "+c_it+"\t"+accuracy_folds[c_it]);
                                }
                    System.out.println("w1_random_"+max_iter);
                    System.out.println(accuracy_folds[0]);
                    System.out.println(accuracy_folds[1]);
                    System.out.println(accuracy_folds[2]);
                    System.out.println(accuracy_folds[3]);
                    System.out.println(accuracy_folds[4]);
                    System.out.println(accuracy_folds[5]);
                    System.out.println(accuracy_folds[6]);
                    System.out.println((accuracy_folds[0]+accuracy_folds[1]+accuracy_folds[2]+accuracy_folds[3]+accuracy_folds[4]+accuracy_folds[5]+accuracy_folds[6])/7);

//                                /**save classification result**/
//                                DocResultOut[it].append(docId_test);
//                                DocResultOut[it].append("\t");
//                                if (docId_test < classChange_Test) {
//                                    if (Score_F > Score_T) {
//                                        // 1 correct  0 not correct
//                                        DocResultOut[it].append("1");
//                                        tn++;
//                                    } else {
//                                        DocResultOut[it].append("0");
//                                        fp++;
//                                    }
//                                } else {
//                                    if (Score_F > Score_T) {
//                                        DocResultOut[it].append("0");
//                                        fn++;
//                                    } else {
//                                        DocResultOut[it].append("1");
//                                        tp++;
//                                    }
//                                }
//                                DocResultOut[it].append("\t");
//                                DocResultOut[it].append(Score_F);
//                                DocResultOut[it].append("\t");
//                                DocResultOut[it].append(Score_T);
//                                if (docId_test != totalNumOfReview / 5 - 1) {
//                                    DocResultOut[it].append("\n");
//                                }
//                                docId_test++;
//                            }
//                            accuracy = (tn + tp) / (tn + tp + fn + fp);
//                            double precision_F = tn / (fn + tn);
//                            double precision_T = tp / (fp + tp);
//                            double recall_F = tn / (tn + fp);
//                            double recall_T = tp / (tp + fn);
//                            double F1_F = 2 * precision_F * recall_F / (precision_F + recall_F);
//                            double F1_T = 2 * precision_T * recall_T / (precision_T + recall_T);
//
//                            accuracy_folds[it] = accuracy;
//                            precision_F_folds[it] = precision_F;
//                            precision_T_folds[it] = precision_T;
//                            recall_F_folds[it] = recall_F;
//                            recall_T_folds[it] = recall_T;
//                            F1_F_folds[it] = F1_F;
//                            F1_T_folds[it] = F1_T;
//                            high_cal_itr = cal_itr;
////                        } // for
//                        cal_itr++;
//                        tmp_avg = (accuracy_folds[0] + accuracy_folds[1] + accuracy_folds[2] + accuracy_folds[3] + accuracy_folds[4]) / 5;
//                        if (highest_avg < tmp_avg) {
//                            highest_avg = tmp_avg;
//                            for (int it = 0; it < 5; it++) {
//                                highest_accuracy_folds[it] = accuracy_folds[it];
//                                highest_precision_F_folds[it] = precision_F_folds[it];
//                                highest_precision_T_folds[it] = precision_T_folds[it];
//                                highest_recall_F_folds[it] = recall_F_folds[it];
//                                highest_recall_T_folds[it] = recall_T_folds[it];
//                                highest_F1_F_folds[it] = F1_F_folds[it];
//                                highest_F1_T_folds[it] = F1_T_folds[it];
//                                High_DocResultOut[it] = DocResultOut[it];
//                            }
//
//                            highfeatureSelection = featureSelection;
//                            System.out.println("topicNum: " + topicNum + "\t" + "docNum " + totalNumOfReview + "\titeration:" + s_itr + "\titr\t" + cal_itr);
//                            System.out.println("Result:\t" + highest_avg);
//
//                        }
//                    highest_accuracy_folds
                    } //while
//
//
//                    /**Evaluation on testdata**/
//                    endTime = System.currentTimeMillis();
//                    System.out.println("Time spent for fold:\t" + (endTime - startTime) / 60000 + " min.");
//                    FileWriter fw = new FileWriter(classificationResultFile);
//                    fw.append("fold1\n");
//                    fw.append(High_DocResultOut[0]);
//                    fw.append("\n");
//                    fw.append("fold2\n");
//                    fw.append(High_DocResultOut[1]);
//                    fw.append("\n");
//                    fw.append("fold3\n");
//                    fw.append(High_DocResultOut[2]);
//                    fw.append("\n");
//                    fw.append("fold4\n");
//                    fw.append(High_DocResultOut[3]);
//                    fw.append("\n");
//                    fw.append("fold5\n");
//                    fw.append(High_DocResultOut[4]);
//                    fw.append("\n");
//                    fw.flush();
//                    fw.close();
//                    FSResultOut.append(high_cal_itr);
//                    FSResultOut.append("\t");
//
//                    for (int ii = 0; ii < topicNum; ii++) {
//                        FSResultOut.append(highfeatureSelection[ii]);
//                        FSResultOut.append("\t");
//                    }
//                    fw = new FileWriter(featureSelectionResultFile);
//                    fw.append(FSResultOut);
//                    fw.flush();
//                    fw.close();
//                    System.out.println("topicNum: " + topicNum + "\t" + "docNum " + totalNumOfReview + "\tsampling iteration:" + s_itr);
//                    System.out.println(highest_accuracy_folds[0]);
//                    System.out.println(highest_accuracy_folds[1]);
//                    System.out.println(highest_accuracy_folds[2]);
//                    System.out.println(highest_accuracy_folds[3]);
//                    System.out.println(highest_accuracy_folds[4]);
//                    System.out.println((highest_accuracy_folds[0] + highest_accuracy_folds[1] + highest_accuracy_folds[2] + highest_accuracy_folds[3] + highest_accuracy_folds[4]) / 5);
//                    Total_ResultOut_folds.append(totalNumOfReview);
//                    Total_ResultOut_folds.append("\t");
//                    Total_ResultOut_folds.append(topicNum);
//                    Total_ResultOut_folds.append("\t");
//                    Total_ResultOut_folds.append(s_itr);
//                    for (int iii = 0; iii < 5; iii++) {
//                        ResultOut_folds.append(highest_accuracy_folds[iii]);
//                        ResultOut_folds.append("\t");
//                        ResultOut_folds.append(highest_precision_F_folds[iii]);
//                        ResultOut_folds.append("\t");
//                        ResultOut_folds.append(highest_recall_F_folds[iii]);
//                        ResultOut_folds.append("\t");
//                        ResultOut_folds.append(highest_F1_F_folds[iii]);
//                        ResultOut_folds.append("\t");
//                        ResultOut_folds.append(highest_precision_T_folds[iii]);
//                        ResultOut_folds.append("\t");
//                        ResultOut_folds.append(highest_recall_T_folds[iii]);
//                        ResultOut_folds.append("\t");
//                        ResultOut_folds.append(highest_F1_T_folds[iii]);
//                        ResultOut_folds.append("\n");
//
//                        Total_ResultOut_folds.append("\n");
//                        Total_ResultOut_folds.append(highest_accuracy_folds[iii]);
//                        Total_ResultOut_folds.append("\t");
//                        Total_ResultOut_folds.append(highest_precision_F_folds[iii]);
//                        Total_ResultOut_folds.append("\t");
//                        Total_ResultOut_folds.append(highest_recall_F_folds[iii]);
//                        Total_ResultOut_folds.append("\t");
//                        Total_ResultOut_folds.append(highest_F1_F_folds[iii]);
//                        Total_ResultOut_folds.append("\t");
//                        Total_ResultOut_folds.append(highest_precision_T_folds[iii]);
//                        Total_ResultOut_folds.append("\t");
//                        Total_ResultOut_folds.append(highest_recall_T_folds[iii]);
//                        Total_ResultOut_folds.append("\t");
//                        Total_ResultOut_folds.append(highest_F1_T_folds[iii]);
////                    Total_ResultOut_folds.append("\n");
////                    Total_ResultOut_folds.append("\n");
//
//                    }
//                    Total_ResultOut_folds.append("\n");
//                    Total_ResultOut_folds.append((highest_accuracy_folds[0] + highest_accuracy_folds[1] + highest_accuracy_folds[2] + highest_accuracy_folds[3] + highest_accuracy_folds[4]) / 5);
//                    Total_ResultOut_folds.append("\t");
//                    Total_ResultOut_folds.append((highest_precision_F_folds[0] + highest_precision_F_folds[1] + highest_precision_F_folds[2] + highest_precision_F_folds[3] + highest_precision_F_folds[4]) / 5);
//                    Total_ResultOut_folds.append("\t");
//                    Total_ResultOut_folds.append((highest_recall_F_folds[0] + highest_recall_F_folds[1] + highest_recall_F_folds[2] + highest_recall_F_folds[3] + highest_recall_F_folds[4]) / 5);
//                    Total_ResultOut_folds.append("\t");
//                    Total_ResultOut_folds.append((highest_F1_F_folds[0] + highest_F1_F_folds[1] + highest_F1_F_folds[2] + highest_F1_F_folds[3] + highest_F1_F_folds[4]) / 5);
//                    Total_ResultOut_folds.append("\t");
//                    Total_ResultOut_folds.append((highest_precision_T_folds[0] + highest_precision_T_folds[1] + highest_precision_T_folds[2] + highest_precision_T_folds[3] + highest_precision_T_folds[4]) / 5);
//                    Total_ResultOut_folds.append("\t");
//                    Total_ResultOut_folds.append((highest_recall_T_folds[0] + highest_recall_T_folds[1] + highest_recall_T_folds[2] + highest_recall_T_folds[3] + highest_recall_T_folds[4]) / 5);
//                    Total_ResultOut_folds.append("\t");
//                    Total_ResultOut_folds.append((highest_F1_T_folds[0] + highest_F1_T_folds[1] + highest_F1_T_folds[2] + highest_F1_T_folds[3] + highest_F1_T_folds[4]) / 5);
//                    Total_ResultOut_folds.append("\n");
//                    String resultFile = resultFile_helper_setting + categories + "_" + totalNumOfReview + s_itr + "\\" + categories + "_" + totalNumOfReview + "_lda" + topicNum + ".txt";
//                    try {
//                        fw = new FileWriter(resultFile);
//                        fw.append(ResultOut_folds);
//                        fw.flush();
//                        fw.close();
//                    } catch (IOException e) {
//                        System.err.println(e);
//                        System.exit(1);
//                    }
//                }
////            }
//            FileWriter fw = new FileWriter(TotalResultFile);
//            fw.append(Total_ResultOut_folds);
//            fw.flush();
//            fw.close();
            //


    }
}
