import java.io.*;
import java.util.ArrayList;

public class LDAEvaluation_allcate_itr100 {

    public static void main(String[] args) throws Exception {

        //  1/topic
        /**Choose**/
        //KY
        String where = "D:\\";
        //or Dear IRbig2?
//		String where="D:\\KY\\";
        double tau = 0.0;

        int classChange_Train = 0; //classChange ��° doc ���� �ٸ� class
        int classChange_Test = 0;
        int topicNum = 0;

        double highAccuracy = 0.0;
        ArrayList<Integer> highTopic_F = new ArrayList<Integer>();
        ArrayList<Integer> highTopic_T = new ArrayList<Integer>();


        double ave;
        double aveOfAve = 0.0;

        String categories = "";

        String s_itr = "";
//	    String s_itr=" (2)";
//        String s_itr=" (3)";
//	    String s_itr=" (4)";
//	    String s_itr=" (5)";

//			changeID=115;
        categories = "restaurants";

        String trainingLDAFile = "";
        String testingLDAFile = "";
//		if(args.length!=0){
//			if(args.length!=6){
//				System.out.println("You have to type 5 argument\n # of topics\ttraining file\t# of fake reviews in training file\ttest file\tresult file\tvalue of tau");
//			}
//			numTopics = Integer.parseInt(args[0]);
//			trainingFile=args[1];
//			classChange_Train=Integer.parseInt(args[2]);
//			testingFile=args[3];
//			outDirPath=args[4];
//			tau=Double.parseDouble(args[5]);
//
//		}else{

        StringBuffer Total_ResultOut_folds = new StringBuffer();
//        ArrayList<Integer> listOfDocNum = new ArrayList<Integer>();
//        listOfDocNum.add(200);
////       listOfDocNum.add(300);
//        listOfDocNum.add(500);
//        listOfDocNum.add(1000);
//        listOfDocNum.add(5000);
//        listOfDocNum.add(10000);
//        listOfDocNum.add(20000);
//        listOfDocNum.add(35000);
        ArrayList<Integer> listOfTopicNum = new ArrayList<Integer>();
//        listOfTopicNum.add(2);
//        listOfTopicNum.add(10);
//        listOfTopicNum.add(50);
        listOfTopicNum.add(100);
//        listOfTopicNum.add(200);
//        listOfTopicNum.add(300);
        int maxdocnum;
        int maxtopicnum;
        int doc_i;
        int topic_i;
        long startTime=0;
        long endTime;
        //////
        int changeID=0;

        int totalNumOfReview=0;
        for(int c_it=0;c_it<7;c_it++) {
            if (c_it == 0) {
                //Electronics
                totalNumOfReview=440;
                categories = "electronics";
            } else if (c_it == 1) {
                //Fashion
                totalNumOfReview = 2690;
                categories = "fashion";
            } else if (c_it == 2) {
                //hotels
                totalNumOfReview = 1100;
                categories = "hotels";
            } else if (c_it == 3) {
                //hospitals
                totalNumOfReview = 200;
                categories = "hospitals";
            } else if (c_it == 4) {
                //Insurance
                totalNumOfReview = 130;
                categories = "insurance";
            } else if (c_it == 5) {
                //Musicvenues
                totalNumOfReview = 1190;
                categories = "musicvenues";
            } else if (c_it == 6) {
                //Restaurant

                totalNumOfReview = 37990;
//			changeID=115;
                categories = "restaurants";
            }
            String resultFile_helper = where + "FakeReviewDetectionData\\Yelp\\"+categories+"\\";
            String resultFile_helper_setting = where + "FakeReviewDetectionData\\result\\Yelp\\"+categories+"\\";
            String TotalResultFile= resultFile_helper_setting+"result_total.txt";
            File  forCheckCategory = new File(resultFile_helper_setting);
            if (!forCheckCategory.isDirectory()) {
                forCheckCategory.mkdir();
            }
//            for (doc_i = 0, maxdocnum = listOfDocNum.size(); doc_i < maxdocnum; doc_i++) {
                classChange_Test = totalNumOfReview / 10;        // the # of fake reviews
                classChange_Train = classChange_Test * 4;

                String classificationResultFile_check = resultFile_helper_setting + categories + "_" + totalNumOfReview + s_itr;
                forCheckCategory = new File(classificationResultFile_check);
                if (!forCheckCategory.isDirectory()) {
                    forCheckCategory.mkdir();
                }
                for (topic_i = 0, maxtopicnum = listOfTopicNum.size(); topic_i < maxtopicnum; topic_i++) {
                    topicNum = listOfTopicNum.get(topic_i);
                    tau = 1 / (double) topicNum;
                    String test = "";
                    String train = "";
                    double tmp_max_iter = Math.pow(2, (Math.pow((double) topicNum, (1.0 / 3.0)) * 2.0));
                    int max_iter = (int) tmp_max_iter;
                    double[] highest_accuracy_folds = new double[5];
                    double highest_avg = 0.0;
                    double tmp_avg = 0.0;
                    double[] accuracy_folds = new double[5];
                    double[] precision_F_folds = new double[5];
                    double[] precision_T_folds = new double[5];
                    double[] recall_F_folds = new double[5];
                    double[] recall_T_folds = new double[5];
                    double[] F1_F_folds = new double[5];
                    double[] F1_T_folds = new double[5];
//                double[] highest_accuracy_folds = new double[5];
                    double[] highest_precision_F_folds = new double[5];
                    double[] highest_precision_T_folds = new double[5];
                    double[] highest_recall_F_folds = new double[5];
                    double[] highest_recall_T_folds = new double[5];
                    double[] highest_F1_F_folds = new double[5];
                    double[] highest_F1_T_folds = new double[5];
                    StringBuffer ResultOut_folds = new StringBuffer();
                    StringBuffer[] High_DocResultOut = new StringBuffer[5];
                    int cal_itr = 0;

                    double[] highfeatureSelection = new double[topicNum];
                    String featureSelectionResultFile = resultFile_helper_setting  + categories + "_" + totalNumOfReview + "_lda" + topicNum + "_FSresult.txt";
                    StringBuffer FSResultOut = new StringBuffer();
                    int high_cal_itr = 0;
                    String classificationResultFile = resultFile_helper_setting  + categories + "_" + totalNumOfReview + "_lda" + topicNum + "_result.txt";
                    startTime = System.currentTimeMillis();
                    String line = "";
//                while (cal_itr < 25000) {
                    while (cal_itr < 101) {
                        // first try for all topic on.  it will be criteria for other 100 iteration.
//                        if (cal_itr == 1000) {
//                            System.out.println(cal_itr);
//                        } else if (cal_itr == 2000) {
//                            System.out.println(cal_itr);
//                        } else if (cal_itr == 3000) {
//                            System.out.println(cal_itr);
//                        } else if (cal_itr == 4000) {
//                            System.out.println(cal_itr);
//                        } else if (cal_itr == 5000) {
//                            System.out.println(cal_itr);
//                        } else if (cal_itr == 6000) {
//                            System.out.println(cal_itr);
//                        } else if (cal_itr == 7000) {
//                            System.out.println(cal_itr);
//                        } else if (cal_itr == 8000) {
//                            System.out.println(cal_itr);
//                        } else if (cal_itr == 9000) {
//                            System.out.println(cal_itr);
//                        }

                        //random here
                        double[] featureSelection = new double[topicNum];
                        for (int u = 0; u < topicNum; u++) {
//                            if(topicNum==2){
//                                featureSelection[0]=1.0;
//                                featureSelection[1]=1.0;
//                                break;
//                            }
//                            double ttt = Math.random() * 2;
//                            int dd = (int) ttt;
                            featureSelection[u] = 1;
//                            featureSelection[u] = (double) dd;
//			System.out.print((double)dd);
                        }
                        if(cal_itr!=0) {
                            featureSelection[cal_itr - 1] = 0;
                        }
                        StringBuffer[] DocResultOut = new StringBuffer[5];
                        int tmp_it = -1;

                        for (int it = 0; it < 5; it++) {
                            if (it == 0) {
                                test = "fold1";
                                train = "fold2,3,4,5";
                            } else if (it == 1) {
                                test = "fold2";
                                train = "fold1,3,4,5";
                            } else if (it == 2) {
                                test = "fold3";
                                train = "fold1,2,4,5";
                            } else if (it == 3) {
                                test = "fold4";
                                train = "fold1,2,3,5";
                            } else if (it == 4) {
                                test = "fold5";
                                train = "fold1,2,3,4";
                            }
                            DocResultOut[it] = new StringBuffer();
                            highAccuracy = 0;

                            double tp = 0.0;
                            double fp = 0.0;
                            double tn = 0.0;
                            double fn = 0.0;
                            double precision = 0.0;
                            double recall = 0.0;
                            double F1 = 0.0;
                            double accuracy = 0.0;

//            trainingLDAFile = where + "FakeReviewDetectionData\\Yelp\\" + categories + "\\BalancedForLDA\\" + categories + "_" + train + "_lda.txt";
//            testingLDAFile = where + "FakeReviewDetectionData\\Yelp\\" + categories + "\\BalancedForLDA\\" + categories + "_" + test + "_lda.txt";
//                String trainingLDAFile_check = where + "FakeReviewDetectionData\\Yelp\\numOfReviews\\" + categories + "_" + totalNumOfReview+s_itr;
//                File forCheckCategory = new File(trainingLDAFile_check);
//                if(!forCheckCategory.isDirectory()){
//                    forCheckCategory.mkdir();
//                }
                            if (it != tmp_it) {
                                trainingLDAFile = resultFile_helper+ "BalancedForLDA\\" +categories+"_"+train+"_lad"+topicNum+".txt";
                                testingLDAFile = resultFile_helper + "BalancedForLDA\\" +categories+"_"+test+"_lad"+topicNum+".txt";
                                tmp_it = it;
                            }
//		Reader fileReader = new InputStreamReader(new FileInputStream(new File(args[0])), "UTF-8");
//            fileReader = new InputStreamReader(new FileInputStream(new File(testingFile)), "UTF-8");
//		fileReader = new InputStreamReader(new FileInputStream(new File("E:\\FakeReviewDetectionData\\DefaultTraining.txt")), "UTF-8");
//            testingInstances.addThruPipe(new CsvIterator(fileReader, Pattern.compile("^(\\S*)[\\s,]*(\\S*)[\\s,]*(.*)$"),

//		System.out.println("get 0: "+testingInstances.get(0));
//		System.out.println("get 1: "+testingInstances.get(1));
//		System.out.println("get 2: "+testingInstances.get(2));
//            BufferedReader br = new BufferedReader(new FileReader(testingFile));
                            ArrayList<String> reviewContent = new ArrayList();
//            while ((line = br.readLine()) != null) {
//                array = line.split("\t");
//                reviewContent.add(array[2]);
//            }
                            /**Preparation**/
                            BufferedReader br = new BufferedReader(new FileReader(trainingLDAFile));
                            int[] cnt_F = new int[topicNum];
                            int[] cnt_T = new int[topicNum];

                            String[] array;
//                ArrayList<Double> LDA_Train = new ArrayList();

                            int docId_train = 0;
                            int trainforNOTselection_start=0;
                            int trainforNOTselection_end=0;
                            while ((line = br.readLine()) != null) {
                                ///// 4fold divide into 3:1
                                double[] LDA_Train = new double[topicNum];
                                array = line.split("\t");
                                int testtt = 0;
                                for (int ii = 0; ii < topicNum; ii++) {
//                            for (int ii = 4; ii < 5; ii++) {
//                    LDA_Train[2+ii]=Double.parseDouble(array[2+ii]);
                                    double tmpProb = Double.parseDouble(array[2 + ii]);
                                    if (tmpProb > tau) {
                                        testtt++;
                                        if (docId_train < classChange_Train) {
                                            cnt_F[ii]++;
                                        } else {
                                            cnt_T[ii]++;
                                        }
                                    }
                                }
                                docId_train++;
                                /**Score calculation**/
                            }

//                double[] Score_F = new double[totalNumOfReview/5];
//                double[] Score_T = new double[totalNumOfReview/5];
                            double Score_F = 0;
                            double Score_T = 0;

                            double[] w_F = new double[topicNum];
                            double[] w_T = new double[topicNum];
                            for (int j = 0; j < topicNum; j++) {
                                w_F[j] = Math.log10(((double) cnt_F[j] + 1.0) / ((double) cnt_T[j] + 1.0));
                                w_T[j] = Math.log10(((double) cnt_T[j] + 1.0) / ((double) cnt_F[j] + 1.0));
                            }
                            br = new BufferedReader(new FileReader(testingLDAFile));
                            int docId_test = 0;
                            while ((line = br.readLine()) != null) {
                                array = line.split("\t");
                                double temp_cal_F;
                                double temp_cal_T;
//                if(i<changeID) {
//                    LDATestOut+=i+"\tf";
//                }else{
//                    LDATestOut+=i+"\tt";
//                }
                                for (int ii = 0; ii < topicNum; ii++) {
                                    temp_cal_F = Double.parseDouble(array[2 + ii]) * w_F[ii] * featureSelection[ii];
                                    temp_cal_T = Double.parseDouble(array[2 + ii]) * w_T[ii] * featureSelection[ii];
                                    Score_F += temp_cal_F;
                                    Score_T += temp_cal_T;
////			System.out.print(i+":: "+j+": prob: "+testProbabilities[j]+"\tcnt_F = "+cnt_F[j]+"\t");
////			System.out.print("cnt_T = "+cnt_T[j]+"\t");
////			System.out.print("w_F = "+w_F[j]+"\t");
////			System.out.print("w_T = "+w_T[j]);
////			System.out.println();
////			System.out.print("calculated Score_F= "+w_T[j]);
//                    if (Temp_F < testProbabilities[j] * w_F[j]) {
//                        Temp_F = testProbabilities[j] * w_F[j];
//                        Topic_F = j;
//                    }
//
//                    if (Temp_T < testProbabilities[j] * w_T[j]) {
//                        Temp_T = testProbabilities[j] * w_T[j];
//                        Topic_T = j;
//                    }
                                }
                                /**save classification result**/
                                DocResultOut[it].append(docId_test);
                                DocResultOut[it].append("\t");
                                if (docId_test < classChange_Test) {
                                    if (Score_F > Score_T) {
                                        // 1 correct  0 not correct
                                        DocResultOut[it].append("1");
                                        tn++;
                                    } else {
                                        DocResultOut[it].append("0");
                                        fp++;
                                    }
                                } else {
                                    if (Score_F > Score_T) {
                                        DocResultOut[it].append("0");
                                        fn++;
                                    } else {
                                        DocResultOut[it].append("1");
                                        tp++;
                                    }
                                }
                                DocResultOut[it].append("\t");
                                DocResultOut[it].append(Score_F);
                                DocResultOut[it].append("\t");
                                DocResultOut[it].append(Score_T);
                                if (docId_test != totalNumOfReview / 5 - 1) {
                                    DocResultOut[it].append("\n");
                                }
                                docId_test++;
                            }
                            accuracy = (tn + tp) / (tn + tp + fn + fp);
                            double precision_F = tn / (fn + tn);
                            double precision_T = tp / (fp + tp);
                            double recall_F = tn / (tn + fp);
                            double recall_T = tp / (tp + fn);
                            double F1_F = 2 * precision_F * recall_F / (precision_F + recall_F);
                            double F1_T = 2 * precision_T * recall_T / (precision_T + recall_T);

                            accuracy_folds[it] = accuracy;
                            precision_F_folds[it] = precision_F;
                            precision_T_folds[it] = precision_T;
                            recall_F_folds[it] = recall_F;
                            recall_T_folds[it] = recall_T;
                            F1_F_folds[it] = F1_F;
                            F1_T_folds[it] = F1_T;
                            high_cal_itr = cal_itr;
                            /// compare here

                        }
                        cal_itr++;
                        tmp_avg = (accuracy_folds[0] + accuracy_folds[1] + accuracy_folds[2] + accuracy_folds[3] + accuracy_folds[4]) / 5;
                        if (highest_avg < tmp_avg) {
                            highest_avg = tmp_avg;
                            for (int it = 0; it < 5; it++) {
                                highest_accuracy_folds[it] = accuracy_folds[it];
                                highest_precision_F_folds[it] = precision_F_folds[it];
                                highest_precision_T_folds[it] = precision_T_folds[it];
                                highest_recall_F_folds[it] = recall_F_folds[it];
                                highest_recall_T_folds[it] = recall_T_folds[it];
                                highest_F1_F_folds[it] = F1_F_folds[it];
                                highest_F1_T_folds[it] = F1_T_folds[it];
                                High_DocResultOut[it] = DocResultOut[it];
                            }

                            highfeatureSelection = featureSelection;
                            System.out.println("topicNum: " + topicNum + "\t" + "docNum " + totalNumOfReview + "\titeration:" + s_itr + "\titr\t" + cal_itr);
                            System.out.println("Result:\t" + highest_avg);

                        }
//                    highest_accuracy_folds
                    }
                    endTime = System.currentTimeMillis();
                    System.out.println("Time spent for fold:\t" + (endTime - startTime) / 60000 + " min.");
                    FileWriter fw = new FileWriter(classificationResultFile);
                    fw.append("fold1\n");
                    fw.append(High_DocResultOut[0]);
                    fw.append("\n");
                    fw.append("fold2\n");
                    fw.append(High_DocResultOut[1]);
                    fw.append("\n");
                    fw.append("fold3\n");
                    fw.append(High_DocResultOut[2]);
                    fw.append("\n");
                    fw.append("fold4\n");
                    fw.append(High_DocResultOut[3]);
                    fw.append("\n");
                    fw.append("fold5\n");
                    fw.append(High_DocResultOut[4]);
                    fw.append("\n");
                    fw.flush();
                    fw.close();
                    FSResultOut.append(high_cal_itr);
                    FSResultOut.append("\t");

                    for (int ii = 0; ii < topicNum; ii++) {
                        FSResultOut.append(highfeatureSelection[ii]);
                        FSResultOut.append("\t");
                    }
                    fw = new FileWriter(featureSelectionResultFile);
                    fw.append(FSResultOut);
                    fw.flush();
                    fw.close();
                    System.out.println("topicNum: " + topicNum + "\t" + "docNum " + totalNumOfReview + "\tsampling iteration:" + s_itr);
                    System.out.println(highest_accuracy_folds[0]);
                    System.out.println(highest_accuracy_folds[1]);
                    System.out.println(highest_accuracy_folds[2]);
                    System.out.println(highest_accuracy_folds[3]);
                    System.out.println(highest_accuracy_folds[4]);
                    System.out.println((highest_accuracy_folds[0] + highest_accuracy_folds[1] + highest_accuracy_folds[2] + highest_accuracy_folds[3] + highest_accuracy_folds[4]) / 5);
                    Total_ResultOut_folds.append(totalNumOfReview);
                    Total_ResultOut_folds.append("\t");
                    Total_ResultOut_folds.append(topicNum);
                    Total_ResultOut_folds.append("\t");
                    Total_ResultOut_folds.append(s_itr);
                    for (int iii = 0; iii < 5; iii++) {
                        ResultOut_folds.append(highest_accuracy_folds[iii]);
                        ResultOut_folds.append("\t");
                        ResultOut_folds.append(highest_precision_F_folds[iii]);
                        ResultOut_folds.append("\t");
                        ResultOut_folds.append(highest_recall_F_folds[iii]);
                        ResultOut_folds.append("\t");
                        ResultOut_folds.append(highest_F1_F_folds[iii]);
                        ResultOut_folds.append("\t");
                        ResultOut_folds.append(highest_precision_T_folds[iii]);
                        ResultOut_folds.append("\t");
                        ResultOut_folds.append(highest_recall_T_folds[iii]);
                        ResultOut_folds.append("\t");
                        ResultOut_folds.append(highest_F1_T_folds[iii]);
                        ResultOut_folds.append("\n");

                        Total_ResultOut_folds.append("\n");
                        Total_ResultOut_folds.append(highest_accuracy_folds[iii]);
                        Total_ResultOut_folds.append("\t");
                        Total_ResultOut_folds.append(highest_precision_F_folds[iii]);
                        Total_ResultOut_folds.append("\t");
                        Total_ResultOut_folds.append(highest_recall_F_folds[iii]);
                        Total_ResultOut_folds.append("\t");
                        Total_ResultOut_folds.append(highest_F1_F_folds[iii]);
                        Total_ResultOut_folds.append("\t");
                        Total_ResultOut_folds.append(highest_precision_T_folds[iii]);
                        Total_ResultOut_folds.append("\t");
                        Total_ResultOut_folds.append(highest_recall_T_folds[iii]);
                        Total_ResultOut_folds.append("\t");
                        Total_ResultOut_folds.append(highest_F1_T_folds[iii]);
//                    Total_ResultOut_folds.append("\n");
//                    Total_ResultOut_folds.append("\n");

                    }
                    Total_ResultOut_folds.append("\n");
                    Total_ResultOut_folds.append((highest_accuracy_folds[0] + highest_accuracy_folds[1] + highest_accuracy_folds[2] + highest_accuracy_folds[3] + highest_accuracy_folds[4]) / 5);
                    Total_ResultOut_folds.append("\t");
                    Total_ResultOut_folds.append((highest_precision_F_folds[0] + highest_precision_F_folds[1] + highest_precision_F_folds[2] + highest_precision_F_folds[3] + highest_precision_F_folds[4]) / 5);
                    Total_ResultOut_folds.append("\t");
                    Total_ResultOut_folds.append((highest_recall_F_folds[0] + highest_recall_F_folds[1] + highest_recall_F_folds[2] + highest_recall_F_folds[3] + highest_recall_F_folds[4]) / 5);
                    Total_ResultOut_folds.append("\t");
                    Total_ResultOut_folds.append((highest_F1_F_folds[0] + highest_F1_F_folds[1] + highest_F1_F_folds[2] + highest_F1_F_folds[3] + highest_F1_F_folds[4]) / 5);
                    Total_ResultOut_folds.append("\t");
                    Total_ResultOut_folds.append((highest_precision_T_folds[0] + highest_precision_T_folds[1] + highest_precision_T_folds[2] + highest_precision_T_folds[3] + highest_precision_T_folds[4]) / 5);
                    Total_ResultOut_folds.append("\t");
                    Total_ResultOut_folds.append((highest_recall_T_folds[0] + highest_recall_T_folds[1] + highest_recall_T_folds[2] + highest_recall_T_folds[3] + highest_recall_T_folds[4]) / 5);
                    Total_ResultOut_folds.append("\t");
                    Total_ResultOut_folds.append((highest_F1_T_folds[0] + highest_F1_T_folds[1] + highest_F1_T_folds[2] + highest_F1_T_folds[3] + highest_F1_T_folds[4]) / 5);
                    Total_ResultOut_folds.append("\n");
                    String resultFile = resultFile_helper_setting + categories + "_" + totalNumOfReview + s_itr + "\\" + categories + "_" + totalNumOfReview + "_lda" + topicNum + ".txt";
                    try {
                        fw = new FileWriter(resultFile);
                        fw.append(ResultOut_folds);
                        fw.flush();
                        fw.close();
                    } catch (IOException e) {
                        System.err.println(e);
                        System.exit(1);
                    }
                }
//            }
            FileWriter fw = new FileWriter(TotalResultFile);
            fw.append(Total_ResultOut_folds);
            fw.flush();
            fw.close();
            //
        }

    }
}
