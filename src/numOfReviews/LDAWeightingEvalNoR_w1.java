package numOfReviews;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;

public class LDAWeightingEvalNoR_w1 {

    public static void main(String[] args) throws Exception {
        String porter="";
        String mode="w1_100";
        /**w1: capital theta
         * w2: df
         * w3: w1*w2
         * w4: (w1+w2)
         *                   **/
        //  1/topic
        /**Choose**/
        //KY
        String where = "D:\\";
        //or Dear IRbig2?
//		String where="D:\\KY\\";
        ArrayList<Integer> listOfNoR = new ArrayList<Integer>();
        listOfNoR.add(50);
        listOfNoR.add(100);
        listOfNoR.add(200);
        listOfNoR.add(300);
        listOfNoR.add(500);
        listOfNoR.add(1000);


        double tau = 0.0;

        int classChange_Train = 0; //classChange ��° doc ���� �ٸ� class
        int classChange_Test = 0;
        int topicNum = 0;
//        int max_iter=10;
        int max_iter = 400;
        double highAccuracy = 0.0;
        ArrayList<Integer> highTopic_F = new ArrayList<Integer>();
        ArrayList<Integer> highTopic_T = new ArrayList<Integer>();
        double ave;
        double aveOfAve = 0.0;
        String categories = "";


//			changeID=115;

        String trainingLDAFile = "";
        String testingLDAFile = "";
//		if(args.length!=0){
//			if(args.length!=6){
//				System.out.println("You have to type 5 argument\n # of topics\ttraining file\t# of fake reviews in training file\ttest file\tresult file\tvalue of tau");
//			}
//			numTopics = Integer.parseInt(args[0]);
//			trainingFile=args[1];
//			classChange_Train=Integer.parseInt(args[2]);
//			testingFile=args[3];
//			outDirPath=args[4];
//			tau=Double.parseDouble(args[5]);
//
//		}else{

        StringBuffer Total_ResultOut_folds = new StringBuffer();
//        ArrayList<Integer> listOfDocNum = new ArrayList<Integer>();
//        listOfDocNum.add(200);
////       listOfDocNum.add(300);
//        listOfDocNum.add(500);
//        listOfDocNum.add(1000);
//        listOfDocNum.add(5000);
//        listOfDocNum.add(10000);
//        listOfDocNum.add(20000);
//        listOfDocNum.add(35000);
        ArrayList<Integer> listOfTopicNum = new ArrayList<Integer>();
//        listOfTopicNum.add(2);
//        listOfTopicNum.add(10);
//        listOfTopicNum.add(50);
        listOfTopicNum.add(100);
//        listOfTopicNum.add(200);
//        listOfTopicNum.add(300);
        ArrayList<String> listOfSampling = new ArrayList<String>();
        listOfSampling.add("");
        listOfSampling.add(" (2)");
        listOfSampling.add(" (3)");
        listOfSampling.add(" (4)");
        listOfSampling.add(" (5)");
        //        later
        listOfSampling.add(" (6)");
        listOfSampling.add(" (7)");
        listOfSampling.add(" (8)");
        listOfSampling.add(" (9)");
        listOfSampling.add(" (10)");
        int maxdocnum;
        int maxtopicnum;
        int doc_i;
        int topic_i;
        long startTime = 0;
        long endTime;
//        double constance=1.0; // w4
        double constance = 0.2;
        //////
        int changeID = 0;
        FileWriter totalSampleWriter = new FileWriter(where + "FakeReviewDetectionData\\result\\Yelp\\numOfReviews\\Total" + porter + "_Result" + "_" + mode + ".txt");

        ArrayList<Double> totalSample[][] = new ArrayList[listOfSampling.size()][7]; // NoS  category
        for (int ss = 0; ss < listOfSampling.size(); ss++) {
            totalSample[ss] = new ArrayList[7];
            for (int cc = 0; cc < 7; cc++) {
                totalSample[ss][cc] = new ArrayList();
            }
        }
        for (int sam = 0; sam < listOfSampling.size(); sam++) {
            String s_itr = listOfSampling.get(sam);
            for (int nor = 0; nor < listOfNoR.size(); nor++) {
                int totalNumOfReview = listOfNoR.get(nor);
//        for(int c_it=0;c_it<7;c_it++) {
                for (int c_it = 0; c_it < 7; c_it++) {
//        for(int c_it=0;c_it<3;c_it++) {
                    if (c_it == 0) {
                        if (totalNumOfReview > 440) {
                            continue;
                        }
                        //Electronics
//                changeID = 44;
                        categories = "electronics";
                    } else if (c_it == 1) {
                        if (totalNumOfReview > 26900) {
                            continue;
                        }
                        //Fashion
                        categories = "fashion";
                    } else if (c_it == 2) {
                        if (totalNumOfReview >= 200) {
                            continue;
                        }
                        //hospitals
                        categories = "hospitals";
                    } else if (c_it == 3) {
                        if (totalNumOfReview > 11000) {
                            continue;
                        }
                        //hotels
                        categories = "hotels";

                    } else if (c_it == 4) {
                        if (totalNumOfReview >= 130) {
                            continue;
                        }
                        //Insurance
                        categories = "insurance";
                    } else if (c_it == 5) {
                        if (totalNumOfReview > 1190) {
                            continue;
                        }
                        //Musicvenues
                        categories = "musicvenues";
                    } else if (c_it == 6) {
                        if (totalNumOfReview > 37980) {
                            continue;
                        }
                        //Restaurant
//                totalNumOfReview = 37990;
                        categories = "restaurants";
                    }
//            System
                    String resultFile_helper = where + "FakeReviewDetectionData\\Yelp\\numOfReviews\\";
//            String resultFile_helper_setting = where + "FakeReviewDetectionData\\result\\Yelp\\"+categories+"\\";
//            String TotalResultFile= resultFile_helper_setting+"result_total.txt";
//            File  forCheckCategory = new File(resultFile_helper_setting);
//            if (!forCheckCategory.isDirectory()) {
//                forCheckCategory.mkdir();
//            }
//            for (doc_i = 0, maxdocnum = listOfDocNum.size(); doc_i < maxdocnum; doc_i++) {
                    classChange_Test = totalNumOfReview / 10;        // the # of fake reviews
                    classChange_Train = classChange_Test * 4;

//                String classificationResultFile_check = resultFile_helper_setting + categories + "_" + totalNumOfReview + s_itr;
//                forCheckCategory = new File(classificationResultFile_check);
//                if (!forCheckCategory.isDirectory()) {
//                    forCheckCategory.mkdir();
//                }
                    for (topic_i = 0, maxtopicnum = listOfTopicNum.size(); topic_i < maxtopicnum; topic_i++) {
                        topicNum = listOfTopicNum.get(topic_i);
                        int numOfFinD = topicNum; // num of features in the document
                        tau = 1 / (double) topicNum;
                        String test = "";
                        String train = "";

                        double[] highest_accuracy_folds = new double[5];
                        double highest_avg = 0.0;
                        double tmp_avg = 0.0;
                        double[] accuracy_folds = new double[5];
                        double[] precision_F_folds = new double[5];
                        double[] precision_T_folds = new double[5];
                        double[] recall_F_folds = new double[5];
                        double[] recall_T_folds = new double[5];
                        double[] F1_F_folds = new double[5];
                        double[] F1_T_folds = new double[5];
//                double[] highest_accuracy_folds = new double[5];
                        double[] highest_precision_F_folds = new double[5];
                        double[] highest_precision_T_folds = new double[5];
                        double[] highest_recall_F_folds = new double[5];
                        double[] highest_recall_T_folds = new double[5];
                        double[] highest_F1_F_folds = new double[5];
                        double[] highest_F1_T_folds = new double[5];
                        StringBuffer ResultOut_folds = new StringBuffer();
                        StringBuffer[] High_DocResultOut = new StringBuffer[5];
                        int cal_itr = 0;

                        double[] highfeatureSelection = new double[topicNum];
//                    String featureSelectionResultFile = resultFile_helper_setting  + categories + "_" + totalNumOfReview + "_lda" + topicNum + "_FSresult.txt";
//                    StringBuffer FSResultOut = new StringBuffer();
                        int high_cal_itr = 0;
//                    String classificationResultFile = resultFile_helper_setting  + categories + "_" + totalNumOfReview + "_lda" + topicNum + "_result.txt";
                        startTime = System.currentTimeMillis();
                        String line = "";
//                while (cal_itr < 25000) {
                        /**inner CV**/

                        StringBuffer[] DocResultOut = new StringBuffer[5];
                        int tmp_it = -1;

                        for (int it = 0; it < 5; it++) {
                            if (it == 0) {
                                test = "fold1";
                                train = "fold2,3,4,5";
                            } else if (it == 1) {
                                test = "fold2";
                                train = "fold1,3,4,5";
                            } else if (it == 2) {
                                test = "fold3";
                                train = "fold1,2,4,5";
                            } else if (it == 3) {
                                test = "fold4";
                                train = "fold1,2,3,5";
                            } else if (it == 4) {
                                test = "fold5";
                                train = "fold1,2,3,4";
                            }
                            if (it != tmp_it) {
                                trainingLDAFile = resultFile_helper + categories + "_" + totalNumOfReview + s_itr + "\\" + categories + "_" + totalNumOfReview + "_" + train + "_lda" + topicNum + ".txt";
                                testingLDAFile = resultFile_helper + categories + "_" + totalNumOfReview + s_itr + "\\" + categories + "_" + totalNumOfReview + "_" + test + "_lda" + topicNum + ".txt";
                                tmp_it = it;
                            }
                            double[] SelectedFeature = new double[topicNum];
                            double highAvgAcc = 0.0;

                            cal_itr = 0;
                            while (cal_itr < max_iter) {
                                if (cal_itr % 100 == 0) {
                                    System.out.print( cal_itr);
                                }
                                //random here
                                double[] featureSelection = new double[topicNum];
                                for (int u = 0; u < topicNum; u++) {
//                            if(topicNum==2){
//                                featureSelection[0]=1.0;
//                                featureSelection[1]=1.0;
//                                break;
//                            }
                                    double ttt = Math.random() * 2;
                                    int dd = (int) ttt;
                                    featureSelection[u] = (double) dd;
//			System.out.print((double)dd);
                                }

                                Double[] doc_topicDis = new Double[topicNum];

                                int inner_test_start_f = 0;
                                int inner_test_end_f = 0;
                                int inner_test_start_t = 0;
                                int inner_test_end_t = 0;


                                int numOfTrainDoc = totalNumOfReview / 5 * 4;
                                double[] inner_accs = new double[4];

                                double tmpAvgAcc = 0.0;

                                for (int inner_it = 0; inner_it < 4; inner_it++) {
                                    if (inner_it == 0) {
                                        inner_test_start_f = 0;
                                        inner_test_end_f = numOfTrainDoc / 8 - 1; // include test_start & end th doc
                                        inner_test_start_t = inner_test_start_f + numOfTrainDoc / 2;
                                        inner_test_end_t = inner_test_end_f + numOfTrainDoc / 2; // include test_start & end th doc
                                    } else if (inner_it == 1) {
                                        inner_test_start_f = numOfTrainDoc / 8;
                                        inner_test_end_f = numOfTrainDoc / 8 * 2 - 1; // include test_start & end th doc
                                        inner_test_start_t = inner_test_start_f + numOfTrainDoc / 2;
                                        inner_test_end_t = inner_test_end_f + numOfTrainDoc / 2; // include test_start & end th doc
                                    } else if (inner_it == 2) {
                                        inner_test_start_f = numOfTrainDoc / 8 * 2;
                                        inner_test_end_f = numOfTrainDoc / 8 * 3 - 1; // include test_start & end th doc
                                        inner_test_start_t = inner_test_start_f + numOfTrainDoc / 2;
                                        inner_test_end_t = inner_test_end_f + numOfTrainDoc / 2; // include test_start & end th doc
                                    } else if (inner_it == 3) {
                                        inner_test_start_f = numOfTrainDoc / 8 * 3;
                                        inner_test_end_f = numOfTrainDoc / 8 * 4 - 1; // include test_start & end th doc
                                        inner_test_start_t = inner_test_start_f + numOfTrainDoc / 2;
                                        inner_test_end_t = inner_test_end_f + numOfTrainDoc / 2; // include test_start & end th doc
                                    }
                                    Double acc = 0.0;
                                    BufferedReader br = new BufferedReader(new FileReader(trainingLDAFile));
                                    int id_inner = 0;
                                    int id_inner_train = 0;
                                    int id_inner_test = 0;
                                    Double[] w1_F = new Double[topicNum];
                                    Double[] w1_T = new Double[topicNum];
                                    for (int i = 0; i < topicNum; i++) {
                                        w1_F[i] = 0.0;
                                        w1_T[i] = 0.0;
                                    }


                                    while ((line = br.readLine()) != null) {
//                                        if (id_inner%30==0) {
//                                            System.out.println(cal_itr+"\t"+inner_it+"\t"+id_inner);
//                                        }
                                        //divide train and test in inner part
                                        if ((id_inner < inner_test_start_f || id_inner > inner_test_end_f) && (id_inner < inner_test_start_t || id_inner > inner_test_end_t)) {
                                            String[] temp = line.split(" ");
                                            if (temp[0].equals("-1")) {
                                                for (int ii = 0; ii < numOfFinD; ii++) {
                                                    String[]temp2=temp[ii+1].split(":");
                                                    w1_F[ii] += Double.parseDouble(temp2[1]); //temp[ ] should be "value"


                                                }
                                            } else {
                                                for (int ii = 0; ii < numOfFinD; ii++) {
                                                    String[]temp2=temp[ii+1].split(":");
                                                    w1_T[ii] += Double.parseDouble(temp2[1]); //temp[ ] should be "value"


                                                }
                                            }
                                            id_inner_train++;
                                        }
                                        id_inner++;
                                    }
                                    // Calculating w_1 in inner
                                    for (int i = 0; i < topicNum; i++) {
                                        double tmp = w1_F[i] + w1_T[i];
                                        w1_F[i] /= tmp;
                                        w1_T[i] /= tmp;
                                    }

                                    br = new BufferedReader(new FileReader(trainingLDAFile));
                                    double inner_acc = 0.0;
                                    id_inner = 0;
                                    id_inner_test = 0;
                                    while ((line = br.readLine()) != null) {
                                        Double F_Score = 0.0;
                                        Double T_Score = 0.0;
                                        if ((id_inner >= inner_test_start_f && id_inner <= inner_test_end_f) || (id_inner >= inner_test_start_t && id_inner <= inner_test_end_t)) {
//                            if(inner_it==3){System.out.println("test: "+id_inner_test);}
                                            String[] temp = line.split(" ");
                                            // Fill the vectors
                                            for (int ii = 0; ii < numOfFinD; ii++) {
                                                String[]temp2=temp[ii+1].split(":");
//                                                F_Score += globalTheta_F[ii] * Double.parseDouble(temp[ii + 2])*featureSelection[ii];
//                                                T_Score += globalTheta_T[ii] * Double.parseDouble(temp[ii + 2])*featureSelection[ii];
//                                                F_Score +=w2_F[ii] * Double.parseDouble(temp[ii + 2])*featureSelection[ii];
//                                                T_Score += w2_T[ii] * Double.parseDouble(temp[ii + 2])*featureSelection[ii];
                                                F_Score += (w1_F[ii] ) * Double.parseDouble(temp2[1]) * featureSelection[ii];
                                                T_Score += (w1_T[ii] ) * Double.parseDouble(temp2[1]) * featureSelection[ii];
                                            }
                                            // Store dimension/value pairs in new LabeledFeatureVector object
                                            // Use cosine similarities (LinearKernel with L2-normalized input vectors)
                                            if (F_Score > T_Score && (temp[0].equals("-1"))) {
                                                inner_acc++;
                                            } else if (F_Score < T_Score && (temp[0].equals("1"))) {
                                                inner_acc++;
                                            }
                                            id_inner_test++;
//                                            System.out.println(F_Score+"\t"+T_Score);
//                                            if(F_Score>T_Score){
//                                                System.out.println(">");
//                                            }else{
//                                                System.out.println("<");
//                                            }
                                        }
                                        id_inner++;

                                    }
                                    inner_accs[inner_it] = inner_acc / ((double) totalNumOfReview / 5.0);
                                    ///
                                }
                                tmpAvgAcc = (inner_accs[0] + inner_accs[1] + inner_accs[2] + inner_accs[3]) / 4;
                                if (highAvgAcc < tmpAvgAcc) {
                                    highAvgAcc = tmpAvgAcc;
                                    System.out.print(" inner avgAcc: " + highAvgAcc + " ");
                                    SelectedFeature = featureSelection;
                                }
                                cal_itr++;
                            }
                            System.out.println("//////////selection complete for " + categories + " fold" + (it + 1) + "////////////////");
                            String[] temp;
                            BufferedReader br = new BufferedReader(new FileReader(trainingLDAFile));
                            double[] w1_F = new double[topicNum];
                            double[] w1_T = new double[topicNum];


                            while ((line = br.readLine()) != null) {
                                temp = line.split(" ");
                                if (temp[0].equals("-1")) {
                                    for (int ii = 0; ii < numOfFinD; ii++) {
                                        String[]temp2=temp[ii+1].split(":");
                                        double tmpProb = Double.parseDouble(temp2[1]);
                                        w1_F[ii] += tmpProb; //temp[ ] should be "value"
                                    }
                                } else {
                                    for (int ii = 0; ii < numOfFinD; ii++) {
                                        String[]temp2=temp[ii+1].split(":");
                                        double tmpProb = Double.parseDouble(temp2[1]);
                                        w1_T[ii] += tmpProb; //temp[ ] should be "value"

                                    }
                                }
                            }


                            // Calculating w_1 in train
                            for (int i = 0; i < topicNum; i++) {
                                double tmp = w1_F[i] + w1_T[i];
                                w1_F[i] /= tmp;
                                w1_T[i] /= tmp;
                            }


//                            DocResultOut[it] = new StringBuffer();
                            highAccuracy = 0;

                            double tp = 0.0;
                            double fp = 0.0;
                            double tn = 0.0;
                            double fn = 0.0;
                            double precision = 0.0;
                            double recall = 0.0;
                            double F1 = 0.0;
                            double accuracy = 0.0;

                            /**Score calculation**/
                            br = new BufferedReader(new FileReader(testingLDAFile));
//                            int[] cnt_F = new int[topicNum];
//                            int[] cnt_T = new int[topicNum];

//                ArrayList<Double> LDA_Train = new ArrayList();
                            while ((line = br.readLine()) != null) {
                                double[] LDA_Train = new double[topicNum];
                                temp = line.split(" ");
                                int testtt = 0;
                                double F_Score = 0.0;
                                double T_Score = 0.0;
                                for (int ii = 0; ii < topicNum; ii++) {
                                    String[]temp2=temp[ii+1].split(":");
//                                    F_Score += Double.parseDouble(temp[2 + ii]) * globalTheta_F[ii] * SelectedFeature[ii];
//                                    T_Score += Double.parseDouble(temp[2 + ii]) * globalTheta_T[ii] * SelectedFeature[ii];
//                                    F_Score +=w2_F[ii] * Double.parseDouble(temp[ii + 2])*SelectedFeature[ii];
//                                    T_Score += w2_T[ii] * Double.parseDouble(temp[ii + 2])*SelectedFeature[ii];
//                                    System.out.println("w1 f: "+w1_F[ii]+"w2 t: "+w1_T[ii]);
//                                    System.out.println("w2 f: "+w2_F[ii]+"w2 t: "+w2_T[ii]);
//                                    System.out.println();
                                    F_Score += (w1_F[ii] ) * Double.parseDouble(temp2[1]) * SelectedFeature[ii];
                                    T_Score += (w1_T[ii]) * Double.parseDouble(temp2[1]) * SelectedFeature[ii];
                                }

                                if (F_Score < T_Score) {
                                    if (temp[0].equals("1")) {
                                        tp++;
                                    } else {
                                        fp++;
                                    }
                                } else {
                                    if (temp[0].equals("1")) {
                                        fn++;
                                    } else {
                                        tn++;
                                    }
                                }

                            }
                            accuracy_folds[it] = (tn + tp) / (tn + tp + fn + fp);
                            precision_F_folds[it] = tn / (fn + tn);
                            precision_T_folds[it] = tp / (fp + tp);
                            recall_F_folds[it] = tn / (tn + fp);
                            recall_T_folds[it] = tp / (tp + fn);
                            F1_F_folds[it] = 2 * precision_F_folds[it] * recall_F_folds[it] / (precision_F_folds[it] + recall_F_folds[it]);
                            F1_T_folds[it] = 2 * precision_T_folds[it] * recall_T_folds[it] / (precision_T_folds[it] + recall_T_folds[it]);
//                            System.out.println("it: "+it+"\t"+accuracy_folds[it]);
                        }
//                        System.out.println(categories + "\t" + totalNumOfReview + "\tw5_random_" + max_iter + "\t" + constance + s_itr);
                        System.out.println(accuracy_folds[0]);
                        System.out.println(accuracy_folds[1]);
                        System.out.println(accuracy_folds[2]);
                        System.out.println(accuracy_folds[3]);
                        System.out.println(accuracy_folds[4]);
                        double avg=(accuracy_folds[0] + accuracy_folds[1] + accuracy_folds[2] + accuracy_folds[3] + accuracy_folds[4]) / 5;
                        System.out.println("topic "+topicNum+" sam "+sam+"\tnor "+nor+"\t"+categories);
                        System.out.println(avg);
                    totalSample[sam][c_it].add(avg);
//                                /**save classification result**/
//                                DocResultOut[it].append(docId_test);
//                                DocResultOut[it].append("\t");
//                                if (docId_test < classChange_Test) {
//                                    if (Score_F > Score_T) {
//                                        // 1 correct  0 not correct
//                                        DocResultOut[it].append("1");
//                                        tn++;
//                                    } else {
//                                        DocResultOut[it].append("0");
//                                        fp++;
//                                    }
//                                } else {
//                                    if (Score_F > Score_T) {
//                                        DocResultOut[it].append("0");
//                                        fn++;
//                                    } else {
//                                        DocResultOut[it].append("1");
//                                        tp++;
//                                    }
//                                }
//                                DocResultOut[it].append("\t");
//                                DocResultOut[it].append(Score_F);
//                                DocResultOut[it].append("\t");
//                                DocResultOut[it].append(Score_T);
//                                if (docId_test != totalNumOfReview / 5 - 1) {
//                                    DocResultOut[it].append("\n");
//                                }
//                                docId_test++;
//                            }
//                            accuracy = (tn + tp) / (tn + tp + fn + fp);
//                            double precision_F = tn / (fn + tn);
//                            double precision_T = tp / (fp + tp);
//                            double recall_F = tn / (tn + fp);
//                            double recall_T = tp / (tp + fn);
//                            double F1_F = 2 * precision_F * recall_F / (precision_F + recall_F);
//                            double F1_T = 2 * precision_T * recall_T / (precision_T + recall_T);
//
//                            accuracy_folds[it] = accuracy;
//                            precision_F_folds[it] = precision_F;
//                            precision_T_folds[it] = precision_T;
//                            recall_F_folds[it] = recall_F;
//                            recall_T_folds[it] = recall_T;
//                            F1_F_folds[it] = F1_F;
//                            F1_T_folds[it] = F1_T;
//                            high_cal_itr = cal_itr;
////                        } // for
//                        cal_itr++;
//                        tmp_avg = (accuracy_folds[0] + accuracy_folds[1] + accuracy_folds[2] + accuracy_folds[3] + accuracy_folds[4]) / 5;
//                        if (highest_avg < tmp_avg) {
//                            highest_avg = tmp_avg;
//                            for (int it = 0; it < 5; it++) {
//                                highest_accuracy_folds[it] = accuracy_folds[it];
//                                highest_precision_F_folds[it] = precision_F_folds[it];
//                                highest_precision_T_folds[it] = precision_T_folds[it];
//                                highest_recall_F_folds[it] = recall_F_folds[it];
//                                highest_recall_T_folds[it] = recall_T_folds[it];
//                                highest_F1_F_folds[it] = F1_F_folds[it];
//                                highest_F1_T_folds[it] = F1_T_folds[it];
//                                High_DocResultOut[it] = DocResultOut[it];
//                            }
//
//                            highfeatureSelection = featureSelection;
//                            System.out.println("topicNum: " + topicNum + "\t" + "docNum " + totalNumOfReview + "\titeration:" + s_itr + "\titr\t" + cal_itr);
//                            System.out.println("Result:\t" + highest_avg);
//
//                        }
//                    highest_accuracy_folds
                    } //while
//
//
//                    /**Evaluation on testdata**/
//                    endTime = System.currentTimeMillis();
//                    System.out.println("Time spent for fold:\t" + (endTime - startTime) / 60000 + " min.");
//                    FileWriter fw = new FileWriter(classificationResultFile);
//                    fw.append("fold1\n");
//                    fw.append(High_DocResultOut[0]);
//                    fw.append("\n");
//                    fw.append("fold2\n");
//                    fw.append(High_DocResultOut[1]);
//                    fw.append("\n");
//                    fw.append("fold3\n");
//                    fw.append(High_DocResultOut[2]);
//                    fw.append("\n");
//                    fw.append("fold4\n");
//                    fw.append(High_DocResultOut[3]);
//                    fw.append("\n");
//                    fw.append("fold5\n");
//                    fw.append(High_DocResultOut[4]);
//                    fw.append("\n");
//                    fw.flush();
//                    fw.close();
//                    FSResultOut.append(high_cal_itr);
//                    FSResultOut.append("\t");
//
//                    for (int ii = 0; ii < topicNum; ii++) {
//                        FSResultOut.append(highfeatureSelection[ii]);
//                        FSResultOut.append("\t");
//                    }
//                    fw = new FileWriter(featureSelectionResultFile);
//                    fw.append(FSResultOut);
//                    fw.flush();
//                    fw.close();
//                    System.out.println("topicNum: " + topicNum + "\t" + "docNum " + totalNumOfReview + "\tsampling iteration:" + s_itr);
//                    System.out.println(highest_accuracy_folds[0]);
//                    System.out.println(highest_accuracy_folds[1]);
//                    System.out.println(highest_accuracy_folds[2]);
//                    System.out.println(highest_accuracy_folds[3]);
//                    System.out.println(highest_accuracy_folds[4]);
//                    System.out.println((highest_accuracy_folds[0] + highest_accuracy_folds[1] + highest_accuracy_folds[2] + highest_accuracy_folds[3] + highest_accuracy_folds[4]) / 5);
//                    Total_ResultOut_folds.append(totalNumOfReview);
//                    Total_ResultOut_folds.append("\t");
//                    Total_ResultOut_folds.append(topicNum);
//                    Total_ResultOut_folds.append("\t");
//                    Total_ResultOut_folds.append(s_itr);
//                    for (int iii = 0; iii < 5; iii++) {
//                        ResultOut_folds.append(highest_accuracy_folds[iii]);
//                        ResultOut_folds.append("\t");
//                        ResultOut_folds.append(highest_precision_F_folds[iii]);
//                        ResultOut_folds.append("\t");
//                        ResultOut_folds.append(highest_recall_F_folds[iii]);
//                        ResultOut_folds.append("\t");
//                        ResultOut_folds.append(highest_F1_F_folds[iii]);
//                        ResultOut_folds.append("\t");
//                        ResultOut_folds.append(highest_precision_T_folds[iii]);
//                        ResultOut_folds.append("\t");
//                        ResultOut_folds.append(highest_recall_T_folds[iii]);
//                        ResultOut_folds.append("\t");
//                        ResultOut_folds.append(highest_F1_T_folds[iii]);
//                        ResultOut_folds.append("\n");
//
//                        Total_ResultOut_folds.append("\n");
//                        Total_ResultOut_folds.append(highest_accuracy_folds[iii]);
//                        Total_ResultOut_folds.append("\t");
//                        Total_ResultOut_folds.append(highest_precision_F_folds[iii]);
//                        Total_ResultOut_folds.append("\t");
//                        Total_ResultOut_folds.append(highest_recall_F_folds[iii]);
//                        Total_ResultOut_folds.append("\t");
//                        Total_ResultOut_folds.append(highest_F1_F_folds[iii]);
//                        Total_ResultOut_folds.append("\t");
//                        Total_ResultOut_folds.append(highest_precision_T_folds[iii]);
//                        Total_ResultOut_folds.append("\t");
//                        Total_ResultOut_folds.append(highest_recall_T_folds[iii]);
//                        Total_ResultOut_folds.append("\t");
//                        Total_ResultOut_folds.append(highest_F1_T_folds[iii]);
////                    Total_ResultOut_folds.append("\n");
////                    Total_ResultOut_folds.append("\n");
//
//                    }
//                    Total_ResultOut_folds.append("\n");
//                    Total_ResultOut_folds.append((highest_accuracy_folds[0] + highest_accuracy_folds[1] + highest_accuracy_folds[2] + highest_accuracy_folds[3] + highest_accuracy_folds[4]) / 5);
//                    Total_ResultOut_folds.append("\t");
//                    Total_ResultOut_folds.append((highest_precision_F_folds[0] + highest_precision_F_folds[1] + highest_precision_F_folds[2] + highest_precision_F_folds[3] + highest_precision_F_folds[4]) / 5);
//                    Total_ResultOut_folds.append("\t");
//                    Total_ResultOut_folds.append((highest_recall_F_folds[0] + highest_recall_F_folds[1] + highest_recall_F_folds[2] + highest_recall_F_folds[3] + highest_recall_F_folds[4]) / 5);
//                    Total_ResultOut_folds.append("\t");
//                    Total_ResultOut_folds.append((highest_F1_F_folds[0] + highest_F1_F_folds[1] + highest_F1_F_folds[2] + highest_F1_F_folds[3] + highest_F1_F_folds[4]) / 5);
//                    Total_ResultOut_folds.append("\t");
//                    Total_ResultOut_folds.append((highest_precision_T_folds[0] + highest_precision_T_folds[1] + highest_precision_T_folds[2] + highest_precision_T_folds[3] + highest_precision_T_folds[4]) / 5);
//                    Total_ResultOut_folds.append("\t");
//                    Total_ResultOut_folds.append((highest_recall_T_folds[0] + highest_recall_T_folds[1] + highest_recall_T_folds[2] + highest_recall_T_folds[3] + highest_recall_T_folds[4]) / 5);
//                    Total_ResultOut_folds.append("\t");
//                    Total_ResultOut_folds.append((highest_F1_T_folds[0] + highest_F1_T_folds[1] + highest_F1_T_folds[2] + highest_F1_T_folds[3] + highest_F1_T_folds[4]) / 5);
//                    Total_ResultOut_folds.append("\n");
//                    String resultFile = resultFile_helper_setting + categories + "_" + totalNumOfReview + s_itr + "\\" + categories + "_" + totalNumOfReview + "_lda" + topicNum + ".txt";
//                    try {
//                        fw = new FileWriter(resultFile);
//                        fw.append(ResultOut_folds);
//                        fw.flush();
//                        fw.close();
//                    } catch (IOException e) {
//                        System.err.println(e);
//                        System.exit(1);
//                    }
//                }
////            }
//            FileWriter fw = new FileWriter(TotalResultFile);
//            fw.append(Total_ResultOut_folds);
//            fw.flush();
//            fw.close();
                    //
                }
            }
        }
        totalSampleWriter.append("size of sample " + listOfSampling.size());
        totalSampleWriter.append("\n");
        ArrayList<Double>[] avg = new ArrayList[7];
        for (int c = 0; c < 7; c++) {
            for (int r = 0; r < totalSample[0][c].size(); r++) {
                double tmp_avg = 0.0;
                for (int s = 0; s < listOfSampling.size(); s++) {
                    tmp_avg += totalSample[s][c].get(r);
                }
                tmp_avg /= listOfSampling.size();
                double std = 0.0;
//                    double se = 0.0;
                for (int s = 0; s < listOfSampling.size(); s++) {
                    std += Math.pow(totalSample[s][c].get(r) - tmp_avg, 2);
                }
                std = Math.sqrt((std / listOfSampling.size()));
//                se=std/Math.sqrt() // stop
                totalSampleWriter.append(Double.toString(tmp_avg));
                totalSampleWriter.append("\t");
                totalSampleWriter.append(Double.toString(std));
                totalSampleWriter.append("\t");

            }
            totalSampleWriter.append("\n");


        }
        totalSampleWriter.close();
    }
}
