package nlp;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale.Category;

public class tfidfSum {

	public static void main(String[] args) throws Exception {
		String output="";
		double Average=0.0;
		
		String where="D:\\FakeReviewDetectionData";
//		String categories ="hotels";
//		String categories ="restaurants";
//		String categories ="electronics";
//		String categories ="fashion";
//		String categories ="hospital";
//		String categories ="musicvenues";
		String categories ="insurance";
		
		for(int ii=1; ii<2;ii++){
		String test="";
//		String test="fold2";
//		String test="fold3";
//		String test="fold4";
//		String test="fold5";
		String trainingFold="";
		if(ii==1){
			//fodl1
//			System.out.println("fold1 correct!");
			test="fold1";
			trainingFold="fold2,3,4,5";
		}else if(ii==2){
//			System.out.println("fold2 correct!");
			 //fold2
			test="fold2";
			trainingFold="fold1,3,4,5";
		}else if(ii==3){
//			System.out.println("fold3 correct!");
			//fold3
			test="fold3";
			trainingFold="fold1,2,4,5";
		}else if(ii==4){

			//fold4
			test="fold4";
			trainingFold="fold1,2,3,5";
		}else if(ii==5){
		    //fold5
			test="fold5";
			trainingFold="fold1,2,3,4";
		}
		
		/**each**/
//		String trainingFile=where+"\\Yelp\\"+categories+"\\BalancedForLDA\\"+categories+"_"+trainingFold+".csv";
//		String testingFile=where+"\\Yelp\\"+categories+"\\BalancedForLDA\\"+categories+"_"+test+".csv";
		
		
		/**for Cross**/
		
		

		/** document를 ArrayList로 만들고 document안의 단어 **/
//		String trainingFile=where+"\\Yelp\\"+categories+"\\BalancedForLDA\\"+categories+"_"+trainingFold+".csv";
		String trainingFile=where+"\\Yelp\\forCrossEvaluation\\for_"+categories+".csv";
		String testingFile=where+"\\Yelp\\forCrossEvaluation\\"+categories+".csv";
		// read csv for test data
		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ",";
		/**tf per doucument**/
		HashMap<String,Double> tf_T= new HashMap<String,Double>();
		HashMap<String,Double> tf_F= new HashMap<String,Double>();
		HashMap<String,Double> idf_T=new HashMap<String,Double>();
		HashMap<String,Double> idf_F=new HashMap<String,Double>();
		HashMap<String,Double> tfidf_T=new HashMap<String,Double>();
		HashMap<String,Double> tfidf_F=new HashMap<String,Double>();
		double Score_T;
		double Score_F;
		double ra=0;
		double rb=0;
		double rc=0;
		double rd=0;
		double Accuracy=0;
		double Precision=0;
		double Recall=0;
		double F1=0;
		int tt=0;
		int ff=0;
		
		/* resultAL
	     * 
	     * 0: Accuracy
	     * 1: T-P	
	     * 2: T-R	
	     * 3: T-F	
	     * 4: F-P
	     * 5: F-R
	     * 6: F-F
	     * etc
	     */
		ArrayList<Double> resultAL=new ArrayList<Double>();
		double totalNumDocs_T=0.0;
		double totalNumDocs_F=0.0;
		double log2=Math.log(2);
		String[] tempArray;
		String[] temSent;
		String tempWord="";
		ArrayList<String> docByWord= new ArrayList<String>();
		try {
			br = new BufferedReader(new FileReader(trainingFile));

				for(int i=0;(line = br.readLine()) != null;i++) {
					
				// use comma as separator
				tempArray = line.split(cvsSplitBy);
				
				/**calculate idf first**/
				temSent=tempArray[4].toLowerCase().replace("\"", "").replace("?", "").replace("!", "").replace(".", "").replace("|", "").replace("(", "").replace(")", "").replace("'s", " 's").split(" ");
				/**senetence to AL for each word it has**/
				
				for(int i2=0;i2<temSent.length;i2++){
					if(!temSent[i2].equals("")){
						if(!docByWord.contains("temSent[i2]")){
							//just appearance
						
				docByWord.add(temSent[i2]);
						}
					}
				}
				if(tempArray[1].equals("T")){
				for(int i3=0;i3<docByWord.size();i3++){
					
					tempWord=docByWord.get(i3);
					if(!idf_T.containsKey(tempWord)){
						idf_T.put(tempWord, 1.0);
					}else{
						idf_T.put(tempWord,idf_T.get(tempWord)+1);
					}
				}
				totalNumDocs_T++;
				}else 
				if(tempArray[1].equals("F")){
					for(int i3=0;i3<docByWord.size();i3++){
						
						tempWord=docByWord.get(i3);
						if(!idf_F.containsKey(tempWord)){
							idf_F.put(tempWord, 1.0);
						}else{
							idf_F.put(tempWord,idf_F.get(tempWord)+1);
						}
					}
					totalNumDocs_F++;
				}
				docByWord.clear();
			}
//				System.out.println(totalNumDocs);
				/**idf= log2( num of D/idf value)**/
				Iterator<String> iterator = idf_T.keySet().iterator();
			    while (iterator.hasNext()) {
			        String key = (String) iterator.next();
//			        System.out.print("key="+key);
//			        System.out.println("\tvalue="+idf.get(key));
//			        System.out.println("finish================");
			        idf_T.put(key, Math.log(totalNumDocs_T/(1+idf_T.get(key)))/log2);
//			        System.out.print(key);
//			        System.out.println("\t"+idf_T.get(key));
			    }
			    Iterator<String> iterator_F = idf_F.keySet().iterator();
			    while (iterator_F.hasNext()) {
			    	String key = (String) iterator_F.next();
//			        System.out.print("key="+key);
//			        System.out.println("\tvalue="+idf.get(key));
//			        System.out.println("finish================");
			    	idf_F.put(key, Math.log(totalNumDocs_F/(1+idf_F.get(key)))/log2);
//			        System.out.print(key);
//			        System.out.println("\t"+idf_F.get(key));
			    }
			    
			    
			    /**Calcualting tf**/
			    br = new BufferedReader(new FileReader(trainingFile));
			    for(int i=0;(line = br.readLine()) != null;i++) {
			    	tempArray = line.split(cvsSplitBy);
			    	
			    	
			    	tf_T.clear();
			    	tf_F.clear();
			    	tempArray = line.split(cvsSplitBy);
			    	
			    	temSent=tempArray[4].toLowerCase().replace("\"", "").replace("?", "").replace("!", "").replace(".", "").replace("|", "").replace("(", "").replace(")", "").replace("'s", " 's").split(" ");
			    	/**senetence to AL for each word it has**/
					
					for(int i2=0;i2<temSent.length;i2++){
						if(!temSent[i2].equals("")){
					docByWord.add(temSent[i2]);
						}
					}
					
					if(tempArray[1].equals("T")){
					
					for(int i3=0;i3<docByWord.size();i3++){
						tempWord=docByWord.get(i3);
						if(!tf_T.containsKey(tempWord)){
							tf_T.put(tempWord, 1.0);
//							tt++;
//							System.out.println("S:\t"+tf_T.size());
						}else{
							tf_T.put(tempWord,tf_T.get(tempWord)+1);
						}
					}
					iterator = tf_T.keySet().iterator();
//					System.out.println(i+"th review of T");
				    while (iterator.hasNext()) {
				        String key = (String) iterator.next();
//				        System.out.print("key="+key);
//				        System.out.println("\tvalue="+idf.get(key));
//				        System.out.println("finish================");
//				        idf.put(key, Math.log(totalNumDocs/(1+idf.get(key)))/log2);
//				        System.out.print(key);
//				        System.out.println("\t"+tf_T.get(key)*idf_T.get(key));
				        if(!tfidf_T.containsKey(key)){
							tfidf_T.put(key, tf_T.get(key)*idf_T.get(key));
//							tt++;
//							System.out.println("S:\t"+tf_T.size());
						}else{
							if(tfidf_T.get(key)<tf_T.get(key)*idf_T.get(key)){
							tfidf_T.put(key,tf_T.get(key)*idf_T.get(key));
							}
						}
//				        tfidf_T.put(key, tf_T.get(key)*idf_T.get(key));
				    }
					
			    }else
			    	if(tempArray[1].equals("F")){
			    		
			    		for(int i3=0;i3<docByWord.size();i3++){
							tempWord=docByWord.get(i3);
							if(!tf_F.containsKey(tempWord)){
								tf_F.put(tempWord, 1.0);
//								System.out.println("fS:\t"+tf_F.size());
//								ff++;
							}else{
								tf_F.put(tempWord,tf_F.get(tempWord)+1);
							}
						}
			    		
			    		
			    		iterator = tf_F.keySet().iterator();
//						System.out.println(i+"th review of F");
					    while (iterator.hasNext()) {
					        String key = (String) iterator.next();
//					        System.out.print("key="+key);
//					        System.out.println("\tvalue="+idf.get(key));
//					        System.out.println("finish================");
//					        idf.put(key, Math.log(totalNumDocs/(1+idf.get(key)))/log2);
//					        System.out.print(key);
//					        System.out.println("\t"+tf_F.get(key)*idf_F.get(key));
					        if(!tfidf_F.containsKey(key)){
								tfidf_F.put(key, tf_F.get(key)*idf_F.get(key));
//								tt++;
//								System.out.println("S:\t"+tf_T.size());
							}else{
								if(tfidf_F.get(key)<tf_F.get(key)*idf_F.get(key)){
								tfidf_F.put(key,tf_F.get(key)*idf_F.get(key));
								}
							}
//					        tfidf_F.put(key, tf_F.get(key)*idf_F.get(key));
					    }
			    	}
					
					iterator = tfidf_T.keySet().iterator();
//					System.out.println(i+"th review of T");
				    while (iterator.hasNext()) {
				        String key = (String) iterator.next();
//				 System.out.println(key+"\t"+tfidf_T.get(key));
				        
				    }
				    iterator = tfidf_F.keySet().iterator();
//					System.out.println(i+"th review of F");
				    while (iterator.hasNext()) {
				        String key = (String) iterator.next();
//				 System.out.println(key+"\t"+tfidf_F.get(key));
				        
				    }
			    	docByWord.clear();
			    }
			    
			    System.out.println("tfidf_T====");
			    iterator = tfidf_T.keySet().iterator();
			    while (iterator.hasNext()) {
			    	String key = (String) iterator.next();
			    	System.out.print(key);
			        System.out.println("\t"+tfidf_T.get(key));
			    }
			    System.out.println("tfidf_F====");
			    iterator = tfidf_F.keySet().iterator();
			    while (iterator.hasNext()) {
			    	String key = (String) iterator.next();
			    	System.out.print(key);
			        System.out.println("\t"+tfidf_F.get(key));
			    }

			  
			    /*calculating score1
			     * Sum of tfidf / num of words for F and T each*/
			    br = new BufferedReader(new FileReader(testingFile));
			    for(int i=0;(line = br.readLine()) != null;i++) {
			    	Score_T=0;
					Score_F=0;
			    	tempArray = line.split(cvsSplitBy);
			    	String Answer=tempArray[1];
	
			    	
			    	tempArray = line.split(cvsSplitBy);
			    	
			    	temSent=tempArray[4].toLowerCase().replace("\"", "").replace("?", "").replace("!", "").replace(".", "").replace("|", "").replace("(", "").replace(")", "").replace("'s", " 's").split(" ");
			    	/**senetence to AL for each word it has**/
					
					for(int i2=0;i2<temSent.length;i2++){
						if(!temSent[i2].equals("")){
					docByWord.add(temSent[i2]);
						}
					}
					/**Calculating Score_T and Score_F
					 * (Sigma tf-idf)/ # of words in review
					 * **/
					
					double Tcnt=1.0;
					double Fcnt=1.0;
					for(int i3=0;i3<docByWord.size();i3++){
						tempWord=docByWord.get(i3);
//						if(tfidf_T.containsKey(tempWord)&&(tfidf_T.get(tempWord)>0)){
						if(tfidf_T.containsKey(tempWord)){
						Score_T+=tfidf_T.get(tempWord);
//						}else if(tfidf_F.containsKey(tempWord)&&(tfidf_F.get(tempWord)>0)){
						Tcnt++;
						}else if(tfidf_F.containsKey(tempWord)){
							Score_F += tfidf_F.get(tempWord);
							Fcnt++;
						}
//						Score_T/=Math.sqrt(Tcnt);
//						Score_F/=Math.sqrt(Fcnt);
						Score_T/=(Tcnt);
						Score_F/=Fcnt;
					}
					
//			labe	 true  false
//					
//			positive	ra   rb
//					
//			negative	rc   rd
					
					System.out.println("ScoreT: "+Score_T+"\tScoreF: "+Score_F);
					
					System.out.print("Answer: "+Answer+"\t");
					
					if (Score_T > Score_F) {
						
						if (Answer.equals("T")) {
							System.out.println("correct");
							ra++;
						} else {
							System.out.println("Incorrect");
							rc++;
						}
					} else {
						if (Answer.equals("F")) {
							System.out.println("correct");
							rd++;
						} else {
							System.out.println("Incorrect");
							rb++;
						}
					}
					
			    
			    	docByWord.clear();
			    }
			    System.out.println("ra rb rc rd\t"+ra+"\t"+rb+"\t"+rc+"\t"+rd);
			    Accuracy=(ra+rd)/(ra+rb+rc+rd);
			    System.out.println(categories+"\tAccuracy: "+Accuracy);
			    output+=Accuracy+"\t";
			    Average+=Accuracy;
			    br.close();
			    
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		}
		Average/=5;
		 System.out.println(categories+"\t"+output+Average);
	}
}