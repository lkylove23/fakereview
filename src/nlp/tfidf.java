package nlp;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale.Category;

public class tfidf {

	public static void main(String[] args) throws Exception {
		
		String where="D:\\FakeReviewDetectionData";
//		String categories ="hotels";
//		String categories ="restaurants";
		String categories ="electronics";
//		String categories ="fashion";
//		String categories ="hospitals";
//		String categories ="musicvenues";
//		String categories ="insurance";
		String trainingFold="fold2,3,4,5";
		
		String TorF="T";
//		String TorF="F";
		
		/** document를 ArrayList로 만들고 document안의 단어 **/
//		String trainingFile=where+"\\Yelp\\"+categories+"\\BalancedForLDA\\"+categories+"_"+trainingFold+".csv";
		String trainingFile=where+"\\Yelp\\forCrossEvaluation\\for_"+categories+".csv";
		// read csv for test data
		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ",";
		/**tf per doucument**/
		HashMap<String,Double> tf= new HashMap<String,Double>();
		HashMap<String,Double> idf=new HashMap<String,Double>();
		double totalNumDocs=0.0;
		double log2=Math.log(2);
		String[] tempArray;
		String[] temSent;
		String tempWord="";
		ArrayList<String> docByWord= new ArrayList<String>();
		try {
			br = new BufferedReader(new FileReader(trainingFile));

				for(int i=0;(line = br.readLine()) != null;i++) {
					
				// use comma as separator
				tempArray = line.split(cvsSplitBy);
				if(tempArray[1].equals(TorF)){
				/**calculate idf first**/
				temSent=tempArray[4].toLowerCase().replace("\"", "").replace("?", "").replace("!", "").replace(".", "").replace("|", "").replace("(", "").replace(")", "").replace("'s", " 's").split(" ");
				/**senetence to AL for each word it has**/
				
				for(int i2=0;i2<temSent.length;i2++){
					if(!temSent[i2].equals("")){
						if(!docByWord.contains("temSent[i2]")){
							//just appearance
						
				docByWord.add(temSent[i2]);
						}
					}
				}
				
				for(int i3=0;i3<docByWord.size();i3++){
					tempWord=docByWord.get(i3);
					if(!idf.containsKey(tempWord)){
						idf.put(tempWord, 1.0);
					}else{
						idf.put(tempWord,idf.get(tempWord)+1);
					}
				}
				totalNumDocs++;
				}
				docByWord.clear();
			}
//				System.out.println(totalNumDocs);
				/**idf= num of D/idf value**/
				Iterator<String> iterator = idf.keySet().iterator();
			    while (iterator.hasNext()) {
			        String key = (String) iterator.next();
//			        System.out.print("key="+key);
//			        System.out.println("\tvalue="+idf.get(key));
//			        System.out.println("finish================");
			        idf.put(key, Math.log(totalNumDocs/(1+idf.get(key)))/log2);
//			        System.out.print(key);
//			        System.out.println("\t"+idf.get(key));
			    }
			    docByWord.clear();
			    /**Calcualting tf**/
			    br = new BufferedReader(new FileReader(trainingFile));
			    for(int i=0;(line = br.readLine()) != null;i++) {
			    	tempArray = line.split(cvsSplitBy);
			    	
			    	if(tempArray[1].equals(TorF)){
			    	tf.clear();
			    	tempArray = line.split(cvsSplitBy);
			    	
			    	temSent=tempArray[4].toLowerCase().replace("\"", "").replace("?", "").replace("!", "").replace(".", "").replace("|", "").replace("(", "").replace(")", "").replace("'s", " 's").split(" ");
			    	/**senetence to AL for each word it has**/
					
					for(int i2=0;i2<temSent.length;i2++){
						if(!temSent[i2].equals("")){
					docByWord.add(temSent[i2]);
						}
					}
					
					for(int i3=0;i3<docByWord.size();i3++){
						tempWord=docByWord.get(i3);
						if(!tf.containsKey(tempWord)){
							tf.put(tempWord, 1.0);
						}else{
							tf.put(tempWord,tf.get(tempWord)+1);
						}
					}
					iterator = tf.keySet().iterator();
					System.out.println(i+"th review of "+TorF);
				    while (iterator.hasNext()) {
				        String key = (String) iterator.next();
//				        System.out.print("key="+key);
//				        System.out.println("\tvalue="+idf.get(key));
//				        System.out.println("finish================");
//				        idf.put(key, Math.log(totalNumDocs/(1+idf.get(key)))/log2);
				        System.out.print(key);
				        System.out.println("\t"+tf.get(key)*idf.get(key));
				    }
					
			    }
			    	docByWord.clear();
			    }
			    br.close();
			    
			     
			    
			    
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
	}
}