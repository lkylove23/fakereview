import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;

public class LDAWeightingAllcate_w5 {

    public static void main(String[] args) throws Exception {
        String porter="_porter";
//        String mode="w5_100";
        /**w1: capital theta
         * w2: df
         * w3: w1*w2
         * w4: (w1+w2)
         *                   **/
        //  1/topic
        /**Choose**/
        //KY
        String where = "D:\\";
        //or Dear IRbig2?
//		String where="D:\\KY\\";

        double tau = 0.0;

        int classChange_Train = 0; //classChange ��° doc ���� �ٸ� class
        int classChange_Test = 0;
        int topicNum = 0;
//        int max_iter=10;
        int max_iter = 400;
        double highAccuracy = 0.0;
        ArrayList<Integer> highTopic_F = new ArrayList<Integer>();
        ArrayList<Integer> highTopic_T = new ArrayList<Integer>();
        double ave;
        double aveOfAve = 0.0;
        String categories = "";


//			changeID=115;

        String trainingLDAFile = "";
        String testingLDAFile = "";
//		if(args.length!=0){
//			if(args.length!=6){
//				System.out.println("You have to type 5 argument\n # of topics\ttraining file\t# of fake reviews in training file\ttest file\tresult file\tvalue of tau");
//			}
//			numTopics = Integer.parseInt(args[0]);
//			trainingFile=args[1];
//			classChange_Train=Integer.parseInt(args[2]);
//			testingFile=args[3];
//			outDirPath=args[4];
//			tau=Double.parseDouble(args[5]);
//
//		}else{

        StringBuffer Total_ResultOut_folds = new StringBuffer();

        ArrayList<Integer> listOfTopicNum = new ArrayList<Integer>();
//        listOfTopicNum.add(2);
//        listOfTopicNum.add(10);
//        listOfTopicNum.add(50);
//        listOfTopicNum.add(100);
//        listOfTopicNum.add(200);
        listOfTopicNum.add(300);
//        listOfTopicNum.add(500);
//        listOfTopicNum.add(1000);

        int maxdocnum;
        int maxtopicnum;
        int doc_i;
        int topic_i;
        long startTime = 0;
        long endTime;
//        double constance=1.0; // w4
        double constance = 0.2;
        //////
        int changeID = 0;



                int totalNumOfReview = 0;
//        for(int c_it=0;c_it<7;c_it++) {
                for (int c_it = 6; c_it < 7; c_it++) {
//        for(int c_it=0;c_it<3;c_it++) {
                    if (c_it == 0) {
                        if (totalNumOfReview > 440) {
                            continue;
                        }
                        //Electronics
//                changeID = 44;
                        categories = "electronics";
                    } else if (c_it == 1) {
                        if (totalNumOfReview > 26900) {
                            continue;
                        }
                        //Fashion
                        categories = "fashion";
                    } else if (c_it == 2) {
                        if (totalNumOfReview >= 200) {
                            continue;
                        }
                        //hospitals
                        categories = "hospitals";
                    } else if (c_it == 3) {
                        if (totalNumOfReview > 11000) {
                            continue;
                        }
                        //hotels
                        categories = "hotels";

                    } else if (c_it == 4) {
                        if (totalNumOfReview >= 130) {
                            continue;
                        }
                        //Insurance
                        categories = "insurance";
                    } else if (c_it == 5) {
                        if (totalNumOfReview > 1190) {
                            continue;
                        }
                        //Musicvenues
                        categories = "musicvenues";
                    } else if (c_it == 6) {
                        if (totalNumOfReview > 37980) {
                            continue;
                        }
                        //Restaurant
//                totalNumOfReview = 37990;
                        categories = "restaurants";
                    }
//            System
                    String File_helper = where + "FakeReviewDetectionData\\Yelp\\"+categories+"\\";
//            String resultFile_helper_setting = where + "FakeReviewDetectionData\\result\\Yelp\\"+categories+"\\";
//            String TotalResultFile= resultFile_helper_setting+"result_total.txt";
//            File  forCheckCategory = new File(resultFile_helper_setting);
//            if (!forCheckCategory.isDirectory()) {
//                forCheckCategory.mkdir();
//            }
//            for (doc_i = 0, maxdocnum = listOfDocNum.size(); doc_i < maxdocnum; doc_i++) {
                    classChange_Test = totalNumOfReview / 10;        // the # of fake reviews
                    classChange_Train = classChange_Test * 4;

//                String classificationResultFile_check = resultFile_helper_setting + categories + "_" + totalNumOfReview + s_itr;
//                forCheckCategory = new File(classificationResultFile_check);
//                if (!forCheckCategory.isDirectory()) {
//                    forCheckCategory.mkdir();
//                }
                    for (topic_i = 0, maxtopicnum = listOfTopicNum.size(); topic_i < maxtopicnum; topic_i++) {
                        topicNum = listOfTopicNum.get(topic_i);
                        int numOfFinD = topicNum; // num of features in the document
                        tau = 1 / (double) topicNum;
                        String test = "";
                        String train = "";

                        double[] highest_accuracy_folds = new double[5];
                        double highest_avg = 0.0;
                        double tmp_avg = 0.0;
                        double[] accuracy_folds = new double[5];
                        double[] precision_F_folds = new double[5];
                        double[] precision_T_folds = new double[5];
                        double[] recall_F_folds = new double[5];
                        double[] recall_T_folds = new double[5];
                        double[] F1_F_folds = new double[5];
                        double[] F1_T_folds = new double[5];
//                double[] highest_accuracy_folds = new double[5];
                        double[] highest_precision_F_folds = new double[5];
                        double[] highest_precision_T_folds = new double[5];
                        double[] highest_recall_F_folds = new double[5];
                        double[] highest_recall_T_folds = new double[5];
                        double[] highest_F1_F_folds = new double[5];
                        double[] highest_F1_T_folds = new double[5];
                        StringBuffer ResultOut_folds = new StringBuffer();
                        StringBuffer[] High_DocResultOut = new StringBuffer[5];
                        int cal_itr = 0;

                        double[] highfeatureSelection = new double[topicNum];
//                    String featureSelectionResultFile = resultFile_helper_setting  + categories + "_" + totalNumOfReview + "_lda" + topicNum + "_FSresult.txt";
//                    StringBuffer FSResultOut = new StringBuffer();
                        int high_cal_itr = 0;
//                    String classificationResultFile = resultFile_helper_setting  + categories + "_" + totalNumOfReview + "_lda" + topicNum + "_result.txt";
                        startTime = System.currentTimeMillis();
                        String line = "";
//                while (cal_itr < 25000) {
                        /**inner CV**/

                        StringBuffer[] DocResultOut = new StringBuffer[5];
                        int tmp_it = -1;

                        for (int it = 0; it < 5; it++) {
                            if (it == 0) {
                                test = "fold1";
                                train = "fold2,3,4,5";
                            } else if (it == 1) {
                                test = "fold2";
                                train = "fold1,3,4,5";
                            } else if (it == 2) {
                                test = "fold3";
                                train = "fold1,2,4,5";
                            } else if (it == 3) {
                                test = "fold4";
                                train = "fold1,2,3,5";
                            } else if (it == 4) {
                                test = "fold5";
                                train = "fold1,2,3,4";
                            }
                            if (it != tmp_it) {
                                trainingLDAFile = File_helper +  "BalancedForLDA\\" + categories + "_" + train + "_lda" + topicNum +porter+ ".txt";
                                testingLDAFile  = File_helper +  "BalancedForLDA\\" + categories + "_" + test + "_lda" + topicNum +porter+ ".txt";
                                tmp_it = it;
                            }

                            String[] temp;
                            BufferedReader br = new BufferedReader(new FileReader(trainingLDAFile));
                            double[] w1_F = new double[topicNum];
                            double[] w1_T = new double[topicNum];
                            double[] cnt_F = new double[topicNum];
                            double[] cnt_T = new double[topicNum];
                            for (int i = 0; i < topicNum; i++) {
                                cnt_F[i] = 0.0;
                                cnt_T[i] = 0.0;
                            }
                            while ((line = br.readLine()) != null) {
                                temp = line.split(" ");
                                if (temp[0].equals("-1")) {
                                    for (int ii = 0; ii < numOfFinD; ii++) {
                                        String[] temp2 = temp[ii + 1].split(":");
                                        double tmpProb = Double.parseDouble(temp2[1]);
                                        w1_F[ii] += tmpProb; //temp[ ] should be "value"
                                        if (tmpProb > tau) {
                                            cnt_F[ii]++;
                                        }
                                    }
                                } else {
                                    for (int ii = 0; ii < numOfFinD; ii++) {
                                        String[] temp2 = temp[ii + 1].split(":");
                                        double tmpProb = Double.parseDouble(temp2[1]);
                                        w1_T[ii] += tmpProb; //temp[ ] should be "value"
                                        if (tmpProb > tau) {
                                            cnt_T[ii]++;
                                        }
                                    }
                                }
                            }


                            // Calculating w_1 in train
                            for (int i = 0; i < topicNum; i++) {
                                double tmp = w1_F[i] + w1_T[i];
                                w1_F[i] /= tmp;
                                w1_T[i] /= tmp;
                            }
                            // Calculating w_2 in inner
                            double[] w2_F = new double[topicNum];
                            double[] w2_T = new double[topicNum];
                            for (int j = 0; j < topicNum; j++) {
                                w2_F[j] = Math.log10((cnt_F[j] + 1.0) / (cnt_T[j] + 1.0));
                                w2_T[j] = Math.log10((cnt_T[j] + 1.0) / (cnt_F[j] + 1.0));
                            }

//                            DocResultOut[it] = new StringBuffer();
                            highAccuracy = 0;

                            double tp = 0.0;
                            double fp = 0.0;
                            double tn = 0.0;
                            double fn = 0.0;
                            double precision = 0.0;
                            double recall = 0.0;
                            double F1 = 0.0;
                            double accuracy = 0.0;

                            /**Weighting on Trainfile**/
                            br = new BufferedReader(new FileReader(trainingLDAFile));
                            StringBuffer Total_temp_train = new StringBuffer();
                            while ((line = br.readLine()) != null) {
                                double[] LDA_Train = new double[topicNum];
                                temp = line.split(" ");
                                int testtt = 0;
                                StringBuffer Ftemp = new StringBuffer();
                                StringBuffer Ttemp = new StringBuffer();
                                Ftemp.append(temp[0]);

                                for (int ii = 0; ii < topicNum; ii++) {
                                    String[] temp2 = temp[ii + 1].split(":");
//                                    F_Score += Double.parseDouble(temp[2 + ii]) * globalTheta_F[ii] * SelectedFeature[ii];
//                                    T_Score += Double.parseDouble(temp[2 + ii]) * globalTheta_T[ii] * SelectedFeature[ii];
//                                    F_Score +=w2_F[ii] * Double.parseDouble(temp[ii + 2])*SelectedFeature[ii];
//                                    T_Score += w2_T[ii] * Double.parseDouble(temp[ii + 2])*SelectedFeature[ii];
//                                    System.out.println("w1 f: "+w1_F[ii]+"w2 t: "+w1_T[ii]);
//                                    System.out.println("w2 f: "+w2_F[ii]+"w2 t: "+w2_T[ii]);
//                                    System.out.println();
                                    Ftemp.append(" ");
                                    Ftemp.append(ii + 1);
                                    Ftemp.append(":");
                                    Ftemp.append((w1_F[ii] + constance * w2_F[ii]) * Double.parseDouble(temp2[1]));
                                    Ttemp.append(" ");
                                    Ttemp.append(ii +topicNum+ 1);
                                    Ttemp.append(":");
                                    Ttemp.append((w1_T[ii] + constance * w2_T[ii]) * Double.parseDouble(temp2[1]));
                                }

                                Total_temp_train.append(Ftemp.toString());
                                Total_temp_train.append(Ttemp.toString());
                                Total_temp_train.append("\n");


                            }
                            String outputTrainFile  = File_helper +  "BalancedForLDA\\" + categories + "_" + train + "_lda" + topicNum+"_w5" +porter+ ".txt";
                            FileWriter fw = new FileWriter(outputTrainFile);
                            fw.append(Total_temp_train.toString().trim());
                            fw.flush();
                            fw.close();

                            /**Weighting on Testfile**/
                            br = new BufferedReader(new FileReader(testingLDAFile));
//                            int[] cnt_F = new int[topicNum];
//                            int[] cnt_T = new int[topicNum];

//                ArrayList<Double> LDA_Train = new ArrayList();
                            StringBuffer Total_temp = new StringBuffer();
                            while ((line = br.readLine()) != null) {
                                double[] LDA_Train = new double[topicNum];
                                temp = line.split(" ");
                                int testtt = 0;
                                StringBuffer Ftemp = new StringBuffer();
                                StringBuffer Ttemp = new StringBuffer();
                                Ftemp.append(temp[0]);

                                for (int ii = 0; ii < topicNum; ii++) {
                                    String[] temp2 = temp[ii + 1].split(":");
//                                    F_Score += Double.parseDouble(temp[2 + ii]) * globalTheta_F[ii] * SelectedFeature[ii];
//                                    T_Score += Double.parseDouble(temp[2 + ii]) * globalTheta_T[ii] * SelectedFeature[ii];
//                                    F_Score +=w2_F[ii] * Double.parseDouble(temp[ii + 2])*SelectedFeature[ii];
//                                    T_Score += w2_T[ii] * Double.parseDouble(temp[ii + 2])*SelectedFeature[ii];
//                                    System.out.println("w1 f: "+w1_F[ii]+"w2 t: "+w1_T[ii]);
//                                    System.out.println("w2 f: "+w2_F[ii]+"w2 t: "+w2_T[ii]);
//                                    System.out.println();
                                    Ftemp.append(" ");
                                    Ftemp.append(ii + 1);
                                    Ftemp.append(":");
                                    Ftemp.append((w1_F[ii] + constance * w2_F[ii]) * Double.parseDouble(temp2[1]));
                                    Ttemp.append(" ");
                                    Ttemp.append(ii + 1+topicNum);
                                    Ttemp.append(":");
                                    Ttemp.append((w1_T[ii] + constance * w2_T[ii]) * Double.parseDouble(temp2[1]));
                                }

                                Total_temp.append(Ftemp.toString());
                                Total_temp.append(Ttemp.toString());
                                Total_temp.append("\n");
                            }
                            String outputTestFile= File_helper +  "BalancedForLDA\\" + categories + "_" + test + "_lda" + topicNum+"_w5" +porter+ ".txt";
                            fw = new FileWriter(outputTestFile);
                            fw.append(Total_temp.toString().trim());
                            fw.flush();
                            fw.close();
                        }

                    }
                }
            }
        }
