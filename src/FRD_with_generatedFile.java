import cc.mallet.pipe.*;
import cc.mallet.pipe.iterator.CsvIterator;
import cc.mallet.topics.ParallelTopicModel;
import cc.mallet.topics.TopicInferencer;
import cc.mallet.types.*;

import java.io.*;
import java.util.*;
import java.util.regex.Pattern;

public class FRD_with_generatedFile {
/**Iteration fitting **/
	public static void main(String[] args) throws Exception {
        File path = new File("");
        /**Choose**/
        //KY
        String where="D:\\";
        //or Dear IRbig2?
//		String where="D:\\KY\\";
        double wt=1.0;		double wn=1.0; 		double wf=1.0;
        double threshold =0.35;
        ArrayList<Double> param_tw= new ArrayList<Double>();
        double highestTotal=0.0;
        double tmp_highestTotal=0.0;
        double[] highest_category;
        highest_category = new double[7];
        double[] tmp_highest_category;
        tmp_highest_category = new double[7];
        double[] bestTau= new double[5];

        double[][] highest_category_fold;
        highest_category_fold=new double[7][5];
        double temphighest=0.0;
        String str="";
        double tau =0.0;

        int cnt=0;
		int classChange_Train = 0; //classChange ��° doc ���� �ٸ� class
		int classChange_Test =0;
		int topicNum = 100;

		double highAccuracy=0.0;
		double highTau=0.0;
		String outDirPath="";
		ArrayList<Integer> highTopic_F =new ArrayList<Integer>();
		ArrayList<Integer> highTopic_T =new ArrayList<Integer>();


		
		// Begin by importing documents from text to feature sequences
		ArrayList<Pipe> pipeListForTraining = new ArrayList<Pipe>();
		//  (TrainingFile,classChange)
		// Pipes: lowercase, tokenize, remove stopwords, map to features
		pipeListForTraining.add( new CharSequenceLowercase() );
		pipeListForTraining.add( new CharSequence2TokenSequence(Pattern.compile("\\p{L}[\\p{L}\\p{P}]+\\p{L}")) );
//		pipeListForTraining.add( new TokenSequenceRemoveStopwords(new File("stoplists/en.txt"), "UTF-8", false, false, false) );
		pipeListForTraining.add( new TokenSequenceRemoveStopwords(new File("D:\\workspace\\FRD\\fakereview\\stoplists\\en.txt"), "UTF-8", false, false, false) );
		pipeListForTraining.add( new TokenSequence2FeatureSequence() );

        double ave;
        double aveOfAve=0.0;
        int changeID=0; // the # of fake reviews
        String categories ="";


//  	int numOfB=25;
        int numOfB=50;

//	    int RPB_half =1;
//	    int RPB_half =2;
//	    int RPB_half =5;
        int RPB_half =10;
//	    int RPB_half =15;
//	    int RPB_half =20;
//	    int RPB_half =25;
	    String s_itr="";
//	    String s_itr=" (2)";
//        String s_itr=" (3)";
//	    String s_itr=" (4)";
//	    String s_itr=" (5)";

        int c_it= 0;
//		for(int c_it=0;c_it<7;c_it++){
        if(c_it==0){
            //Electronics
            changeID=44;
            categories ="electronics";
        }else if(c_it==1){
            //Fashion
            changeID=269;
            categories ="fashion";
        }else if(c_it==2){
            //hotels
            changeID=110;
            categories ="hotels";
        }else if(c_it==3){
            //hospitals
            changeID=20;
            categories ="hospitals";
        }else if(c_it==4){
            //Insurance
            changeID=13;
            categories ="insurance";
        }else if(c_it==5){
            //Musicvenues
            changeID=119;
            categories ="musicvenues";
        }else if(c_it==6){
            //Restaurant

            changeID=RPB_half*numOfB/5;
//			changeID=115;
            categories ="restaurants";
        }
        /** Sampling Part**/
        String forResult =where+"FakeReviewDetectionData\\result\\topic"+topicNum+"\\itr1500\\yelp\\RPB";

        File forCheckCategory = new File(forResult);
        if(!forCheckCategory.isDirectory()){
            forCheckCategory.mkdir();
        }

		InstanceList trainingInstances = new InstanceList (new SerialPipes(pipeListForTraining));

//		Reader fileReader = new InputStreamReader(new FileInputStream(new File(args[0])), "UTF-8");
//		Reader fileReader = new InputStreamReader(new FileInputStream(new File("E:\\forma\\ap.txt")), "UTF-8");
//		Reader fileReader = new InputStreamReader(new FileInputStream(new File("E:\\FakeReviewDetectionData\\DefaultTraining.txt")), "UTF-8");
		String trainingFile="";
		String testingFile="";
		String trainingLDAFile="";
		String testingLDAFile="";
//		if(args.length!=0){
//			if(args.length!=6){
//				System.out.println("You have to type 5 argument\n # of topics\ttraining file\t# of fake reviews in training file\ttest file\tresult file\tvalue of tau");
//			}
//			numTopics = Integer.parseInt(args[0]);
//			trainingFile=args[1];
//			classChange_Train=Integer.parseInt(args[2]);
//			testingFile=args[3];
//			outDirPath=args[4];
//			tau=Double.parseDouble(args[5]);
//
//		}else{
        String test ="";
        String train= "";
        for(int it=0;it<5;it++) {
            if (it == 0) {
                test = "fold1";
                train = "fold2,3,4,5";
            } else if (it == 1) {
                test = "fold2";
                train = "fold1,3,4,5";
            } else if (it == 2) {
                test = "fold3";
                train = "fold1,2,4,5";
            } else if (it == 3) {
                test = "fold4";
                train = "fold1,2,3,5";
            } else if (it == 4) {
                test = "fold5";
                train = "fold1,2,3,4";
            }

            classChange_Train = changeID * 4;
            trainingFile = where + "FakeReviewDetectionData\\Yelp\\" + categories + "\\BalancedForLDA\\"+categories+"_"+train+".txt";
            testingFile = where + "FakeReviewDetectionData\\Yelp\\" + categories + "\\BalancedForLDA\\"+categories+"_"+test+".txt";
            trainingLDAFile = where + "FakeReviewDetectionData\\Yelp\\" + categories + "\\BalancedForLDA\\"+categories+"_"+train+"_lda.txt";
            testingLDAFile = where + "FakeReviewDetectionData\\Yelp\\" + categories + "\\BalancedForLDA\\"+categories+"_"+test+"_lda.txt";
//			tau=0.0014812492527767063;
            tau = 0.0;

            File f = new File(trainingFile);
            String line ="";

            if (!f.isFile()) {
                try {
                    BufferedReader br = new BufferedReader(new FileReader(where + "FakeReviewDetectionData\\Yelp\\" + categories + "\\BalancedForLDA\\" + categories + "_" + train + ".csv"));
                    String txtHelper="";
                    String label= "";
                    String txtContents="";
                    for (int i = 0; (line = br.readLine()) != null; i++) {
                        // use comma as separator
                        String[] tempArray = line.split(",");
                        if(i<classChange_Train){
                            label="f";
                        }else{
                            label="t";
                        }

                        txtContents = tempArray[4].replace("\n", " ").replace("?"," ").replace("|",",").replace("\""," ");
                        txtHelper+=i+"\t"+label+"\t"+txtContents+"\n";
                    }
                    FileWriter writer = new FileWriter(trainingFile,true);
                    writer.append(txtHelper);
                    writer.flush();
                    writer.close();
//                    outDirPath = "C:/result.txt";
                } catch(IOException e)
                {
                    e.printStackTrace();
                }
            }

            Reader fileReader = new InputStreamReader(new FileInputStream(new File(trainingFile)), "UTF-8");

            ///////////
            trainingInstances.addThruPipe(new CsvIterator(fileReader, Pattern.compile("^(\\S*)[\\s,]*(\\S*)[\\s,]*(.*)$"),
                    3, 2, 1)); // data, label, name fields

            // Create a model with 100 topics, alpha_t = 0.01, beta_w = 0.01
            //  Note that the first parameter is passed as the sum over topics, while
            //  the second is

            ParallelTopicModel model = new ParallelTopicModel(topicNum, 1.0, 0.01);

            model.addInstances(trainingInstances);

            // Use two parallel samplers, which each look at one half the corpus and combine
            //  statistics after every iteration.
            model.setNumThreads(2);

            // Run the model for 50 iterations and stop (this is for testing only,
            //  for real applications, use 1000 to 2000 iterations)
            model.setNumIterations(1500);
            model.estimate();

            // Show the words and topics in the first instance

            // The data alphabet maps word IDs to strings
            Alphabet dataAlphabet = trainingInstances.getDataAlphabet();

            FeatureSequence tokens = (FeatureSequence) model.getData().get(0).instance.getData();
            LabelSequence topics = model.getData().get(0).topicSequence;

            Formatter out = new Formatter(new StringBuilder(), Locale.US);
            for (int position = 0; position < tokens.getLength(); position++) {
                out.format("%s-%d ", dataAlphabet.lookupObject(tokens.getIndexAtPosition(position)), topics.getIndexAtPosition(position));
            }
//		System.out.println(out);

            // Estimate the topic distribution of the first instance,
            //  given the current Gibbs state.
            double[] topicDistribution = model.getTopicProbabilities(0); // �̰� �̿��ϱ�


            // Get an array of sorted sets of word ID/count pairs
            ArrayList<TreeSet<IDSorter>> topicSortedWords = model.getSortedWords();

            // Show top 5 words in topics with proportions for the first document
            for (int topic = 0; topic < topicNum; topic++) {
                Iterator<IDSorter> iterator = topicSortedWords.get(topic).iterator();

                out = new Formatter(new StringBuilder(), Locale.US);
                out.format("%d\t%.3f\t", topic, topicDistribution[topic]);
//			System.out.println("??\t"+out);
                int rank = 0;
                while (iterator.hasNext() && rank < 5) {
                    IDSorter idCountPair = iterator.next();
                    out.format("%s (%.0f) ", dataAlphabet.lookupObject(idCountPair.getID()), idCountPair.getWeight());
                    rank++;
                }
//			System.out.println(out);
            }

            // Create a new instance with high probability of topic 0
            StringBuilder topicZeroText = new StringBuilder();
            Iterator<IDSorter> iterator = topicSortedWords.get(0).iterator();

            int rank = 0;
            while (iterator.hasNext() && rank < 5) {
                IDSorter idCountPair = iterator.next();
                topicZeroText.append(dataAlphabet.lookupObject(idCountPair.getID()) + " ");
                rank++;
            }

            // Create a new instance named "test instance" with empty target and source fields.
//		InstanceList testing = new InstanceList(traningInstances.getPipe());
//		testing.addThruPipe(new Instance(topicZeroText.toString(), null, "test instance", null));
//
//		TopicInferencer inferencer = model.getInferencer();
//		double[] testProbabilities = inferencer.getSampledDistribution(testing.get(0), 10, 1, 5);
            /**Calculation for Weight**/


            /**Inference on New document**/
              System.out.println("check");
            ArrayList<Pipe> pipeListForTest = new ArrayList<Pipe>();
            //  (TrainingFile,classChange)
            // Pipes: lowercase, tokenize, remove stopwords, map to features
            pipeListForTest.add(new CharSequenceLowercase());
            pipeListForTest.add(new CharSequence2TokenSequence(Pattern.compile("\\p{L}[\\p{L}\\p{P}]+\\p{L}")));
//            pipeListForTest.add(new TokenSequenceRemoveStopwords(new File("stoplists/en.txt"), "UTF-8", false, false, false));
            pipeListForTest.add(new TokenSequenceRemoveStopwords(new File("D:\\workspace\\FRD\\fakereview\\stoplists\\en.txt"), "UTF-8", false, false, false));
            pipeListForTest.add(new TokenSequence2FeatureSequence());
            InstanceList testingInstances = new InstanceList(new SerialPipes(pipeListForTest));
//////////////
            f = new File(testingFile);
            if (!f.isFile()) {
                try {
                    BufferedReader br = new BufferedReader(new FileReader(where + "FakeReviewDetectionData\\Yelp\\" + categories + "\\BalancedForLDA\\" + categories + "_" + test + ".csv"));
                    String txtHelper="";
                    String label= "";
                    String txtContents="";
                    for (int i = 0; (line = br.readLine()) != null; i++) {
                        // use comma as separator
                        String[] tempArray = line.split(",");
                        if(i<changeID){
                            label="f";
                        }else{
                            label="t";
                        }

                        txtContents = tempArray[4].replace("\n", " ").replace("?"," ").replace("|",",");
                        txtHelper+=i+"\t"+label+"\t"+txtContents+"\n";
                    }
                    FileWriter writer = new FileWriter(testingFile,true);
                    writer.append(txtHelper);
                    writer.flush();
                    writer.close();
//                    outDirPath = "C:/result.txt";
                } catch(IOException e)
                {
                    e.printStackTrace();
                }
            }
        System.out.println("check 2");



//		Reader fileReader = new InputStreamReader(new FileInputStream(new File(args[0])), "UTF-8");
            fileReader = new InputStreamReader(new FileInputStream(new File(testingFile)), "UTF-8");
//		fileReader = new InputStreamReader(new FileInputStream(new File("E:\\FakeReviewDetectionData\\DefaultTraining.txt")), "UTF-8");
            testingInstances.addThruPipe(new CsvIterator(fileReader, Pattern.compile("^(\\S*)[\\s,]*(\\S*)[\\s,]*(.*)$"),
                    3, 2, 1)); // data, label, name fields
            TopicInferencer inferencer = InferenceModel(testingFile, model);
//		System.out.println("get 0: "+testingInstances.get(0));
//		System.out.println("get 1: "+testingInstances.get(1));
//		System.out.println("get 2: "+testingInstances.get(2));
            System.out.println("check 4");
            BufferedReader br = new BufferedReader(new FileReader(testingFile));

            String[] array;
            ArrayList<String> reviewContent = new ArrayList();
            while ((line = br.readLine()) != null) {
                array = line.split("\t");
                reviewContent.add(array[2]);
            }
            /**Score calculation**/
            System.out.println("check 5");
//		while(true){
//		tau=Math.random();
//		tau=0.0;
//		tau=0.0;
            double[] Score_F = new double[testingInstances.size()];
            double[] Score_T = new double[testingInstances.size()];
            double[] cnt_F = new double[model.numTopics];
            double[] cnt_T = new double[model.numTopics];
            String LDATrainout ="";
            for (int i = 0; i < model.getData().size(); i++) {

                double[] topicDistributionOfDoc = model.getTopicProbabilities(i); // �̰� �̿��ϱ�
                if(i<classChange_Train) {
                    //으야으우
                    LDATrainout+=i+"\tf";
                }else{
                    LDATrainout+=i+"\tt";
                }
                for (int jj = 0; jj < model.numTopics; jj++) {
                    LDATrainout+="\t"+topicDistributionOfDoc[jj];
                }
                LDATrainout+="\n";
                for (int j = 0; j < model.numTopics; j++) {
                    if (topicDistributionOfDoc[j] > tau) {
                        if(i<classChange_Train) {
                            cnt_F[j]++;
                        }else{
                            cnt_T[j]++;
                        }
                    }

                }


            }



            double[] w_F = new double[model.numTopics];
            double[] w_T = new double[model.numTopics];
            for (int j = 0; j < model.numTopics; j++) {
                w_F[j] = Math.log10((cnt_F[j] + 1) / (cnt_T[j] + 1));
                w_T[j] = Math.log10((cnt_T[j] + 1) / (cnt_F[j] + 1));
            }
            for (int i = 0; i < testingInstances.size(); i++) {
                double[] testProbabilities = inferencer.getSampledDistribution(testingInstances.get(i), 10, 1, 5);
                double Temp_F = 0.0;
                int Topic_F = 0;
                double Temp_T = 0.0;
                int Topic_T = 0;
                for (int j = 0; j < model.numTopics; j++) {

//			System.out.println("test p: "+testProbabilities[j]+"\tw_F: "+w_F[j]+"\tw_T: "+w_T[j]);
                    Score_F[i] += testProbabilities[j] * w_F[j];
                    Score_T[i] += testProbabilities[j] * w_T[j];
//			System.out.print(i+":: "+j+": prob: "+testProbabilities[j]+"\tcnt_F = "+cnt_F[j]+"\t");
//			System.out.print("cnt_T = "+cnt_T[j]+"\t");
//			System.out.print("w_F = "+w_F[j]+"\t");
//			System.out.print("w_T = "+w_T[j]);
//			System.out.println();
//			System.out.print("calculated Score_F= "+w_T[j]);
                    if (Temp_F < testProbabilities[j] * w_F[j]) {
                        Temp_F = testProbabilities[j] * w_F[j];
                        Topic_F = j;
                    }

                    if (Temp_T < testProbabilities[j] * w_T[j]) {
                        Temp_T = testProbabilities[j] * w_T[j];
                        Topic_T = j;
                    }
                }
//		System.out.println("F S: "+Score_F[i]+"\tT S: "+Score_T[i]);
//	
//		System.out.print(i+"::");
//		System.out.print("Score_F = "+Score_F[i]+"\t");
//		System.out.print("Score_T = "+Score_T[i]);
//		System.out.println();

//		
//		System.out.println("size of testprob. :"+testProbabilities.length);
////		System.out.println("0\t" + testProbabilities[0]);
//		System.out.println("distribution of 0 document" + testProbabilities[0]+" "+testProbabilities[1]+" "+testProbabilities[2]+" "+testProbabilities[3]+" "+testProbabilities[4]);
                highTopic_F.add(Topic_F);
                highTopic_T.add(Topic_T);
            }
            //output
            double tp = 0.0;
            double fp = 0.0;
            double tn = 0.0;
            double fn = 0.0;
            double precision = 0.0;
            double recall = 0.0;
            double F1 = 0.0;
            double accuracy = 0.0;

//		for(int p=0;p<testingInstances.size();p++){
//			if(p<classChange_Test){
//				if(Score_F[p]>Score_T[p]){
//					tn++;
//				}else{
//					fp++;
//				}
//			}else{
//				if(Score_F[p]>Score_T[p]){
//					fn++;
//				}else{
//					tp++;
//				}
//			}
//		}
            try {
                ////
                FileWriter fw = new FileWriter(outDirPath);
                BufferedWriter bw = new BufferedWriter(fw);
                for (int p = 0; p < testingInstances.size(); p++) {
                    System.out.println(reviewContent.get(p));
                    bw.write(reviewContent.get(p) + "\n");
//			System.out.println("F S: "+Score_F[p]+"\tT S: "+Score_T[p]);

                    int topic;
                    if (Score_F[p] > Score_T[p]) {
                        topic = highTopic_F.get(p);
                        System.out.println("=> Fake !");
                        bw.write("=> Fake !\n");
                    } else {
                        topic = highTopic_T.get(p);
                        System.out.println("=> Truthful !");
                        bw.write("=> Truthful !\n");
                    }
                    iterator = topicSortedWords.get(topic).iterator();

                    out = new Formatter(new StringBuilder(), Locale.US);
//						out.format("%d\t%.3f\t", topic, topicDistribution[topic]);
//					System.out.println("??\t"+out);
                    rank = 0;
                    while (iterator.hasNext() && rank < 5) {
                        IDSorter idCountPair = iterator.next();
                        out.format("%s ", dataAlphabet.lookupObject(idCountPair.getID()));
                        rank++;
                    }
                    System.out.println("The Topic words which have most persive influence:\t");
                    System.out.println("=> " + out);
                    bw.write("The Topic words which have most persive influence:\n");
                    bw.write("=> " + out + "\n");


                }

//		accuracy=(tn+tp)/(tn+tp+fn+fp);
//		precision
//		recall
//		if(highAccuracy<accuracy){
//		highAccuracy=accuracy;
//			System.out.println("Acc: " + highAccuracy+"\t tau: "+tau);
//		}
//	}
                bw.close();
            } catch (IOException e) {
                System.err.println(e);
                System.exit(1);
            }
        }
	}
	public ParallelTopicModel CreateModel(String trainingFile, int numTopics) throws IOException{
		// Begin by importing documents from text to feature sequences
		ArrayList<Pipe> pipeListForTraining = new ArrayList<Pipe>();
		//  (TrainingFile,classChange)
		// Pipes: lowercase, tokenize, remove stopwords, map to features
		pipeListForTraining.add( new CharSequenceLowercase() );
		pipeListForTraining.add( new CharSequence2TokenSequence(Pattern.compile("\\p{L}[\\p{L}\\p{P}]+\\p{L}")) );
		pipeListForTraining.add( new TokenSequenceRemoveStopwords(new File("stoplists/en.txt"), "UTF-8", false, false, false) );
		pipeListForTraining.add( new TokenSequence2FeatureSequence() );

		InstanceList traningInstances = new InstanceList (new SerialPipes(pipeListForTraining));
		
		Reader fileReader = new InputStreamReader(new FileInputStream(new File(trainingFile)), "UTF-8");
		traningInstances.addThruPipe(new CsvIterator (fileReader, Pattern.compile("^(\\S*)[\\s,]*(\\S*)[\\s,]*(.*)$"),
											   3, 2, 1)); // data, label, name fields
		ParallelTopicModel model = new ParallelTopicModel(numTopics, 1.0, 0.01);
		
		model.addInstances(traningInstances);

		// Use two parallel samplers, which each look at one half the corpus and combine
		//  statistics after every iteration.
		model.setNumThreads(2);

		// Run the model for 50 iterations and stop (this is for testing only, 
		//  for real applications, use 1000 to 2000 iterations)
		model.setNumIterations(1500);
		model.estimate();
		return model;
	}
	public static TopicInferencer InferenceModel(String testingFile, ParallelTopicModel model) throws IOException{
		
		
		TopicInferencer inferencer = model.getInferencer();

		return inferencer;
	}
}