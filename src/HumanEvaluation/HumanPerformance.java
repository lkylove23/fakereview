package HumanEvaluation;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.TreeMap;

public class HumanPerformance {
	public static void main(String[] args) throws IOException {
		
		String cvsSplitBy = ",";
		
		// E:\FakeReviewDetectionData\result\HumanPerformanceYelp

		
		//Choose
//		String Training ="before";
		String Training ="after";
		
		
//		String categories ="hotels";
		String categories ="restaurants";
//		String categories ="electronics";
//		String categories ="fashion";
//		String categories ="hospitals";
//		String categories ="musicvenues";
//		String categories ="insurance";
		
		String input="";
		String answer ="";
		if(Training.equals("before")){
		answer="E:\\FakeReviewDetectionData\\Yelp\\"+categories+"\\forHuman\\"+categories+"_1test_answer.csv";
		input= "E:\\FakeReviewDetectionData\\result\\HumanPerformanceYelp\\Fake Review Detection - "+categories+" 1) Quiz.csv";
		}else {
			answer="E:\\FakeReviewDetectionData\\Yelp\\"+categories+"\\forHuman\\"+categories+"_3test_answer.csv";
			input= "E:\\FakeReviewDetectionData\\result\\HumanPerformanceYelp\\Fake Review Detection - "+categories+" 2) Training & Final Quiz.csv";
		}
		
		String forKappaFile="E:\\FakeReviewDetectionData\\result\\HumanPerformanceYelp\\"+Training+"_"+categories+"_kappa.csv";
		
		//Construt answer
		ArrayList<Integer> answerAL =new ArrayList<Integer>();
		ArrayList<Integer> numberOfTruthfulAL =new ArrayList<Integer>();
		ArrayList<Integer> tmpAnswerAL =new ArrayList<Integer>();
		TreeMap<String,ArrayList<Integer>> humanJudgeAL =new TreeMap<String,ArrayList<Integer>>();
		TreeMap<String,Double> humanPerformance =new TreeMap<String,Double>();
		String Judge ="";
		String line ="";
		
		int t[]={0,0,0,0,0,0,0,0,0,0,0,0};
		
		BufferedReader br = new BufferedReader(new FileReader(answer));
		while( (line =br.readLine()) != null){
			String[] tempArray = line.split(cvsSplitBy);
			answerAL.add(Integer.parseInt((tempArray[0])));
		}
		
		double score;
		
		br= new BufferedReader(new FileReader(input));
		

		while( (line =br.readLine()) != null){
			String[] tempArray = line.split(cvsSplitBy);
			if(tempArray.length==0){
				continue;
			}
			if(tempArray[0].equals("Ÿ�ӽ�����")){
				
				continue;
			}
			score=0;
			tmpAnswerAL.clear();
			System.out.println(line);
			
			for(int i=0;i<12;i++){
				System.out.println(tempArray.length);
				int tmpA;
				if(Training.equals("before")){
				if(tempArray[i+4].equals("Truthful")){
					tmpA=1;
					t[i]++;
				}else{
					//2 for fake
					tmpA=2;
				}
				}else{
					if(tempArray[i+52].equals("Truthful")){
						tmpA=1;
						t[i]++;
					}else{
						//2 for fake
						tmpA=2;
					}
				}
				tmpAnswerAL.add(tmpA);
				if(answerAL.get(i)==tmpA){
					//1 correct 
					//0 incorrect
					score++;
				}
				
				
			}
			if(tmpAnswerAL.size()!=12){
				System.out.println("error");
			}
			humanPerformance.put(tempArray[1], score/12);
			humanJudgeAL.put(tempArray[1], tmpAnswerAL);
		}
		String output1="";
		String output2="";
		
		for(int j=0;j<12;j++){
			System.out.print(t[j]+"\t");
		}
		System.out.println();
		
		for( String key : humanPerformance.keySet() ){
            output1+=key+"\t";
            output2+=humanPerformance.get(key)+"\t";
        }
		
		
		System.out.println(output1.trim());
		System.out.println(output2.trim());
		if(humanPerformance.size()!=14){
			System.out.println("something wrong");
		}
	}
}
