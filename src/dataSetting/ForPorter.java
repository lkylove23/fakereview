package dataSetting;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.StringTokenizer;

/**
 * Created by lkylove23 on 2015-02-21.
 */
public class ForPorter {



    public static void main(String args[]) throws IOException {
        String where="D:\\";
        String categories="";
        String test="";
        String train="";
        String withstopword="";
        String stemming="_porter"; //NoR 에서는 없는게 porter였음 여긴느 다름
        int topicNum=100;
        int totalNumOfReviews=0;
        for (int c_it = 0; c_it < 7; c_it++) {
//        for (int c_it = 4; c_it < 5; c_it++) {
            if (c_it == 0) {

                //Electronics
//                changeID = 44;
                categories = "electronics";
                totalNumOfReviews = 440;
            } else if (c_it == 1) {

                //Fashion
                categories = "fashion";
                totalNumOfReviews = 2690;
            } else if (c_it == 2) {

                //hospitals
                categories = "hospitals";
                totalNumOfReviews = 200;
            } else if (c_it == 3) {

                //hotels
                categories = "hotels";
                totalNumOfReviews = 1100;

            } else if (c_it == 4) {

                //Insurance
                categories = "insurance";
                totalNumOfReviews = 130;
            } else if (c_it == 5) {

                //Musicvenues
                categories = "musicvenues";
                totalNumOfReviews = 1190;
            } else if (c_it == 6) {

                //Restaurant
//                totalNumOfReview = 37990;
                categories = "restaurants";
                totalNumOfReviews = 37990;
            }
            //read
            String resultFile_helper = where + "FakeReviewDetectionData\\Yelp\\"+categories+"\\";
            for (int it = 0; it < 5; it++) {
                if (it == 0) {
                    test = "fold1";
                    train = "fold2,3,4,5";
                } else if (it == 1) {
                    test = "fold2";
                    train = "fold1,3,4,5";
                } else if (it == 2) {
                    test = "fold3";
                    train = "fold1,2,4,5";
                } else if (it == 3) {
                    test = "fold4";
                    train = "fold1,2,3,5";
                } else if (it == 4) {
                    test = "fold5";
                    train = "fold1,2,3,4";
                }

                String trainingResultFile = resultFile_helper+ "BalancedForLDA\\" +categories+"_"+train+stemming+".txt";
                String trainingFile = resultFile_helper+ "BalancedForLDA\\" +categories+"_"+train+".txt";
                String testingResultFile = resultFile_helper + "BalancedForLDA\\" +categories+"_"+test+stemming+".txt";
                String testingFile = resultFile_helper + "BalancedForLDA\\" +categories+"_"+test+".txt";
                BufferedReader br = new BufferedReader(new FileReader(trainingFile));
                String line="";
                StringBuffer trainout = new StringBuffer();
                while ((line = br.readLine()) != null) {
                    String[] token = line.split("\t");
                    int label;
                    if(token[1].equals("f")){
//                                    TrainIDLabel.add(-1);
                        label=-1;
                    }else{
//                                    TrainIDLabel.add(1);
                        label=1;
                    }
                    line = token[2];
                    line = line.replace(".", " . ");
                    line = line.replace("!", " ! ");
                    line = line.replace("(", " ( ");
                    line = line.replace(")", " ) ");
                    line = line.replace("$", " $ ");
                    line = line.replace("-", " ");
                    line = line.replace("/", " ");
                    line = line.replace("~", " ");
                    line = line.replace("?", " ");
                    line = line.replace("\"", " ");
                    line = line.toLowerCase();
                    StringTokenizer st;
                    st = new StringTokenizer(line, " ,|");
                    ///poter
                    //porter here
                    String newLine="";
                    StringBuffer newLinehelp = new StringBuffer();
                    while (st.hasMoreTokens()) {
                        String temp = st.nextToken();
                        Stemmer porter = new Stemmer();
                        char[] temptemp = temp.toCharArray();
                        porter.add(temptemp, temptemp.length);
                        porter.stem();
                        temp = porter.toString();
                        newLinehelp.append(temp);
                        newLinehelp.append(" ");
                    }

                   trainout.append(token[0]);
                   trainout.append("\t");
                   trainout.append(label);
                   trainout.append("\t");
                   trainout.append(newLinehelp.toString());
                   trainout.append("\n");
                }
                FileWriter writer1 = new FileWriter(trainingResultFile);
                writer1.append(trainout.toString().trim());
                writer1.close();


                 br = new BufferedReader(new FileReader(testingFile));
                 line="";
                StringBuffer testout = new StringBuffer();
                while ((line = br.readLine()) != null) {
                    String[] token = line.split("\t");
                    int label;
                    if(token[1].equals("f")){
//                                    TrainIDLabel.add(-1);
                        label=-1;
                    }else{
//                                    TrainIDLabel.add(1);
                        label=1;
                    }
                    line = token[2];
                    line = line.replace(".", " . ");
                    line = line.replace("!", " ! ");
                    line = line.replace("(", " ( ");
                    line = line.replace(")", " ) ");
                    line = line.replace("$", " $ ");
                    line = line.replace("-", " ");
                    line = line.replace("/", " ");
                    line = line.replace("~", " ");
                    line = line.replace("?", " ");
                    line = line.replace("\"", " ");

                    line = line.toLowerCase();
                    StringTokenizer st;
                    st = new StringTokenizer(line, " ,|");
                    ///poter
                    //porter here
                    String newLine="";
                    StringBuffer newLinehelp = new StringBuffer();
                    while (st.hasMoreTokens()) {
                        String temp = st.nextToken();
                        Stemmer porter = new Stemmer();
                        char[] temptemp = temp.toCharArray();
                        porter.add(temptemp, temptemp.length);
                        porter.stem();
                        temp = porter.toString();
                        newLinehelp.append(temp);
                        newLinehelp.append(" ");
                    }

                   testout.append(token[0]);
                   testout.append("\t");
                   testout.append(label);
                   testout.append("\t");
                   testout.append(newLinehelp.toString());
                   testout.append("\n");
                }
                FileWriter writer2 = new FileWriter(testingResultFile);
                writer2.append(testout.toString().trim());
                writer2.close();


            }
        }
    }
}
