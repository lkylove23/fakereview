package dataSetting;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeMap;

public class NgramforSvm {

	
	/**유닛별 nomalise 해줄것!!!!!!!!!!!!!!!!!!!!! 하고이거 지우기***/
	public static void main(String args[]) {

		String line = "";
		String cvsSplitBy = ",";
//		Map tm = new TreeMap();
		ArrayList tm= new ArrayList();
		HashMap hm= new HashMap();
		
		//max cnt =31 이었음
		//int maxCnt = 31;
		
		// String path = Maketrain.class.getResource("").getPath();
		
//		 String test ="fold1";
//		 String test ="fold2";
//		 String test ="fold3";
//		 String test ="fold4";
//		 String test ="fold5";

		 String test ="fold2,3,4,5";	
//		 String test ="fold1,3,4,5";
//		 String test ="fold1,2,4,5";
//		 String test ="fold1,2,3,5";
//		 String test ="fold1,2,3,4";
		 
		 String extraMode="apear";

//		String mode = "Unigram";
		 String mode ="Bigram";

		//전체불러와서 인덱싱후 돌리는거로 어짜피 시간 많이 안결러서 ㅎ
		
		//t,f 각각 불러와서 처리후 합쳐줌
		String inputTotalFile = "E:\\FakeReviewDetectionData\\input\\positive\\"
				+ "fold_total.txt";
		String inputFile_f = "E:\\FakeReviewDetectionData\\input\\positive\\"
				+ test+"_f.txt";
		String inputFile_t = "E:\\FakeReviewDetectionData\\input\\positive\\"
				+ test+"_t.txt";
		String outputFile = "E:\\FakeReviewDetectionData\\input\\positive\\N-gram\\"
				+ mode + "\\" + test+".dat";
		String extra_outputFile = "E:\\FakeReviewDetectionData\\input\\positive\\N-gram\\"
				+ mode + "\\" + test+"_"+extraMode+".dat";
		String index_outputFile = "E:\\FakeReviewDetectionData\\input\\positive\\N-gram\\"
				+ mode + "\\Index.txt";

		double cnt=0;
		int currentId;
		// LinkedHashMap<String, Integer> map = new LinkedHashMap<String,
		// Integer>();
		ArrayList<TreeMap<Integer, Double>> map = new ArrayList<TreeMap<Integer, Double>>();
		ArrayList<TreeMap<Integer, Double>> map_fold_t = new ArrayList<TreeMap<Integer, Double>>();
		ArrayList<TreeMap<Integer, Double>> map_fold_f = new ArrayList<TreeMap<Integer, Double>>();
		HashMap<Integer, String> indexIS = new HashMap<Integer, String>();
		HashMap<String, Integer> indexSI = new HashMap<String, Integer>();
		int indexCnt = 1;
		// HashMap<String, Integer> map = new HashMap<String, Integer>();
		try {

			BufferedReader br = new BufferedReader(new FileReader(inputTotalFile));
			// 사실 토탈에 대해서는 인덱싱만 해주면 된다
			//test for max value
//			double max = 0;
			double max = 1;
			String preTemp;
			String nGramTemp;
			while ((line = br.readLine()) != null) {
				// use comma as separator
				//docMap <word id , word count>
				HashMap<Integer, Double> docMap = new HashMap<Integer, Double>();
				TreeMap<Integer, Double> tMap = new TreeMap<Integer, Double>();
				String tmp;
//				String match = "[^\uAC00-\uD7A3xfe0-9a-zA-Z\\s]";
//			      cols[4] =tmp.replaceAll(match, " ");
//				tmp = tmp.replace("`s", "");
//				tmp = tmp.replace("'s", "");
//				tmp = tmp.replace("", "");
//				String match = "[^\uAC00-\uD7A3xfe0-9a-zA-Z\\s]";
//			      tmp =tmp.replaceAll(match, " ");
				
//				//
//				line = line.replace("'s", "");
//				line = line.replace("n't", " not");
				line = line.replace(".", "");
				line = line.replace("!", "");
				line = line.replace("(", "");
				line = line.replace(")", "");
				line = line.replace("$", "");
				line = line.replace("-", "");
				line = line.replace("/", " ");
				line = line.replace("~", " ");
				line = line.replace("?", "");
				line = line.replace("\"", "");
				
				
				
//				String match = "[^\uAC00-\uD7A3xfe0-9a-zA-Z\\s]";
//			     line =line.replaceAll(match, "");
				line = line.toLowerCase();
				
				StringTokenizer st = new StringTokenizer(line, " ,");
				
		
				preTemp ="";
				nGramTemp="";
				int tag=0;
				while (st.hasMoreTokens()) {
					String temp = st.nextToken(); // 토근을 temp 변수에 저장
//					String temp2 = temp.replace(".", "");
//					temp2 = temp2.replace("!", "");
//					temp2 = temp2.toLowerCase();
					// System.out.println("temp=" + temp2);
					// delete "."
					// 일단 특수문자 제거
					if(mode.equals("Bigram")){
						if(tag==0){
							tag++;
							preTemp=temp;
							continue;
						}
						
						nGramTemp=preTemp+" "+temp;
						}else if(mode.equals("Unigram")){
							nGramTemp=temp;
						}
					
					// Indexing
//					if (!indexIS.containsValue(temp)) {
//						indexIS.put(indexCnt, temp);
//						indexSI.put(temp, indexCnt);
//						// get the id of this input word : case 1	
//						currentId=indexCnt;
//						indexCnt++;
//					}else{
//						// get the id of this input word : case 2
//						currentId=indexSI.get(temp);
//					}
					
					if (!indexIS.containsValue(nGramTemp)) {
						indexIS.put(indexCnt, nGramTemp);
						indexSI.put(nGramTemp, indexCnt);
						// get the id of this input word : case 1	
//						currentId=indexCnt;
						indexCnt++;
					}
					//To make preTemp for Bigram
					preTemp=temp;
					
					
//					if (!docMap.containsKey(currentId)) {
//						docMap.put(currentId, 1.0);
//					} else{
//						cnt = docMap.get(currentId) + 1;
//						docMap.put(currentId, cnt);
//					}
					//test
//					if(max<cnt){
//						max=cnt;
//					}
				}
//				System.out.println(docMap);
				//tree <- has
			}
//			System.out.println("max value is "+max);
			
			//double 로 하는구간 만들기
			
			int check=0;
			
			/** for fold_t **/
			
			BufferedReader br_fold_t = new BufferedReader(new FileReader(inputFile_t));
			
			while ((line = br_fold_t.readLine()) != null) {
				// use comma as separator
				
				check++;
				//docMap <word id , word count>
//				HashMap<Integer, Integer> docMap_fold_t = new HashMap<Integer, Integer>();
				HashMap<Integer, Double> docMap_fold_t = new HashMap<Integer, Double>();
//				TreeMap<Integer, Integer> tMap_fold_t = new TreeMap<Integer, Integer>();
				TreeMap<Integer, Double> tMap_fold_t = new TreeMap<Integer, Double>();
				
				String tmp;
				
//				String match = "[^\uAC00-\uD7A3xfe0-9a-zA-Z\\s]";
//			      cols[4] =tmp.replaceAll(match, " ");
//				tmp = tmp.replace("`s", "");
//				tmp = tmp.replace("'s", "");
//				tmp = tmp.replace("", "");
//				String match = "[^\uAC00-\uD7A3xfe0-9a-zA-Z\\s]";
//			      tmp =tmp.replaceAll(match, " ");

//				//
//				line = line.replace("'s", "");
//				line = line.replace("n't", " not");
				line = line.replace(".", "");
				line = line.replace("!", "");
				line = line.replace("(", "");
				line = line.replace(")", "");
				line = line.replace("$", "");
				line = line.replace("-", "");
				line = line.replace("/", " ");
				line = line.replace("~", " ");
				line = line.replace("?", "");
				line = line.replace("\"", "");
				
//				String match = "[^\uAC00-\uD7A3xfe0-9a-zA-Z\\s]";
//			     line =line.replaceAll(match, "");
				line = line.toLowerCase();
				
				StringTokenizer st_t = new StringTokenizer(line, " ,");
				
				
				while (st_t.hasMoreTokens()) {
					String temp = st_t.nextToken(); // 토근을 temp 변수에 저장
//					String temp2 = temp.replace(".", "");
//					temp2 = temp2.replace("!", "");
//					temp2 = temp2.toLowerCase();
					// System.out.println("temp=" + temp2);
					// delete "."
					// 일단 특수문자 제거
					
					
			
					// get the id of this input word : case 2
					int	currentId_fold_t=indexSI.get(temp);
				
					
					
					// input term frequencies
					if (!docMap_fold_t.containsKey(currentId_fold_t)) {
						docMap_fold_t.put(currentId_fold_t, 1.0/max);
//						System.out.println("1/31: "+docMap_fold_t.put(currentId_fold_t, 1.0/max));
					
					} else{
//						if(check==5){
//						if(currentId_fold_t==6){
//						System.out.println("id"+currentId_fold_t+"원래 cnt"+docMap_fold_t.get(currentId_fold_t));
////						System.out.println("원래 cnt + 1 ="+(docMap_fold_t.get(currentId_fold_t)+1.0));
//						System.out.println("(원래 cnt )*31+1 ="+((docMap_fold_t.get(currentId_fold_t)*max)+1.0));
//						}
//						}
						
						cnt = (docMap_fold_t.get(currentId_fold_t)*max + 1.0)/max;
						docMap_fold_t.put(currentId_fold_t, cnt);
						
//						if(check==5){
//						if(currentId_fold_t==6){
//							
////						System.out.println("그다음= "+(docMap_fold_t.get(currentId_fold_t)*max + 1.0)/max);
//						System.out.println("그다음= "+cnt);
//						}
//						}
//						
					}
					
					
				}
//				System.out.println(docMap);
				//tree <- has
				tMap_fold_t.putAll(docMap_fold_t);
				map_fold_t.add(tMap_fold_t);
			}
			
			/**for forld_f**/
			BufferedReader br_fold_f = new BufferedReader(new FileReader(inputFile_f));
			
			while ((line = br_fold_f.readLine()) != null) {
				// use comma as separator
				
				//docMap <word id , word count>
				HashMap<Integer, Double> docMap_fold_f = new HashMap<Integer, Double>();
				TreeMap<Integer, Double> tMap_fold_f = new TreeMap<Integer, Double>();
				
				String tmp;
				
//				String match = "[^\uAC00-\uD7A3xfe0-9a-zA-Z\\s]";
//			      cols[4] =tmp.replaceAll(match, " ");
//				tmp = tmp.replace("`s", "");
//				tmp = tmp.replace("'s", "");
//				tmp = tmp.replace("", "");
//				String match = "[^\uAC00-\uD7A3xfe0-9a-zA-Z\\s]";
//			      tmp =tmp.replaceAll(match, " ");
				
//				//
//				line = line.replace("'s", "");
//				line = line.replace("n't", " not");
				line = line.replace(".", "");
				line = line.replace("!", "");
				line = line.replace("(", "");
				line = line.replace(")", "");
				line = line.replace("$", "");
				line = line.replace("-", "");
				line = line.replace("/", " ");
				line = line.replace("~", " ");
				line = line.replace("?", "");
				line = line.replace("\"", "");
				
//				String match = "[^\uAC00-\uD7A3xfe0-9a-zA-Z\\s]";
//			     line =line.replaceAll(match, "");
				line = line.toLowerCase();
				
				StringTokenizer st_f = new StringTokenizer(line, " ,");
				
				
				while (st_f.hasMoreTokens()) {
					String temp = st_f.nextToken(); // 토근을 temp 변수에 저장
//					String temp2 = temp.replace(".", "");
//					temp2 = temp2.replace("!", "");
//					temp2 = temp2.toLowerCase();
					// System.out.println("temp=" + temp2);
					// delete "."
					// 일단 특수문자 제거
					
					
					
					// get the id of this input word : case 2
					int	currentId_fold_f=indexSI.get(temp);
					
					
//					
//					// input term frequencies
//					if (!docMap_fold_f.containsKey(currentId_fold_f)) {
//						docMap_fold_f.put(currentId_fold_f, 1.0);
//					} else{
//						cnt = docMap_fold_f.get(currentId_fold_f) + 1;
//						docMap_fold_f.put(currentId_fold_f, cnt);
//					}
//					
					
					// input term frequencies
					if (!docMap_fold_f.containsKey(currentId_fold_f)) {
						docMap_fold_f.put(currentId_fold_f, 1.0/max);
//						System.out.println("1/31: "+docMap_fold_t.put(currentId_fold_t, 1.0/max));
					
					} else{
//						if(check==5){
//						if(currentId_fold_t==6){
//						System.out.println("id"+currentId_fold_t+"원래 cnt"+docMap_fold_t.get(currentId_fold_t));
////						System.out.println("원래 cnt + 1 ="+(docMap_fold_t.get(currentId_fold_t)+1.0));
//						System.out.println("(원래 cnt )*31+1 ="+((docMap_fold_t.get(currentId_fold_t)*max)+1.0));
//						}
//						}
						
						cnt = (docMap_fold_f.get(currentId_fold_f)*max + 1.0)/max;
						docMap_fold_f.put(currentId_fold_f, cnt);
						
//						if(check==5){
//						if(currentId_fold_t==6){
//							
////						System.out.println("그다음= "+(docMap_fold_t.get(currentId_fold_t)*max + 1.0)/max);
//						System.out.println("그다음= "+cnt);
//						}
//						}
//						
					}
					
					
				}
//				System.out.println(docMap);
				//tree <- has
				tMap_fold_f.putAll(docMap_fold_f);
				map_fold_f.add(tMap_fold_f);
			}
			// System.out.println(indexIS);

			// System.out.println(docMap);
			// write
			// i is doc cnt
			int testcnt=1;
			
			/**write file**/
			
			try {
				// //////////////////////////////////////////////////////////////
				BufferedWriter out = new BufferedWriter(new FileWriter(outputFile));
				String output;
				
				
				/**for truthful**/
				String outputdoc_fold = "";
				int test1=0;

				
				for (int i = 1; i <= map_fold_t.size(); i++) {
					
//					System.out.println("i=" + i);
					
					
					outputdoc_fold += "1 ";
					
					Set keySet_fold = map_fold_t.get(i - 1).keySet();
//					System.out.println("term size of document="+map_fold.get(i - 1).size());
					Iterator iterator_fold_t = keySet_fold.iterator();

					while (iterator_fold_t.hasNext()) {
//						System.out.println(test1);
						test1++;
						int wordId = (Integer) iterator_fold_t.next();
						double value = map_fold_t.get(i - 1).get(wordId);
						outputdoc_fold += wordId + ":" + value + " ";
					}

//					Iterator it = (tm).keySet().iterator();
						
//					Object obj;
//				    while (it.hasNext()) {  // Key를 뽑아낸 Iterator 를 돌려가며
//				      obj = it.next(); // Kef 를 하나씩 뽑아
////				      System.out.println(obj + ": " + map.get((Integer) obj)); // Value 를 출력
//				      outputdoc += obj + ":" + tm.get(obj)+ " ";
//				      
//				    }
						outputdoc_fold += "\n";
						testcnt++;
				}
				
				/**for fake**/
//				System.out.println("size of map_fold_f=" + map_fold_f.size());
					for (int i = 1; i <= map_fold_f.size(); i++) {
					
//					System.out.println("i=" + i);
						
						outputdoc_fold += "-1 ";
					
					Set keySet_fold = map_fold_f.get(i - 1).keySet();
//					System.out.println("term size of document="+map_fold.get(i - 1).size());
					Iterator iterator_fold_f = keySet_fold.iterator();

					while (iterator_fold_f.hasNext()) {
//						System.out.println(test1);
						test1++;
						int wordId = (Integer) iterator_fold_f.next();
						double value = map_fold_f.get(i - 1).get(wordId);
						outputdoc_fold += wordId + ":" + value + " ";
					}

					if(!(i==map_fold_f.size())){
						outputdoc_fold += "\n";
					}
						testcnt++;
				}
				
				
				
				
				
//				System.out.println(testcnt-1);
//				System.out.print(outputdoc);
				out.write(outputdoc_fold);
//				out.newLine();
				// out.write(s); out.newLine();

				out.close();

				// //////////////////////////////////////////////////////////////
			} catch (IOException e) {
				System.err.println(e); // 에러가 있다면 메시지 출력
				System.exit(1);
			}

			// i+":"+

			// for(int j=0;j<map.get(i).size();j++){
			// // map.get(i).g
			//
			// index.
			// }

			// Index print out
			try {
				// //////////////////////////////////////////////////////////////
				BufferedWriter out_index = new BufferedWriter(new FileWriter(index_outputFile));
				// for fake
				String outputdoc = "";
				for (int i = 1; i <= indexIS.size(); i++) {
					// j is index of docMap
					outputdoc += i + "," + indexIS.get(i);
					outputdoc += "\n";
				}

//				System.out.print(outputdoc);
				out_index.write(outputdoc);
				// out_index.newLine();
				// out.write(s); out.newLine();
				out_index.close();
				// //////////////////////////////////////////////////////////////
			} catch (IOException e) {
				System.err.println(e); 
				System.exit(1);
			}
			
			

			br.close();
		} catch (IOException e) {
			System.err.println(e);
			System.exit(1);
			
		}

	}

}
