package dataSetting;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class ForHumanEvaluation {
	public static void main(String args[]) throws IOException {
		String line="";
		String cvsSplitBy = ",";
		Boolean checkforloop= true; 
		/**Choose**/
		int reviewPerBusiness=2;
//		int reviewPerBusiness=11; //for error case
		int numOfBusinessForSampling =18;
//		int numOfBusinessForSampling =3; // for error case
		//KY 
		String where="E:\\FakeReviewDetectionData";
		//or Dear IRbig2?
//		String where="D:\\KY\\FakeReviewDetectionData";
//		String categories ="hotels";
		String categories ="restaurants";
//		String categories ="electronics";
//		String categories ="fashion";
//		String categories ="hospitals";
//		String categories ="musicvenues";
//		String categories ="insurance";
		
		String inputFile_f = where+"\\Yelp\\"+categories+"\\F";
		String inputFile_t = where+"\\Yelp\\"+categories+"\\T";
//		String inputFile_t = where+"\\input\\positive\\"
//				+ test+"_t.txt";
		String resultFile_t=where+"\\Yelp\\"+categories+"\\forHuman";

		inputFile_f +="\\Balanced";
		inputFile_t +="\\Balanced";
		
		ArrayList<String> list=new ArrayList<String>();
		
		File ForCheck = new File(resultFile_t);
		if(!ForCheck.isDirectory()){
			ForCheck.mkdir();
		}

		File Folder_f = new File(inputFile_f);
		File [] arrFile_f = Folder_f.listFiles();
		
		HashMap<String,String> tempDocs_f=new HashMap<String,String>();
		HashMap<String,String> tempDocs_t=new HashMap<String,String>();
		// doc, 1: t/2: f
		HashMap<String,Integer> sampled1=new HashMap<String,Integer>();
		HashMap<String,Integer> sampled2=new HashMap<String,Integer>();
		HashMap<String,Integer> sampled3=new HashMap<String,Integer>();
		// T,f2 이상랜덤으로 18개의 비지니스 뽑은후 갯수 안모자라나 테스트 한다 모자라면 다시 랜덤돌리기
		// 다 뽑은후 랜덤으로 배열 한다 이때 정답셋도 만들어놓은다 train 에서는 t f 표시해주기
		int numOfDoc;
		try {
			BufferedReader br = null;
			/** for each fake business **/
			for (File file : arrFile_f) {
				numOfDoc=0;
				br = new BufferedReader(new FileReader(file));
				String BusName="";
				while( (line =br.readLine()) != null){
					numOfDoc++;
					String[] tempArray2 = line.split(cvsSplitBy);
					BusName=tempArray2[0];
				}
				if(numOfDoc<reviewPerBusiness){
					continue;
				}
				list.add(BusName);
			}
			System.out.println(list.size());
			//random 뽑기
			//0,1,2 	1-test
			//3~14		2-train
			//15,16,17	3-test
			ArrayList<Integer> randomBusinessAL = new ArrayList<Integer>();
			ArrayList<Integer> randomReviewAL = new ArrayList<Integer>();
			
			ArrayList<String> forRandom1 = new ArrayList<String>();
			ArrayList<String> forRandom2 = new ArrayList<String>();
			ArrayList<String> forRandom3 = new ArrayList<String>();
			ArrayList<String> forRandomHelper1 = new ArrayList<String>();
			ArrayList<String> forRandomHelper2 = new ArrayList<String>();
			ArrayList<String> forRandomHelper3 = new ArrayList<String>();
			while(checkforloop){
			sampled1.clear();
			sampled2.clear();
			sampled3.clear();
			
			randomBusinessAL.clear();
			while(randomBusinessAL.size()<numOfBusinessForSampling){
				int randN =(int) (Math.random() * list.size());
				if(!randomBusinessAL.contains(randN)){
				randomBusinessAL.add(randN);
				}
			}
			int busiCheck=1;
			int test1=0;
			int test2=0;
			int test3=0;
			
			/**Choosing for Fake**/
			//2개 뽑기
			for(int j=0;j<numOfBusinessForSampling;j++){
//				System.out.println(list.get(randomBusinessAL.get(j)));
				br = new BufferedReader(new FileReader(inputFile_f+"\\"+list.get(randomBusinessAL.get(j))+".csv"));
				BufferedReader br2 = new BufferedReader(new FileReader(inputFile_f+"\\"+list.get(randomBusinessAL.get(j))+".csv"));
				String BusName="";
				
				int totalNumOfDoc=0;
				//Counting num of doc for random range
				while( (line =br.readLine()) != null){
					totalNumOfDoc++;
				}
				
				randomReviewAL.clear();
				while(randomReviewAL.size()<reviewPerBusiness){
					int randN =(int) (Math.random() * totalNumOfDoc) + 1;
					if(!randomReviewAL.contains(randN)){
						randomReviewAL.add(randN);
					}
			
				}
				numOfDoc=0;
				while( (line =br2.readLine()) != null){
					numOfDoc++;
					if(numOfBusinessForSampling==18){
					if(randomReviewAL.contains(numOfDoc)){
						String[] temp = line.split(cvsSplitBy);
						 switch (j) {
					      case 0	: 
					      case 1	: 
					      case 2	:
					    	  test1++;
					    	  sampled1.put(temp[4].replace("|", ","), 2);
					                   break;
					      case 3	:
					      case 4	:
					      case 5	:
					      case 6 	:
					      case 7	:
					      case 8 	:
					      case 9 	:
					      case 10 	:
					      case 11 	:
					      case 12 	:
					      case 13 	:
					      case 14 	:
					    	  test2++;
					    	  sampled2.put("["+temp[1]+"] "+temp[4].replace("|", ","), 2);
					                   break;
					      case 15	: 
					      case 16	: 
					      case 17	:
					    	  test3++;
					    	  sampled3.put(temp[4].replace("|", ","), 2);
					                   break;
					      default    : System.out.println("error");
					                   break;
					    }
					}
					}else{
					if(randomReviewAL.contains(numOfDoc)){
						String[] temp = line.split(cvsSplitBy);
						 switch (j) {
					      case 0	: 
					      case 1	: 
					    	  test1++;
					    	  sampled1.put(temp[4].replace("|", ","), 2);
					                   break;
					      case 2	:
					      case 3	:
					      case 4	:
					      case 5	:
					      case 6 	:
					      case 7	:
					      case 8 	:
					      case 9 	:
					      case 10 	:
					    	  test2++;
					    	  sampled2.put("["+temp[1]+"] "+temp[4].replace("|", ","), 2);
					                   break;
					      case 11 	:
					      case 12 	:
					    	  test3++;
					    	  sampled3.put(temp[4].replace("|", ","), 2);
					                   break;
					      default    : System.out.println("error");
					                   break;
					    }
					}
					}
				}
//				System.out.println("numOfDoc: "+numOfDoc);
//				System.out.println(randomReviewAL);
			
			}
			test1=0;
			test2=0;
			test3=0;
			
//			System.out.println("truthful");
			/**Choosing for Truthful**/
			//2개 뽑기

			for(int j=0;j<numOfBusinessForSampling;j++){
				br = new BufferedReader(new FileReader(inputFile_t+"\\"+list.get(randomBusinessAL.get(j))+".csv"));
				BufferedReader br2 = new BufferedReader(new FileReader(inputFile_t+"\\"+list.get(randomBusinessAL.get(j))+".csv"));
				String BusName="";
				
				int totalNumOfDoc=0;
				//Counting num of doc for random range
				while( (line =br.readLine()) != null){
					totalNumOfDoc++;
				}
				randomReviewAL.clear();
				while(randomReviewAL.size()<reviewPerBusiness){

					int randN =(int) (Math.random() * totalNumOfDoc) + 1;
					if(!randomReviewAL.contains(randN)){
						randomReviewAL.add(randN);
					}
				}
//				System.out.println("totalNumOfDoc: "+totalNumOfDoc);
//				System.out.println(randomReviewAL);
				numOfDoc=0;
				int checkf=0;
				while( (line =br2.readLine()) != null){

					numOfDoc++;
					if(numOfBusinessForSampling==18){
					if(randomReviewAL.contains(numOfDoc)){
						checkf++;
						String[] temp = line.split(cvsSplitBy);
						
						 switch (j) {
					      case 0	: 
					      case 1	: 
					      case 2	:
					    	  test1++;
					    	  sampled1.put(temp[4].replace("|", ","), 1);
					    	           break;
					      case 3	:
					      case 4	:
					      case 5	:
					      case 6 	:
					      case 7	:
					      case 8 	:
					      case 9 	:
					      case 10 	:
					      case 11 	:
					      case 12 	:
					      case 13 	:
					      case 14 	:
					    	  test2++;
					    	  String tmp= "["+temp[1]+"] "+temp[4].replace("|", ",");
					    	  int s = sampled2.size();
					    	  sampled2.put(tmp, 1);
					    	  int s2 = sampled2.size();
					    	  if(!(s2-s==1)){
//					    	  System.out.println(s2-s);
//					    	  System.out.println("line:\t"+line);
					    	  }
					                   break;
					      case 15	: 
					      case 16	: 
					      case 17	: 
					    	  test3++;
					    	  sampled3.put(temp[4].replace("|", ","), 1);
					                   break;
					      default    : System.out.println("error");
					                   break;
					    }
					}}else{
					if(randomReviewAL.contains(numOfDoc)){
						String[] temp = line.split(cvsSplitBy);
						 switch (j) {
					      case 0	: 
					    	  test1++;
					    	  sampled1.put(temp[4].replace("|", ","), 1);
					                   break;
					      case 1	:
					    	  test2++;
					    	  sampled2.put("["+temp[1]+"] "+temp[4].replace("|", ","), 1);
					                   break;
					      case 2 	:
					    	  test3++;
					    	  sampled3.put(temp[4].replace("|", ","), 1);
					                   break;
					      default    : System.out.println("error");
					                   break;
					    }
					}
				}
				}
//				System.out.println("numOfDoc: "+numOfDoc);
//				System.out.println(randomReviewAL);
			}
			
			FileWriter writer1 = new FileWriter(resultFile_t+"\\"+categories+"_1test"+".csv");
			FileWriter writer2 = new FileWriter(resultFile_t+"\\"+categories+"_2train"+".csv");
			FileWriter writer3 = new FileWriter(resultFile_t+"\\"+categories+"_3test"+".csv");
			FileWriter writer1_a = new FileWriter(resultFile_t+"\\"+categories+"_1test_answer"+".csv");
			FileWriter writer3_a = new FileWriter(resultFile_t+"\\"+categories+"_3test_answer"+".csv");
			
		
			
			
			System.out.println("sample1 size:"+sampled1.size());
//			System.out.println(sampled1);
			System.out.println("sample2 size:"+sampled2.size());
//			System.out.println(sampled2);
			System.out.println("sample3 size:"+sampled3.size());
			
			
//			if(sampled1.size()==12&sampled2.size()==48&sampled3.size()==12){
//			if(sampled1.size()>=12&sampled2.size()>=48&sampled3.size()>=12){
				if(true){
				
//				System.out.println(sampled3);
				checkforloop=false;

			
			for (String key : sampled1.keySet()) {
				forRandom1.add(key);
			}
			forRandomHelper1.clear();
			while(forRandomHelper1.size()<forRandom1.size()){
				int randN =(int) (Math.random() * forRandom1.size());
				if(!forRandomHelper1.contains(forRandom1.get(randN))){
					forRandomHelper1.add(forRandom1.get(randN));
				}
			}
			
			
//			for()
			for (int r=0;r<forRandomHelper1.size();r++) {
				writer1.append(forRandomHelper1.get(r));
				writer1.append("\n");
				writer1_a.append(String.valueOf(sampled1.get(forRandomHelper1.get(r))));
				writer1_a.append("\n");
			}
			writer1.close();
			writer1_a.close();
			
			for (String key : sampled2.keySet()) {
				forRandom2.add(key);
			}
			forRandomHelper2.clear();
			while(forRandomHelper2.size()<forRandom2.size()){
				int randN =(int) (Math.random() * forRandom2.size());
				if(!forRandomHelper2.contains(forRandom2.get(randN))){
					forRandomHelper2.add(forRandom2.get(randN));
				}
			}

			for (int r=0;r<forRandomHelper2.size();r++) {
				writer2.append(forRandomHelper2.get(r));
				writer2.append("\n");
			}
			writer2.close();
			
			for (String key : sampled3.keySet()) {
				forRandom3.add(key);
			}
			forRandomHelper3.clear();
			while(forRandomHelper3.size()<forRandom3.size()){
				int randN =(int) (Math.random() * forRandom3.size());
				if(!forRandomHelper3.contains(forRandom3.get(randN))){
					forRandomHelper3.add(forRandom3.get(randN));
				}
			}
			for (int r=0;r<forRandomHelper3.size();r++) {
				writer3.append(forRandomHelper3.get(r));
				writer3.append("\n");
				writer3_a.append(String.valueOf(sampled3.get(forRandomHelper3.get(r))));
				writer3_a.append("\n");
			}
			writer3.close();
			writer3_a.close();

			}
		}
			
		} catch(FileNotFoundException e) {
			e.printStackTrace();

		}

	}
}
