package dataSetting;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeMap;

public class MakingListOfNgramforSvmWeka {

	public static void main(String args[]) {

		String line = "";
		String cvsSplitBy = ",";
//		Map tm = new TreeMap();
		ArrayList tm= new ArrayList();
		HashMap hm= new HashMap();
		
		//max cnt =31 이었음
		//int maxCnt = 31;
		
		
		// String path = Maketrain.class.getResource("").getPath();
		
//		 String test ="fold1";
//		 String test ="fold2";
//		 String test ="fold3";
//		 String test ="fold4";
//		 String test ="fold5";

		 String test ="fold2,3,4,5";	
//		 String test ="fold1,3,4,5";
//		 String test ="fold1,2,4,5";
//		 String test ="fold1,2,3,5";
//		 String test ="fold1,2,3,4";
		 
		 String extraMode="apear";

		String mode = "Unigram";
		// String mode ="Bigram";

		//전체불러와서 인덱싱후 돌리는거로 어짜피 시간 많이 안결러서 ㅎ
		
		//t,f 각각 불러와서 처리후 합쳐줌
		String inputTotalFile = "E:\\FakeReviewDetectionData\\input\\positive\\"
				+ "fold_total.txt";
		String inputFile_f = "E:\\FakeReviewDetectionData\\input\\positive\\"
				+ test+"_f.txt";
		String inputFile_t = "E:\\FakeReviewDetectionData\\input\\positive\\"
				+ test+"_t.txt";
		String outputFile = "E:\\FakeReviewDetectionData\\input\\positive\\N-gram\\"
				+ mode + "\\" + test+".dat";
		String extra_outputFile = "E:\\FakeReviewDetectionData\\input\\positive\\N-gram\\"
				+ mode + "\\" + test+"_"+extraMode+".dat";
		String index_outputFile = "E:\\FakeReviewDetectionData\\input\\positive\\N-gram\\"
				+ mode + "\\Index.txt";

		double cnt=0;
		int currentId;
		// LinkedHashMap<String, Integer> map = new LinkedHashMap<String,
		// Integer>();
		ArrayList<TreeMap<Integer, Double>> map = new ArrayList<TreeMap<Integer, Double>>();
		ArrayList<TreeMap<Integer, Double>> map_fold_t = new ArrayList<TreeMap<Integer, Double>>();
		ArrayList<TreeMap<Integer, Double>> map_fold_f = new ArrayList<TreeMap<Integer, Double>>();
		HashMap<Integer, String> indexIS = new HashMap<Integer, String>();
		HashMap<String, Integer> indexSI = new HashMap<String, Integer>();
//		weka 는 0 부터 시작
		int indexCnt = 0;
		// HashMap<String, Integer> map = new HashMap<String, Integer>();
		try {

			BufferedReader br = new BufferedReader(new FileReader(inputTotalFile));
			
			// 사실 토탈에 대해서는 인덱싱만 해주면 된다
			
			//test for max value
			
//			double max = 0;
			double max = 1;
		
			
			while ((line = br.readLine()) != null) {
				// use comma as separator
				
				//docMap <word id , word count>
				HashMap<Integer, Double> docMap = new HashMap<Integer, Double>();
				TreeMap<Integer, Double> tMap = new TreeMap<Integer, Double>();

				String tmp;

//				String match = "[^\uAC00-\uD7A3xfe0-9a-zA-Z\\s]";
//			      cols[4] =tmp.replaceAll(match, " ");
//				tmp = tmp.replace("`s", "");
//				tmp = tmp.replace("'s", "");
//				tmp = tmp.replace("", "");
//				String match = "[^\uAC00-\uD7A3xfe0-9a-zA-Z\\s]";
//			      tmp =tmp.replaceAll(match, " ");

				
//				//
//				line = line.replace("'s", "");
//				line = line.replace("n't", " not");
				line = line.replace(".", "");
				line = line.replace("!", " ! ");
				line = line.replace("(", " ( ");
				line = line.replace(")", " ) ");
//				line = line.replace("$", "");
				line = line.replace("$", " $ ");
				line = line.replace("-", "");
				line = line.replace("/", " ");
				line = line.replace("~", " ");
				line = line.replace("?", " ? ");
				line = line.replace("\"", "");
				
				
				
//				String match = "[^\uAC00-\uD7A3xfe0-9a-zA-Z\\s]";
//			     line =line.replaceAll(match, "");
				line = line.toLowerCase();
				
				StringTokenizer st = new StringTokenizer(line, " ,");
				
		
				
				while (st.hasMoreTokens()) {
					String temp = st.nextToken(); // 토근을 temp 변수에 저장
//					String temp2 = temp.replace(".", "");
//					temp2 = temp2.replace("!", "");
//					temp2 = temp2.toLowerCase();
					// System.out.println("temp=" + temp2);
					// delete "."
					// 일단 특수문자 제거
					
					
					// Indexing
					if (!indexIS.containsValue(temp)) {
						indexIS.put(indexCnt, temp);
						indexSI.put(temp, indexCnt);
						// get the id of this input word : case 1	
						currentId=indexCnt;
						indexCnt++;
					}else{
						// get the id of this input word : case 2
						currentId=indexSI.get(temp);
					}
					
					
					
//					if (!docMap.containsKey(currentId)) {
//						docMap.put(currentId, 1.0);
//					} else{
//						cnt = docMap.get(currentId) + 1;
//						docMap.put(currentId, cnt);
//					}


				}
//				System.out.println(docMap);
				//tree <- has
//				tMap.putAll(docMap);
//				map.add(tMap);
			
			}
//			System.out.println("max value is "+max);
			
			//double 로 하는구간 만들기
			
			int check=0;
			
			

			// System.out.println(indexIS);

			// System.out.println(docMap);
			// write
			// i is doc cnt
			int testcnt=1;
			
	
			// i+":"+

			// for(int j=0;j<map.get(i).size();j++){
			// // map.get(i).g
			//
			// index.
			// }

			// Index print out
			try {
				// //////////////////////////////////////////////////////////////
				BufferedWriter out_index = new BufferedWriter(new FileWriter(index_outputFile));
				// for fake
				String outputdoc = "";
				for (int i = 1; i <= indexIS.size(); i++) {
					// j is index of docMap
					outputdoc += i + "," + indexIS.get(i);
					outputdoc += "\n";
				}

//				System.out.print(outputdoc);
				out_index.write(outputdoc);
				// out_index.newLine();
				// out.write(s); out.newLine();
				out_index.close();
				// //////////////////////////////////////////////////////////////
			} catch (IOException e) {
				System.err.println(e); 
				System.exit(1);
			}
			
			

			br.close();
		} catch (IOException e) {
			System.err.println(e);
			System.exit(1);
			
		}

	}

}
