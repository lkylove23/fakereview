package dataSetting;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;
import java.util.TreeMap;

public class addTDsimple_NgramforSvm2_fast {

	
	/**유닛별 nomalise 해줄것!!!!!!!!!!!!!!!!!!!!! 하고이거 지우기***/
	public static void main(String args[]) {

		
		
//		String where = "E:\\FakeReviewDetectionData";
		String where = "D:\\KY\\";
		
		String line = "";
		String cvsSplitBy = ",";
//		Map tm = new TreeMap();
		ArrayList tm= new ArrayList();
		ArrayList<String> fromTD= new ArrayList();

		HashMap hm= new HashMap();
		
		int million=1000000;
		//max cnt =31 이었음
		//int maxCnt = 31;
		
		
		// String path = Maketrain.class.getResource("").getPath();
		
		
		 ArrayList<String> listOfFold= new ArrayList<String>();
		 listOfFold.add("fold1");
		 listOfFold.add("fold2"); 
		 listOfFold.add("fold3"); 
		 listOfFold.add("fold4"); 
		 listOfFold.add("fold5");
		 ArrayList<String> listOfFold2= new ArrayList<String>();
		 listOfFold2.add("fold2,3,4,5");	
		 listOfFold2.add("fold1,3,4,5");	
		 listOfFold2.add("fold1,2,4,5");	
		 listOfFold2.add("fold1,2,3,5");	
		 listOfFold2.add("fold1,2,3,4");	
		 
//			String categories ="hotels";
			String categories ="restaurants";
//			String categories ="electronics";
//			String categories ="fashion";
//			String categories ="hospitals";
//			String categories ="musicvenues";
//			String categories ="insurance";
		 
		 
//		String mode = "Unigram";
		String mode ="Bigram";

		//전체불러와서 인덱싱후 돌리는거로 어짜피 시간 많이 안결러서 ㅎ

		//2에서는 그냥 fold.csv 불러와서 처리함
//		String input = where+"\\Yelp\\"+categories+"\\BalancedForLDA\\";
		
		String input_Bigram = where+"FakeReviewDetectionData\\Yelp\\"+categories+"\\BalancedForSVMLight\\restaurants1";
		String input_TDsimple = where+"FakeReviewDetectionLDA\\Yelp\\";
//		String input = where+"\\Ott\\positive_polarity\\";
		
		
//		String inputTotalFile = where+"Yelp\\"+categories+"\\input\\positive\\"	+ "fold_total.txt";
//		String inputFile_f = where+"\\input\\positive\\"
//				+ test+"_f.txt";
//		String inputFile_t = where+"\\input\\positive\\"
//				+ test+"_t.txt";
//		String outputFile = where+"\\Yelp\\"+categories+"\\BalancedForSVMLight\\";
		String outputFile = where+"FakeReviewDetectionData\\Yelp\\"+categories+"\\BalancedForSVMLight\\restaurants1\\TDsimple";
//		String extra_outputFile = where+"\\input\\positive\\N-gram\\"
//				+ mode + "\\" + test+"_"+extraMode+".dat";
//		String index_outputFile = where+"\\Yelp\\"+categories+"\\BalancedForSVMLight\\"+categories+"_"+mode+"_Index.txt";
		String index_outputFile = where+"\\Yelp\\"+categories+"\\BalancedForSVMLight2\\"+categories+"_"+mode+"_Index.txt";

		double cnt=0;
		int currentId;
		// LinkedHashMap<String, Integer> map = new LinkedHashMap<String,
		// Integer>();
//		ArrayList<TreeMap<Integer, Double>> map_fold_t = new ArrayList<TreeMap<Integer, Double>>();
		HashMap<Integer, String> indexIS = new HashMap<Integer, String>();
		HashMap<String, Integer> indexSI = new HashMap<String, Integer>();
		ArrayList<String> testAL = new ArrayList<String>();

		String fold1="";
		String fold2="";
		String fold3="";
		String fold4="";
		String fold5="";
//		array list로 하면됨!
		//		testAL.
		int indexCnt = 1;
		// HashMap<String, Integer> map = new HashMap<String, Integer>();
		
		File ForCheck_category = new File(outputFile);
		if(!ForCheck_category.isDirectory()){
			ForCheck_category.mkdir();
		}
		
		try {
			//load bigram
			
			
			
			
			//for each csv file	
//			for(int foldIndex=0;foldIndex<listOfFold.size();foldIndex++){
//			String inputFile = input+categories+"_"+listOfFold.get(foldIndex)+".csv";
//			BufferedReader br = new BufferedReader(new FileReader(inputFile));
			//test for max value
			String temp;
			double max = 1;
			int linenum=0;
			
			String preTemp;
			String nGramTemp;
			StringTokenizer st;


			//double 로 하는구간 만들기
			
			int check=0;
			String test="";
			String train="";
			/** for fold_t **/
			for(int it=0;it<5;it++){
				System.out.println(it);
				if(it==0){
					test ="fold1";
					train="fold2,3,4,5";
				}else if(it==1){
					 test ="fold2";
					 train="fold1,3,4,5";
				}else if(it==2){
					 test ="fold3";		
					 train="fold1,2,4,5";
				}else if(it==3){
					 test ="fold4";	
					 train="fold1,2,3,5";
				}else if(it==4){
					 test ="fold5";		
					 train="fold1,2,3,4";
				}
				String input_TDFile = input_TDsimple+"\\"+categories+"_"+train+"_100\\"+categories+"_"+test+"-document-topic-distributuions.csv";
				BufferedReader br_td = new BufferedReader(new FileReader(input_TDFile));
				while ((line = br_td.readLine()) != null) {
					String[] tmp=line.split(",");
					String save ="";
							for(int c=0;c<100;c++){
								save+=(million+c)+":"+tmp[c+1]+" ";
							}
					fromTD.add(save);
				}
				String input_BigramFile = input_Bigram+"\\"+categories+"_Bigram_"+test+".dat";
				BufferedReader br = new BufferedReader(new FileReader(input_BigramFile));
				int cc=0;
				while ((line = br.readLine()) != null) {
					if(it==0){
						fold1+=line+fromTD.get(cc)+"\n";
					}else if(it==1){
						fold2+=line+fromTD.get(cc)+"\n";
					}else if(it==2){
						fold3+=line+fromTD.get(cc)+"\n";
					}else if(it==3){
						fold4+=line+fromTD.get(cc)+"\n";
					}else if(it==4){
						fold5+=line+fromTD.get(cc)+"\n";
					}
					cc++;
				}
				/**write file**/
				
				try {
					BufferedWriter out = new BufferedWriter(new FileWriter(outputFile+"\\"+categories+"_Bigram_TDsimple_"+test+".dat"));
					if(it==0){
						out.write(fold1);
					}else if(it==1){
						out.write(fold2);
					}else if(it==2){
						out.write(fold3);
					}else if(it==3){
						out.write(fold4);
					}else if(it==4){
						out.write(fold5);
					}
					
					out.close();
				} catch (IOException e) {
					System.err.println(e); // 에러가 있다면 메시지 출력
					System.exit(1);
				}
				
			}
			System.out.println("test complete");
			for(int it=0;it<5;it++){
				if(it==0){
					test ="fold1";
					train="fold2,3,4,5";
				}else if(it==1){
					 test ="fold2";
					 train="fold1,3,4,5";
				}else if(it==2){
					 test ="fold3";		
					 train="fold1,2,4,5";
				}else if(it==3){
					 test ="fold4";	
					 train="fold1,2,3,5";
				}else if(it==4){
					 test ="fold5";		
					 train="fold1,2,3,4";
				}
				BufferedWriter out = new BufferedWriter(new FileWriter(outputFile+"\\"+categories+"_Bigram_TDsimple_"+train+".dat"));
				
				if(it==0){
					out.write(fold2);
					out.write(fold3);
					out.write(fold4);
					out.write(fold5);
				}else if(it==1){
					out.write(fold1);
					out.write(fold3);
					out.write(fold4);
					out.write(fold5);

				}else if(it==2){
					out.write(fold1);
					out.write(fold2);
					out.write(fold4);
					out.write(fold5);
				}else if(it==3){
					out.write(fold1);
					out.write(fold2);
					out.write(fold3);
					out.write(fold5);
				}else if(it==4){
					out.write(fold1);
					out.write(fold2);
					out.write(fold3);
					out.write(fold4);
				}
				out.close();	
			}
		
			// Index print out
			
		} catch (IOException e) {
			System.err.println(e);
			System.exit(1);
			
		}

	}

}
