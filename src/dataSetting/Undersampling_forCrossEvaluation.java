package dataSetting;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class Undersampling_forCrossEvaluation {
	public static void main(String args[]) throws IOException {

		String line = "";
		String cvsSplitBy = ",";

		ArrayList tm= new ArrayList();
		HashMap hm= new HashMap();
		String output ="";
		String output1 ="";
		String output2 ="";
		String output3 ="";
		String output4 ="";
		String output5 ="";
		/**Choose**/
		//KY 
		String where="E:\\FakeReviewDetectionData";
		//or Dear IRbig2?
//		String where="D:\\KY\\FakeReviewDetectionData";

		
		/** Choose **/
		String location="Chicago";	

		String categories ="electronics";
//		String categories ="fashion";
//		String categories ="hospitals";		
//		String categories ="hotels";
//		String categories ="insurance";
//		String categories ="musicvenues";
//		String categories ="restaurants";





		
		 String test ="fold1";
//		 String test ="fold2";
//		 String test ="fold3";
//		 String test ="fold4";
//		 String test ="fold5";

//		 String test ="fold2,3,4,5";	
//		 String test ="fold1,3,4,5";
//		 String test ="fold1,2,4,5";
//		 String test ="fold1,2,3,5";
//		 String test ="fold1,2,3,4";
		String mode = "Unigram";
		// String mode ="Bigram";

		//전체불러와서 인덱싱후 돌리는거로 어짜피 시간 많이 안결러서 ㅎ
		
		//t,f 각각 불러와서 처리후 합쳐줌

//		String inputFile_f = where+"\\input\\positive\\"
//				+ test+"_f.txt";
		String inputFile_f = where+"\\Yelp\\"+categories+"\\F";
		String inputFile_t = where+"\\Yelp\\"+categories+"\\T";
//		String inputFile_t = where+"\\input\\positive\\"
//				+ test+"_t.txt";
//		String resultFile=where+"\\Yelp\\"+categories+"\\BalancedForLDA2";
		String resultFile=where+"\\Yelp\\"+categories+"\\BalancedForCross100100";
		
		inputFile_f +="\\5_star";
		inputFile_t +="\\5_star";
		
	
		HashMap<String,Integer> tag =new HashMap<String,Integer>();
		
		File ForCheck = new File(resultFile);
		if(!ForCheck.isDirectory()){
			ForCheck.mkdir();
		}
		// E:\FakeReviewDetectionData\Yelp\insurance\F\5_star
		File Folder_f = new File(inputFile_f);
		File Folder_t = new File(inputFile_t);
		

		File [] arrFile_f = Folder_f.listFiles();
		File [] arrFile_t = Folder_t.listFiles();

//			불러와서 매칭 맞으면 /balanced 해서 저장
//			fake 는 그냥이기 때문에 상관없음 그래도 balanced에 저장?
//			일단 비지니스 이름으로 저장만 ㅎ
			/**비지니스이름, fake 리뷰 수**/
			HashMap<String,Integer> NameCnt_f=new HashMap<String,Integer>();
			HashMap<String,Integer> NameCnt_t=new HashMap<String,Integer>();
			HashMap<String,Integer> NameCntForBal=new HashMap<String,Integer>();
			HashMap<String,String> tempDocs_f=new HashMap<String,String>();
			HashMap<String,String> tempDocs_t=new HashMap<String,String>();
			//제외할 비지니스(0개 리뷰)
			String outForTemp="";
			
			int numOfDoc;
			int forCnt=1;
		try{
			BufferedReader br = null;
			NameCnt_f.clear();
			/**for each fake business**/
			
		for (File file : arrFile_f) {
			System.out.println("setting f\t"+forCnt+"of"+arrFile_f.length);
			forCnt++;
			outForTemp="";
			numOfDoc=0;
			br = new BufferedReader(new FileReader(file));
			String BusName="";
			while( (line =br.readLine()) != null){
				numOfDoc++;
				String[] tempArray2 = line.split(cvsSplitBy);
				outForTemp+=line+"\n";
				BusName=tempArray2[0];
			}
			if(numOfDoc<5){
				continue;
			}
			NameCnt_f.put(BusName, numOfDoc);
			tempDocs_f.put(BusName, outForTemp);
		}
		forCnt=1;
		for (File file : arrFile_t) {
			System.out.println("setting t\t"+forCnt+"of"+arrFile_t.length);
			forCnt++;
			outForTemp="";
			numOfDoc=0;
			br = new BufferedReader(new FileReader(file));
			String BusName="";
			while( (line =br.readLine()) != null){
				numOfDoc++;
				String[] tempArray2 = line.split(cvsSplitBy);
				outForTemp+=line+"\n";
				BusName=tempArray2[0];
			}
			if(numOfDoc<5){
				//remove
				continue;
			}
			if(NameCnt_f.containsKey(BusName)){
			NameCnt_t.put(BusName, numOfDoc);
			tempDocs_t.put(BusName, outForTemp);
			}	
		}
		
		 for( String key : NameCnt_f.keySet() ){
	        
		System.out.println(NameCnt_f.get(key));
		System.out.println(NameCnt_t.get(key));
			if((NameCnt_f.get(key)==null)|(NameCnt_t.get(key)==null)){
				continue;
				}
					if(NameCnt_f.get(key)<NameCnt_t.get(key)){
				NameCntForBal.put(key,NameCnt_f.get(key));
				tag.put(key,1);
			}else{
				NameCntForBal.put(key,NameCnt_t.get(key));
				tag.put(key,2);
			}
		}
		/**choosing count**/
		
		
//		System.out.println(NameCnt.size());
		System.out.println("Busnumber"+NameCntForBal.size());

		forCnt=0;
		ArrayList<Integer> random115 = new ArrayList<Integer>();
//		make random
		while(random115.size()<20){
			int randN =(int) (Math.random() * 20)+1;
			if(!random115.contains(randN)){
			random115.add(randN);
			}
		}
		
		/**for each truthful business**/
		
		int numOfDoc_t=0;
		for (File file : arrFile_t) {
			System.out.println("saving\t"+forCnt+"of"+arrFile_t.length);
			
			output="";
			br = new BufferedReader(new FileReader(file));
			numOfDoc=0;
			String busName="";
			int numOfDoc_f=0;
//			size 잰다음에 랜덤으로 뽑기 같아질때 까지
			while( (line =br.readLine()) != null){
				numOfDoc++;
				String[] tempArray2 = line.split(cvsSplitBy);
				busName=tempArray2[0];
//				System.out.println("Busname:\t"+BusName);
			}
			System.out.println(tag.get(busName));
			if(!tag.containsKey(busName)){
				continue;
			}
			forCnt++;
			if(!random115.contains(forCnt)){
				continue;
			}
				System.out.println("reduce truthful");
				ArrayList<Integer> randomAL = new ArrayList<Integer>();
//				make random
				while(randomAL.size()<5){
					int randN =(int) (Math.random() * numOfDoc) + 1;
					if(!randomAL.contains(randN)){
					randomAL.add(randN);
					}
				}
				System.out.println("busName:"+busName);
//				System.out.println(randomAL);
				br = new BufferedReader(new FileReader(file));
				//random number에 맞는 review 추출
				numOfDoc=0;
				while( (line =br.readLine()) != null){
					numOfDoc++;
					if(randomAL.contains(numOfDoc)){
						numOfDoc_t++;
//						if(numOfDoc_t<=115){
//							output1+=line+"\n";
//						}else if(115<numOfDoc_t&&numOfDoc_t<=230){
//							output2+=line+"\n";
//						}else if(230<numOfDoc_t&&numOfDoc_t<=345){
//							output3+=line+"\n";
//						}else if(345<numOfDoc_t&&numOfDoc_t<=460){
//							output4+=line+"\n";
//						}else{
//							output5+=line+"\n";
//						}
						output=line+"\n";
					}
				}
//				output=output.trim();
//				System.out.println(output);
//				FileWriter writer = new FileWriter(resultFile+"\\"+busName+".csv");
//				writer.append(output);
//				writer.close();
				//fake는 이름 바꿔서 복사
				
		}
				
				/**for each fake business**/
				System.out.println("sample fake");
				forCnt=0;
				int numOfDoc_f=0;
				for (File file : arrFile_f) {
				
					br = new BufferedReader(new FileReader(file));
					numOfDoc=0;
					String busName="";
					
//					size 잰다음에 랜덤으로 뽑기 같아질때 까지
					while( (line =br.readLine()) != null){
						numOfDoc++;
						String[] tempArray2 = line.split(cvsSplitBy);
						busName=tempArray2[0];
//						System.out.println("Busname:\t"+BusName);
					}
					if(!tag.containsKey(busName)){
						continue;
					}
					forCnt++;
					if(!random115.contains(forCnt)){
						continue;
					}
						ArrayList<Integer> randomAL = new ArrayList<Integer>();
//						make random
						while(randomAL.size()<5){
							int randN =(int) (Math.random() * numOfDoc) + 1;
							if(!randomAL.contains(randN)){
							randomAL.add(randN);
							}
						}
						br = new BufferedReader(new FileReader(file));
						//random number에 맞는 review 추출
						numOfDoc=0;
						while( (line =br.readLine()) != null){
							numOfDoc++;
							if(randomAL.contains(numOfDoc)){
								numOfDoc_f++;
//								if(numOfDoc_f<=115){
//									output1+=line+"\n";
//								}else if(115<numOfDoc_f&&numOfDoc_f<=230){
//									output2+=line+"\n";
//								}else if(230<numOfDoc_f&&numOfDoc_f<=345){
//									output3+=line+"\n";
//								}else if(345<numOfDoc_f&&numOfDoc_f<=460){
//									output4+=line+"\n";
//								}else if(460<numOfDoc_f&&numOfDoc_f<=575){
//									output5+=line+"\n";
//								}
								output=line+"\n";
							}
						}
//						output=output.trim();
//						System.out.println(output);
//						FileWriter writer = new FileWriter(resultFile+"\\"+busName+".csv");
//						writer.append(output);
//						writer.close();
						//fake는 이름 바꿔서 복사
						
				}
//				output1=output1.trim();
//				output2=output2.trim();
//				output3=output3.trim();
//				output4=output4.trim();
//				output5=output5.trim();
				output=output.trim();
				
//				FileWriter writer1 = new FileWriter(resultFile+"\\"+categories+"_fold1.csv");
//				writer1.append(output1);
//				writer1.close();
//				FileWriter writer2 = new FileWriter(resultFile+"\\"+categories+"_fold2.csv");
//				writer2.append(output2);
//				writer2.close();
//				FileWriter writer3 = new FileWriter(resultFile+"\\"+categories+"_fold3.csv");
//				writer3.append(output3);
//				writer3.close();
//				FileWriter writer4 = new FileWriter(resultFile+"\\"+categories+"_fold4.csv");
//				writer4.append(output4);
//				writer4.close();
//				FileWriter writer5 = new FileWriter(resultFile+"\\"+categories+"_fold5.csv");
//				writer5.append(output5);
//				writer5.close();
				FileWriter writer = new FileWriter(resultFile+"\\"+categories+".csv");
				writer.append(output);
				writer.close();
				
				
		
			
		
		
		}catch(FileNotFoundException e){
			e.printStackTrace();
		}
		}
		




	

}
	

