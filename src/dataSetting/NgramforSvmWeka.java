package dataSetting;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeMap;

public class NgramforSvmWeka {
	
	


	public static void main(String args[]) {

		String line = "";
		String cvsSplitBy = ",";
//		Map tm = new TreeMap();
		ArrayList tm= new ArrayList();
		HashMap hm= new HashMap();
		
		//max cnt =31 이었음
		//int maxCnt = 31;
		
		
		// String path = Maketrain.class.getResource("").getPath();
		
		 String test ="fold1";
//		 String test ="fold2";
//		 String test ="fold3";
//		 String test ="fold4";
//		 String test ="fold5";

//		 String test ="fold2,3,4,5";	
//		 String test ="fold1,3,4,5";
//		 String test ="fold1,2,4,5";
//		 String test ="fold1,2,3,5";
//		 String test ="fold1,2,3,4";

		String mode = "Unigram";
		// String mode ="Bigram";

		//전체불러와서 인덱싱후 돌리는거로 어짜피 시간 많이 안결러서 ㅎ
		
		//t,f 각각 불러와서 처리후 합쳐줌

		String inputFile_f = "E:\\FakeReviewDetectionData\\input\\positive\\"
				+ test+"_f.txt";
		String inputFile_t = "E:\\FakeReviewDetectionData\\input\\positive\\"
				+ test+"_t.txt";
		String outputFile = "E:\\FakeReviewDetectionData\\input\\positive\\N-gram\\"
				+ mode + "\\" + test+".arff";

		String index_inputFile = "E:\\FakeReviewDetectionData\\input\\positive\\N-gram\\"
				+ mode + "\\Index.txt";
		
		String outputString="@RELATION review\n";
		double cnt=0;
		int currentId;
		// LinkedHashMap<String, Integer> map = new LinkedHashMap<String,
		// Integer>();
		ArrayList<TreeMap<Integer, Double>> map = new ArrayList<TreeMap<Integer, Double>>();
		ArrayList<TreeMap<Integer, Double>> map_fold_t = new ArrayList<TreeMap<Integer, Double>>();
		ArrayList<TreeMap<Integer, Double>> map_fold_f = new ArrayList<TreeMap<Integer, Double>>();
		HashMap<Integer, String> indexIS = new HashMap<Integer, String>();
		HashMap<String, Integer> indexSI = new HashMap<String, Integer>();
		HashMap<Integer, Double> docMap_fold = new HashMap<Integer, Double>();
//		TreeMap<Integer, Integer> tMap_fold_t = new TreeMap<Integer, Integer>();
		TreeMap<Integer, Double> tMap_fold = new TreeMap<Integer, Double>();
		
		
		
		int indexCnt = 1;
		// HashMap<String, Integer> map = new HashMap<String, Integer>();
		try {

			BufferedReader br = new BufferedReader(new FileReader(index_inputFile));
			
			// 사실 토탈에 대해서는 인덱싱만 해주면 된다
			
			//test for max value
			
			double max = 0;
			
			/**Loading index file**/
			
			while ((line = br.readLine()) != null) {
				// use comma as separator
				
				//docMap <word id , word count>

				String tmp;

				StringTokenizer st = new StringTokenizer(line, " ,");
				
				while (st.hasMoreTokens()) {
					String temp = st.nextToken();
					String temp2 = st.nextToken();
					
					indexSI.put(temp2, Integer.parseInt(temp));
				}
			}
//			System.out.println("max value is "+max);
			/**head setting**/
			for( String key : indexSI.keySet() ){
				outputString+="@ATTRIBUTE\t"+key+"\tNUMERIC\n";
	        }
			outputString+="@ATTRIBUTE\tclass\t{truthful,fake}\n";
			outputString+="@DATA\n";
			
			
			//double 로 하는구간 만들기
			
			int check=0;
			
			
			/** for fold_t **/
			
			BufferedReader br_fold_t = new BufferedReader(new FileReader(inputFile_t));
			
			while ((line = br_fold_t.readLine()) != null) {
				// use comma as separator
				tMap_fold.clear();
				docMap_fold.clear();
				check++;
				//docMap <word id , word count>
//				HashMap<Integer, Integer> docMap_fold_t = new HashMap<Integer, Integer>();
				String tmp;

				line = line.replace(".", "");
				line = line.replace("!", "");
				line = line.replace("(", "");
				line = line.replace(")", "");
				line = line.replace("$", "");
				line = line.replace("-", "");
				line = line.replace("/", " ");
				line = line.replace("~", " ");
				line = line.replace("?", "");
				line = line.replace("\"", "");
				
//				String match = "[^\uAC00-\uD7A3xfe0-9a-zA-Z\\s]";
//			     line =line.replaceAll(match, "");
				line = line.toLowerCase();
				
				StringTokenizer st_t = new StringTokenizer(line, " ,");
				
				max=0;
				while (st_t.hasMoreTokens()) {
					max++;
					String temp = st_t.nextToken(); // 토근을 temp 변수에 저장
					currentId=indexSI.get(temp);
				
					// input term frequencies
					if (!docMap_fold.containsKey(currentId)) {
						docMap_fold.put(currentId, 1.0);
					} else{
//			
						cnt = (docMap_fold.get(currentId) + 1.0);
						docMap_fold.put(currentId, cnt);
					}
				}
				/**Normalization**/
				for( Integer key : docMap_fold.keySet() ){
		           docMap_fold.put(key, docMap_fold.get(key)/max);
				}
//				System.out.println(docMap);
				//tree <- has
				tMap_fold.putAll(docMap_fold);
				
				for( Integer key : tMap_fold.keySet() ){
			        outputString+=key+" "+tMap_fold.get(key)+", ";
					}
				outputString+= indexSI.size()+" truthful\n";
			}
			
			
			/**for forld_f**/
			BufferedReader br_fold_f = new BufferedReader(new FileReader(inputFile_f));
			
			while ((line = br_fold_f.readLine()) != null) {
				// use comma as separator
				
				//docMap <word id , word count>
				docMap_fold.clear();
				tMap_fold.clear();
				
				String tmp;
				
//				String match = "[^\uAC00-\uD7A3xfe0-9a-zA-Z\\s]";
//			      cols[4] =tmp.replaceAll(match, " ");
//				tmp = tmp.replace("`s", "");
//				tmp = tmp.replace("'s", "");
//				tmp = tmp.replace("", "");
//				String match = "[^\uAC00-\uD7A3xfe0-9a-zA-Z\\s]";
//			      tmp =tmp.replaceAll(match, " ");
				
//				//
//				line = line.replace("'s", "");
//				line = line.replace("n't", " not");
				line = line.replace(".", "");
				line = line.replace("!", "");
				line = line.replace("(", "");
				line = line.replace(")", "");
				line = line.replace("$", "");
				line = line.replace("-", "");
				line = line.replace("/", " ");
				line = line.replace("~", " ");
				line = line.replace("?", "");
				line = line.replace("\"", "");
				
//				String match = "[^\uAC00-\uD7A3xfe0-9a-zA-Z\\s]";
//			     line =line.replaceAll(match, "");
				line = line.toLowerCase();
				
				StringTokenizer st_f = new StringTokenizer(line, " ,");
				
				max=0;
				while (st_f.hasMoreTokens()) {
					max++;
					String temp = st_f.nextToken(); // 토근을 temp 변수에 저장
					currentId=indexSI.get(temp);
					if (!docMap_fold.containsKey(currentId)) {
						docMap_fold.put(currentId, 1.0);
					} else{
//			
						cnt = (docMap_fold.get(currentId) + 1.0);
						docMap_fold.put(currentId, cnt);
					}
//						
					}
				/**Normalization**/
				for( Integer key : docMap_fold.keySet() ){
		           docMap_fold.put(key, docMap_fold.get(key)/max);
				}
//				System.out.println(docMap);
				//tree <- has
				tMap_fold.putAll(docMap_fold);
				
				for( Integer key : tMap_fold.keySet() ){
			        outputString+=key+" "+tMap_fold.get(key)+", ";
					}
				outputString+= indexSI.size()+" fake\n";
	
			}
			// System.out.println(indexIS);

			// System.out.println(docMap);
			// write
			// i is doc cnt
			int testcnt=1;
			
			/**write file**/
			
			try {
				// //////////////////////////////////////////////////////////////
				BufferedWriter out = new BufferedWriter(new FileWriter(outputFile));
				String output;
				
				out.write(outputString);
				out.close();

				// //////////////////////////////////////////////////////////////
			} catch (IOException e) {
				System.err.println(e); // 에러가 있다면 메시지 출력
				System.exit(1);
			}
			

			br.close();
		} catch (IOException e) {
			System.err.println(e);
			System.exit(1);
			
		}

	}

}
