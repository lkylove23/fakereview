package dataSetting;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;

public class Undersampling_numOfReviews_txt {
	public static void main(String args[]) throws IOException {
        /**Choose**/
        //KY
        String where = "D:\\FakeReviewDetectionData";
        //or Dear IRbig2?
//		String where="D:\\KY\\FakeReviewDetectionData";
        /** Choose **/
//          int totalNumOfReview = 35000;
//          int totalNumOfReview = 20000;

//        int numOfReview = 1000;
//        int numOfReview = 200;
//        int numOfReview = 300;
        int numOfReview = 130; // use for experiments
//          int totalNumOfReview = 200;
//          int totalNumOfReview = 200;
        int totalNumOfReviews=0; // in corpus

//        String s_itr = "";
//	    String s_itr=" (2)";
//        String s_itr=" (3)";
	    String s_itr=" (4)";
//	    String s_itr=" (5)";


//		String categories ="hotels";
        String categories = "";
//		String categories ="electronics";
//		String categories ="fashion";
//		String categories ="hospitals";
//		String categories ="musicvenues";
//		String categories ="insurance";

        String test = "fold1";
//		 String test ="fold2";
//		 String test ="fold3";
//		 String test ="fold4";
//		 String test ="fold5";

//		 String test ="fold2,3,4,5";	
//		 String test ="fold1,3,4,5";
//		 String test ="fold1,2,4,5";
//		 String test ="fold1,2,3,5";
//		 String test ="fold1,2,3,4";


        //��ü�ҷ��ͼ� �ε����� �����°ŷ� ��¥�� �ð� ���� �Ȱᷯ�� ��
//        for (int c_it = 0; c_it < 7; c_it++) {
        for (int c_it = 4; c_it < 5; c_it++) {
            if (c_it == 0) {
                if(numOfReview>440){
                    continue;
                }
                //Electronics
//                changeID = 44;
                categories = "electronics";
                totalNumOfReviews= 440;
            } else if (c_it == 1) {
                if(numOfReview>2690){
                    continue;
                }
                //Fashion
                categories = "fashion";
                totalNumOfReviews= 2690;
            } else if (c_it == 2) {
                if(numOfReview>=200){
                    continue;
                }
                //hospitals
                categories = "hospitals";
                totalNumOfReviews= 200;
            } else if (c_it == 3) {
                if(numOfReview>1100){
                    continue;
                }
                //hotels
                categories = "hotels";
                totalNumOfReviews= 1100;

            }
            else if (c_it == 4) {
                if(numOfReview>130){
                    continue;
                }
                //Insurance
                categories = "insurance";
                totalNumOfReviews= 130;
            }
            else if (c_it == 5) {
                if(numOfReview>1190){
                    continue;
                }
                //Musicvenues
                categories = "musicvenues";
                totalNumOfReviews= 1190;
            } else if (c_it == 6) {
                if(numOfReview>37980){
                    continue;
                }
                //Restaurant
//                totalNumOfReview = 37990;
                categories = "restaurants";
                totalNumOfReviews= 37990;
            }
            String line = "";
            String cvsSplitBy = ",";
            ArrayList tm = new ArrayList();
            HashMap hm = new HashMap();
            String Fout1 = "";
            String Fout2 = "";
            String Fout3 = "";
            String Fout4 = "";
            String Fout5 = "";
            String Tout1 = "";
            String Tout2 = "";
            String Tout3 = "";
            String Tout4 = "";
            String Tout5 = "";
            //t,f ���� �ҷ��ͼ� ó���� ������


//		String inputFile_f = where+"\\input\\positive\\"
//				+ test+"_f.txt";
            String inputFile_f = where + "\\Yelp\\" + categories + "\\F";
            String inputFile_t = where + "\\Yelp\\" + categories + "\\T";
//		String inputFile_t = where+"\\input\\positive\\"
//				+ test+"_t.txt";
            String resultFile = where + "\\Yelp\\numOfReviews\\" + categories + "_" + numOfReview + s_itr;
            inputFile_f += "\\5_star";
            inputFile_t += "\\5_star";
            int docID = 0;
            ArrayList<String> tempSL = new ArrayList<String>();
            ArrayList<String> selectedBiz = new ArrayList<String>();
            ArrayList<String> selectedBizfold1 = new ArrayList<String>();
            ArrayList<String> selectedBizfold2 = new ArrayList<String>();
            ArrayList<String> selectedBizfold3 = new ArrayList<String>();
            ArrayList<String> selectedBizfold4 = new ArrayList<String>();
            ArrayList<String> selectedBizfold5 = new ArrayList<String>();

            HashMap<String, Integer> tag = new HashMap<String, Integer>();

            File ForCheck = new File(resultFile);
            if (!ForCheck.isDirectory()) {
                ForCheck.mkdir();
            }

            try {
                BufferedReader br = null;
                /**choosing count**/

                ArrayList<Integer> randomForSelection = new ArrayList<Integer>();
                /**    make random**/
                while (randomForSelection.size() < numOfReview / 2) {
                    int randN = (int) (Math.random() * totalNumOfReviews/2);
                    if (!randomForSelection.contains(randN)) {
                        randomForSelection.add(randN);
                        System.out.println(randN);
                    }
                }
                br = new BufferedReader(new FileReader("D:\\FakeReviewDetectionData\\Yelp\\"+categories+"\\T\\BalancedForLDA\\T_"+categories+"_total.csv"));
                int lineNum = 0;
                int check = 0;
                while ((line = br.readLine()) != null) {
                    if (randomForSelection.contains(lineNum)) {
                        String[] tempArray2 = line.split(",");
                        String tempcontent = tempArray2[4].replace("\n", " ").replace("?", " ").replace("|", ",").replace("\"", " ").replace("`", " ").replace("\'", " ").replace("'", " ").replace("-", " ").replace(".", " ").replace("!", " ");
                        if (check < numOfReview / 10) {
                            Tout1 += check + "\tt\t" + tempcontent + "\n";
                        } else if (numOfReview / 10 <= check && check < numOfReview / 10 * 2) {
                            Tout2 += check + "\tt\t" + tempcontent + "\n";
                        } else if (numOfReview / 10 * 2 <= check && check < numOfReview / 10 * 3) {
                            Tout3 += check + "\tt\t" + tempcontent + "\n";
                        } else if (numOfReview / 10 * 3 <= check && check < numOfReview / 10 * 4) {
                            Tout4 += check + "\tt\t" + tempcontent + "\n";
                        } else if (numOfReview / 10 * 4 <= check) {
                            Tout5 += check + "\tt\t" + tempcontent + "\n";
                        }
                        check++;
                    }
                    lineNum++;
                }
                br = new BufferedReader(new FileReader("D:\\FakeReviewDetectionData\\Yelp\\"+categories+"\\F\\BalancedForLDA\\F_"+categories+"_total.csv"));
                lineNum = 0;
                check = 0;
                while ((line = br.readLine()) != null) {
                    if (randomForSelection.contains(lineNum)) {
                        String[] tempArray2 = line.split(",");
                        String tempcontent = tempArray2[4].replace("\n", " ").replace("?", " ").replace("|", ",").replace("\"", " ");
                        if (check < numOfReview / 10) {
                            Fout1 += check + "\tf\t" + tempcontent + "\n";
                        } else if (numOfReview / 10 <= check && check < numOfReview / 10 * 2) {
                            Fout2 += check + "\tf\t" + tempcontent + "\n";
                        } else if (numOfReview / 10 * 2 <= check && check < numOfReview / 10 * 3) {
                            Fout3 += check + "\tf\t" + tempcontent + "\n";
                        } else if (numOfReview / 10 * 3 <= check && check < numOfReview / 10 * 4) {
                            Fout4 += check + "\tf\t" + tempcontent + "\n";
                        } else if (numOfReview / 10 * 4 <= check) {
                            Fout5 += check + "\tf\t" + tempcontent + "\n";
                        }
                        check++;
                    }
                    lineNum++;
                }

                Tout1 = Tout1.trim();
                Tout2 = Tout2.trim();
                Tout3 = Tout3.trim();
                Tout4 = Tout4.trim();
                Tout5 = Tout5.trim();
                Fout1 = Fout1.trim();
                Fout2 = Fout2.trim();
                Fout3 = Fout3.trim();
                Fout4 = Fout4.trim();
                Fout5 = Fout5.trim();

                FileWriter writer1 = new FileWriter(resultFile + "\\" + categories + "_" + numOfReview + "_fold1.txt");
                writer1.append(Fout1);
                writer1.append("\n");
                writer1.append(Tout1);
                writer1.close();
                FileWriter writer2 = new FileWriter(resultFile + "\\" + categories + "_" + numOfReview + "_fold2.txt");
                writer2.append(Fout2);
                writer2.append("\n");
                writer2.append(Tout2);
                writer2.close();
                FileWriter writer3 = new FileWriter(resultFile + "\\" + categories + "_" + numOfReview + "_fold3.txt");
                writer3.append(Fout3);
                writer3.append("\n");
                writer3.append(Tout3);
                writer3.close();
                FileWriter writer4 = new FileWriter(resultFile + "\\" + categories + "_" + numOfReview + "_fold4.txt");
                writer4.append(Fout4);
                writer4.append("\n");
                writer4.append(Tout4);
                writer4.close();
                FileWriter writer5 = new FileWriter(resultFile + "\\" + categories + "_" + numOfReview + "_fold5.txt");
                writer5.append(Fout5);
                writer5.append("\n");
                writer5.append(Tout5);
                writer5.close();

                FileWriter writer = new FileWriter(resultFile + "\\" + categories + "_" + numOfReview + "_fold1,2,3,4.txt");
                writer.append(Fout1 + "\n" + Fout2 + "\n" + Fout3 + "\n" + Fout4);
                writer.append("\n");
                writer.append(Tout1 + "\n" + Tout2 + "\n" + Tout3 + "\n" + Tout4);
                writer.close();
                writer = new FileWriter(resultFile + "\\" + categories + "_" + numOfReview + "_fold1,2,3,5.txt");
                writer.append(Fout1 + "\n" + Fout2 + "\n" + Fout3 + "\n" + Fout5);
                writer.append("\n");
                writer.append(Tout1 + "\n" + Tout2 + "\n" + Tout3 + "\n" + Tout5);
                writer.close();
                writer = new FileWriter(resultFile + "\\" + categories + "_" + numOfReview + "_fold1,2,4,5.txt");
                writer.append(Fout1 + "\n" + Fout2 + "\n" + Fout4 + "\n" + Fout5);
                writer.append("\n");
                writer.append(Tout1 + "\n" + Tout2 + "\n" + Tout4 + "\n" + Tout5);
                writer.close();
                writer = new FileWriter(resultFile + "\\" + categories + "_" + numOfReview + "_fold1,3,4,5.txt");
                writer.append(Fout1 + "\n" + Fout3 + "\n" + Fout4 + "\n" + Fout5);
                writer.append("\n");
                writer.append(Tout1 + "\n" + Tout3 + "\n" + Tout4 + "\n" + Tout5);
                writer.close();
                writer = new FileWriter(resultFile + "\\" + categories + "_" + numOfReview + "_fold2,3,4,5.txt");
                writer.append(Fout2 + "\n" + Fout3 + "\n" + Fout4 + "\n" + Fout5);
                writer.append("\n");
                writer.append(Tout2 + "\n" + Tout3 + "\n" + Tout4 + "\n" + Tout5);
                writer.close();
                writer = new FileWriter(resultFile + "\\" + categories + "_" + numOfReview + "_foldTotal.txt");
                writer.append(Fout1 + "\n" + Fout2 + "\n" + Fout3 + "\n" + Fout4 + "\n" + Fout5);
                writer.append("\n");
                writer.append(Tout1 + "\n" + Tout2 + "\n" + Tout3 + "\n" + Tout4 + "\n" + Tout5);
                writer.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }

    }



	

}
	

