package dataSetting;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class ForMakingFoldWithTF {
	public static void main(String args[]) throws IOException {
		/**각 fold 별로 T,F 합치기**/
		String line="";
		String cvsSplitBy = ",";
		Boolean checkforloop= true; 
		/**Choose**/
		//KY 
				String where="E:\\FakeReviewDetectionData";
				//or Dear IRbig2?
//				String where="D:\\KY\\FakeReviewDetectionData";
//				String categories ="hotels";
				String categories ="restaurants";
//				String categories ="electronics";
//				String categories ="fashion";
//				String categories ="hospitals";
//				String categories ="musicvenues";
//				String categories ="insurance";
				String inputFile_t = where+"\\Yelp\\"+categories+"\\T\\BalancedForLDA";
				String inputFile_f = where+"\\Yelp\\"+categories+"\\F\\BalancedForLDA";
				String resultFile = where+"\\Yelp\\"+categories+"\\BalancedForLDA";
				File ForCheck = new File(resultFile);
				int docNum =0;
				
				
				String outString ="";
				String outString_total ="";
				
				if(!ForCheck.isDirectory()){
					ForCheck.mkdir();
				}
			
				try {
					for(int n=1;n<=5;n++){
					outString="";
					BufferedReader br_t = null;
					BufferedReader br_f = null;
					/** for each business **/
					String input_t=inputFile_t+"\\T_"+categories+"_fold"+n+".csv";
					String input_f=inputFile_f+"\\F_"+categories+"_fold"+n+".csv";
					File Folder_t = new File(input_t);
					File Folder_f = new File(input_f);
					
						br_t = new BufferedReader(new FileReader(Folder_t));
						br_f = new BufferedReader(new FileReader(Folder_f));
						while( (line =br_f.readLine()) != null){
							outString+=line;
							outString+="\n";
							outString_total+=line;
							outString_total+="\n";
					}
						
						while( (line =br_t.readLine()) != null){
							outString+=line;
							outString+="\n";
							outString_total+=line;
							outString_total+="\n";
					}
					
					
					FileWriter writer_n = new FileWriter(resultFile+"\\"+categories+"_fold"+n+".csv");
					writer_n.append(outString.trim());
					writer_n.close();
					
					}
					FileWriter writer = new FileWriter(resultFile+"\\"+categories+"_foldT"+".csv");
					writer.append(outString_total.trim());
					writer.close();
				} catch(FileNotFoundException e) {
					e.printStackTrace();

				}
	}
}
