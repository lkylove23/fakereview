package dataSetting;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;

public class ForAnalysisRemovingStopwords {
	public static void main(String args[]) throws IOException {
		/** 각 fold 별로 T,F 합치기 **/
		String line = "";
		String cvsSplitBy = ",";
		Boolean checkforloop = true;
		String output = "";
		
		/** Choose **/
		// KY
		String where = "E:\\FakeReviewDetectionData";
		// or Dear IRbig2?
		// String where="D:\\KY\\FakeReviewDetectionData";
		String categories = "hotels";
		// String categories ="restaurants";
		// String categories ="electronics";
		// String categories ="fashion";
		// String categories ="hospitals";
		// String categories ="musicvenues";
		// String categories ="insurance";
//		String inputFile = where + "\\truthful biased topic.txt";
//		 String inputFile = where+"\\fake biased topic.txt";
		 String inputFile = where+"\\neutral topic.txt";
		String inputFile_stopwords = where + "\\stopword.csv";

	
		ArrayList<String> stopwordAL= new ArrayList<String>();
		try {

			BufferedReader br = null;
			BufferedReader br_s = null;
			/** for each business **/

			File input_File = new File(inputFile);
			File stopwords = new File(inputFile_stopwords);

			br = new BufferedReader(new FileReader(input_File));
			br_s = new BufferedReader(new FileReader(stopwords));
			while ((line = br_s.readLine()) != null) {
				 String[] token = line.split(",");
				for(int i=0;i<token.length;i++){
				 stopwordAL.add(token[i]);
				}
			}

			while ((line = br.readLine()) != null) {
				
				
				 StringTokenizer st1 = new StringTokenizer(line, ", \t");
			        while(st1.hasMoreTokens()){
			            String temp = st1.nextToken();
			           if(stopwordAL.contains(temp)){
			        	   continue;
			           }
			           output+=temp+", ";
			        }
			        output+="\n";
			}

//			FileWriter writer_n = new FileWriter(where + "\\[stopword Removed]truthful biased topic.txt");
//			FileWriter writer_n = new FileWriter(where + "\\[stopword Removed]fake biased topic.txt");
			FileWriter writer_n = new FileWriter(where + "\\[stopword Removed]neutral topic.txt");
			writer_n.append(output.trim());
			writer_n.close();

		
		} catch (FileNotFoundException e) {
			e.printStackTrace();

		}
	}
}
