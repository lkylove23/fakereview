package dataSetting;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;
import java.util.TreeMap;

public class NgramforSvm2_fast_crossEvaluation {

	
	/**유닛별 nomalise 해줄것!!!!!!!!!!!!!!!!!!!!! 하고이거 지우기***/
	public static void main(String args[]) {

		
		
		String where = "E:\\FakeReviewDetectionData";
//		String where = "D:\\KY\\FakeReviewDetectionData";
		
		String line = "";
		String cvsSplitBy = ",";
//		Map tm = new TreeMap();
		ArrayList tm= new ArrayList();
		HashMap hm= new HashMap();
		
		//max cnt =31 이었음
		//int maxCnt = 31;
		
		
		// String path = Maketrain.class.getResource("").getPath();
		
		
		 ArrayList<String> listOfFold= new ArrayList<String>();
//		 listOfFold.add("electronics");
//		 listOfFold.add("for_electronics");
		 listOfFold.add("fashion"); 
		 listOfFold.add("for_fashion"); 
		 listOfFold.add("hospitals"); 
		 listOfFold.add("for_hospitals"); 
		 listOfFold.add("hotels"); 
		 listOfFold.add("for_hotels"); 
		 listOfFold.add("insurance"); 
		 listOfFold.add("for_insurance"); 
		 listOfFold.add("musicvenues");	
		 listOfFold.add("for_musicvenues");	
		 listOfFold.add("restaurants");	
		 listOfFold.add("for_restaurants");	


//			String categories ="fashion";
//			String categories ="hospitals";		
//			String categories ="hotels";
//			String categories ="insurance";
//			String categories ="musicvenues";
//			String categories ="restaurants";
		 
//		String mode = "Unigram";
		String mode ="Bigram";

		//전체불러와서 인덱싱후 돌리는거로 어짜피 시간 많이 안결러서 ㅎ

		//2에서는 그냥 fold.csv 불러와서 처리함
		String input = where+"\\Yelp\\forCrossEvaluation\\";
//		String inputTotalFile = where+"Yelp\\"+categories+"\\input\\positive\\"	+ "fold_total.txt";
//		String inputFile_f = where+"\\input\\positive\\"
//				+ test+"_f.txt";
//		String inputFile_t = where+"\\input\\positive\\"
//				+ test+"_t.txt";
		String outputFile = where+"\\Yelp\\forCrossEvaluation_svm\\";
//		String extra_outputFile = where+"\\input\\positive\\N-gram\\"
//				+ mode + "\\" + test+"_"+extraMode+".dat";
		String index_outputFile = where+"\\Yelp\\forCrossEvaluation_svm\\"+mode+"_Index.txt";

		double cnt=0;
		int currentId;
		// LinkedHashMap<String, Integer> map = new LinkedHashMap<String,
		// Integer>();
//		ArrayList<TreeMap<Integer, Double>> map_fold_t = new ArrayList<TreeMap<Integer, Double>>();
		HashMap<Integer, String> indexIS = new HashMap<Integer, String>();
		HashMap<String, Integer> indexSI = new HashMap<String, Integer>();
		ArrayList<String> testAL = new ArrayList<String>();

//		array list로 하면됨!
		//		testAL.
		int indexCnt = 1;
		// HashMap<String, Integer> map = new HashMap<String, Integer>();
		
		File ForCheck_category = new File(outputFile);
		if(!ForCheck_category.isDirectory()){
			ForCheck_category.mkdir();
		}
		
		try {
			//make word index
			String listFile = input+"total.csv";
			BufferedReader br = new BufferedReader(new FileReader(listFile));
			
			//for each csv file
//			for(int foldIndex=0;foldIndex<listOfFold.size();foldIndex++){
//			String inputFile = input+categories+"_"+listOfFold.get(foldIndex)+".csv";
//			BufferedReader br = new BufferedReader(new FileReader(inputFile));
			//test for max value
			String temp;
			double max = 1;
			int linenum=0;
			
			String preTemp;
			String nGramTemp;
			StringTokenizer st;
			while ((line = br.readLine()) != null) {
				
				linenum++;
				System.out.println("making list: "+linenum);
				// use comma as separator
				String[] token = line.split(",");
				line=token[4];
				//docMap <word id , word count>	
				//여기서 docMap 필요없지 않나? ㅎ
//				String match = "[^\uAC00-\uD7A3xfe0-9a-zA-Z\\s]";
//			      cols[4] =tmp.replaceAll(match, " ");
//				tmp = tmp.replace("`s", "");
//				tmp = tmp.replace("'s", "");
//				tmp = tmp.replace("", "");
//				String match = "[^\uAC00-\uD7A3xfe0-9a-zA-Z\\s]";
//			      tmp =tmp.replaceAll(match, " ");
//				line = line.replace("'s", "");
//				line = line.replace("n't", " not");
				line = line.replace(".", "");
				line = line.replace("!", "");
				line = line.replace("(", "");
				line = line.replace(")", "");
				line = line.replace("$", "");
				line = line.replace("-", "");
				line = line.replace("/", " ");
				line = line.replace("~", " ");
				line = line.replace("?", "");
				line = line.replace("\"", "");
							
//				String match = "[^\uAC00-\uD7A3xfe0-9a-zA-Z\\s]";
//			     line =line.replaceAll(match, "");
				line = line.toLowerCase();
				
				st = new StringTokenizer(line, " |");
				preTemp ="";
				nGramTemp="";
				int tag=0;
				while(st.hasMoreTokens()) {
					
					temp = st.nextToken(); // 토근을 temp 변수에 저장
					
					//첫단어는 건너뛴다
					if(mode.equals("Bigram")){
						if(tag==0){
							tag++;
							preTemp=temp;
							continue;
						}
						
						nGramTemp=preTemp+" "+temp;
						}else if(mode.equals("Unigram")){
							nGramTemp=temp;
						}
//					String temp2 = temp.replace(".", "");
//					temp2 = temp2.replace("!", "");
//					temp2 = temp2.toLowerCase();
					// System.out.println("temp=" + temp2);
					// delete "."
					// Indexing
					if (!indexIS.containsValue(nGramTemp)) {
						indexIS.put(indexCnt, nGramTemp);
						indexSI.put(nGramTemp, indexCnt);
						// get the id of this input word : case 1	
//						currentId=indexCnt;
						indexCnt++;
					}
					//To make preTemp for Bigram
					preTemp=temp;
				}

			}
			
			try {
				// //////////////////////////////////////////////////////////////
				BufferedWriter out_index = new BufferedWriter(new FileWriter(
						index_outputFile));
				// for fake
//				String outputdoc = "";
				StringBuffer outputdoc = new StringBuffer(80000000);

				for (int i = 1; i <= indexIS.size(); i++) {
					// j is index of docMap
					outputdoc.append(i + "," + indexIS.get(i));
					outputdoc.append("\n");
//					outputdoc += i + "," + indexIS.get(i);
//					outputdoc += "\n";
				}
				// System.out.print(outputdoc);
				out_index.write(outputdoc.toString());
				// out_index.newLine();
				// out.write(s); out.newLine();
				out_index.close();
				// //////////////////////////////////////////////////////////////
			} catch (IOException e) {
				System.err.println(e);
				System.exit(1);
			}
			
			System.out.println("making N-gram index completed!");
			
			//double 로 하는구간 만들기
			
			int check=0;
			
			/** for fold_t **/
			for(int foldIndex=0;foldIndex<listOfFold.size();foldIndex++){
				String outputdoc_fold;
				outputdoc_fold="";
//				map_fold_t.clear();
				String TorF="";
				outputFile+=listOfFold.get(foldIndex)+".dat";
			String inputFile = input+listOfFold.get(foldIndex)+".csv";
			br = new BufferedReader(new FileReader(inputFile));
			linenum=0;
			while ((line = br.readLine()) != null) {
				
				linenum++;
				System.out.println("making"+listOfFold.get(foldIndex)+": "+linenum);
				
				// use comma as separator
				String[] token = line.split(",");
				line=token[4];
				
				//T,F를 0:으로 받아서 하기
				TorF=token[1];
				
				check++;
				//docMap <word id , word count>
//				HashMap<Integer, Integer> docMap_fold_t = new HashMap<Integer, Integer>();
				HashMap<Integer, Double> docMap = new HashMap<Integer, Double>();
//				TreeMap<Integer, Integer> tMap_fold_t = new TreeMap<Integer, Integer>();
				TreeMap<Integer, Double> tMap_fold = new TreeMap<Integer, Double>();
				
				docMap.clear();
				tMap_fold.clear();
				
				String tmp;
				
				
				if(TorF.equals("T")){
					docMap.put(0, 1.0);
					}else if(TorF.equals("F")){
						docMap.put(0, -1.0);	
					}
//				String match = "[^\uAC00-\uD7A3xfe0-9a-zA-Z\\s]";
//			      cols[4] =tmp.replaceAll(match, " ");
//				tmp = tmp.replace("`s", "");
//				tmp = tmp.replace("'s", "");
//				tmp = tmp.replace("", "");
//				String match = "[^\uAC00-\uD7A3xfe0-9a-zA-Z\\s]";
//			      tmp =tmp.replaceAll(match, " ");

//				
//				line = line.replace("'s", "");
//				line = line.replace("n't", " not");
				line = line.replace(".", "");
				line = line.replace("!", "");
				line = line.replace("(", "");
				line = line.replace(")", "");
				line = line.replace("$", "");
				line = line.replace("-", "");
				line = line.replace("/", " ");
				line = line.replace("~", " ");
				line = line.replace("?", "");
				line = line.replace("\"", "");
				
//				String match = "[^\uAC00-\uD7A3xfe0-9a-zA-Z\\s]";
//			     line =line.replaceAll(match, "");
				line = line.toLowerCase();
				
				st = new StringTokenizer(line, " ,|");
				preTemp="";
				nGramTemp="";
				int tag=0;
				while (st.hasMoreTokens()) {
					temp = st.nextToken(); // 토근을 temp 변수에 저장
					if(mode.equals("Bigram")){
						if(tag==0){
							tag++;
							preTemp=temp;
							continue;
						}
					nGramTemp=preTemp+" "+temp;
					}else if(mode.equals("Unigram")){
						nGramTemp=temp;
					}
			
//					String temp2 = temp.replace(".", "");
//					temp2 = temp2.replace("!", "");
//					temp2 = temp2.toLowerCase();
					// System.out.println("temp=" + temp2);
					// delete "."
			
					// get the id of this input word : case 2
					int	currentIdofNgram=indexSI.get(nGramTemp);
				
					// input term frequencies
					if (!docMap.containsKey(currentIdofNgram)) {
						docMap.put(currentIdofNgram, 1.0);
//						System.out.println("1/31: "+docMap_fold_t.put(currentId_fold_t, 1.0/max));
					} else{
//						if(check==5){
//						if(currentId_fold_t==6){
//						System.out.println("id"+currentId_fold_t+"원래 cnt"+docMap_fold_t.get(currentId_fold_t));
////						System.out.println("원래 cnt + 1 ="+(docMap_fold_t.get(currentId_fold_t)+1.0));
//						System.out.println("(원래 cnt )*31+1 ="+((docMap_fold_t.get(currentId_fold_t)*max)+1.0));
//						}
//						}
						cnt = (docMap.get(currentIdofNgram) + 1.0);
						docMap.put(currentIdofNgram, cnt);
//						if(check==5){
//						if(currentId_fold_t==6){
//							
////						System.out.println("그다음= "+(docMap_fold_t.get(currentId_fold_t)*max + 1.0)/max);
//						System.out.println("그다음= "+cnt);
//						}
//						}
					}
					preTemp=temp;
				}
				double maxOfMap=0;
			      for( int key : docMap.keySet() ){
			    	  if(key!=0)
			             maxOfMap+=docMap.get(key);
			        }
			      for( int key : docMap.keySet() ){
			    	  if(key!=0)
			    	  docMap.put(key,docMap.get(key)/maxOfMap);
			        }
//				System.out.println(docMap);
				//tree <- has
				tMap_fold.putAll(docMap);
//				map_fold_t.add(tMap_fold);
				for( int key : tMap_fold.keySet() ){
					double value=tMap_fold.get(key);
					if(key==0){
						outputdoc_fold += (int)value+" ";
					}else{
					outputdoc_fold += key + ":" + value + " ";
					}
				}
				outputdoc_fold += "\n";    
				//write file HERE!!!
			}
			
			System.out.println("Making map finished!");
			// System.out.println(indexIS);

			// System.out.println(docMap);
			// write
			// i is doc cnt
			int testcnt=1;
			
			/**write file**/
			
			try {
				BufferedWriter out = new BufferedWriter(new FileWriter(outputFile));
				out.write(outputdoc_fold);
				out.close();
			} catch (IOException e) {
				System.err.println(e); // 에러가 있다면 메시지 출력
				System.exit(1);
			}
			// i+":"+
			// for(int j=0;j<map.get(i).size();j++){
			// // map.get(i).g
			//
			// index.
			// }
			br.close();
			}
			// Index print out
			
		} catch (IOException e) {
			System.err.println(e);
			System.exit(1);
			
		}

	}

}
