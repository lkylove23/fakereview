package dataSetting;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

public class CheckingSamplingError {
	public static void main(String args[]) throws IOException {
		/**각 fold 별로 T,F 합치기**/
		String line="";
		String cvsSplitBy = ",";
		Boolean checkforloop= true; 
		/**Choose**/
		//KY 
				String where="E:\\FakeReviewDetectionData";
				//or Dear IRbig2?
//				String where="D:\\KY\\FakeReviewDetectionData";
//				String categories ="hotels";
				String categories ="restaurants";
//				String categories ="electronics";
//				String categories ="fashion";
//				String categories ="hospitals";
//				String categories ="musicvenues";
//				String categories ="insurance";
				String inputFile = where+"\\Yelp\\"+categories+"\\BalancedForLDA";
				int docNum_t =0;
				int docNum_f =0;
				HashMap<String,Integer> OnlyForCheck_t=new HashMap<String,Integer>();
				HashMap<String,Integer> OnlyForCheck_f=new HashMap<String,Integer>();
				File Folder = new File(inputFile);
				File [] arrFile = Folder.listFiles();
				String outString ="";
				String outString_total ="";
				
				try {
					
					BufferedReader br = null;
//					for (File file : arrFile) {
						// For counting
//						br = new BufferedReader(new FileReader(file));
						br = new BufferedReader(new FileReader(inputFile+"\\restaurants_fold2.csv"));
												
						while( (line =br.readLine()) != null){
							String[] token = line.split(",");
							if(token[1].equals("T")){
								docNum_t++;
							if(!OnlyForCheck_t.containsKey(token[0])){
								OnlyForCheck_t.put(token[0], 1);
							}else{
								OnlyForCheck_t.put(token[0],OnlyForCheck_t.get(token[0])+1);
							}
							
							}else{
								docNum_f++;
								if(!OnlyForCheck_f.containsKey(token[0])){
									OnlyForCheck_f.put(token[0], 1);
								}else{
									OnlyForCheck_f.put(token[0],OnlyForCheck_f.get(token[0])+1);
								}
							}
							
						}
//						}
						for( String key : OnlyForCheck_t.keySet() ){
							
							if(OnlyForCheck_t.get(key)>OnlyForCheck_f.get(key)){
								System.out.println("T>F");
								System.out.println(key);
							}else if(OnlyForCheck_t.get(key)<OnlyForCheck_f.get(key)){
								System.out.println("T<F");
								System.out.println(key);
				        }
						
					}
						System.out.println("T: "+docNum_t);
						System.out.println("F: "+docNum_f);
					
					
				
					
					
				} catch(FileNotFoundException e) {
					e.printStackTrace();

				}
	}
}
