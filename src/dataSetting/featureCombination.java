package dataSetting;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;

/**
 * Created by lkylove23 on 2015-02-12.
 */
public class featureCombination {
    public static void main(String[] args) throws Exception {
//        int topicNum = 100;
        ArrayList<Integer> listOfTopicNum = new ArrayList<Integer>();
        listOfTopicNum.add(2);
        listOfTopicNum.add(10);
        listOfTopicNum.add(50);
//        listOfTopicNum.add(100); //already done
        listOfTopicNum.add(200);
        listOfTopicNum.add(300);
//        listOfTopicNum.add(500);
//        listOfTopicNum.add(1000);

        //        String mode2 = "Unigram";
//        String mode2 = "Bigram";
        String mode2 = "UB";


        String where="D:\\";
        //or Dear IRbig2?
//		String where="D:\\KY\\";
        String porter = "_porter";
        ArrayList<String> listOfFold = new ArrayList<String>();
        listOfFold.add("fold1");
        listOfFold.add("fold2");
        listOfFold.add("fold3");
        listOfFold.add("fold4");
        listOfFold.add("fold5");
        listOfFold.add("fold2,3,4,5");
        listOfFold.add("fold1,3,4,5");
        listOfFold.add("fold1,2,4,5");
        listOfFold.add("fold1,2,3,5");
        listOfFold.add("fold1,2,3,4");

        for (int tn = 0; tn < listOfTopicNum.size(); tn++) {
            int topicNum = listOfTopicNum.get(tn);
//            String mode1 = "lda" + Integer.toString(topicNum);  //일단이렇게
//          int featureNum= topicNum;
            String mode1 = "lda" + Integer.toString(topicNum)+"_w5";  //w5
            int featureNum= topicNum*2;
//            String mode1 = "lda" + Integer.toString(topicNum)+"_w1";  //w5

            String mode_out=mode1+mode2;

                int numOfReview = 0;

                    String categories="";
                    for (int c_it = 0; c_it < 7; c_it++) {
                        if (c_it == 0) {
                            if (numOfReview > 440) {
                                continue;
                            }
                            //Electronics
//                changeID = 44;
                            categories = "electronics";
                        } else if (c_it == 1) {
                            if (numOfReview > 26900) {
                                continue;
                            }
                            //Fashion
                            categories = "fashion";
                        } else if (c_it == 2) {
                            if (numOfReview >= 200) {
                                continue;
                            }
                            //hospitals
                            categories = "hospitals";
                        } else if (c_it == 3) {
                            if (numOfReview > 11000) {
                                continue;
                            }
                            //hotels
                            categories = "hotels";
                        } else if (c_it == 4) {
                            if (numOfReview >= 130) {
                                continue;
                            }
                            //Insurance
                            categories = "insurance";
                        } else if (c_it == 5) {
                            if (numOfReview > 1190) {
                                continue;
                            }
                            //Musicvenues
                            categories = "musicvenues";
                        } else if (c_it == 6) {
                            if (numOfReview > 37980) {
                                continue;
                            }
                            //Restaurant
//                totalNumOfReview = 37990;
                            categories = "restaurants";
                        }

                        String File_helper = where + "FakeReviewDetectionData\\Yelp\\" + categories + "\\";
                        for (int foldIndex = 0; foldIndex < listOfFold.size(); foldIndex++) {

                            String inputFile1 = File_helper + "BalancedForLDA\\"+categories + "_" + listOfFold.get(foldIndex) + "_" + mode1 + porter+".txt";
                            String inputFile2 = File_helper +"BalancedForLDA\\"+ categories + "_"  + listOfFold.get(foldIndex) + "_" + mode2 +porter+ ".txt";
                            String outputFile = File_helper +"BalancedForLDA\\"+ categories + "_"  + listOfFold.get(foldIndex) + "_" + mode_out +porter+ ".txt";

                            //input1 읽어온거에다가 뒤에꺼 feature id 바꾼거 덧붙인후 한줄 저장
                            String line ="";

                            ArrayList<String> input1 = new ArrayList<String>();
                            ArrayList<String> input2 = new ArrayList<String>();

                            BufferedReader br = new BufferedReader(new FileReader(inputFile1));
                            while ((line = br.readLine()) != null) {
                                input1.add(line);
                            }
                            br = new BufferedReader(new FileReader(inputFile2));
                            while ((line = br.readLine()) != null) {
                                String[] temp= line.split(" ");
                                StringBuffer in2 = new StringBuffer();
                                for(int i=1;i<temp.length;i++){
                                    String[] temp2 =temp[i].split(":"); // temp2:  id value
                                    temp2[0]=String.valueOf(Integer.parseInt(temp2[0])+featureNum);// normal lda

                                    in2.append(" ");
                                    in2.append(temp2[0]);
                                    in2.append(":");
                                    in2.append(temp2[1]);
                                }
                                input2.add(in2.toString());
                            }
                            if(input1.size()!=input2.size()){
                                System.out.println("error///");
                            }
                            StringBuffer output = new StringBuffer();
                            for(int ii=0;ii<input1.size();ii++){
                                output.append(input1.get(ii));
                                output.append(input2.get(ii));
                                output.append("\n");

                            }
                            FileWriter fw = new FileWriter(outputFile);
                            fw.append(output.toString().trim());
                            fw.flush();
                            fw.close();

                        }
                    }


        }

    }
}
