package dataSetting;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;

/**
 * Created by lkylove23 on 2015-02-27.
 */
public class txt2arff_allcate {
    public static void main(String[] args) throws Exception {

        /**
         * @RELATION FRD
         * @ATTRIBUTE 1 NUMERIC
         *
         * @ATTRIBUTE class {f,t}
         * **/
        /** Choose **/
        String porter="_porter";

        ArrayList<Integer> listOfTopicNum = new ArrayList<Integer>();
//        listOfTopicNum.add(2);
//        listOfTopicNum.add(10);
//        listOfTopicNum.add(50);
        listOfTopicNum.add(100);
//        listOfTopicNum.add(200);
//        listOfTopicNum.add(300);
//        listOfTopicNum.add(500);
//        listOfTopicNum.add(1000);

         int topicNum=0;
         int numOfReview=0;
        String test="";
        String train="";
        String categories="";
        // topic num
        // category_mode_index
        String where="D:\\";

        int numOfNgram=0;
        int maxFeature=0;
        String RELATION="@RELATION FRD";
        String CLASS="@ATTRIBUTE class {f,t}";
//        String CLASS="@ATTRIBUTE class {-1,1}";
        String DATA="@DATA";
        String ATTRIBUTE="@ATTRIBUTE";
        String numeric="numeric";




        for (int tn = 0; tn < listOfTopicNum.size(); tn++) {

            /**Mode Selection**/
            topicNum = listOfTopicNum.get(tn);
            //
            // only lda
            String mode = "lda" + Integer.toString(topicNum);

            // weighted lda svm
//            String mode = "lda" + Integer.toString(topicNum)+"_w1";
//            String mode = "lda" + Integer.toString(topicNum)+"_w2";
//            String mode = "lda" + Integer.toString(topicNum)+"_w5";
            // weighted lda svm + UB
//            String mode = "lda" + Integer.toString(topicNum)+"_w1UB";
//            String mode = "lda" + Integer.toString(topicNum)+"_w2";
//            String mode = "lda" + Integer.toString(topicNum)+"_w5UB";

//        String mode = "Unigram";
//        String mode = "Bigram";
//        String mode = "UB";
//            String mode = tm + "UB";




            for (int c_it = 0; c_it < 7; c_it++) {

//        for(int c_it=0;c_it<4;c_it++) {
//        for(int c_it=5;c_it<7;c_it++) {
                if (c_it == 0) {
                    if (numOfReview > 440) {
                        continue;
                    }
                    //Electronics
//                changeID = 44;
                    categories = "electronics";
                } else if (c_it == 1) {
                    if (numOfReview > 26900) {
                        continue;
                    }
                    //Fashion
                    categories = "fashion";
                } else if (c_it == 2) {
                    if (numOfReview >= 200) {
                        continue;
                    }
                    //hospitals
                    categories = "hospitals";
                } else if (c_it == 3) {
                    if (numOfReview > 11000) {
                        continue;
                    }
                    //hotels
                    categories = "hotels";
                } else if (c_it == 4) {
                    if (numOfReview > 130) {
                        continue;
                    }
                    //Insurance
                    categories = "insurance";
                } else if (c_it == 5) {
                    if (numOfReview > 1190) {
                        continue;
                    }
                    //Musicvenues
                    categories = "musicvenues";
                } else if (c_it == 6) {
                    if (numOfReview > 37980) {
                        continue;
                    }
                    categories = "restaurants";
                }
                //counting Ngram

                //
                String File_helper = where + "FakeReviewDetectionData\\Yelp\\" + categories + "\\BalancedForLDA" + "\\";
                maxFeature = topicNum + numOfNgram;

                for (int it = 0; it < 5; it++) {
                    if (it == 0) {
                        test = "fold1";
                        train = "fold2,3,4,5";
                    } else if (it == 1) {
                        test = "fold2";
                        train = "fold1,3,4,5";
                    } else if (it == 2) {
                        test = "fold3";
                        train = "fold1,2,4,5";
                    } else if (it == 3) {
                        test = "fold4";
                        train = "fold1,2,3,5";
                    } else if (it == 4) {
                        test = "fold5";
                        train = "fold1,2,3,4";
                    }

                    String trainFile = File_helper + categories + "_" + train + "_" + mode + porter+ ".txt";
                    String testFile = File_helper + categories  + "_" + test + "_" + mode +porter+ ".txt";
                    String trainOutFile = File_helper + categories + "_" + train + "_" + mode + porter+ ".arff";
                    String testOutFile = File_helper + categories  + "_" + test + "_" + mode +porter+ ".arff";
                StringBuffer Declaration = new StringBuffer();
                StringBuffer trainOutB = new StringBuffer();
                StringBuffer testOutB = new StringBuffer();
                FileWriter trainOut = new FileWriter(trainOutFile);
                FileWriter testOut = new FileWriter(testOutFile);
                    Declaration.append(RELATION);
                    Declaration.append("\n");
                    Declaration.append(ATTRIBUTE);
                    Declaration.append(" ");
                    Declaration.append(0);
                    Declaration.append(" ");
                    Declaration.append(numeric);
                    Declaration.append("\n");

                for (int i = 1; i <= maxFeature; i++) {
                    Declaration.append(ATTRIBUTE);
                    Declaration.append(" ");
                    Declaration.append(i);
                    Declaration.append(" ");
                    Declaration.append(numeric);
                    Declaration.append("\n");
                }
                    Declaration.append(CLASS);
                    Declaration.append("\n");

                    //train output
                    trainOutB.append(Declaration.toString());
                    trainOutB.append("\n");
                    trainOutB.append(DATA);
                    trainOutB.append("\n");


                    BufferedReader br = new BufferedReader(new FileReader(trainFile));
                    String line="";
                    while (( line = br.readLine()) != null) {
                        trainOutB.append("{");
                       String[] temp = line.split(" ");
                        for(int ii=1;ii<temp.length;ii++) {
                            trainOutB.append(temp[ii].replace(":"," "));
                                trainOutB.append(", ");
                        }
                        if(temp[0].equals("-1")){
                            trainOutB.append(String.valueOf(maxFeature+1));
                            trainOutB.append(" ");
                            trainOutB.append("\"f\"");
                        }else{
                            trainOutB.append(String.valueOf(maxFeature+1));
                            trainOutB.append(" ");
                            trainOutB.append("\"t\"");
                        }
                        trainOutB.append("}");
                        trainOutB.append("\n");

                    }
                    trainOut.append(trainOutB.toString().trim());
                    trainOut.close();

                    //test output
                    testOutB.append(Declaration.toString());
                    testOutB.append("\n");
                    testOutB.append(DATA);
                    testOutB.append("\n");


                     br = new BufferedReader(new FileReader(testFile));
                    line="";
                    while (( line = br.readLine()) != null) {
                        testOutB.append("{");
                        String[] temp = line.split(" ");
                        for(int ii=1;ii<temp.length;ii++) {
                            testOutB.append(temp[ii].replace(":"," "));
                            if(ii<temp.length-1) {
                                testOutB.append(", ");
                            }
                        }
                        if(temp[0].equals("-1")){
                            testOutB.append(String.valueOf(maxFeature+1));
                            testOutB.append(" ");
                            testOutB.append("\"f\"");
                        }else{
                            testOutB.append(String.valueOf(maxFeature+1));
                            testOutB.append(" ");
                            testOutB.append("\"t\"");
                        }
                        testOutB.append("}");
                        testOutB.append("\n");

                    }
                    testOut.append(testOutB.toString().trim());
                    testOut.close();

            }
        }
        }

        //
     }
}
