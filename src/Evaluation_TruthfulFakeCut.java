import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

public class Evaluation_TruthfulFakeCut {
	public static void main(String[] args) {
		/**Choose**/
		//KY 
		String where="E:\\FakeReviewDetectionData";
		//or Dear IRbig2?
//		String where="D:\\KY\\FakeReviewDetectionData";
		
//		String TruthfulOrFake ="Truthful";
		String TruthfulOrFake ="Fake";
//		
//		int topicNum=8;
		int topicNum=50;
//		int topicNum=100;

		String test ="fold1";
//		String test ="fold2";
//		String test ="fold3";
//		String test ="fold4";
//		String test ="fold5";
		
		
		/** Fold selection
		 * e.g. 23450101 -> 2,3,4,5 Fake normalised e.g. 23450000 -> 2,3,4,5
		 * truthful not normalised
		 																			**/
		
	    //fold1
	    String tdf_f_n="23450101";
	    String tdf_t_n="23450001";
	    //fold2
//	    String tdf_f_n="13450101";
//	    String tdf_t_n="13450001";
	    //fold3
//	    String tdf_f_n="12450101";
//	    String tdf_t_n="12450001";
	    //fold4
//	    String tdf_f_n="12350101";
//	    String tdf_t_n="12350001";	    
	    //fold5
//	    String tdf_f_n="12340101";
//	    String tdf_t_n="12340001";
		


	    double highestScore=0;
		int cntResult=0;
		
		
		double cut;
		//
	    
	    long time = System.currentTimeMillis(); 
		SimpleDateFormat dayTime = new SimpleDateFormat("dd-hh-mm");
		String str = dayTime.format(new Date(time));
	    
		//for KY
	    String testFile = where+"\\input\\topic"+topicNum+"\\itr1500\\DT_"+test+".csv";
	    String TDF = where+"\\input\\topic"+topicNum+"\\itr1500\\TDF.csv";
	    
		//Truth and Fake Cut mode
	    String resultFile = where+"\\result\\topic"+topicNum+"\\itr1500\\"+TruthfulOrFake+"Cut\\"+test+"-"+str+".csv";
	    String recordFile = where+"\\result\\topic"+topicNum+"\\itr1500\\"+TruthfulOrFake+"Cut\\record\\"+test+"-"+str+".csv";

	    /* resultAL
	     * 
	     * 0: Accuracy
	     * 1: T-P	
	     * 2: T-R	
	     * 3: T-F	
	     * 4: F-P
	     * 5: F-R
	     * 6: F-F
	     * etc
	     */
	    
	    ArrayList<Double> resultAL= new ArrayList<Double>();
	    
	    /* Array List of DocumentTopicD
	     * 0: DocID
	     * 1: Dist of Topic 0
	     * 2: Dist of Topic 1
	     * 3: Dist of Topic 2
	     * 4: Dist of Topic 3
	     * 5: Dist of Topic 4
	     * 6: Dist of Topic 5
	     * 7: Dist of Topic 6
	     * 8: Dist of Topic 7
	     * 9: result of 1-1 (normalise and major topics 두배이상 차이)
	     * 10: result of 1-2 (not normalise)
	     * 11: result of 2-1 all topics
	     * 12: result of 2-2 
	     * 13: several topic combination to find best for each fold 
	     */
	    
	    HashMap<String, ArrayList<Double>> documentTopicD = new HashMap<String, ArrayList<Double>>();
	    
	    /* Array List of TopicDF
	     * 0: foldID + F or T  + Normalise or not
	     * e.g. 23450101 -> 2,3,4,5 Fake normalised
	     * e.g. 23450000 -> 2,3,4,5 truthful not normalised
	     * 1: Dist of Topic 0
	     * 2: Dist of Topic 1
	     * 3: Dist of Topic 2
	     * 4: Dist of Topic 3
	     * 5: Dist of Topic 4
	     * 6: Dist of Topic 5
	     * 7: Dist of Topic 6
	     * 8: Dist of Topic 7
	     * 
	     */
	    HashMap<String, ArrayList<Double>> topicDF = new HashMap<String, ArrayList<Double>>();
	    
	
	    
		// read csv for test data
		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ",";
		try {

			br = new BufferedReader(new FileReader(testFile));

				for(int i=0;(line = br.readLine()) != null;i++) {
					
				// use comma as separator
				String[] tempArray = line.split(cvsSplitBy);
				ArrayList<Double> tempAL= new ArrayList<Double>();
				for(int j = 0; j < tempArray.length; j++) {
						tempAL.add(Double.parseDouble(tempArray[j]));
		        }
				documentTopicD.put(tempArray[0], tempAL);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		/*Topic Distribution for fold 2,3,4,5*/
		try {
			br = new BufferedReader(new FileReader(TDF));
				for(int i=0;(line = br.readLine()) != null;i++) {
				// use comma as separator
				String[] tempArray2 = line.split(cvsSplitBy);
				ArrayList<Double> tempAL= new ArrayList<Double>();
				for(int j = 0; j < tempArray2.length; j++) {
						tempAL.add(Double.parseDouble(tempArray2[j]));
		        }
				topicDF.put(tempArray2[0], tempAL);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
/////////////////////////////////////////////////////////
		

/*Parametre*/
		
//		long start = System.currentTimeMillis();
		long end;
		
		//Topic Weight parametre
		ArrayList<Double> param_tw= new ArrayList<Double>();
		
		
		long start = System.currentTimeMillis();

		
		while(true) {
			
		param_tw.clear();
/*Parametre setting*/

		for(int u=0;u<topicNum;u++){
		param_tw.add(Math.random());
		}
		cut=Math.random();
		
	    /*<0(a),1(b),2(c),3(d) type and  1 for correct 0 for incorrect>
	     * 일단 truthful review-> true 인거로 함 (바꿔서 결과내면되니)
	     * */
//	    HashMap<Integer, Integer> resultType = new HashMap<Integer, Integer>();
	    double ra=0;
	    double rb=0;
	    double rc=0;
	    double rd=0;

		

/*Calculation*/
		    resultAL.clear();
	    	
			  Iterator<String> itr1 = documentTopicD.keySet().iterator();
			    while (itr1.hasNext()) {
			    	
			    	
			    	
			        String key = (String) itr1.next();
			        double scoreFake=0;
			        double scoreTruthful=0;
			        
			        for(int i=0;i<topicNum;i++){
			        
			        	scoreFake += documentTopicD.get(key).get(i+1)*topicDF.get(tdf_f_n).get(i+1)*param_tw.get(i);
			        	scoreTruthful += documentTopicD.get(key).get(i+1)*topicDF.get(tdf_t_n).get(i+1)*param_tw.get(i);
			        }
			        			        
//			        System.out.println("Fake Score: "+sum1_1_1);
//			        totalformean+=sum1_1_1;
			        
//			        System.out.println("after "+cntformean+", mean value: "+totalformean/(cntformean+1));
//			        cntformean++;
				if (Integer.parseInt(key) < 401) {
					if (scoreFake > cut) {
						// If there is little difference we can use another
						// approach $$
						// 1 correct, 0 incorrect
						if (scoreFake == 0) {
							// filter 0 but correct
						} else {
								// true negative
							rd++;
						}
					} else {
						//false positive
						
						rc++;
					}

				} else {
					if (scoreFake > cut) {
						// false negative
						rb++;
					} else {
						// true positive	
						ra++;

					}

				}

			    }
			   
			    /*evaluation*/
			   
			    /* resultAL
			     * 
			     * 0: Accuracy
			     * 1: T-P	
			     * 2: T-R	
			     * 3: T-F	
			     * 4: F-P
			     * 5: F-R
			     * 6: F-F
			     * etc
			     */
			    
			    resultAL.add(0, ((ra+rd)/(ra+rb+rc+rd)));
			    // for 
			    resultAL.add(1, ((ra)/(ra+rc)));
			    resultAL.add(2, ((ra)/(ra+rb)));
			    //F1 measure
			    resultAL.add(3, (2*(resultAL.get(1)*resultAL.get(2))/(resultAL.get(1)+resultAL.get(2))));
			    
			    // for fake
			    resultAL.add(4, ((rd)/(rd+rb)));
			    resultAL.add(5, ((rd)/(rd+rc)));
			    //F1 measure
			    resultAL.add(6, (2*(resultAL.get(4)*resultAL.get(5))/(resultAL.get(4)+resultAL.get(5))));
			    
//			    results.get(test).add(0,((double)sumR1_1/160));
//			    results.get(test).add(0,(double)(sumR/160));
		
			   
			    
			    
			    /*make csv for document F,T score*/
			    
			    
			    /*make csv for results*/
			    
			    double record=0;
			    // record highest one so far
			    if(resultAL.get(0)>highestScore){
			    try
				{
			    	highestScore=resultAL.get(0);
			    	end = System.currentTimeMillis();
			    	record=( end - start )/1000.0;
			    	System.out.println("The number of topics:"+topicNum);
			    	System.out.println(TruthfulOrFake+" "+test);
			    	System.out.println("\ttime for "+cntResult+"-highest:\t" + record +"\t A:\t"+resultAL.get(0)+"\t T-P:\t"+resultAL.get(1)+"\t T-R:\t"+resultAL.get(2)+"\t T-F:\t"+resultAL.get(3)+"\t F-P:\t"+resultAL.get(4)+"\t F-R:\t"+resultAL.get(5)+"\t F-F:\t"+resultAL.get(6));
					 start = System.currentTimeMillis();
			    	
			    	
			    	
			    	cntResult++;
			    	
				    FileWriter writer = new FileWriter(resultFile,true);
				    
				    //Accuracy	T-P	T-R	T-F	F-P	F-R	F-F  
				    // record는 record 파일에만
				    
				    for(int r=0;r<7;r++){
				    	writer.append(Double.toString(resultAL.get(r)));
				    	writer.append(',');
				    }
				   				    
				    for(int k=0;k<topicNum;k++){
			
				    writer.append(Double.toString(param_tw.get(k)));
				    	writer.append(',');
				    
				    
				    }
				    writer.append(Double.toString(cut));

				    writer.append('\n');

				    writer.flush();
				    writer.close();
				    
				    /*record for reresult*/
				    FileWriter writerR = new FileWriter(recordFile,true);
				    writerR.append(Double.toString(resultAL.get(0)));
				    writerR.append(',');
				    writerR.append(Double.toString(record));
				    writerR.append('\n');

				    writerR.flush();
				    writerR.close();
				    
//				    System.out.println(cntResult);
				}
				catch(IOException e)
				{
				     e.printStackTrace();
				}
			    
			   
				
			    
			    }
		}
			    
			    
	}
			    
							
						
					
				
			
		
	}
			    
	

	
	