package libSVM;

import libsvm.svm;
import libsvm.svm_model;

import java.io.*;
import java.util.ArrayList;

/**
 * Created by lkylove23 on 2015-01-27.
 */
public class Evaluation_libSVM_Ngram_NoR_v2_fortest {

    // public static String mode ="Unigram";
    public static String mode = "Bigram";
    public static String s_itr = "";

    public static String categories = "";

    public static void main(String args[]) throws IOException {
//        mode = "Unigram";
        mode = "Bigram";
//        s_itr = "";
//        s_itr = " (2)";
//        s_itr=" (3)";
	      s_itr=" (4)";
//	      s_itr=" (5)";

        String porter=""; //using porter
//        String porter = "_noP"; // without using porter


        String where = "D:\\";

        String test = "";
        String train = "";

//        int numOfReview = 200;

        int numOfReview = 0;
        ArrayList<Integer> listOfNoR = new ArrayList<Integer>();
//        listOfNoR.add(50);
        listOfNoR.add(100);
//        listOfNoR.add(200);
//        listOfNoR.add(300);
//        listOfNoR.add(500);
//        listOfNoR.add(1000);
        // for test
//        listOfNoR.add(130);

        for (int nor=0;nor<listOfNoR.size();nor++){
            numOfReview = listOfNoR.get(nor);
        FileWriter totalWriter = new FileWriter(where + "FakeReviewDetectionData\\Yelp\\numOfReviews\\result\\" + numOfReview + "_" + mode + porter+s_itr + "_Result.txt");
//        for (int c_it = 0; c_it < 7; c_it++) {

        for(int c_it=4;c_it<5;c_it++) {
//        for(int c_it=5;c_it<7;c_it++) {
            if (c_it == 0) {
                if (numOfReview > 440) {
                    continue;
                }
                //Electronics
//                changeID = 44;
                categories = "electronics";
            } else if (c_it == 1) {
                if (numOfReview > 26900) {
                    continue;
                }
                //Fashion
                categories = "fashion";
            } else if (c_it == 2) {
                if (numOfReview >= 200) {
                    continue;
                }
                //hospitals
                categories = "hospitals";
            } else if (c_it == 3) {
                if (numOfReview > 11000) {
                    continue;
                }
                //hotels
                categories = "hotels";
            } else if (c_it == 4) {
                if (numOfReview > 130) {
                    continue;
                }
                //Insurance
                categories = "insurance";
            } else if (c_it == 5) {
                if (numOfReview > 1190) {
                    continue;
                }
                //Musicvenues
                categories = "musicvenues";
            } else if (c_it == 6) {
                if (numOfReview > 37980) {
                    continue;
                }
                //Restaurant
//                totalNumOfReview = 37990;
                categories = "restaurants";
            }
            String File_helper = where + "FakeReviewDetectionData\\Yelp\\numOfReviews\\" + categories + "_" + numOfReview + s_itr + "\\";
            File ForCheckDir = new File(File_helper);
            if (!ForCheckDir.isDirectory()) {
                ForCheckDir.mkdir();
            }
            FileWriter writer = new FileWriter(File_helper + categories + "_" + numOfReview + "_" + mode + porter + "_Result.txt");
            double[] Acc_fold = new double[5];
            for (int it = 0; it < 5; it++) {
                if (it == 0) {
                    test = "fold1";
                    train = "fold2,3,4,5";
                } else if (it == 1) {
                    test = "fold2";
                    train = "fold1,3,4,5";
                } else if (it == 2) {
                    test = "fold3";
                    train = "fold1,2,4,5";
                } else if (it == 3) {
                    test = "fold4";
                    train = "fold1,2,3,5";
                } else if (it == 4) {
                    test = "fold5";
                    train = "fold1,2,3,4";
                }
                String trainFile = File_helper + categories + "_" + numOfReview + "_" + mode + porter + "_" + train + ".txt";
                String resultFile = File_helper + categories + "_" + numOfReview + "_" + mode + porter + "_" + test + "_result.txt";
                String trainModelFile = File_helper + categories + "_" + numOfReview + "_" + mode + porter + "_" + train + "_m.txt";

                /**TRAIN PART**/
                File f = new File(trainModelFile);
                // 파일 존재 여부 판단
//                if (!f.isFile()) {
//                String testFile = File_helper + categories + "_" + numOfReview + "_" + mode + "_" + test + ".dat";
                String[] cmdLine = new String[2]; // [option] [training_data] [model_file]
                // -c cost : set the parameter C of C-SVC, epsilon-SVR, and nu-SVR (default 1)
                cmdLine[0] = trainFile;
                cmdLine[1] = trainModelFile;
                svm_train t = new svm_train();
                t.run(cmdLine);
//                }
                /**PREDICT PART**/
                int i, predict_probability = 0; // what is
                try {
                    String testFile = File_helper + categories + "_" + numOfReview + "_" + mode + "_" + test + ".dat";
                    BufferedReader testFileB = new BufferedReader(new FileReader(testFile)); //test file
                    DataOutputStream output = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(resultFile)));
                    svm_model model = svm.svm_load_model(trainModelFile);
                    if (model == null) {
                        System.err.print("can't open model file " + trainModelFile + "\n");
                        System.exit(1);
                    }
                    if (predict_probability == 1) {
                        if (svm.svm_check_probability_model(model) == 0) {
                            System.err.print("Model does not support probabiliy estimates\n");
                            System.exit(1);
                        }
                    } else {
                        if (svm.svm_check_probability_model(model) != 0) {
                            svm_predict.info("Model supports probability estimates, but disabled in prediction.\n");
                        }
                    }
                    Acc_fold[it] = svm_predict.predict(testFileB, output, model, predict_probability);
                    testFileB.close();
                    output.close();
                } catch (FileNotFoundException e) {
                    svm_predict.exit_with_help();
                } catch (ArrayIndexOutOfBoundsException e) {
                    svm_predict.exit_with_help();
                }

            }
            double temp_avg = 0.0;
            totalWriter.append(categories);
            for (int it = 0; it < 5; it++) {
                System.out.println(Acc_fold[it]);
                writer.append("\n");
                totalWriter.append("\n");
                writer.append(Double.toString(Acc_fold[it]));
                totalWriter.append(Double.toString(Acc_fold[it]));
                temp_avg += Acc_fold[it];
            }
            temp_avg /= 5;
            System.out.println(temp_avg);
            writer.append("\n");
            totalWriter.append("\n");
            writer.append(Double.toString(temp_avg));
            totalWriter.append(Double.toString(temp_avg));

            writer.append("\n");
            totalWriter.append("\n");
            writer.append("\n");
            totalWriter.append("\n");
            writer.close();
        }
        totalWriter.close();
    }
}

    //    public static void predict(BufferedReader input, DataOutputStream output, svm_model model, int predict_probability) throws IOException
//    {
//        int correct = 0;
//        int total = 0;
//        double error = 0;
//        double sumv = 0, sumy = 0, sumvv = 0, sumyy = 0, sumvy = 0;
//
//        int svm_type=svm.svm_get_svm_type(model);
//        int nr_class=svm.svm_get_nr_class(model);
//        double[] prob_estimates=null;
//
//        if(predict_probability == 1)
//        {
//            if(svm_type == svm_parameter.EPSILON_SVR ||
//                    svm_type == svm_parameter.NU_SVR)
//            {
//                svm_predict.info("Prob. model for test data: target value = predicted value + z,\nz: Laplace distribution e^(-|z|/sigma)/(2sigma),sigma="+svm.svm_get_svr_probability(model)+"\n");
//            }
//            else
//            {
//                int[] labels=new int[nr_class];
//                svm.svm_get_labels(model,labels);
//                prob_estimates = new double[nr_class];
//                output.writeBytes("labels");
//                for(int j=0;j<nr_class;j++)
//                    output.writeBytes(" "+labels[j]);
//                output.writeBytes("\n");
//            }
//        }
//        while(true)
//        {
//            String line = input.readLine();
//            if(line == null) break;
//
//            StringTokenizer st = new StringTokenizer(line," \t\n\r\f:");
//
//            double target = atof(st.nextToken());
//            int m = st.countTokens()/2;
//            svm_node[] x = new svm_node[m];
//            for(int j=0;j<m;j++)
//            {
//                x[j] = new svm_node();
//                x[j].index = atoi(st.nextToken());
//                x[j].value = atof(st.nextToken());
//            }
//
//            double v;
//            if (predict_probability==1 && (svm_type==svm_parameter.C_SVC || svm_type==svm_parameter.NU_SVC))
//            {
//                v = svm.svm_predict_probability(model,x,prob_estimates);
//                output.writeBytes(v+" ");
//                for(int j=0;j<nr_class;j++)
//                    output.writeBytes(prob_estimates[j]+" ");
//                output.writeBytes("\n");
//            }
//            else
//            {
//                v = svm.svm_predict(model,x);
//                output.writeBytes(v+"\n");
//            }
//
//            if(v == target)
//                ++correct;
//            error += (v-target)*(v-target);
//            sumv += v;
//            sumy += target;
//            sumvv += v*v;
//            sumyy += target*target;
//            sumvy += v*target;
//            ++total;
//        }
//        if(svm_type == svm_parameter.EPSILON_SVR ||
//                svm_type == svm_parameter.NU_SVR)
//        {
//            svm_predict.info("Mean squared error = "+error/total+" (regression)\n");
//            svm_predict.info("Squared correlation coefficient = "+
//                    ((total*sumvy-sumv*sumy)*(total*sumvy-sumv*sumy))/
//                            ((total*sumvv-sumv*sumv)*(total*sumyy-sumy*sumy))+
//                    " (regression)\n");
//        }
//        else
////            svm_predict.info("Accuracy = "+(double)correct/total*100+"% ("+correct+"/"+total+") (classification)\n");
//            svm_predict.info(categories+"\tAccuracy\n" +(double)correct/total+"\n");
//    }
//    public static void exit_with_help() {
//        System.err.print("usage: svm_predict [options] test_file model_file output_file\n"
//                + "options:\n"
//                + "-b probability_estimates: whether to predict probability estimates, 0 or 1 (default 0); one-class SVM not supported yet\n"
//                + "-q : quiet mode (no outputs)\n");
//        System.exit(1);
//    }


}