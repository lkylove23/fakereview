package libSVM;

import libsvm.svm;
import libsvm.svm_model;
import libsvm.svm_parameter;

import java.io.*;
import java.util.ArrayList;

/**
 * Created by lkylove23 on 2015-01-27.
 */
public class Evaluation_libSVM_allcate_v3 {



    public static void main(String args[]) throws IOException {
        /** allcate:  "_porter"
         *  NoR: ""
         * **/

         /** Choose **/
        ArrayList<Integer> listOfTopicNum = new ArrayList<Integer>();
        listOfTopicNum.add(2);
        listOfTopicNum.add(10);
        listOfTopicNum.add(50);
//        listOfTopicNum.add(100);
        listOfTopicNum.add(200);
        listOfTopicNum.add(300);
//        listOfTopicNum.add(500);
//        listOfTopicNum.add(700);
//        listOfTopicNum.add(1000);


        String categories = "";
        String porter = "_porter"; //using porter
//        String porter = "_noP"; // without using porter


        String where = "D:\\";

        String test = "";
        String train = "";
//        FileWriter totalSampleWriter = new FileWriter(where + "FakeReviewDetectionData\\result\\Yelp\\numOfReviews\\Total" + porter + "_Result" + "_" + mode + ".txt");
//        int numOfReview = 200;

        int numOfReview = 0;


        for (int tn = 0; tn < listOfTopicNum.size(); tn++) {

            /**Mode Selection**/
            int topicNum = listOfTopicNum.get(tn);

        // only lda
//            String mode =  "lda" + Integer.toString(topicNum);
            String mode =  "ldaT"+ Integer.toString(topicNum); ;

            // weighted lda svm
//            String mode = "lda" + Integer.toString(topicNum)+"_w1";
//            String mode = "lda" + Integer.toString(topicNum)+"_w2";
//            String mode = "lda" + Integer.toString(topicNum)+"_w5";
            // weighted lda svm + UB
//            String mode = "lda" + Integer.toString(topicNum)+"_w1UB";
//            String mode = "lda" + Integer.toString(topicNum)+"_w2";
//            String mode = "lda" + Integer.toString(topicNum)+"_w5UB";

//        String mode = "Unigram";
//        String mode = "Bigram";
//        String mode = "UB";
//            String mode = tm + "UB";
            FileWriter totalSampleWriter = new FileWriter(where + "FakeReviewDetectionData\\result\\Yelp\\Total" + porter + "_Result" + "_" + mode + ".txt");
            Double totalResult[][] = new Double[7][5];
        //

//                for (int c_it = 0; c_it < 7; c_it++) {
                for (int c_it = 0; c_it < 6; c_it++) {

//        for(int c_it=0;c_it<4;c_it++) {
//        for(int c_it=5;c_it<7;c_it++) {
                    if (c_it == 0) {
                        if (numOfReview > 440) {
                            continue;
                        }
                        //Electronics
//                changeID = 44;
                        categories = "electronics";
                    } else if (c_it == 1) {
                        if (numOfReview > 26900) {
                            continue;
                        }
                        //Fashion
                        categories = "fashion";
                    } else if (c_it == 2) {
                        if (numOfReview >= 200) {
                            continue;
                        }
                        //hospitals
                        categories = "hospitals";
                    } else if (c_it == 3) {
                        if (numOfReview > 11000) {
                            continue;
                        }
                        //hotels
                        categories = "hotels";
                    } else if (c_it == 4) {
                        if (numOfReview > 130) {
                            continue;
                        }
                        //Insurance
                        categories = "insurance";
                    } else if (c_it == 5) {
                        if (numOfReview > 1190) {
                            continue;
                        }
                        //Musicvenues
                        categories = "musicvenues";
                    } else if (c_it == 6) {
                        if (numOfReview > 37980) {
                            continue;
                        }
                        //Restaurant
//                totalNumOfReview = 37990;
                        categories = "restaurants";
                    }
                    String File_helper = where + "FakeReviewDetectionData\\Yelp\\" + categories + "\\BalancedForLDA" + "\\";
                    String ResultFile_helper = where + "FakeReviewDetectionData\\result\\Yelp\\";
                    File ForCheckDir = new File(File_helper);
                    if (!ForCheckDir.isDirectory()) {
                        ForCheckDir.mkdir();
                    }
//                    FileWriter categoryWriter = new FileWriter(ResultFile_helper + categories + "\\" +categories + porter + "_Result.txt");
                    for (int it = 0; it < 5; it++) {
                        if (it == 0) {
                            test = "fold1";
                            train = "fold2,3,4,5";
                        } else if (it == 1) {
                            test = "fold2";
                            train = "fold1,3,4,5";
                        } else if (it == 2) {
                            test = "fold3";
                            train = "fold1,2,4,5";
                        } else if (it == 3) {
                            test = "fold4";
                            train = "fold1,2,3,5";
                        } else if (it == 4) {
                            test = "fold5";
                            train = "fold1,2,3,4";
                        }
                        String trainFile = File_helper + categories + "_" + train + "_" + mode + porter+ ".txt";
                        String svmWeightFile = File_helper + categories + "_" + train + "_w_" + mode + porter+ ".txt";
//                        String svmCoefFile = File_helper + categories + "_" + train + "_coef_" + mode + porter+ ".txt";

                        String resultFile = File_helper + categories  + "_" + test + "_" + mode+  porter + "_result.txt";
                        String trainModelFile = File_helper + categories   + "_" + train + "_m_" + mode + porter+ ".txt";
//                        String trainWeightFile = File_helper + categories  + "_" + train + "_w" + "_" + mode+ porter + ".txt";

                        /**TRAIN PART**/

                        String[] cmdLine = new String[2]; // [option] [training_data] [model_file]
                        // -c cost : set the parameter C of C-SVC, epsilon-SVR, and nu-SVR (default 1)
                        cmdLine[0] = trainFile;
                        cmdLine[1] = trainModelFile;
                        svm_train t = new svm_train();
                        t.run(cmdLine);
//                        t.saveWeight(svmWeightFile);

//                        FileWriter SVWriter= new FileWriter(svmWeightFile);
//                        StringBuffer SVOut= new StringBuffer();

//                        for(int i=0;i<SV.length;i++){
//                            for(int ii=0; ii<SV[i].length;ii++){
//                                SVOut.append(SV[i][ii].index);
//                                SVOut.append(":");
//                                SVOut.append(SV[i][ii].value);
//                                SVOut.append(" ");
//                            }
//                            SVOut.append("\n");
//                        }
//                        SVWriter.append(SVOut.toString().trim());
//                        SVWriter.close();
//
////                        FileWriter sv_coefWriter= new FileWriter(svmCoefFile);
//                        StringBuffer sv_coefOut= new StringBuffer();
//                        double[][] sv_coef= t.run2(cmdLine).sv_coef;// run2 return generated model
//                        for(int i=0;i<sv_coef.length;i++){
//                            for(int ii=0; ii<sv_coef[i].length;ii++){
//                                sv_coefOut.append(ii);
//                                sv_coefOut.append(":");
//                                sv_coefOut.append(sv_coef[i][ii]);
//                                sv_coefOut.append(" ");
//                            }
//                            sv_coefOut.append("\n");
//                        }
//                        sv_coefWriter.append(sv_coefOut.toString().trim());
//                        sv_coefWriter.close();

                        // test


//                        FileWriter WeightWriter= new FileWriter(trainWeightFile);
//                        WeightWriter.append("Size of weight: ");
//                        WeightWriter.append(String.valueOf(t.param.weight.length));
//                        WeightWriter.append("\n");
//                        for(int wi=0; wi<t.param.weight_label.length;wi++){
//                            WeightWriter.append(String.valueOf(t.param.weight_label[wi]));
//                            WeightWriter.append("\t");
//                        }
//                        WeightWriter.append("\n");
//                        for(int wi=0; wi<t.param.weight.length;wi++){
//                            WeightWriter.append(String.valueOf(t.param.weight[wi]));
//                            WeightWriter.append("\t");
//                        }
//                        WeightWriter.close();
//                }
                        /**PREDICT PART**/
                    //precision recall => add later
                        int i, predict_probability = 0; // what is
                        try {
                            String testFile = File_helper + categories  + "_" + test + "_" + mode +porter+ ".txt";
                            BufferedReader testFileB = new BufferedReader(new FileReader(testFile)); //test file
                            DataOutputStream output = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(resultFile)));
                            svm_model model = svm.svm_load_model(trainModelFile);
                            if (model == null) {
                                System.err.print("can't open model file " + trainModelFile + "\n");
                                System.exit(1);
                            }
                            if (predict_probability == 1) {
                                if (svm.svm_check_probability_model(model) == 0) {
                                    System.err.print("Model does not support probabiliy estimates\n");
                                    System.exit(1);
                                }
                            } else {
                                if (svm.svm_check_probability_model(model) != 0) {
                                    svm_predict.info("Model supports probability estimates, but disabled in prediction.\n");
                                }
                            }

                            totalResult[c_it][it]=svm_predict.predict(testFileB, output, model, predict_probability);
                            testFileB.close();
                            output.close();
                        } catch (FileNotFoundException e) {
                            svm_predict.exit_with_help();
                        } catch (ArrayIndexOutOfBoundsException e) {
                            svm_predict.exit_with_help();
                        }
                    }
                    double temp_avg = 0.0;
                    totalSampleWriter.append(categories);
                    for (int it = 0; it < 5; it++) {
                        System.out.println(totalResult[c_it][it]);
//                        categoryWriter.append("\n");
                        totalSampleWriter.append("\n");
//                        categoryWriter.append(Double.toString(Acc_fold[it]));
                        totalSampleWriter.append(Double.toString(totalResult[c_it][it]));
                        temp_avg += totalResult[c_it][it];
                    }
                    temp_avg /= 5;
//                    categoryWriter.append("\n");
                    totalSampleWriter.append("\n");
//                    categoryWriter.append(Double.toString(temp_avg));
                    totalSampleWriter.append(Double.toString(temp_avg));

//                    categoryWriter.append("\n");
                    totalSampleWriter.append("\n");
//                    categoryWriter.append("\n");
                    totalSampleWriter.append("\n");
//                    categoryWriter.close();
                    System.out.println(mode+"\t" +categories);
                }
                totalSampleWriter.close();



    }
}
}