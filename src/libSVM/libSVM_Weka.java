package libSVM;

//import weka.classifiers.functions.LibSVM;
import libsvm.LibSVM;
import libsvm.svm_parameter;
import net.sf.javaml.classification.Classifier;
import net.sf.javaml.tools.data.ARFFHandler;
import weka.core.Instances;
import weka.core.SelectedTag;
import weka.core.converters.ConverterUtils.DataSource;
import weka.classifiers.Evaluation;

import java.io.File;
import java.io.IOException;
import java.util.Random;
import net.sf.javaml.classification.AbstractClassifier;
import net.sf.javaml.core.Dataset;
import net.sf.javaml.core.Instance;
/**
 * Created by lkylove23 on 2015-02-27.
 */
public class libSVM_Weka {
    public static void main(String args[]) throws Exception {
        Dataset data= ARFFHandler.loadARFF(new File("D:\\FakeReviewDetectionData\\Yelp\\electronics\\BalancedForLDA\\electronics_fold1,2,3,4_lda100_porter.arff"),101);
//        Dataset data= ARFFHandler.loadARFF(new File("D:\\FakeReviewDetectionData\\Yelp\\electronics\\BalancedForLDA\\electronics_fold1,2,3,4_lda100_porter.arff"));
//        Instances train_data = source.getDataSet();
        data.classIndex(101);
        System.out.println(data.noAttributes());
        LibSVM svm = new LibSVM();
                svm_parameter param=new svm_parameter();
        param.svm_type=svm_parameter.C_SVC;
        param.kernel_type=svm_parameter.LINEAR;
        svm.setParameters(param);
        System.out.println(svm.getParameters().svm_type);

        svm.buildClassifier(data);
        double[] w= svm.getWeights();
        for(int j=0;j<w.length;j++){
            System.out.println(j+1+":"+ w[j]);
        }


//        // setting class attribute if the data format does not provide this information
//        // For example, the XRFF format saves the class attribute information as well
//        if (train_data.classIndex() == -1)
//            train_data.setClassIndex(train_data.numAttributes() - 1);
//
//        LibSVM svm = new LibSVM();
//        svm.buildClassifier(train_data);
//        Evaluation evaluation = null;
//        evaluation = new Evaluation (train_data);
//        evaluation.crossValidateModel(svm, train_data, 10, new Random(1));
//        System.out.println("weights = "+ svm.getWeights());
//
//                if (train_data.classIndex() == -1){
//            train_data.setClassIndex(train_data.numAttributes() - 1);}
//        LibSVM svm = new LibSVM();
//        svm_parameter param=new svm_parameter();
//        param.svm_type=svm_parameter.C_SVC;
//        param.kernel_type=svm_parameter.LINEAR;
//        svm.setParameters(param);
////        svm.setKernelType(new SelectedTag(LibSVM.KERNELTYPE_LINEAR,LibSVM.TAGS_KERNELTYPE));
////        svm.setSVMType(new SelectedTag(LibSVM.SVMTYPE_C_SVC, LibSVM.TAGS_SVMTYPE));
//
//        svm.buildClassifier(train_data);
//        Evaluation evaluation = null;
//        evaluation = new Evaluation (train_data);
//        evaluation.crossValidateModel(svm, train_data, 10, new Random(1));
//        System.out.println("weights = "+ svm.getWeights());


    }
}
