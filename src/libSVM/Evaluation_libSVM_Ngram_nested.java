package libSVM;

import libsvm.svm;
import libsvm.svm_model;
import libsvm.svm_node;
import libsvm.svm_parameter;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;

/**
 * Created by lkylove23 on 2015-01-27.
 */
public class Evaluation_libSVM_Ngram_nested {

    // public static String mode ="Unigram";
    public static String mode ="Bigram";
    public static String s_itr ="";

   public static String categories="";
    public static void main(String args[]) throws IOException {
//        mode ="Unigram";
        mode ="Bigram";
        s_itr = "";
//        s_itr=" (2)";
//        s_itr=" (3)";
//	      s_itr=" (4)";
//	      s_itr=" (5)";
        // as Wiki showed  http://en.wikipedia.org/wiki/Support_vector_machine
//        HashMap<Integer,Double> mapOfParamC= new HashMap<Integer,Double>();
        ArrayList<Double> listOfParamC= new ArrayList<Double>();
//for test
        listOfParamC.add(Math.pow(2, -1));
        listOfParamC.add(Math.pow(2, 1));
        listOfParamC.add(Math.pow(2, 3));

//

//        listOfParamC.add(0, Math.pow(2, -5));
//        listOfParamC.add(1, Math.pow(2, -3));
//        listOfParamC.add(2, Math.pow(2, -1));
//        listOfParamC.add(3, Math.pow(2, 1));
//        listOfParamC.add(4, Math.pow(2, 3));
//        listOfParamC.add(5, Math.pow(2, 5));
//        listOfParamC.add(6, Math.pow(2, 7));
//        listOfParamC.add(7, Math.pow(2, 9));
//        listOfParamC.add(8, Math.pow(2, 10));
//        listOfParamC.add(9, Math.pow(2, 11));
//        listOfParamC.add(10, Math.pow(2, 13));
//        listOfParamC.add(11, Math.pow(2, 15));
        ArrayList<Double> listOfParamGamma= new ArrayList<Double>();
//        for test
        listOfParamGamma.add(Math.pow(2,-11));
        listOfParamGamma.add(Math.pow(2,-9));
//

//        listOfParamGamma.add(0,Math.pow(2,-15));
//        listOfParamGamma.add(1,Math.pow(2,-13));
//        listOfParamGamma.add(2,Math.pow(2,-11));
//        listOfParamGamma.add(3,Math.pow(2,-9));
//        listOfParamGamma.add(4,Math.pow(2,-7));
//        listOfParamGamma.add(5,Math.pow(2,-5));
//        listOfParamGamma.add(6,Math.pow(2,-3));
//        listOfParamGamma.add(7,Math.pow(2,-1));
//        listOfParamGamma.add(8,Math.pow(2,1));
//        listOfParamGamma.add(9,Math.pow(2,3));

        String test="";
        String train= "";
        String where = "D:\\";
        int numOfReview=200;


        for(int c_it=0;c_it<7;c_it++) {
//        for(int c_it=0;c_it<4;c_it++) {
//        for(int c_it=5;c_it<7;c_it++) {
            if (c_it == 0) {
                if (numOfReview > 440) {
                    continue;
                }
                //Electronics
//                changeID = 44;
                categories = "electronics";
            } else if (c_it == 1) {
                if (numOfReview > 26900) {
                    continue;
                }
                //Fashion
                categories = "fashion";
            } else if (c_it == 2) {
                if (numOfReview >= 200) {
                    continue;
                }
                //hospitals
                categories = "hospitals";
            } else if (c_it == 3) {
                if (numOfReview > 11000) {
                    continue;
                }
                //hotels
                categories = "hotels";
            } else if (c_it == 4) {
                if (numOfReview >= 130) {
                    continue;
                }
                //Insurance
                categories = "insurance";
            } else if (c_it == 5) {
                if (numOfReview > 1190) {
                    continue;
                }
                //Musicvenues
                categories = "musicvenues";
            } else if (c_it == 6) {
                if (numOfReview > 37980) {
                    continue;
                }
                //Restaurant
//                totalNumOfReview = 37990;
                categories = "restaurants";
            }

            String File_helper = where + "FakeReviewDetectionData\\Yelp\\numOfReviews\\" + categories + "_" + numOfReview + s_itr + "\\";
            FileWriter writer = new FileWriter(File_helper + categories + "_" + numOfReview + "_Result.txt");
            double best_inner_Acc[] = new double[5];
            int bestC_id[] = new int[5];
            int bestGamma_id[] = new int[5];
            for (int it = 0; it < 5; it++) {
                if (it == 0) {
                    test = "fold1";
                    train = "fold2,3,4,5";
                } else if (it == 1) {
                    test = "fold2";
                    train = "fold1,3,4,5";
                } else if (it == 2) {
                    test = "fold3";
                    train = "fold1,2,4,5";
                } else if (it == 3) {
                    test = "fold4";
                    train = "fold1,2,3,5";
                } else if (it == 4) {
                    test = "fold5";
                    train = "fold1,2,3,4";
                }
                String trainFile = File_helper + categories + "_" + numOfReview + "_" + mode + "_" + train + ".dat";
                String resultFile = File_helper + categories + "_" + numOfReview + "_" + mode + "_" + test + "_result.txt";
                String trainModelFile = File_helper + categories + "_" + numOfReview + "_" + mode + "_" + train + "_m.txt";
//                String[] inner_trainModelFile= new String[4];
                ArrayList<String>[] inner_trainModelFile = new ArrayList[4];

                /**size check**/
                String line = "";
                BufferedReader br = new BufferedReader(new FileReader(trainFile));
                int sizeOfTrain = 0;
                while ((line = br.readLine()) != null) {
                    sizeOfTrain++;
                }
                // sizeOfFold is also size of test (5 fold)
                int sizeOfFold = sizeOfTrain / 4;
                /**inner train**/
                File f = new File(trainModelFile);
                // 파일 존재 여부 판단
//                if (!f.isFile()) {

                    //for c
                    // for gamma
                    for (int i = 0; i < 4; i++) {
                        // i : inner_it
                        for (int ii = 0; ii < listOfParamC.size(); ii++) {
                            for (int iii = 0; iii < listOfParamGamma.size(); iii++) {
                                // inner_it     id_C    id_Gamma
//                                inner_trainModelFile[i].add(File_helper + categories + "_" + numOfReview + "_" + mode + "_" + train +"_"+i+"_"+ii+"_"+iii+ "_m.txt");
                                System.out.println(categories+" it: "+it+" inner_it: "+i+" id of C: "+ii+"\tid of Gamma: "+iii+"\t of"+listOfParamC.size()+"   "+listOfParamGamma.size());
                                long start = System.currentTimeMillis();
                                String temp_innerM = File_helper + categories + "_" + numOfReview + "_" + mode + "_" + train + "_" + i + "_" + ii + "_" + iii + "_m.txt";
                                svm_train inner_t = new svm_train();
                                inner_t.inner_run(trainFile, temp_innerM,sizeOfFold, i, listOfParamC.get(ii), listOfParamGamma.get(iii));
                                long end = System.currentTimeMillis();
                                System.out.println( "Spent time : " + ( end - start )/60000.0 +" min.");
                            }
                        }
                    }
//                }
                /**inner test**/
                // choose best parameter
//                int bestC_id=0;
//                int bestGamma_id=0;
                best_inner_Acc[it] = 0;
//                for(int i=0;i<4;i++) {
                // i : inner_it
                for (int ii = 0; ii < listOfParamC.size(); ii++) {
                    for (int iii = 0; iii < listOfParamGamma.size(); iii++) {
                        double tmp_inner_Acc_avg = 0;
                        for (int i = 0; i < 4; i++) {
                            double tmp_inner_Acc = 0;
                            int predict_probability = 0; // what is
                            try {
//                                String testFile = File_helper + categories + "_" + numOfReview + "_" + mode + "_" + test + ".dat";
//                                BufferedReader testFileB = new BufferedReader(new FileReader(testFile)); //test file
                                BufferedReader inner_testFileB = new BufferedReader(new FileReader(trainFile)); //test file
                                //change
                                String tempResultFile = File_helper + categories + "_" + numOfReview + "_" + mode + "_" + train + "_" + i + "_" + ii + "_" + iii + "_r.txt";
                                String tempModelFile = File_helper + categories + "_" + numOfReview + "_" + mode + "_" + train + "_" + i + "_" + ii + "_" + iii + "_m.txt";
                                DataOutputStream output = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(tempResultFile)));
                                //change
                                svm_model model = svm.svm_load_model(tempModelFile);
                                if (model == null) {
                                    System.err.print("can't open model file " + trainModelFile + "\n");
                                    System.exit(1);
                                }
                                if (predict_probability == 1) {
                                    if (svm.svm_check_probability_model(model) == 0) {
                                        System.err.print("Model does not support probabiliy estimates\n");
                                        System.exit(1);
                                    }
                                } else {
                                    if (svm.svm_check_probability_model(model) != 0) {
                                        svm_predict.info("Model supports probability estimates, but disabled in prediction.\n");
                                    }
                                }
                                tmp_inner_Acc = svm_predict.inner_predict(inner_testFileB, output, model, predict_probability, i, sizeOfFold);
                                tmp_inner_Acc_avg += tmp_inner_Acc;
                                inner_testFileB.close();
                                output.close();
                            } catch (FileNotFoundException e) {
                                exit_with_help();
                            } catch (ArrayIndexOutOfBoundsException e) {
                                exit_with_help();
                            }
                        }
                        tmp_inner_Acc_avg /= 4;
                        if (best_inner_Acc[it] < tmp_inner_Acc_avg) {
                            best_inner_Acc[it] = tmp_inner_Acc_avg;
                            bestC_id[it] = ii;
                            bestGamma_id[it] = iii;
                        }
                    }
                }
            }
            System.out.println("iter    best_C  best_Gamma  inner_Acc");
            for (int i = 0; i < 4; i++) {
                System.out.println(i + "\t" + bestC_id[i] + "\t" + bestGamma_id[i] + "\t" + best_inner_Acc[i]);
            }


            /////////////////////////////////////////Here///////////////////////////////

            /**TRAIN PART**/
        double[] Acc_fold=new double[5];
            for (int it = 0; it < 5; it++) {
                if (it == 0) {
                    test = "fold1";
                    train = "fold2,3,4,5";
                } else if (it == 1) {
                    test = "fold2";
                    train = "fold1,3,4,5";
                } else if (it == 2) {
                    test = "fold3";
                    train = "fold1,2,4,5";
                } else if (it == 3) {
                    test = "fold4";
                    train = "fold1,2,3,5";
                } else if (it == 4) {
                    test = "fold5";
                    train = "fold1,2,3,4";
                }
                System.out.println("train "+it);
//                String File_helper = where + "FakeReviewDetectionData\\Yelp\\numOfReviews\\" + categories + "_" + numOfReview + s_itr + "\\";
                String trainFile = File_helper + categories + "_" + numOfReview + "_" + mode + "_" + train + ".dat";
                String resultFile = File_helper + categories + "_" + numOfReview + "_" + mode + "_" + test + "_result.txt";
                String trainModelFile = File_helper + categories + "_" + numOfReview + "_" + mode + "_" + train + "_m.txt";

                File f = new File(trainModelFile);
                // 파일 존재 여부 판단
                //use setting instead of parsecommand
//                if (!f.isFile()) {

//                String testFile = File_helper + categories + "_" + numOfReview + "_" + mode + "_" + test + ".dat";
                String[] cmdLine = new String[2]; // [option] [training_data] [model_file]
                // -c cost : set the parameter C of C-SVC, epsilon-SVR, and nu-SVR (default 1)
                cmdLine[0] = trainFile;
                cmdLine[1] = trainModelFile;
                svm_train t = new svm_train();
//                t.run(cmdLine);
                    t.run(trainFile,trainModelFile,bestC_id[it],bestGamma_id[it]);
//            }
            /**PREDICT PART**/
                System.out.println("predict");
            // use best_c result of inner_run(String argv[])
            int i, predict_probability = 0; // what is
            try {
                String testFile = File_helper + categories + "_" + numOfReview + "_" + mode + "_" + test + ".dat";
                BufferedReader testFileB = new BufferedReader(new FileReader(testFile)); //test file
                DataOutputStream output = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(resultFile)));
                svm_model model = svm.svm_load_model(trainModelFile);
                if (model == null) {
                    System.err.print("can't open model file " + trainModelFile + "\n");
                    System.exit(1);
                }
                if (predict_probability == 1) {
                    if (svm.svm_check_probability_model(model) == 0) {
                        System.err.print("Model does not support probabiliy estimates\n");
                        System.exit(1);
                    }
                } else {
                    if (svm.svm_check_probability_model(model) != 0) {
                        svm_predict.info("Model supports probability estimates, but disabled in prediction.\n");
                    }
                }

                Acc_fold[it]=svm_predict.predict(testFileB, output, model, predict_probability);
                testFileB.close();
                output.close();
            } catch (FileNotFoundException e) {
                exit_with_help();
            } catch (ArrayIndexOutOfBoundsException e) {
                exit_with_help();
            }
        }

            System.out.println(categories);
//            writer.append(categories);

            double temp_avg=0.0;
            for(int it=0;it<5;it++) {
                System.out.println(Acc_fold[it]);
                writer.append("\n");
                writer.append(Double.toString(Acc_fold[it]));
                temp_avg+=Acc_fold[it];
            }
            temp_avg/=5;
            writer.append("\n");
            writer.append(Double.toString(temp_avg));

            writer.append("\n");
            writer.append("\n");
            for(int it=0;it<5;it++) {
                System.out.println(bestC_id[it]);
                System.out.println(bestGamma_id[it]);

                writer.append(Integer.toString(bestGamma_id[it]));
                writer.append("\n");
                writer.append(Integer.toString(bestC_id[it]));
                writer.append("\n");
            }
            writer.close();

        }
        }


    //??
//    public static void predict(BufferedReader input, DataOutputStream output, svm_model model, int predict_probability) throws IOException
//    {
//        int correct = 0;
//        int total = 0;
//        double error = 0;
//        double sumv = 0, sumy = 0, sumvv = 0, sumyy = 0, sumvy = 0;
//
//        int svm_type=svm.svm_get_svm_type(model);
//        int nr_class=svm.svm_get_nr_class(model);
//        double[] prob_estimates=null;
//
//        if(predict_probability == 1)
//        {
//            if(svm_type == svm_parameter.EPSILON_SVR ||
//                    svm_type == svm_parameter.NU_SVR)
//            {
//                svm_predict.info("Prob. model for test data: target value = predicted value + z,\nz: Laplace distribution e^(-|z|/sigma)/(2sigma),sigma="+svm.svm_get_svr_probability(model)+"\n");
//            }
//            else
//            {
//                int[] labels=new int[nr_class];
//                svm.svm_get_labels(model,labels);
//                prob_estimates = new double[nr_class];
//                output.writeBytes("labels");
//                for(int j=0;j<nr_class;j++)
//                    output.writeBytes(" "+labels[j]);
//                output.writeBytes("\n");
//            }
//        }
//        while(true)
//        {
//            String line = input.readLine();
//            if(line == null) break;
//
//            StringTokenizer st = new StringTokenizer(line," \t\n\r\f:");
//
//            double target = atof(st.nextToken());
//            int m = st.countTokens()/2;
//            svm_node[] x = new svm_node[m];
//            for(int j=0;j<m;j++)
//            {
//                x[j] = new svm_node();
//                x[j].index = atoi(st.nextToken());
//                x[j].value = atof(st.nextToken());
//            }
//
//            double v;
//            if (predict_probability==1 && (svm_type==svm_parameter.C_SVC || svm_type==svm_parameter.NU_SVC))
//            {
//                v = svm.svm_predict_probability(model,x,prob_estimates);
//                output.writeBytes(v+" ");
//                for(int j=0;j<nr_class;j++)
//                    output.writeBytes(prob_estimates[j]+" ");
//                output.writeBytes("\n");
//            }
//            else
//            {
//                v = svm.svm_predict(model,x);
//                output.writeBytes(v+"\n");
//            }
//
//            if(v == target)
//                ++correct;
//            error += (v-target)*(v-target);
//            sumv += v;
//            sumy += target;
//            sumvv += v*v;
//            sumyy += target*target;
//            sumvy += v*target;
//            ++total;
//        }
//        if(svm_type == svm_parameter.EPSILON_SVR ||
//                svm_type == svm_parameter.NU_SVR)
//        {
//            svm_predict.info("Mean squared error = "+error/total+" (regression)\n");
//            svm_predict.info("Squared correlation coefficient = "+
//                    ((total*sumvy-sumv*sumy)*(total*sumvy-sumv*sumy))/
//                            ((total*sumvv-sumv*sumv)*(total*sumyy-sumy*sumy))+
//                    " (regression)\n");
//        }
//        else
////            svm_predict.info("Accuracy = "+(double)correct/total*100+"% ("+correct+"/"+total+") (classification)\n");
//            svm_predict.info(categories+"\tAccuracy\n" +(double)correct/total+"\n");
//    }
    public static void exit_with_help()
    {
        System.err.print("usage: svm_predict [options] test_file model_file output_file\n"
                +"options:\n"
                +"-b probability_estimates: whether to predict probability estimates, 0 or 1 (default 0); one-class SVM not supported yet\n"
                +"-q : quiet mode (no outputs)\n");
        System.exit(1);
    }
    public static double atof(String s)
    {
        return Double.valueOf(s).doubleValue();
    }

    public static int atoi(String s)
    {
        return Integer.parseInt(s);
    }
}


