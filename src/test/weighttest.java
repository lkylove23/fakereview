package test;

import libSVM.svm_train;

import java.io.IOException;

/**
 * Created by lkylove23 on 2015-03-07.
 */
public class weighttest {
    public static void main(String args[]) throws IOException {

        String trainFile ="D:\\Download\\iris_test2.txt";
        String trainModelFile ="D:\\Download\\iris_test2_m.txt";
        String svmWeightFile ="D:\\Download\\iris_test2_w.txt";

        String[] cmdLine = new String[2]; // [option] [training_data] [model_file]
        // -c cost : set the parameter C of C-SVC, epsilon-SVR, and nu-SVR (default 1)
        cmdLine[0] = trainFile;
        cmdLine[1] = trainModelFile;
        svm_train t = new svm_train();
        t.run(cmdLine);
        t.saveWeight(svmWeightFile);
    }
}
