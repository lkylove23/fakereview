package test;

import cc.mallet.pipe.*;
import cc.mallet.pipe.iterator.CsvIterator;
import cc.mallet.topics.ParallelTopicModel;
import cc.mallet.topics.TopicInferencer;
import cc.mallet.types.*;

import java.io.*;
import java.util.*;
import java.util.regex.Pattern;

public class LDA_test {

	public static void main(String[] args) throws Exception {
        /**Choose**/
        //KY
        String where="D:\\";
        //or Dear IRbig2?
//		String where="D:\\KY\\";

        ArrayList<Double> param_tw= new ArrayList<Double>();
        String categories="";
        int totalNumOfReview=0;
//        String stemming="";
        String stemming="_porter";
//        String withstopword="_withSW";
        int c_it=0;
        int it=0;
        ArrayList<Integer> listOfTopicNum = new ArrayList<Integer>();
        listOfTopicNum.add(2);
        listOfTopicNum.add(10);
        listOfTopicNum.add(50);
        listOfTopicNum.add(100); //already done
        listOfTopicNum.add(200);
        listOfTopicNum.add(300);

        ///
        listOfTopicNum.add(500);
        listOfTopicNum.add(1000);


        String test="";
        String train="";
        for (int tn = 0; tn < listOfTopicNum.size(); tn++) {
            int topicNum=listOfTopicNum.get(tn);
        for( c_it=0;c_it<7;c_it++) {
            if (c_it == 0) {
                //Electronics
//                changeID = 44;
                totalNumOfReview=440;
                categories = "electronics";
            } else if (c_it == 1) {
                //Fashion
                totalNumOfReview = 2690;
                categories = "fashion";
            } else if (c_it == 2) {
                //hotels
                totalNumOfReview = 1100;
                categories = "hotels";
            } else if (c_it == 3) {
                //hospitals
                totalNumOfReview = 200;
                categories = "hospitals";
            } else if (c_it == 4) {
                //Insurance
                totalNumOfReview = 130;
                categories = "insurance";
            } else if (c_it == 5) {
                //Musicvenues
                totalNumOfReview = 1190;
                categories = "musicvenues";
            } else if (c_it == 6) {
                //Restaurant
                totalNumOfReview = 37990;
//			changeID=115;
                categories = "restaurants";
            }
            for(it=0;it<5;it++) {
        if (it == 0) {
            test = "fold1";
            train = "fold2,3,4,5";
        } else if (it == 1) {
            test = "fold2";
            train = "fold1,3,4,5";
        } else if (it == 2) {
            test = "fold3";
            train = "fold1,2,4,5";
        } else if (it == 3) {
            test = "fold4";
            train = "fold1,2,3,5";
        } else if (it == 4) {
            test = "fold5";
            train = "fold1,2,3,4";
        }

        String resultFile_helper = where + "FakeReviewDetectionData\\Yelp\\"+categories+"\\";
        String str="";
        String trainingLDAFile = resultFile_helper+ "BalancedForLDA\\" +categories+"_"+train+"_lda"+topicNum+stemming+".txt";
        String trainingFile = resultFile_helper+ "BalancedForLDA\\" +categories+"_"+train+"_porter.txt";
        String testingLDAFile = resultFile_helper + "BalancedForLDA\\" +categories+"_"+test+"_lda"+topicNum+stemming+".txt";
        String testingFile = resultFile_helper + "BalancedForLDA\\" +categories+"_"+test+"_porter.txt";



        int cnt=0;
		int classChange_Train = 0; //classChange ��° doc ���� �ٸ� class
		int classChange_Test =0;

        ArrayList<Integer> TrainIDLabel = new ArrayList<Integer>();
        ArrayList<Integer> TestIDLabel = new ArrayList<Integer>();

//      above 500  iteration 300 is maximum....
//		int topicNum = 400;

//		int topicNum = 500;
//		int topicNum = 1000;

//        ArrayList<Integer> listOfDocNum =new ArrayList<Integer>();
//        listOfDocNum.add(200);
//        listOfDocNum.add(500);
//        listOftopicNum.add(1000);
//        listOftopicNum.add(5000);
//        listOftopicNum.add(10000);
//        listOftopicNum.add(20000);
//        listOftopicNum.add(35000);
		double highAccuracy=0.0;
		double highTau=0.0;
		String resultFile="";
		ArrayList<Integer> highTopic_F =new ArrayList<Integer>();
		ArrayList<Integer> highTopic_T =new ArrayList<Integer>();

        FileWriter writer= null;

		
		// Begin by importing documents from text to feature sequences
		ArrayList<Pipe> pipeListForTraining = new ArrayList<Pipe>();
		//  (TrainingFile,classChange)
		// Pipes: lowercase, tokenize, remove stopwords, map to features
		pipeListForTraining.add( new CharSequenceLowercase() );
		pipeListForTraining.add( new CharSequence2TokenSequence(Pattern.compile("\\p{L}[\\p{L}\\p{P}]+\\p{L}")) );
//		pipeListForTraining.add( new TokenSequenceRemoveStopwords(new File("stoplists/en.txt"), "UTF-8", false, false, false) );
		pipeListForTraining.add( new TokenSequenceRemoveStopwords(new File("D:\\workspace\\FRD\\stoplists\\en.txt"), "UTF-8", false, false, false) );
		pipeListForTraining.add( new TokenSequence2FeatureSequence() );

        double ave;
        double aveOfAve=0.0;
        int changeID=0; // the # of fake reviews




        StringBuffer LDATrainOut =new StringBuffer();
        StringBuffer LDATestOut =new StringBuffer();
        StringBuffer LDAResultOut =new StringBuffer();
//          int totalNumOfReview = 200;
//        int totalNumOfReview = 35000;
    System.out.println(totalNumOfReview+"\t"+topicNum+"\t"+it+" start!");
	    String s_itr="";
//	    String s_itr=" (2)";
//        String s_itr=" (3)";
//	    String s_itr=" (4)";
//	    String s_itr=" (5)";


            changeID=totalNumOfReview/10;
//			changeID=115;


		InstanceList trainingInstances = new InstanceList (new SerialPipes(pipeListForTraining));

//		Reader fileReader = new InputStreamReader(new FileInputStream(new File(args[0])), "UTF-8");
//		Reader fileReader = new InputStreamReader(new FileInputStream(new File("E:\\forma\\ap.txt")), "UTF-8");
//		Reader fileReader = new InputStreamReader(new FileInputStream(new File("E:\\FakeReviewDetectionData\\DefaultTraining.txt")), "UTF-8");

//		if(args.length!=0){
//			if(args.length!=6){
//				System.out.println("You have to type 5 argument\n # of topics\ttraining file\t# of fake reviews in training file\ttest file\tresult file\tvalue of tau");
//			}
//			numTopics = Integer.parseInt(args[0]);
//			trainingFile=args[1];
//			classChange_Train=Integer.parseInt(args[2]);
//			testingFile=args[3];
//			outDirPath=args[4];
//			tau=Double.parseDouble(args[5]);
//
//		}else{

//        for(int it=0;it<5;it++) {
//        for(int it=0;it<5;it++) {
            if (it == 0) {
                test = "fold1";
                train = "fold2,3,4,5";
            } else if (it == 1) {
                test = "fold2";
                train = "fold1,3,4,5";
            } else if (it == 2) {
                test = "fold3";
                train = "fold1,2,4,5";
            } else if (it == 3) {
                test = "fold4";
                train = "fold1,2,3,5";
            } else if (it == 4) {
                test = "fold5";
                train = "fold1,2,3,4";
            }


            classChange_Train = changeID * 4;
//            trainingFile = where + "FakeReviewDetectionData\\Yelp\\numOfReviews\\"+categories+"_"+totalNumOfReview+"\\"+categories+"_"+totalNumOfReview+"_"+train+".txt";
//            testingFile = where + "FakeReviewDetectionData\\Yelp\\numOfReviews\\"+categories+"_"+totalNumOfReview+"\\"+categories+"_"+totalNumOfReview+"_"+test+".txt";
//            trainingLDAFile = where + "FakeReviewDetectionData\\Yelp\\numOfReviews\\"+categories+"_"+totalNumOfReview+"\\"+categories+"_"+totalNumOfReview+"_"+train+"_lda"+topicNum+".txt";
//            testingLDAFile = where + "FakeReviewDetectionData\\Yelp\\numOfReviews\\"+categories+"_"+totalNumOfReview+"\\"+categories+"_"+totalNumOfReview+"_"+test+"_lda"+topicNum+".txt";


        String LDAtopicResultFolder =where + "FakeReviewDetectionData\\Yelp\\"+categories+"\\"+"BalancedForLDA\\"+topicNum+"_word";
        String LDATopicResultFile= LDAtopicResultFolder+"\\"+categories+"_"+totalNumOfReview+"_"+train+"_lda"+topicNum+"_word.txt";
        File forCheckCategory = new File(LDAtopicResultFolder);
        if(!forCheckCategory.isDirectory()){
            forCheckCategory.mkdir();
        }

//			tau=0.0014812492527767063;
//            tau = 0.0;
            File f = new File(trainingFile);
            String line ="";
            if (!f.isFile()) {
                try {
                    BufferedReader br = new BufferedReader(new FileReader(where + "FakeReviewDetectionData\\Yelp\\" + categories + "\\BalancedForLDA\\" + categories + "_" + train + ".csv"));
                    String txtHelper="";
                    String label= "";
                    String txtContents="";
                    for (int i = 0; (line = br.readLine()) != null; i++) {
                        // use comma as separator
                        String[] tempArray = line.split(",");
                        if(tempArray[1].equals("F")){
                            label="-1";
                        }else{
                            label="1";
                        }
                        txtContents = tempArray[4].replace("\n", " ").replace("?"," ").replace("|",",").replace("\""," ");
                        txtHelper+=i+"\t"+label+"\t"+txtContents+"\n";
                    }
                    writer = new FileWriter(trainingFile);
                    writer.append(txtHelper);
                    writer.flush();
                    writer.close();
                } catch(IOException e)
                {
                    e.printStackTrace();
                }
            }
        //save id and label infomation
        BufferedReader br = new BufferedReader(new FileReader(where + "FakeReviewDetectionData\\Yelp\\" + categories + "\\BalancedForLDA\\" + categories + "_" + train + ".txt"));
        for (int i = 0; (line = br.readLine()) != null; i++) {
            String[] tempArray = line.split("\t");
            String label="";
            if(tempArray[1].equals("f")){
                TrainIDLabel.add(-1);
            }else{
                TrainIDLabel.add(1);
            }
        }


            Reader fileReader = new InputStreamReader(new FileInputStream(new File(trainingFile)), "UTF-8");
            ///////////
            trainingInstances.addThruPipe(new CsvIterator(fileReader, Pattern.compile("^(\\S*)[\\s,]*(\\S*)[\\s,]*(.*)$"),
                    3, 2, 1)); // data, label, name fields
            // Create a model with 100 topics, alpha_t = 0.01, beta_w = 0.01
            //  Note that the first parameter is passed as the sum over topics, while
            //  the second is
//            ParallelTopicModel model =null;
//            model = new ParallelTopicModel(topicNum, topicNum*1.0, 0.01);
            ParallelTopicModel model_tmp  = new ParallelTopicModel(topicNum, topicNum*1.0, 0.01);
//            ParallelTopicModel model = new ParallelTopicModel(topicNum, topicNum*1.0, 0.01);
            model_tmp.addInstances(trainingInstances);
            // Use two parallel samplers, which each look at one half the corpus and combine
            //  statistics after every iteration.
            model_tmp.setNumThreads(2);
//            model.setSymmetricAlpha(true);
            // Run the model for 50 iterations and stop (this is for testing only,
            //  for real applications, use 1000 to 2000 iterations)
            model_tmp.setNumIterations(1500);
            model_tmp.estimate();
            System.out.println("train end\t"+topicNum+"\t"+totalNumOfReview+"\titer: "+it);
            // Show the words and topics in the first instance

            // The data alphabet maps word IDs to strings
            Alphabet dataAlphabet = trainingInstances.getDataAlphabet();

            FeatureSequence tokens = (FeatureSequence) model_tmp.getData().get(0).instance.getData();
            LabelSequence topics = model_tmp.getData().get(0).topicSequence;

            Formatter out = new Formatter(new StringBuilder(), Locale.US);
            for (int position = 0; position < tokens.getLength(); position++) {
                out.format("%s-%d ", dataAlphabet.lookupObject(tokens.getIndexAtPosition(position)), topics.getIndexAtPosition(position));
            }
//		System.out.println(out);

            // Estimate the topic distribution of the first instance,
            //  given the current Gibbs state.
            double[] topicDistribution = model_tmp.getTopicProbabilities(0); // �̰� �̿��ϱ�


            // Get an array of sorted sets of word ID/count pairs
            ArrayList<TreeSet<IDSorter>> topicSortedWords = model_tmp.getSortedWords();

            // Show top 5 words in topics with proportions for the first document
            for (int topic = 0; topic < topicNum; topic++) {
                Iterator<IDSorter> iterator = topicSortedWords.get(topic).iterator();

                out = new Formatter(new StringBuilder(), Locale.US);
                out.format("%d\t%.3f\t", topic, topicDistribution[topic]);

                int rank = 0;
                while (iterator.hasNext() && rank < 50) {
                    IDSorter idCountPair = iterator.next();
                    out.format("%s (%.0f) ", dataAlphabet.lookupObject(idCountPair.getID()), idCountPair.getWeight());
                    rank++;
                }
//			System.out.println(out);
                LDAResultOut.append(out);
                LDAResultOut.append("\n");
            }


        writer = new FileWriter(LDATopicResultFile);
        writer.append(LDAResultOut);
        writer.flush();
        writer.close();
            // Create a new instance with high probability of topic 0
            StringBuilder topicZeroText = new StringBuilder();
            Iterator<IDSorter> iterator = topicSortedWords.get(0).iterator();

            int rank = 0;
            while (iterator.hasNext() && rank < 5) {
                IDSorter idCountPair = iterator.next();
                topicZeroText.append(dataAlphabet.lookupObject(idCountPair.getID()) + " ");
                rank++;
            }

            // Create a new instance named "test instance" with empty target and source fields.
//		InstanceList testing = new InstanceList(traningInstances.getPipe());
//		testing.addThruPipe(new Instance(topicZeroText.toString(), null, "test instance", null));
//
//		TopicInferencer inferencer = model.getInferencer();
//		double[] testProbabilities = inferencer.getSampledDistribution(testing.get(0), 10, 1, 5);
            /**Calculation for Weight**/
            long startTime = System.currentTimeMillis();

            /**Inference on New document**/
            ArrayList<Pipe> pipeListForTest = new ArrayList<Pipe>();
            //  (TrainingFile,classChange)
            // Pipes: lowercase, tokenize, remove stopwords, map to features
            pipeListForTest.add(new CharSequenceLowercase());
            pipeListForTest.add(new CharSequence2TokenSequence(Pattern.compile("\\p{L}[\\p{L}\\p{P}]+\\p{L}")));
//            pipeListForTest.add(new TokenSequenceRemoveStopwords(new File("stoplists/en.txt"), "UTF-8", false, false, false));
            pipeListForTest.add(new TokenSequenceRemoveStopwords(new File("D:\\workspace\\FRD\\stoplists\\en.txt"), "UTF-8", false, false, false));
            pipeListForTest.add(new TokenSequence2FeatureSequence());
            InstanceList testingInstances = new InstanceList(new SerialPipes(pipeListForTest));
//////////////
            f = new File(testingFile);
            if (!f.isFile()) {
                try {
                    br = new BufferedReader(new FileReader(where + "FakeReviewDetectionData\\Yelp\\" + categories + "\\BalancedForLDA\\" + categories + "_" + test + ".csv"));
                    String txtHelper="";
                    String label= "";
                    String txtContents="";
                    for (int i = 0; (line = br.readLine()) != null; i++) {
                        // use comma as separator
                        String[] tempArray = line.split(",");
                        if(tempArray[1].equals("F")){
                            label="-1";
                        }else{
                            label="1";
                        }
                        txtContents = tempArray[4].replace("\n", " ").replace("?"," ").replace("|",",");
                        txtHelper+=i+"\t"+label+"\t"+txtContents+"\n";
                    }
                   writer = new FileWriter(testingFile);
                    writer.append(txtHelper);
                    writer.flush();
                    writer.close();

//                    outDirPath = "C:/result.txt";
                } catch(IOException e)
                {
                    e.printStackTrace();
                }
            }

        br = new BufferedReader(new FileReader(where + "FakeReviewDetectionData\\Yelp\\" + categories + "\\BalancedForLDA\\" + categories + "_" + test + ".txt"));

        for (int i = 0; (line = br.readLine()) != null; i++) {
            // use comma as separator
            String[] tempArray = line.split("\t");
            if (tempArray[1].equals("f")) {
                TestIDLabel.add(-1);
            } else {
                TestIDLabel.add(1);
            }
        }

//		Reader fileReader = new InputStreamReader(new FileInputStream(new File(args[0])), "UTF-8");
            fileReader = new InputStreamReader(new FileInputStream(new File(testingFile)), "UTF-8");
//		fileReader = new InputStreamReader(new FileInputStream(new File("E:\\FakeReviewDetectionData\\DefaultTraining.txt")), "UTF-8");
            testingInstances.addThruPipe(new CsvIterator(fileReader, Pattern.compile("^(\\S*)[\\s,]*(\\S*)[\\s,]*(.*)$"),
                    3, 2, 1)); // data, label, name fields
            TopicInferencer inferencer = InferenceModel(testingFile, model_tmp);
            System.out.println("Inference end\t"+topicNum+"\t"+totalNumOfReview+"\titer: "+it);
//		System.out.println("get 0: "+testingInstances.get(0));
//		System.out.println("get 1: "+testingInstances.get(1));
//		System.out.println("get 2: "+testingInstances.get(2));
//            BufferedReader br = new BufferedReader(new FileReader(testingFile));
//
//            String[] array;
//            ArrayList<String> reviewContent = new ArrayList();
//            while ((line = br.readLine()) != null) {
//                array = line.split("\t");
//                reviewContent.add(array[2]);
//            }
            /**Score calculation**/
//		while(true){
//		tau=Math.random();
//		tau=0.0;
//		tau=0.0;
//            double[] Score_F = new double[testingInstances.size()];
//            double[] Score_T = new double[testingInstances.size()];
//            double[] cnt_F = new double[model.numTopics];
//            double[] cnt_T = new double[model.numTopics];

            LDATrainOut=null;
            LDATrainOut=new StringBuffer("");

            LDATestOut=null;
            LDATestOut=new StringBuffer("");
            for (int i = 0; i < model_tmp.getData().size(); i++) {

                double[] topicDistributionOfDoc = model_tmp.getTopicProbabilities(i); // �̰� �̿��ϱ�
                if(TrainIDLabel.get(i)==-1) {
//                    LDATrainOut.append(i);
//                    LDATrainOut.append("\tf");
                    LDATrainOut.append("-1");
                }else{
//                    LDATrainOut.append(i);
//                    LDATrainOut.append("\tt");
                    LDATrainOut.append("1");
                }
                 for (int j = 0; j < model_tmp.numTopics; j++) {
                     LDATrainOut.append(" ");
                    LDATrainOut.append(j+1);
                    LDATrainOut.append(":");
                    LDATrainOut.append(topicDistributionOfDoc[j]);
//                    if (topicDistributionOfDoc[j] > tau) {
//                        if(i<classChange_Train) {
//                            cnt_F[j]++;
//                        }else{
//                            cnt_T[j]++;
//                        }
//                    }
                }
                if(i!=model_tmp.getData().size()-1) {
                    LDATrainOut.append("\n");
                }
            }

            writer = new FileWriter(trainingLDAFile);
            writer.append(LDATrainOut);
            writer.flush();
            writer.close();


//            double[] w_F = new double[model.numTopics];
//            double[] w_T = new double[model.numTopics];
//            for (int j = 0; j < model.numTopics; j++) {
//                w_F[j] = Math.log10((cnt_F[j] + 1) / (cnt_T[j] + 1));
//                w_T[j] = Math.log10((cnt_T[j] + 1) / (cnt_F[j] + 1));
//            }
            for (int i = 0; i < testingInstances.size(); i++) {
                double[] testProbabilities = inferencer.getSampledDistribution(testingInstances.get(i), 10, 1, 5);

                if(TestIDLabel.get(i)==-1) {
                    LDATestOut.append("-1");
                }else{
                    LDATestOut.append("1");
                }
                for (int j = 0; j < model_tmp.numTopics; j++) {
                    LDATestOut.append(" ");
                    LDATestOut.append(j+1);
                    LDATestOut.append(":");
                    LDATestOut.append(testProbabilities[j]);
//			System.out.println("test p: "+testProbabilities[j]+"\tw_F: "+w_F[j]+"\tw_T: "+w_T[j]);
//                    Score_F[i] += testProbabilities[j] * w_F[j];
//                    Score_T[i] += testProbabilities[j] * w_T[j];
//			System.out.print(i+":: "+j+": prob: "+testProbabilities[j]+"\tcnt_F = "+cnt_F[j]+"\t");
//			System.out.print("cnt_T = "+cnt_T[j]+"\t");
//			System.out.print("w_F = "+w_F[j]+"\t");
//			System.out.print("w_T = "+w_T[j]);
//			System.out.println();
//			System.out.print("calculated Score_F= "+w_T[j]);
//                    if (Temp_F < testProbabilities[j] * w_F[j]) {
//                        Temp_F = testProbabilities[j] * w_F[j];
//                        Topic_F = j;
//                    }
//
//                    if (Temp_T < testProbabilities[j] * w_T[j]) {
//                        Temp_T = testProbabilities[j] * w_T[j];
//                        Topic_T = j;
//                    }
                }
                if(i!=testingInstances.size()-1) {
                    LDATestOut.append("\n");
                }
//		System.out.println("F S: "+Score_F[i]+"\tT S: "+Score_T[i]);
//		System.out.print(i+"::");
//		System.out.print("Score_F = "+Score_F[i]+"\t");
//		System.out.print("Score_T = "+Score_T[i]);
//		System.out.println();
//
//		System.out.println("size of testprob. :"+testProbabilities.length);
////		System.out.println("0\t" + testProbabilities[0]);
//		System.out.println("distribution of 0 document" + testProbabilities[0]+" "+testProbabilities[1]+" "+testProbabilities[2]+" "+testProbabilities[3]+" "+testProbabilities[4]);
            }
            writer = new FileWriter(testingLDAFile);
            writer.append(LDATestOut);
            writer.flush();
            writer.close();
//            f = new File(LDAtotalFile);
//            if (!f.isFile()) {
//                try {
//                    writer = new FileWriter(LDAtotalFile, true);
//
//                    writer.append(LDATestOut);
//                    writer.append("\n");
//                    writer.append(LDATrainOut);
//                    writer.flush();
//                    writer.close();
////                    outDirPath = "C:/result.txt";
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }

//		for(int p=0;p<testingInstances.size();p++){
//			if(p<classChange_Test){
//				if(Score_F[p]>Score_T[p]){
//					tn++;
//				}else{
//					fp++;
//				}
//			}else{
//				if(Score_F[p]>Score_T[p]){
//					fn++;
//				}else{
//					tp++;
//				}
//			}
//		}
            /**try {
                ////
                FileWriter fw = new FileWriter(resultFile);
                BufferedWriter bw = new BufferedWriter(fw);
                for (int p = 0; p < testingInstances.size(); p++) {
                    System.out.println(reviewContent.get(p));
                    bw.write(reviewContent.get(p) + "\n");
//			System.out.println("F S: "+Score_F[p]+"\tT S: "+Score_T[p]);

                    int topic;
                    if (Score_F[p] > Score_T[p]) {
                        topic = highTopic_F.get(p);
                        System.out.println("=> Fake !");
                        bw.write("=> Fake !\n");
                    } else {
                        topic = highTopic_T.get(p);
                        System.out.println("=> Truthful !");
                        bw.write("=> Truthful !\n");
                    }
                    iterator = topicSortedWords.get(topic).iterator();

                    out = new Formatter(new StringBuilder(), Locale.US);
//						out.format("%d\t%.3f\t", topic, topicDistribution[topic]);
//					System.out.println("??\t"+out);
                    rank = 0;
                    while (iterator.hasNext() && rank < 5) {
                        IDSorter idCountPair = iterator.next();
                        out.format("%s ", dataAlphabet.lookupObject(idCountPair.getID()));
                        rank++;
                    }
                    System.out.println("The Topic words which have most persive influence:\t");
                    System.out.println("=> " + out);
                    bw.write("The Topic words which have most persive influence:\n");
                    bw.write("=> " + out + "\n");


                }

//		accuracy=(tn+tp)/(tn+tp+fn+fp);
//		precision
//		recall
//		if(highAccuracy<accuracy){
//		highAccuracy=accuracy;
//			System.out.println("Acc: " + highAccuracy+"\t tau: "+tau);
//		}
//	}
                bw.close();
            } catch (IOException e) {
                System.err.println(e);
                System.exit(1);
            }**/
            long endTime = System.currentTimeMillis();
            System.out.println("doing! "+it+"\t"+totalNumOfReview);
            System.out.println("##  time spent : " + ( endTime - startTime )/1000.0f/60 +"min");
//        }

        System.out.println("end! "+totalNumOfReview);
        writer.close();
        }
    }
    }
    }

	public ParallelTopicModel CreateModel(String trainingFile, int numTopics) throws IOException{
//        double alphasum= 1;
//        double beta = 0.01;
        double alphasum = 1;
        double beta = 0.01;
		// Begin by importing documents from text to feature sequences
		ArrayList<Pipe> pipeListForTraining = new ArrayList<Pipe>();
		//  (TrainingFile,classChange)
		// Pipes: lowercase, tokenize, remove stopwords, map to features
		pipeListForTraining.add( new CharSequenceLowercase() );
		pipeListForTraining.add( new CharSequence2TokenSequence(Pattern.compile("\\p{L}[\\p{L}\\p{P}]+\\p{L}")) );
		pipeListForTraining.add( new TokenSequenceRemoveStopwords(new File("stoplists/en.txt"), "UTF-8", false, false, false) );
		pipeListForTraining.add( new TokenSequence2FeatureSequence() );

		InstanceList traningInstances = new InstanceList (new SerialPipes(pipeListForTraining));
		
		Reader fileReader = new InputStreamReader(new FileInputStream(new File(trainingFile)), "UTF-8");
		traningInstances.addThruPipe(new CsvIterator (fileReader, Pattern.compile("^(\\S*)[\\s,]*(\\S*)[\\s,]*(.*)$"),
											   3, 2, 1)); // data, label, name fields
		ParallelTopicModel model = new ParallelTopicModel(numTopics, numTopics*alphasum, beta);
		
		model.addInstances(traningInstances);

		// Use two parallel samplers, which each look at one half the corpus and combine
		//  statistics after every iteration.
		model.setNumThreads(2);

		// Run the model for 50 iterations and stop (this is for testing only, 
		//  for real applications, use 1000 to 2000 iterations)
		model.setNumIterations(1500);
		model.estimate();
		return model;
	}
	public static TopicInferencer InferenceModel(String testingFile, ParallelTopicModel model) throws IOException{
		
		
		TopicInferencer inferencer = model.getInferencer();

		return inferencer;
	}
}