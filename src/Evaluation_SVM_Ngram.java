import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.*;

import jnisvmlight.LabeledFeatureVector;
import jnisvmlight.SVMLightModel;
import jnisvmlight.SVMLightInterface;
import jnisvmlight.TrainingParameters;
/**
 * Created by lkylove23 on 2015-01-19.
 */
public class Evaluation_SVM_Ngram {

    //    public static int N = 2000; // number of training docs
    public static int N = 2; // number of training docs

    //    public static int M = 100; // max. number of features per doc
    public static int M = 3; // max. number of features per doc

    public static void main(String[] args) throws Exception {
        String mode = "Unigram";
//		String mode ="Bigram";
//        String mode ="UnigramAndBigram";

        // The trainer interface with the native communication to the SVM-light shared
        // libraries
        SVMLightInterface trainer = new SVMLightInterface();
        String test = "";
        String train = "";
        String categories = "";
        int totalNumOfReview = 0;

        ArrayList<Double> C_factorList= new ArrayList<Double>();  // based on example from http://en.wikipedia.org/wiki/Support_vector_machine
//        C_factorList.add(Math.pow(2,-5));
        C_factorList.add(Math.pow(2,-3));
        C_factorList.add(Math.pow(2,-1));
        C_factorList.add(Math.pow(2,1));
        C_factorList.add(Math.pow(2,3));
        C_factorList.add(Math.pow(2,5));
        C_factorList.add(Math.pow(2,7));
        C_factorList.add(Math.pow(2,9));
//        C_factorList.add(Math.pow(2,11));
//        C_factorList.add(Math.pow(2,13));
//        C_factorList.add(Math.pow(2,15));

          int c_it=0;
//        int c_it=1;
//        int c_it=2;
//        int c_it=3;
//        int c_it=4;
//        int c_it=5;
//        int c_it = 6;
        if (c_it == 0) {
            //Electronics
//                changeID = 44;
            totalNumOfReview = 440;
            categories = "electronics";
        } else if (c_it == 1) {
            //Fashion
            totalNumOfReview = 2690;
            categories = "fashion";
        } else if (c_it == 2) {
            //hotels
            totalNumOfReview = 1100;
            categories = "hotels";
        } else if (c_it == 3) {
            //hospitals
            totalNumOfReview = 200;
            categories = "hospitals";
        } else if (c_it == 4) {
            //Insurance
            totalNumOfReview = 130;
            categories = "insurance";
        } else if (c_it == 5) {
            //Musicvenues
            totalNumOfReview = 1190;
            categories = "musicvenues";
        } else if (c_it == 6) {
            //Restaurant
            totalNumOfReview = 37990;
//			changeID=115;
            categories = "restaurants";
        }
        String resultFile = "D:\\FakeReviewDetectionData\\result\\Yelp\\"+categories+"\\" +categories+"_"+mode+".txt";
        StringBuffer DocResultOut = new StringBuffer();
        double[] accuracy_folds = new double[5];
        double[] precision_F_folds = new double[5];
        double[] precision_T_folds = new double[5];
        double[] recall_F_folds = new double[5];
        double[] recall_T_folds = new double[5];
        double[] F1_F_folds = new double[5];
        double[] F1_T_folds = new double[5];
        double[] best_c= new double[5];
        /**outer cross validation**/

        for (int it = 0; it < 5; it++) {
            if (it == 0) {
                test = "fold1";
                train = "fold2,3,4,5";
            } else if (it == 1) {
                test = "fold2";
                train = "fold1,3,4,5";
            } else if (it == 2) {
                test = "fold3";
                train = "fold1,2,4,5";
            } else if (it == 3) {
                test = "fold4";
                train = "fold1,2,3,5";
            } else if (it == 4) {
                test = "fold5";
                train = "fold1,2,3,4";
            }
            String trainingFile = "D:\\FakeReviewDetectionData\\Yelp\\"+categories+"\\BalancedSVMLight_Porter\\" +categories+"_"+mode+"_"+train+".dat";
            String testingFile = "D:\\FakeReviewDetectionData\\Yelp\\"+categories+"\\BalancedSVMLight_Porter\\" +categories+"_"+mode+"_"+test+".dat";


            // The training data
//        LabeledFeatureVector[] traindata = new LabeledFeatureVector[N];
//            ArrayList<LabeledFeatureVector> traindata = new ArrayList<LabeledFeatureVector>(); //traindata has train documents
//            ArrayList<LabeledFeatureVector> testdata = new ArrayList<LabeledFeatureVector>(); //testdata has test documents

            // Sort all feature vectors in ascedending order of feature dimensions
            // before training the model
            SVMLightInterface.SORT_INPUT_VECTORS = true;
            // Check # of training docs
            BufferedReader br = new BufferedReader(new FileReader(trainingFile));
            String line="";
            int numOfTrainDoc=0;

            while ((line = br.readLine()) != null) {
                numOfTrainDoc++;
            }
            /** Nested (inner) cross validation (4-fold) **/
            int inner_test_start=0;
            int inner_test_end=0;
            Double best_acc = 0.0;
//            best_c = 0.0;
            Double temp_acc=0.0;
            for(int ii=0; ii<C_factorList.size(); ii++) {
                TrainingParameters trainparam = new TrainingParameters();
                trainparam.getLearningParameters().svm_c_factor = C_factorList.get(ii);

                Double[] inner_acc= new Double[4];
//                for (int inner_it = 0; inner_it < 4; inner_it++) {
                for (int inner_it = 0; inner_it < 4; inner_it++) {
                    if (inner_it == 0) {
                        inner_test_start = 0;
                        inner_test_end = numOfTrainDoc / 4 - 1; // include test_start & end th doc
                    } else if (inner_it == 1) {
                        inner_test_start = numOfTrainDoc / 4;
                        inner_test_end = numOfTrainDoc / 4 * 2 - 1; // include test_start & end th doc
                    } else if (inner_it == 2) {
                        inner_test_start = numOfTrainDoc / 4 * 2;
                        inner_test_end = numOfTrainDoc / 4 * 3 - 1; // include test_start & end th doc
                    } else if (inner_it == 3) {
                        inner_test_start = numOfTrainDoc / 4 * 3;
                        inner_test_end = numOfTrainDoc / 4 * 4 - 1; // include test_start & end th doc
                    }
                    Double acc = 0.0;

                    //data setting
                    LabeledFeatureVector[] inner_traindata = new LabeledFeatureVector[numOfTrainDoc / 4 * 3];
                    LabeledFeatureVector[] inner_testdata = new LabeledFeatureVector[numOfTrainDoc / 4];
                    br = new BufferedReader(new FileReader(trainingFile));
                    int id_inner = 0;
                    int id_inner_train = 0;
                    int id_inner_test = 0;
                    while ((line = br.readLine()) != null) {

                        //divide train and test in inner part
                        if (id_inner < inner_test_start || id_inner > inner_test_end) {
                            String[] temp = line.split(" ");
                            int numOfFinD = temp.length - 1; // num of features in the document  -1 because first one is a label
                            int[] dims = new int[numOfFinD];
                            double[] values = new double[numOfFinD];
                            // Fill the vectors
                            for (int j = 0; j < numOfFinD; j++) {
                                String[] temp2 = temp[j + 1].split(":"); //temp[ ] should be "dim:value"
                                dims[j] = Integer.parseInt(temp2[0]);
                                values[j] = Double.parseDouble(temp2[1]);
                            }
                            // Store dimension/value pairs in new LabeledFeatureVector object
                            inner_traindata[id_inner_train] = new LabeledFeatureVector(Double.parseDouble(temp[0]), dims, values);
                            // Use cosine similarities (LinearKernel with L2-normalized input vectors)
                            inner_traindata[id_inner_train].normalizeL2();
                            id_inner_train++;
                        } else {
//                            if(inner_it==3){System.out.println("test: "+id_inner_test);}
                            String[] temp = line.split(" ");
                            int numOfFinD = temp.length - 1; // num of features in the document  -1 because first one is a label
                            int[] dims = new int[numOfFinD];
                            double[] values = new double[numOfFinD];
                            // Fill the vectors
                            for (int j = 0; j < numOfFinD; j++) {
                                String[] temp2 = temp[j + 1].split(":"); //temp[ ] should be "dim:value"
                                dims[j] = Integer.parseInt(temp2[0]);
                                values[j] = Double.parseDouble(temp2[1]);
                            }
                            // Store dimension/value pairs in new LabeledFeatureVector object
                            inner_testdata[id_inner_test] = new LabeledFeatureVector(Double.parseDouble(temp[0]), dims, values);
                            // Use cosine similarities (LinearKernel with L2-normalized input vectors)
                            inner_testdata[id_inner_test].normalizeL2();
                            id_inner_test++;
                        }
                        id_inner++;
                    }
                    //Training of inner CV

                    // Switch on some debugging output
                    trainparam.getLearningParameters().verbosity = 1;

                    /**iteration part for grid search C factor **/
                    System.out.println("\n"+inner_it+" TRAINING SVM-light MODEL .. wiht C: " + trainparam.getLearningParameters().svm_c_factor);
//                    SVMLightModel model = trainer.trainModel(inner_traindata, trainparam);
                    SVMLightModel[] model = new SVMLightModel[4];
                    model[inner_it] = trainer.trainModel(inner_traindata, trainparam);

//                    for (int i = 0; i < numOfTrainDoc / 4; i++) {
                    for (int i = 0; i < numOfTrainDoc / 4; i++) {
                        // Classify a test vector using the Java object
                        // (in a real application, this should not be one of the training vectors)
//                        if(inner_it==3&&i==0){
//                            System.out.println(i);
//                           continue;
//                        }
                        System.out.println(i+"\t"+inner_testdata[i].size());
                        double d = model[inner_it].classify(inner_testdata[i]);
//                        System.out.println("d:\t"+d);
                        if ((inner_testdata[i].getLabel()>0&&d>0)||(inner_testdata[i].getLabel()<0&&d<0)) {
                            acc++;
                        }
//                        if (i % 10 == 0) {
//                            System.out.print(i + ".");
//                        }
                    }
                    inner_acc[inner_it] = acc / (numOfTrainDoc / 4);
//                }
                    System.out.println("inner_acc "+inner_it+"\t"+inner_acc[inner_it]);
                }
//                temp_acc=inner_acc[0]+inner_acc[1]+inner_acc[2]+inner_acc[3];
                temp_acc=inner_acc[0]+inner_acc[1];
                if (best_acc < temp_acc) {
                    best_acc = temp_acc;
                    best_c[it]= trainparam.getLearningParameters().svm_c_factor;
                }
            }

             /**Evaluation for test data**/
            //
            LabeledFeatureVector[] traindata = new LabeledFeatureVector[numOfTrainDoc];
            br = new BufferedReader(new FileReader(trainingFile));
            while ((line = br.readLine()) != null) {
                int id_train=0;
                String[] temp =line.split(" ");
                int numOfFinD=temp.length-1 ; // num of features in the document  -1 because first one is a label
                int[] dims = new int[numOfFinD];
                double[] values = new double[numOfFinD];
                // Fill the vectors
                for (int j=0;j<numOfFinD;j++) {
                    String[] temp2=temp[j+1].split(":"); //temp[ ] should be "dim:value"
                    dims[j]= Integer.parseInt(temp2[0]);
                    values[j]=Double.parseDouble(temp2[1]);
                }
                // Store dimension/value pairs in new LabeledFeatureVector object
                traindata[id_train] = new LabeledFeatureVector(Double.parseDouble(temp[0]), dims, values);
                // Use cosine similarities (LinearKernel with L2-normalized input vectors)
                traindata[id_train].normalizeL2();
                id_train++;
            }
            LabeledFeatureVector[] testdata = new LabeledFeatureVector[numOfTrainDoc];
            br = new BufferedReader(new FileReader(testingFile));
            int id_test=0;
            while ((line = br.readLine()) != null) {

                String[] temp =line.split(" ");
                int numOfFinD=temp.length-1 ; // num of features in the document  -1 because first one is a label
                int[] dims = new int[numOfFinD];
                double[] values = new double[numOfFinD];
                // Fill the vectors
                for (int j=0;j<numOfFinD;j++) {
                    String[] temp2=temp[j+1].split(":"); //temp[ ] should be "dim:value"
                    dims[j]= Integer.parseInt(temp2[0]);
                    values[j]=Double.parseDouble(temp2[1]);
                }
                // Store dimension/value pairs in new LabeledFeatureVector object
                testdata[id_test] = new LabeledFeatureVector(Double.parseDouble(temp[0]), dims, values);
                // Use cosine similarities (LinearKernel with L2-normalized input vectors)
                testdata[id_test].normalizeL2();
                id_test++;
            }
//            System.out.println(" DONE.");
            // Initialize a new TrainingParamteres object with the default SVM-light
            // values
            TrainingParameters trainparam = new TrainingParameters();
            // Switch on some debugging output
            trainparam.getLearningParameters().verbosity = 1;
            trainparam.getLearningParameters().svm_c_factor = best_c[it];

            System.out.println("\nTRAINING SVM-light MODEL ..");
            //what is tp?
            SVMLightModel model_test = trainer.trainModel(traindata, trainparam);
            System.out.println(" DONE.");

            // Use this to store a model to a file or read a model from a URL.
            //model.writeModelToFile("jni_model.dat");
            //model = SVMLightModel.readSVMLightModelFromURL(new java.io.File("jni_model.dat").toURL());

            // Use the classifier on the randomly created feature vectors
            System.out.println("\nVALIDATING SVM-light MODEL ");
            int Accuracy= 0;
            double tp = 0.0;
            double fp = 0.0;
            double tn = 0.0;
            double fn = 0.0;
            for (int i = 0; i < numOfTrainDoc/4; i++) {
                // Classify a test vector using the Java object
                // (in a real application, this should not be one of the training vectors)
                System.out.println(i+"\t"+testdata[i]);
                double d = model_test.classify(testdata[i]);
                if ((testdata[i].getLabel()==1)){
                    if(d==1){
                        tp++;
                    }else{
                        fn++;
                    }
                }else {
                    if(d==1){
                      fp++;
                    }else{
                        tn++;
                    }
                }
            }
            accuracy_folds[it] = (tn + tp) / (tn + tp + fn + fp);
            precision_F_folds[it] = tn / (fn + tn);
            precision_T_folds[it] = tp / (fp + tp);
            recall_F_folds[it] = tn / (tn + fp);
            recall_T_folds[it] = tp / (tp + fn);
            F1_F_folds[it] = 2 * precision_F_folds[it] * recall_F_folds[it] / (precision_F_folds[it] + recall_F_folds[it]);
            F1_T_folds[it] = 2 * precision_T_folds[it] * recall_T_folds[it] / (precision_T_folds[it] + recall_T_folds[it]);
            System.out.println("it: "+it+"\t"+accuracy_folds[it]);

        }
        // category selected c0 ..... c4
        // acc
        // ...
            DocResultOut.append(categories);
            DocResultOut.append("\t");
            DocResultOut.append(best_c[0]);
            DocResultOut.append("\t");
        DocResultOut.append(best_c[1]);
        DocResultOut.append("\t");
        DocResultOut.append(best_c[2]);
            DocResultOut.append("\t");
        DocResultOut.append(best_c[3]);
        DocResultOut.append("\t");
        DocResultOut.append(best_c[4]);
        DocResultOut.append("\n");
        DocResultOut.append((accuracy_folds[0] + accuracy_folds[1] + accuracy_folds[2] + accuracy_folds[3] + accuracy_folds[4]) / 5);
        DocResultOut.append("\t");
        DocResultOut.append((precision_F_folds[0] + precision_F_folds[1] + precision_F_folds[2] + precision_F_folds[3] + precision_F_folds[4]) / 5);
        DocResultOut.append("\t");
        DocResultOut.append((recall_F_folds[0] + recall_F_folds[1] + recall_F_folds[2] + recall_F_folds[3] + recall_F_folds[4]) / 5);
        DocResultOut.append("\t");
        DocResultOut.append((F1_F_folds[0] + F1_F_folds[1] + F1_F_folds[2] + F1_F_folds[3] + F1_F_folds[4]) / 5);
        DocResultOut.append("\t");
        DocResultOut.append((precision_T_folds[0] + precision_T_folds[1] + precision_T_folds[2] + precision_T_folds[3] + precision_T_folds[4]) / 5);
        DocResultOut.append("\t");
        DocResultOut.append((recall_T_folds[0] + recall_T_folds[1] + recall_T_folds[2] + recall_T_folds[3] + recall_T_folds[4]) / 5);
        DocResultOut.append("\t");
        DocResultOut.append((F1_T_folds[0] + F1_T_folds[1] + F1_T_folds[2] + F1_T_folds[3] + F1_T_folds[4]) / 5);
        FileWriter writer = new FileWriter(resultFile);
        writer.append(DocResultOut);
        writer.flush();
        writer.close();
        System.out.println(categories);
        System.out.println("c parameter:\t"+best_c[0]+"\t"+best_c[1]+"\t"+best_c[2]+"\t"+best_c[3]+"\t"+best_c[4]+"\t");
        System.out.println("acc:\t"+(accuracy_folds[0] + accuracy_folds[1] + accuracy_folds[2] + accuracy_folds[3] + accuracy_folds[4])/ 5);
    }
}
