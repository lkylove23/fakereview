import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

public class Evaluation_scoring_modified_trainWithTrain {
	public static void main(String[] args) {
		File path = new File("");
		/**Choose**/
		//KY 
//		String where="D:\\";
		//or Dear IRbig2?
		String where="D:\\KY\\";
		double wt=1.0;		double wn=1.0; 		double wf=1.0;
		double threshold =0.35;
		ArrayList<Double> param_tw= new ArrayList<Double>();
		double highestTotal=0.0;
		double tmp_highestTotal=0.0;
		double[] highest_category;
		highest_category = new double[7];
		double[] tmp_highest_category;
		tmp_highest_category = new double[7];


	    double[][] highest_category_fold;
	    highest_category_fold=new double[7][5];
	    double temphighest=0.0;
	    String str="";
	    double tau =0.0;
	    while(true){
//		int topicNum=8;
//		int topicNum=50;
		int topicNum=100;		
//		int topicNum=150;
//		int topicNum=200;
		//fold1 占쏙옙占쏙옙 palmer test 占쏙옙
		String test="";
		String train="";
		
		tau=Math.random();
		
		double ave;
		double aveOfAve=0.0;
		int changeID=0; // the # of fake reviews
		String categories="";
		
		int c_it= 6;
//		for(int c_it=0;c_it<7;c_it++){
		if(c_it==0){
	//Electronics
			changeID=44;
			categories ="electronics";
		}else if(c_it==1){
			//Fashion
			changeID=269;
			categories ="fashion";
		}else if(c_it==2){
			//hotels
			changeID=110;
			categories ="hotels";
		}else if(c_it==3){
			//hospitals
			changeID=20;
			categories ="hospitals";
		}else if(c_it==4){
			//Insurance
			changeID=13;
			categories ="insurance";
		}else if(c_it==5){
			//Musicvenues
			changeID=119;
			categories ="musicvenues";
		}else if(c_it==6){
			//Restaurant
			
			changeID=3799;
//			changeID=115;
			categories ="restaurants";
		}
		
//		System.out.println(categories);

		ave=0.0;
		for(int it=0;it<5;it++){
			if(it==0){
				test ="fold1";
				train="fold2,3,4,5";
			}else if(it==1){
				 test ="fold2";
				 train="fold1,3,4,5";
			}else if(it==2){
				 test ="fold3";		
				 train="fold1,2,4,5";
			}else if(it==3){
				 test ="fold4";	
				 train="fold1,2,3,5";
			}else if(it==4){
				 test ="fold5";		
				 train="fold1,2,3,4";
			}

		//off
	    String yelp="";
//		on
//	    String yelp="yelp\\";
	
		/** Fold selection
		 * e.g. 23450101 -> 2,3,4,5 Fake normalised e.g. 23450000 -> 2,3,4,5
		 * truthful not normalised
		 																			**/
	    // 1 => 
	    
	    
	    
	    String tdf_f_n="";
	    String tdf_t_n="";
		String foridf_f="";
		String foridf_t="";
		if(test.equals("fold1")){
			//fodl1
//			System.out.println("fold1 correct!");
		    tdf_f_n="23450101";
		    tdf_t_n="23450001";
		    foridf_f="2345_f";
		    foridf_t="2345_t";
		}else if(test.equals("fold2")){
//			System.out.println("fold2 correct!");
			 //fold2
		    tdf_f_n="13450101";
		    tdf_t_n="13450001";
		    foridf_f="1345_f";
		    foridf_t="1345_t";
		}else if(test.equals("fold3")){
//			System.out.println("fold3 correct!");
			//fold3
		    tdf_f_n="12450101";
		    tdf_t_n="12450001";
		    foridf_f="1245_f";
		    foridf_t="1245_t";
		}else if(test.equals("fold4")){

			//fold4
		    tdf_f_n="12350101";
		    tdf_t_n="12350001";
		    foridf_f="1235_f";
		    foridf_t="1235_t";
		}else if(test.equals("fold5")){
		    //fold5
		    tdf_f_n="12340101";
		    tdf_t_n="12340001";
		    foridf_f="1234_f";
		    foridf_t="1234_t";
		}else {
			//for test
			  tdf_f_n="23450101";
			    tdf_t_n="23450001";
		}



		int cntResult=0;
		
	    long time = System.currentTimeMillis(); 
		SimpleDateFormat dayTime = new SimpleDateFormat("dd-hh-mm");
		str = dayTime.format(new Date(time));
	    
		//electronics_fold1-document-topic-distributuions
//	    String testFile = where+"FakeReviewDetectionData\\Yelp\\"+categories+"\\input\\topic"+topicNum+"\\itr1500\\"+categories+"_"+test+"-document-topic-distributuions.csv";
	    String testFile = where+"FakeReviewDetectionLDA\\Yelp\\"+categories+"_"+train+"_"+topicNum+"\\"+categories+"_"+test+"-document-topic-distributuions.csv";
//	    String TDF = where+"\\input\\topic"+topicNum+"\\itr1500\\TDF.csv";
	    String TDF = where+"FakeReviewDetectionData\\Yelp\\"+categories+"\\input\\topic"+topicNum+"\\itr1500\\TDF.csv";
	    String fileForIdf= where+"FakeReviewDetectionLDA\\Yelp\\"+categories+"_"+train+"_"+topicNum+"\\document-topic-distributions.csv";
	  

	    
	    String forResult =where+"FakeReviewDetectionData\\result\\topic"+topicNum+"\\itr1500\\yelp\\"+categories;
	    
	    File forCheckCategory = new File(forResult);
		if(!forCheckCategory.isDirectory()){
			forCheckCategory.mkdir();
		}
		
		String forRecord =forResult+"\\record";
		
		forCheckCategory = new File(forRecord);
		if(!forCheckCategory.isDirectory()){
			forCheckCategory.mkdir();
		}
		
	    String resultFile = forResult+"\\"+test+"-"+str+".csv";
	    String recordFile = forRecord+"\\"+test+"-"+str+".csv";

	    

	    /* resultAL
	     * 
	     * 0: Accuracy
	     * 1: T-P	
	     * 2: T-R	
	     * 3: T-F	
	     * 4: F-P
	     * 5: F-R
	     * 6: F-F
	     * etc
	     */
	    
	    ArrayList<Double> resultAL= new ArrayList<Double>();
	    
	    /* Array List of DocumentTopicD
	     * 0: DocID
	     * 1: Dist of Topic 0
	     * 2: Dist of Topic 1
	     * 3: Dist of Topic 2
	     * 4: Dist of Topic 3
	     * 5: Dist of Topic 4
	     * 6: Dist of Topic 5
	     * 7: Dist of Topic 6
	     * 8: Dist of Topic 7
	     * 9: result of 1-1 (normalise and major topics 占싸뱄옙占싱삼옙 占쏙옙占쏙옙)
	     * 10: result of 1-2 (not normalise)
	     * 11: result of 2-1 all topics
	     * 12: result of 2-2 
	     * 13: several topic combination to find best for each fold 
	     */
	    
	    HashMap<String, ArrayList<Double>> documentTopicD = new HashMap<String, ArrayList<Double>>();
	    
	    /* Array List of TopicDF
	     * 0: foldID + F or T  + Normalise or not
	     * e.g. 23450101 -> 2,3,4,5 Fake normalised
	     * e.g. 23450000 -> 2,3,4,5 truthful not normalised
	     * 1: Dist of Topic 0
	     * 2: Dist of Topic 1
	     * 3: Dist of Topic 2
	     * 4: Dist of Topic 3
	     * 5: Dist of Topic 4
	     * 6: Dist of Topic 5
	     * 7: Dist of Topic 6
	     * 8: Dist of Topic 7
	     * 
	     */
	    HashMap<String, ArrayList<Double>> topicDF = new HashMap<String, ArrayList<Double>>();
	    
		// read csv for test data
		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ",";
		try {
			br = new BufferedReader(new FileReader(testFile));

				for(int i=0;(line = br.readLine()) != null;i++) {
					
				// use comma as separator
				String[] tempArray = line.split(cvsSplitBy);
				ArrayList<Double> tempAL= new ArrayList<Double>();
				for(int j = 0; j < tempArray.length; j++) {
						tempAL.add(Double.parseDouble(tempArray[j]));
		        }
				documentTopicD.put(tempArray[0], tempAL);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		/*Topic Distribution for fold 2,3,4,5*/
//		try {
//			br = new BufferedReader(new FileReader(TDF));
//				for(int i=0;(line = br.readLine()) != null;i++) {
//				// use comma as separator
//				String[] tempArray2 = line.split(cvsSplitBy);
//				ArrayList<Double> tempAL= new ArrayList<Double>();
//				for(int j = 0; j < tempArray2.length; j++) {
//						tempAL.add(Double.parseDouble(tempArray2[j]));
//		        }
//				topicDF.put(tempArray2[0], tempAL);
//			}
//		} catch (FileNotFoundException e) {
//			e.printStackTrace();
//		} catch (IOException e) {
//			e.printStackTrace();
//		} finally {
//			if (br != null) {
//				try {
//					br.close();
//				} catch (IOException e) {
//					e.printStackTrace();
//				}
//			}
//		}
		
		/*for idf*/
		// topic num, #of document contain the topic 
		 HashMap<Integer, Double> idfHM_f = new HashMap<Integer, Double >();
		 HashMap<Integer, Double> idfHM_t = new HashMap<Integer, Double >();
		idfHM_f.clear();
		idfHM_t.clear();
		int [] tmp_f;
		int [] tmp_t;
		tmp_f = new int[100];
		tmp_t = new int[100];
		int length=0;
		try {
			br = new BufferedReader(new FileReader(fileForIdf));
				for(int i=0;(line = br.readLine()) != null;i++) {
				// use comma as separator
				String[] tempArray2 = line.split(cvsSplitBy);
				length = tempArray2.length;
				
				for(int j = 0; j < length-1; j++) {
					if(Double.parseDouble(tempArray2[j+1])>tau){
						if(i<changeID*4){
							
						tmp_f[j]++;
//						System.out.println("tmp_f++ "+j+" :"+tmp_f[j]);
						}else{
							tmp_t[j]++;
//							System.out.println("tmp_f++ "+j+" : "+tmp_t[j]);
						}
					}

		        }
			}
				for(int j = 0; j < length-1; j++) {
					idfHM_f.put(j, (double)tmp_f[j]);
					idfHM_t.put(j, (double)tmp_t[j]);
					}
					
		        	
				
				
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		
/////////////////////////////////////////////////////////
		

/*Parametre*/
		
//		long start = System.currentTimeMillis();
		long end;
		
		//Topic Weight parametre
	
		
		
		long start = System.currentTimeMillis();

		
		
			
		param_tw.clear();
/*Parametre setting*/

//		for(int u=0;u<topicNum;u++){
//			if(topicDF.get(tdf_f_n).get(u+1)<0.35||topicDF.get(tdf_f_n).get(u+1)>0.65){
//				param_tw.add(1.0*w);
////				System.out.println("fake or truthful");
//			}else{
//				param_tw.add(0.5*w);
////				System.out.println("neutral");
//			}
//		}
		for(int u=0;u<topicNum;u++){
			double ttt= Math.random()*2;
			int dd = (int)ttt; 
			param_tw.add((double)dd);
//			System.out.print((double)dd);
		}

		
		
	    /*<0(a),1(b),2(c),3(d) type and  1 for correct 0 for incorrect>
	     * 占싹댐옙 truthful review-> true 占싸거뤄옙 占쏙옙 (占쌕꿔서 占쏙옙占쏙옙풔占�
	     * */
//	    HashMap<Integer, Integer> resultType = new HashMap<Integer, Integer>();
	    double ra=0;
	    double rb=0;
	    double rc=0;
	    double rd=0;

		

/*Calculation*/
		    resultAL.clear();
	    	
			  Iterator<String> itr1 = documentTopicD.keySet().iterator();
			    while (itr1.hasNext()) {
			    	
			    	
			    	
			        String key = (String) itr1.next();
			        double scoreFake=0;
			        double scoreTruthful=0;
//			        
//			        for(int i=0;i<topicNum;i++){
//			        
//			        	scoreFake += (documentTopicD.get(key).get(i+1))*topicDF.get(tdf_f_n).get(i+1)	*param_tw.get(i);
//			        	scoreTruthful += (documentTopicD.get(key).get(i+1))*topicDF.get(tdf_t_n).get(i+1)*param_tw.get(i);
//			        }

			        for(int i=0;i<topicNum;i++){
//			        	if(topicDF.get(tdf_f_n).get(i+1)>0.3){
//			        	scoreFake += (documentTopicD.get(key).get(i+1))*(topicDF.get(tdf_f_n).get(i+1)*topicDF.get(tdf_f_n).get(i+1))	*param_tw.get(i);
//			        	}
//			        	if(topicDF.get(tdf_t_n).get(i+1)>0.3){
//			        	scoreTruthful += (documentTopicD.get(key).get(i+1))*(topicDF.get(tdf_t_n).get(i+1)*topicDF.get(tdf_t_n).get(i+1))*param_tw.get(i);
//			        	}
	
			        	scoreFake += (documentTopicD.get(key).get(i+1))*(Math.log10((idfHM_f.get(i)+1)/(idfHM_t.get(i)+0.01))/Math.log10(2.0));
			        	scoreTruthful += (documentTopicD.get(key).get(i+1))*(Math.log10((idfHM_t.get(i)+1)/(idfHM_f.get(i)+0.01))/Math.log10(2.0));
			        	
			        }
			        			        
//			        System.out.println("Fake Score: "+scoreFake);
//			        System.out.println("Truthful Score: "+scoreTruthful);
//			        totalformean+=sum1_1_1;
			        
//			        System.out.println("after "+cntformean+", mean value: "+totalformean/(cntformean+1));
//			        cntformean++;
			        
			        if(yelp.equals("")){
				if (Integer.parseInt(key) < changeID) {
					if (scoreFake > scoreTruthful) {
						// If there is little difference we can use another
						// approach $$
						// 1 correct, 0 incorrect
						if (scoreFake == 0) {
							// filter 0 but correct
						} else {
								// true negative
							rd++;
						}
					} else {
						//false positive
						
						rc++;
					}

				} else {
					if (scoreFake > scoreTruthful) {
						// false negative
						rb++;
					} else {
						if (scoreTruthful == 0) {
						} else {
				ra++;
						}
					}
			        	
			        }
			        }else{
			        	//truthful 占쏙옙 占쏙옙占쏙옙 占쏙옙占쏙옙占쏙옙 占쏙옙占쏙옙占싶쇽옙 (ott占쏙옙 占쌓놂옙 占쌕뀐옙占쏙옙占쏙옙.. 占쏙옙占쏙옙)
			        	if (Integer.parseInt(key) >= changeID) {
							if (scoreFake > scoreTruthful) {
								// If there is little difference we can use another
								// approach $$
								// 1 correct, 0 incorrect
								if (scoreFake == 0) {
									// filter 0 but correct
								} else {
										// true negative
									rd++;
								}
							} else {
								//false positive
								
								rc++;
							}

						} else {
							if (scoreFake > scoreTruthful) {
								// false negative
								rb++;
							} else {
								if (scoreTruthful == 0) {
								} else {
						ra++;
								}
							}
					        	
					        }
			        	
			        }
			    }
			   
			    /*evaluation*/
			   
			    /* resultAL
			     * 
			     * 0: Accuracy
			     * 1: T-P	
			     * 2: T-R	
			     * 3: T-F	
			     * 4: F-P
			     * 5: F-R
			     * 6: F-F
			     * etc
			     */
			    
			    resultAL.add(0, ((ra+rd)/(ra+rb+rc+rd)));
			    // for 
			    resultAL.add(1, ((ra)/(ra+rc)));
			    resultAL.add(2, ((ra)/(ra+rb)));
			    //F1 measure
			    resultAL.add(3, (2*(resultAL.get(1)*resultAL.get(2))/(resultAL.get(1)+resultAL.get(2))));
			    
			    // for fake
			    resultAL.add(4, ((rd)/(rd+rb)));
			    resultAL.add(5, ((rd)/(rd+rc)));
			    //F1 measure
			    resultAL.add(6, (2*(resultAL.get(4)*resultAL.get(5))/(resultAL.get(4)+resultAL.get(5))));
			    
//			    results.get(test).add(0,((double)sumR1_1/160));
//			    results.get(test).add(0,(double)(sumR/160));
		
			   
			    
			    
			    /*make csv for document F,T score*/
			    
			    
			    /*make csv for results*/
			    
			    double record=0;
			    // record highest one so far
			   
			    try
				{
			    	if(highest_category_fold[c_it][it]<resultAL.get(0)){
			    		highest_category_fold[c_it][it]=resultAL.get(0);
			    		System.out.println("BEST tau for "+train+": "+tau);
			    	
			    	end = System.currentTimeMillis();
			    	record=( end - start )/1000.0;
			    	
			    	
//			    	System.out.println(test+"\t"+resultAL.get(0)+"\t"+resultAL.get(1)+"\t"+resultAL.get(2)+"\t"+resultAL.get(3)+"\t"+resultAL.get(4)+"\t"+resultAL.get(5)+"\t"+resultAL.get(6));
					 start = System.currentTimeMillis();
			    	
			    	
			    	
			    	cntResult++;
			    	
				    FileWriter writer = new FileWriter(resultFile,true);
				    
				    //Accuracy	T-P	T-R	T-F	F-P	F-R	F-F  
				    // record占쏙옙 record 占쏙옙占싹울옙占쏙옙
				    
				    for(int r=0;r<7;r++){
				    	writer.append(Double.toString(resultAL.get(r)));
				    	writer.append(',');
				    }
				   				    
				    for(int k=0;k<topicNum;k++){
				    
//				    writer.append(Double.toString(param_tw.get(k)));
				    
				    if(!(k==(topicNum-1))){
				    	writer.append(',');
				    }
				    
				    }
				    writer.append('\n');


				    

				    writer.flush();
				    writer.close();
				    
				    /*record for reresult*/
				    FileWriter writerR = new FileWriter(recordFile,true);
				    writerR.append(Double.toString(resultAL.get(0)));
				    writerR.append(',');
				    writerR.append(Double.toString(record));
				    writerR.append('\n');

				    writerR.flush();
				    writerR.close();
				    
//				    System.out.println(cntResult);
			    	}
				}
				catch(IOException e)
				{
				     e.printStackTrace();
				}
		
//		System.out.println(test+": "+highest_category_fold[6][0]);
		}
//		System.out.println("tau:\t"+tau);
		highest_category[c_it]=(highest_category_fold[c_it][0]+highest_category_fold[c_it][1]+highest_category_fold[c_it][2]+highest_category_fold[c_it][3]+highest_category_fold[c_it][4])/5;
		if(tmp_highest_category[c_it]<highest_category[c_it]){
			tmp_highest_category[c_it]=highest_category[c_it];
		System.out.println(categories);
		System.out.println(test+":\t "+highest_category_fold[6][0]);
		System.out.println(test+":\t "+highest_category_fold[6][1]);
		System.out.println(test+":\t "+highest_category_fold[6][2]);
		System.out.println(test+":\t "+highest_category_fold[6][3]);
		System.out.println(test+":\t "+highest_category_fold[6][4]);
		System.out.println("AVG:\t "+tmp_highest_category[c_it]);
//		System.out.println("\t"+tmp_highest_category[c_it]);
		for(int r=0;r<100;r++){
//    		System.out.print(param_tw.get(r)+"\t");
    	}
    	System.out.println();
		}
		
		aveOfAve+=ave/5;
//		System.out.println();

//		}
//		System.out.println();
		highestTotal=(tmp_highest_category[0]+tmp_highest_category[1]+tmp_highest_category[2]+tmp_highest_category[3]+tmp_highest_category[4]+tmp_highest_category[5]+tmp_highest_category[6])/7;
		if(tmp_highestTotal<highestTotal){
			tmp_highestTotal=highestTotal;
			
			System.out.println("[Total updated] "+str);
			System.out.println(highestTotal);
			
		}
		
	}
	}

				
			
		
	}
			    
	

	
	