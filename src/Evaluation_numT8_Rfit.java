import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

public class Evaluation_numT8_Rfit {
	public static void main(String[] args) {
		File path = new File("");
//	    System.out.println(path.getAbsolutePath());

//		String test ="fold1";
//		String test ="fold2";
//		String test ="fold3";
//		String test ="fold4";
		String test ="fold5";
		
//	    String csvFile = path.getAbsolutePath()+"data\\data.csv";
//	    String testFile = path.getAbsolutePath()+"\\data\\testFile.csv";
	    String testFile = "E:\\FakeReviewDetectionData\\input\\topic8\\itr1500\\DT_"+test+".csv";
	    String TDF = "E:\\FakeReviewDetectionData\\input\\topic8\\itr1500\\TDF.csv";
//	    int type;
	    int iterK=6;
	    int kval=3;
	    int tag=0;
	    double highestScore=0;
		int cntResult=0;
		    
	    long time = System.currentTimeMillis(); 
		SimpleDateFormat dayTime = new SimpleDateFormat("dd-hh-mm");
		String str = dayTime.format(new Date(time));
	    
	    String resultFile = "E:\\FakeReviewDetectionData\\result\\topic8\\itr1500\\results_"+test+"_it"+iterK+"-"+str+"RF.csv";
	    String recordFile = "E:\\FakeReviewDetectionData\\result\\topic8\\itr1500\\record_"+test+"_it"+iterK+"-"+str+"RF.csv";
	    //fold1
//	    String tdf_f_n="23450101";
//	    String tdf_t_n="23450001";
	    //fold2
//	    String tdf_f_n="13450101";
//	    String tdf_t_n="13450001";
	    //fold3
//	    String tdf_f_n="12450101";
//	    String tdf_t_n="12450001";
	    //fold4
//	    String tdf_f_n="12350101";
//	    String tdf_t_n="12350001";	    
	    //fold5
	    String tdf_f_n="12340101";
	    String tdf_t_n="12340001";	    
	    
	    ArrayList<Double> resultAL= new ArrayList<Double>();
	    double[] it = new double[10];
	   
	    // 1 for correct 0 for incorrect
	    HashMap<String, Integer> documentResult1_1 = new HashMap<String, Integer>();
//	    HashMap<String, Integer> documentResult2_1 = new HashMap<String, Integer>();
	    
	    /* Array List of DocumentTopicD
	     * 0: DocID
	     * 1: Dist of Topic 0
	     * 2: Dist of Topic 1
	     * 3: Dist of Topic 2
	     * 4: Dist of Topic 3
	     * 5: Dist of Topic 4
	     * 6: Dist of Topic 5
	     * 7: Dist of Topic 6
	     * 8: Dist of Topic 7
	     * 9: result of 1-1 (normalise and major topics 두배이상 차이)
	     * 10: result of 1-2 (not normalise)
	     * 11: result of 2-1 all topics
	     * 12: result of 2-2 
	     * 13: several topic combination to find best for each fold 
	     */
	    HashMap<String, ArrayList<Double>> documentTopicD = new HashMap<String, ArrayList<Double>>();
	    
	    /* Array List of TopicDF
	     * 0: foldID + F or T  + Normalise or not
	     * e.g. 23450101 -> 2,3,4,5 Fake normalised
	     * e.g. 23450000 -> 2,3,4,5 truthful not normalised
	     * 1: Dist of Topic 0
	     * 2: Dist of Topic 1
	     * 3: Dist of Topic 2
	     * 4: Dist of Topic 3
	     * 5: Dist of Topic 4
	     * 6: Dist of Topic 5
	     * 7: Dist of Topic 6
	     * 8: Dist of Topic 7
	     * 
	     */
	    HashMap<String, ArrayList<Double>> topicDF = new HashMap<String, ArrayList<Double>>();
	    
	    /* results
	     * String: name of testfold
	     * 
	     * (int type) : name
	     * 0: 1.1
	     * 1: 1.2
	     * 2: 2.1
	     * 3: 2.2
	     * 4 
	     * etc
	     * 
	     * */
	    HashMap<String,ArrayList<Double>> results = new HashMap<String, ArrayList<Double>>();
	    
		// read csv for test data
		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ",";
		try {

			br = new BufferedReader(new FileReader(testFile));

				for(int i=0;(line = br.readLine()) != null;i++) {
					
				// use comma as separator
				String[] tempArray = line.split(cvsSplitBy);
				ArrayList<Double> tempAL= new ArrayList<Double>();
				for(int j = 0; j < tempArray.length; j++) {
						tempAL.add(Double.parseDouble(tempArray[j]));
		        }
				documentTopicD.put(tempArray[0], tempAL);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		/*Topic Distribution for fold 2,3,4,5*/
		try {
			br = new BufferedReader(new FileReader(TDF));
				for(int i=0;(line = br.readLine()) != null;i++) {
				// use comma as separator
				String[] tempArray2 = line.split(cvsSplitBy);
				ArrayList<Double> tempAL= new ArrayList<Double>();
				for(int j = 0; j < tempArray2.length; j++) {
						tempAL.add(Double.parseDouble(tempArray2[j]));
		        }
				topicDF.put(tempArray2[0], tempAL);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
/////////////////////////////////////////////////////////
		
		
//		
////		  Iterator<String> iterator = DocumentTopicD.keySet().iterator();
//		  Iterator<String> iterator = documentTopicD.keySet().iterator();
//		    while (iterator.hasNext()) {
//		        String key = (String) iterator.next();
//		        System.out.print("key="+key);
//		        System.out.println(" value="+documentTopicD.get(key));
//		    }
		
/*Parametre*/
		
//		long start = System.currentTimeMillis();
		long end;
		ArrayList<Double> p1_1= new ArrayList<Double>();
		
		
		long start = System.currentTimeMillis();

		
		do {
			
				
//				if(i0==0){
//				if(i1==1){
//					if(tag==0){
//					end = System.currentTimeMillis();
//					System.out.println( "time for 1st-1st iter: " + ( end - start )/1000.0 );
//					System.out.println( "estimated time of completion: " + (( end - start )/1000.0)*iterK*iterK);
//					tag++;
//					}
//				}}
				
//				System.out.println("i0: "+i0+"\ti1: "+i1);

		p1_1.clear();
/*Parametre setting*/
		//1_1
//		p1_1.add(it[0]);
//		p1_1.add(it[1]);
//		p1_1.add(it[2]);
//		p1_1.add(it[3]);
//		p1_1.add(it[4]);
//		p1_1.add(it[5]);
//		p1_1.add(it[6]);
//		p1_1.add(it[7]);
		for(int u=0;u<8;u++){
		p1_1.add(Math.random());
		}
		
		
//		System.out.println();
//		//2_1
//		p2_1.add(1.0);
//		p2_1.add(1.0);
//		p2_1.add(1.0);
//		p2_1.add(1.0);
//		p2_1.add(1.0);
//		p2_1.add(1.0);
//		p2_1.add(1.0);
//		p2_1.add(1.0);
//		
//		//1_1
//		p1_1.add(1.0);
//		p1_1.add(0.0);
//		p1_1.add(1.0);
//		p1_1.add(0.0);
//		p1_1.add(1.0);
//		p1_1.add(0.0);
//		p1_1.add(1.0);
//		p1_1.add(1.0);
//		
		
/*Calculation*/
		    //for type 1.1
		    resultAL.clear();
		    results.clear();
		    results.put(test, resultAL);

//		    type=0;
			  Iterator<String> itr1 = documentTopicD.keySet().iterator();
			    while (itr1.hasNext()) {
			        String key = (String) itr1.next();
			        double sum1_1_1=0;
			        double sum1_1_2=0;
			        
//			        double sum2_1_1=0;
//			        double sum2_1_2=0;
			        
			     
			        
			        for(int i=0;i<8;i++){
//			        	resultAL.add(documentTopicD.get(key).get(i+1)*topicDF.get(tdf23450101).get(i+1));
			        
			        	sum1_1_1+=documentTopicD.get(key).get(i+1)*topicDF.get(tdf_f_n).get(i+1)*p1_1.get(i);
			        	sum1_1_2+=documentTopicD.get(key).get(i+1)*topicDF.get(tdf_t_n).get(i+1)*p1_1.get(i);
			        }
			        			        
			        
				if (Integer.parseInt(key) < 401) {
					if (sum1_1_1 > sum1_1_2) {
						// If there is little difference we can use another
						// approach $$
						// 1 correct, 0 incorrect
						if (sum1_1_1 == 0) {
							// filter 0 but correct
							documentResult1_1.put(key, 0);
						} else {
							documentResult1_1.put(key, 1);
						}
					} else {
						documentResult1_1.put(key, 0);
					}

				} else {
					if (sum1_1_1 > sum1_1_2) {
						// If there is little difference we can use another
						// approach $$
						// 1 correct, 0 incorrect
						documentResult1_1.put(key, 0);
					} else {
						if (sum1_1_2 == 0) {
							// filter 0 but correct
							documentResult1_1.put(key, 0);
						} else {
							documentResult1_1.put(key, 1);
						}
					}
			        	
			        }
//			        System.out.print(key+"\t"+sum1);
//			        System.out.println("\t"+sum2);
			        
//			        System.out.print(key);
//			        System.out.println("\t "+documentResult.get(key));
//		    
			    }
			   
			    Iterator<String> iterR1_1 = documentResult1_1.keySet().iterator();
			    int sumR1_1=0;
			    while (iterR1_1.hasNext()) {
			    	String key = (String) iterR1_1.next();
			    	sumR1_1+=documentResult1_1.get(key);
			    }
			     
			   
			    
			    results.get(test).add(0,((double)sumR1_1/160));
//			    results.get(test).add(0,(double)(sumR/160));
		
			   
			    
			    
			    /*make csv for document F,T score*/
			    
			    
			    /*make csv for results*/
			    
			    double record=0;
			    // record highest one so far
			    if(results.get(test).get(0)>highestScore){
			    try
				{
			    	end = System.currentTimeMillis();
			    	record=( end - start )/1000.0;
			    	
			    	System.out.println( test+"\ttime for "+cntResult+"-highest:\t" + record +"\t value:\t"+highestScore);
					 start = System.currentTimeMillis();
			    	
			    	highestScore=results.get(test).get(0);
			    	
			    	cntResult++;
			    	
				    FileWriter writer = new FileWriter(resultFile,true);
				    
		    
				    writer.append(Double.toString(results.get(test).get(0)));
				    for(int k=0;k<8;k++){
				    writer.append(',');
				    writer.append(Double.toString(p1_1.get(k)));
				    }
				    
				    //for tag the record
				    writer.append(',');
				    writer.append(Double.toString(record));
				    //
				    
				    
				    writer.append('\n');

				    writer.flush();
				    writer.close();
				    
				    FileWriter writerR = new FileWriter(recordFile,true);
				    writerR.append(Double.toString(highestScore));
				    writerR.append(',');
				    writerR.append(Double.toString(record));
				    writerR.append('\n');

				    writerR.flush();
				    writerR.close();
				    
//				    System.out.println(cntResult);
				}
				catch(IOException e)
				{
				     e.printStackTrace();
				}
			    
			   
				
			    
			    }
		} while(true);
			    
			    
	}
			    
							
						
					
				
			
		
	}
			    
	

	
	